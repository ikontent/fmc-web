import { useEffect, useRef } from "react";
import { gaEvent } from "utils/analytics";
import { isBannerVisible } from "utils/banner";
// import YouTube from "@u-wave/react-youtube";

const Banner = ({
  banner = {
    content: "",
    url: "#",
    campaign: "",
    start: null,
    end: null,
    caption: "",
  },
  isFlat,
  position = "",
  className = "",
}) => {
  // content can be either single or multiple to rotate randomly
  const { current: randomIndexOfContent } = useRef(
    Array.isArray(banner.content)
      ? Math.floor(Math.random() * banner.content.length)
      : 0
  );

  const content = Array.isArray(banner.content)
    ? banner.content[randomIndexOfContent]
    : banner.content;

  const url = Array.isArray(banner.url)
    ? Array.isArray(banner.content)
      ? banner.url[randomIndexOfContent] || "#"
      : banner.url[Math.floor(Math.random() * banner.url.length)] || "#"
    : banner.url || "#";

  const isVideo = content.includes("<video");

  // if campaign is not given then looks for the banner's direct folder name
  // or if banner has not any direct folder then banner's file name.
  const campaign =
    banner.campaign ||
    content.replace(/.+banners\/([^"\/]+).*/, (match, $1) => $1);

  useEffect(() => {
    // make sure video with autoplay attribute starts playing on mobile
    if (isVideo) {
      const video = document.querySelector(".banner.video-anchor video");
      const onVideoLoaded = () => {
        if (
          video.hasAttribute("autoplay") &&
          !video.onplaying &&
          video.readyState >= 3
        ) {
          video.play();
        }
      };

      video && video.addEventListener("loadeddata", onVideoLoaded);
      return removeEventListener("loadeddata", onVideoLoaded);
    }
  }, []);

  const handleClick = () => {
    gaEvent({
      category: `Banner / ${campaign}`,
      action: "Open",
      label: `${position}, ${url}`,
    });
  };

  return process.browser && isBannerVisible(banner, content) ? (
    <div className={"banner-slot" + (isFlat ? " flat" : "") + ` ${className}`}>
      {isVideo ? (
        <div className="banner video-anchor" data-position={position}>
          <div
            className="content"
            dangerouslySetInnerHTML={{
              __html: content,
            }}
          />
          <a className="video-routing" target="_blank" href={url} />
        </div>
      ) : (
        <div className="banner" data-position={position}>
          <a
            href={url}
            target="_blank"
            className="content"
            dangerouslySetInnerHTML={{
              __html: content,
            }}
            onClick={handleClick}
          />
        </div>
      )}

      <div className="caption">{banner.caption || "hirdetés"}</div>
    </div>
  ) : (
    <></>
  );
};

export default Banner;
