import Banner from "components/customPositioned/Banner";

const BannerWrapperDesktop = ({ banners }) => {
  const banner = banners?.common?.wrapper;

  return banner?.desktop?.length > 0 ? (
    <>
      <a
        className="master-banner-wrapper-before"
        href={banner.url}
        style={{
          backgroundImage: banner.left ? `url("${banner.left}")` : null,
        }}
        target="_blank"
      />

      <a
        className="master-banner-wrapper-after"
        href={banner.url}
        style={{
          backgroundImage: banner.left ? `url("${banner.right}")` : null,
        }}
        target="_blank"
      />

      <Banner
        className="wrapper desktop"
        banner={{
          content: banner.desktop,
          url: banner.url,
          caption: null,
        }}
        position="Beöltöztetös hirdetés"
      />
    </>
  ) : (
    <></>
  );
};

export default BannerWrapperDesktop;
