import { useState } from "react";
import styles from './PictureModal.module.css'

export const PictureModal = ({ img }) => {
    if (!img) {
        return false
    }

    const [modalOpen, setOpenModal] = useState(false)

    return (
        <>
            <div className={styles.opener} onClick={() => setOpenModal(true)}>
                <img src={img} />
            </div>
            {modalOpen && (
                <div className={styles.lightboxcontainer} onClick={() => setOpenModal(false)}>
                    <img src={img} />
                </div>
            )
            }
        </>
    );
}
