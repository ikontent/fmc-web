import { useState, useEffect } from "react";
import axios from "axios";
import { Button, Table } from "reactstrap";
import { ArticleTitle } from "components/elements/articles/ArticleComponents";
import { STRAPI_URL } from "utils/store";

import { logEvent } from "utils/analytics";
import { PictureModal } from "./PictureModal";

const Poll = ({ pollId: id }) => {
  const [poll, setPoll] = useState();
  const [answers, setAnswers] = useState([]); // [ {text,count},{text,count},... ]
  const [userAnswers, setUserAnswers] = useState({
    [id]: [],
  });

  useEffect(() => {
    (async () => {
      try {
        const response = await axios(`${STRAPI_URL}/polls/${id}`);
        setPoll(response.data);
      } catch (e) {
        console.log(e, e?.response?.data);
      }
    })();
  }, []);

  const handleChange = (e) => {
    const value = parseInt(e.target.value);

    if (poll.multiple) {
      if (e.target.checked) {
        // adds new ticked one
        setAnswers([...answers, value]);
      } else {
        // filters out current un-ticked
        setAnswers(answers.filter((x) => x !== value));
      }

      setUserAnswers((prevState) => ({
        [id]: [...prevState[id], value],
      }));
    } else {
      setAnswers([value]);
      setUserAnswers({
        [id]: value,
      });
    }
  };

  const handleAnswer = async () => {
    try {
      await axios({
        url: `${STRAPI_URL}/polls/answers/${id}`,
        method: "POST",
        data: { answerArray: answers },
      });
      setAnswered(id);
      answers.forEach((answer) => poll.answers[answer].count++);
      setAnswers([]); // force re-render

      const loggedAnswers = poll.answers
        .filter((answer, index) => answers.includes(index))
        .map((answer, index) => answer.text);

      logEvent(
        "Polling",
        "Vote",
        poll.title || "Cím nélküli szavazás",
        loggedAnswers.join("; ")
      );
    } catch (err) {
      // TODO
      console.log(err);
      alert("Hiba történt a szavazás közben");
    }
  };

  const getAnswered = (id) => {
    return (
      process.browser &&
      (JSON.parse(localStorage.getItem("pollIds")) || []).find(
        (poll) => poll.id == id
      )
    );
  };

  const setAnswered = (id) => {
    const pollIds = JSON.parse(localStorage.getItem("pollIds")) || [];
    pollIds.push({
      id,
      answers: poll.multiple ? userAnswers[id] : answers,
    });
    pollIds.splice(100);
    localStorage.setItem("pollIds", JSON.stringify(pollIds));
  };

  const wasAnswered = getAnswered(id);

  // sums up all counts to display votes as percentages
  let initial = 0;
  const totalCounts = poll?.answers.reduce(
    (accu, current) => accu + current.count,
    0
  );

  return poll ? (
    <div id={`poll${id}`} className="custom-positioned border p-20 my-25">
      <div>
        <ArticleTitle title={"Szavazz!"} />
      </div>
      <h6 className="my-4 px-2 d-flex default-article-headline-normal">
        {poll.title}
      </h6>
      <form className="px-1 default-article-lead-normal">
        <aside className="poll-table">
          {poll.answers.map((answer, index) => (
            <div
              className={`poll-table-row ${
                getAnswered(id)?.answers.includes(index) ? "is-answered" : ""
              }`}
              key={id * 100 + index}
            >
              <div className="poll-table-answer-text">
                {!wasAnswered && (
                  <label>
                    <input
                      id={id * 100 + index}
                      onChange={handleChange}
                      type={poll.multiple ? "checkbox" : "radio"}
                      name={id} // need for grouping radios
                      value={index}
                    />
                    <span>{poll.multiple ? "✓" : ""}</span>
                  </label>
                )}
                {poll?.pictureCampaign ? (
                  <div className="poll-answer my-2">
                    <div className={wasAnswered ? "poll-answered-image" : ""}>
                      <PictureModal img={answer.text} />
                    </div>
                    {answer.title && (
                      <div class="text-left font-bold mt-2">{answer.title}</div>
                    )}
                  </div>
                ) : (
                  <span className={wasAnswered ? "" : "poll-answer"}>
                    {answer.text}
                  </span>
                )}
              </div>
              {wasAnswered && totalCounts && (
                <div className="poll-table-stats">
                  <div className="poll-table-percent">
                    {answer.count > 0
                      ? `${Math.round((answer.count / totalCounts) * 100)}%`
                      : null}
                  </div>
                  <div className="poll-table-count">
                    ({answer.count} szavazat)
                  </div>
                </div>
              )}
            </div>
          ))}
        </aside>

        {!wasAnswered && (
          <Button
            onClick={handleAnswer}
            block
            disabled={wasAnswered}
            className="btn-outline btn-lg btn-void"
          >
            Szavazok
          </Button>
        )}
      </form>
    </div>
  ) : null;
};

export default Poll;
