import { useEffect } from "react";
import Poll from "components/customPositioned/Poll";
import GalleryRenderer from "components/elements/GalleryRenderer";

const WysiwygContent = ({ post }) => {
  const lead = post?.lead;

  useEffect(() => {
    // Facebook Embedded Post
    const fbPost = document.querySelectorAll(
      "iframe[src*='facebook.com/plugins/post.php']"
    );
    if (fbPost.length) {
      fbPost.forEach((d) => {
        d.src = d.src.replace("&width=", "&width=auto&");
        d.width = "100%";
      });
    }
    // Facebook Embedded Video
    const fbVideo = document.querySelectorAll(
      "iframe[src*='facebook.com/plugins/video.php']"
    );
    if (fbVideo.length) {
      fbVideo.forEach((d) => {
        d.src = d.src.replace("&width=", "&width=auto&");
        d.width = "100%";
      });
    }

    // Inject language into blockquote
    const blockquote = document.querySelectorAll("blockquote");
    if (blockquote.length) {
      blockquote.forEach((d) => {
        d.lang = "hu";
      });
    }
  }, []);

  // replace typeform links to typeform element
  let content = (post?.content || "").replace(
    /(https:\/\/\w*\.typeform\.com\/(to|form)\/)(\w*)((\?|\/)([a-zA-Z0-9-_=]*))?/g,
    (all, g1, g2, g3) => {
      return `<div class="typeform-widget" data-url="https://form.typeform.com/to/${g3}?typeform-medium=embed-snippet" style="width: 100%; height: 500px;"></div>`;
    }
  );

  const contentArray = [];
  let startAt = 0;
  const regx = /<[^>]+>Poll#(\d+)[^>]+>/g;

  // matchAll is not IE compliant, thus RegExp.exec() is used instead
  // [...content.matchAll(regx)]
  // .forEach((regexFound) => {
  //   contentArray.push(content.slice(startAt, regexFound.index));
  //   startAt = regexFound.index + regexFound[0].length;
  //   contentArray.push({ id: regexFound[1] });
  // });

  let regexFound;
  while ((regexFound = regx.exec(content)) !== null) {
    contentArray.push(content.slice(startAt, regexFound.index));
    startAt = regexFound.index + regexFound[0].length;
    contentArray.push({ id: regexFound[1] });
  }

  contentArray.push(contentArray.length ? content.slice(startAt) : content);

  return content ? (
    <div id="wysiwyg-content">
      {lead && (
        <div
          className="wysiwyg-lead"
          dangerouslySetInnerHTML={{ __html: lead }}
        />
      )}
      {contentArray.map((section, index) => {
        return section.id ? (
          <Poll key={index} pollId={section.id} />
        ) : (
          <GalleryRenderer key={index} htmlContent={section} />
        );
      })}
    </div>
  ) : (
    ""
  );
};

export default WysiwygContent;
