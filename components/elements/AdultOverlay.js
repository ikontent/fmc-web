import { useState } from "react";
import { useRouter } from "next/router";
import { Button } from "reactstrap";

const AdultOverlay = ({ onPassed }) => {
  const [opened, setOpened] = useState(true);
  const router = useRouter();

  return opened ? (
    <div className="adult-overlay">
      <div>
        <header>
          <div class="eighteen">18</div>
          <h3>Felnőtt tartalom</h3>
        </header>
        <section>
          <h4>Elmúltál már 18 éves?</h4>
          <span>Ezen az oldalon felnőtt tartalom található!</span>
          <div>
            <Button
              onClick={() => {
                setOpened(false);
                onPassed && onPassed();
              }}
              outline
            >
              Igen
            </Button>
            <Button onClick={() => router.push("/")}>Nem</Button>
          </div>
        </section>
      </div>
    </div>
  ) : null;
};

export default AdultOverlay;
