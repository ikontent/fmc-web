import React from "react";
import Link from "next/link";
import Avatar from "components/elements/Avatar";
import { pathFactory } from "utils/path-factory";
import Tags from "components/elements/Tags";

import { daysPassedSince } from "utils/dateStuffs";

const ArticleMeta = ({ article = {}, className }) => {
  const authors = article.author
    ? [article.author, ...article.secondaryAuthors]
    : article.secondaryAuthors
    ? [...article.secondaryAuthors]
    : null;

  return (
    <div id="article-meta" className={className ? className : ""}>
      {authors && authors.length > 0 && (
        <div className="avatars">
          {authors.map((author, indexAuthor) => {
            const url = pathFactory.author(author);

            return (
              <Avatar
                size="2.5rem"
                tooltip={author.name || author.username}
                zIndex={indexAuthor}
                imgUrl={author.avatar?.url}
                pageUrl={url}
                key={indexAuthor}
              />
            );
          })}
        </div>
      )}

      <div
        className="text"
        style={{
          flexDirection: authors.length > 1 ? "column" : "row",
          alignItems: authors.length > 1 ? "unset" : "center",
        }}
      >
        {authors && authors.length > 0 && (
          <div className="names">
            {authors.map((author, indexName) => (
              <React.Fragment key={indexName}>
                {indexName > 0 && indexName < authors.length && <span>,</span>}
                <Link prefetch={false} href={pathFactory.author(author)}>
                  <a className="name" href={pathFactory.author(author)}>
                    {author.name || author.username}
                  </a>
                </Link>
              </React.Fragment>
            ))}
          </div>
        )}

        <div className="tags-box">
          <i>·</i>
          <Tags article={article} showTopic showTag isHighlighted />
          <i>·</i>

          {article.published_at && (
            <span className="timestamp">
              {daysPassedSince(
                article.firstPublishedAt ||
                  article.published_at ||
                  article.created_at
              )}
            </span>
          )}

          {article.modified_at &&
            article.modified_at > article.published_at && (
              <span className="timestamp">
                {"Utolsó frissítés: " + daysPassedSince(article.modified_at)}
              </span>
            )}
        </div>
      </div>
    </div>
  );
};

export default ArticleMeta;
