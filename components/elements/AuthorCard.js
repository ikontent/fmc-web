import Avatar from "components/elements/Avatar";

const AuthorCard = ({
  author = {},
  numberOfAllArticles = 0,
  className = "",
}) => {
  return (
    <div id="author-card" className={className}>
      <Avatar
        size="5rem"
        tooltip={author.name || author.username}
        imgUrl={author.avatar?.url}
      />

      <div className="text">
        <small>SZERZŐ</small>
        <h5>{author.name || author.username}</h5>
        <p>{author.about}</p>
        <address>
          <a href={"mailto:" + author.email}>{author.email}</a>
          <span>{numberOfAllArticles + " cikk"}</span>
        </address>
      </div>
    </div>
  );
};

export default AuthorCard;
