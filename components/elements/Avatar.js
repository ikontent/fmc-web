import { useState, useEffect } from "react";
import Link from "next/link";
import { STRAPI_URL } from "utils/store";
// import { Popover } from "reactstrap";

export default function Avatar({
  size = "2.5rem",
  tooltip,
  zIndex = 0,
  imgUrl = "",
  pageUrl = "",
  className,
}) {
  // Encode avatar placeholder to base64 format
  const [avatarPlaceholder, setAvatarPlaceholder] = useState();
  useEffect(() => {
    setAvatarPlaceholder(() =>
      window.btoa(`
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="2 2 20 20" fill="darkgray">
          <path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 3c1.66 0 3 1.34 3 3s-1.34 3-3 3-3-1.34-3-3 1.34-3 3-3zm0 14.2c-2.5 0-4.71-1.28-6-3.22.03-1.99 4-3.08 6-3.08 1.99 0 5.97 1.09 6 3.08-1.29 1.94-3.5 3.22-6 3.22z" />
        </svg>
      `)
    );
  }, []);

  // === POPOVER RELATED CODE HAS BEEN COMMENTED OUT IN CASE IT IS NEEDED AGAIN ===

  // // initial "false" is needed for first successful render
  // const [popover, setPopover] = useState(false);

  // // creates state for each author's popover
  // useEffect(() => {
  //   const init = {};
  //   authors.map((popInit) => {
  //     init[popInit?.id] = false;
  //   });
  //   setPopover(() => init);
  // }, []);

  // const enter = () => {
  //   const clone = { ...popover };
  //   // swap state of triggered avatar
  //   clone[author?.id] = !clone[author?.id];
  //   // close avatars other than triggered
  //   Object.keys(popover)
  //     .filter((authorID) => authorID != author?.id)
  //     .map((authorIDtoClose) => {
  //       clone[authorIDtoClose] = false;
  //     });
  //   setPopover(() => clone);
  // };

  // const leave = () => {
  //   const clone = { ...popover };
  //   // swap state of triggered avatar
  //   clone[author?.id] = !clone[author?.id];
  //   setPopover(() => clone);
  // };

  return (
    <Link prefetch={false} href={pageUrl}>
      <>
        <a
          className={(className ? " " + className : "") + "avatar"}
          style={{
            zIndex: zIndex + 1,
            backgroundImage: imgUrl
              ? `url("${STRAPI_URL + imgUrl}")`
              : `url("data:image/svg+xml;base64,${avatarPlaceholder}")`,
          }}
          title={tooltip}
          // id={"popover-" + author?.id} // connection with <Popover>
          // onMouseEnter={enter}
          // onMouseLeave={leave}
        >
          {/* <Popover
                className="article-author-popover"
                placement="bottom"
                isOpen={popover[author?.id]}
                target={"popover-" + author?.id} // connection with trigger
                >
                <div className="role">SZERZŐ</div>
                <div className="name">{author?.username}</div>
                <ul className="post-titles">
                  {author?.posts?.map((authorsPost, indexPost) => {
                    return post.id == authorsPost.id ? (
                      ""
                    ) : (
                      <li
                        className="post-title"
                        key={indexAuthor + "-" + indexPost}
                      >
                        {post.title}
                      </li>
                    );
                  })}
                </ul>
              </Popover> */}
        </a>
        <style jsx>{`
          .avatar {
            height: ${size};
            width: ${size};
            border-radius: 50%;
            border: solid 2px white;
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
            transition: transform 0.25s;
          }
          .avatar:not(:first-of-type) {
            margin-left: -0.7rem;
          }
          .avatar:hover {
            transform: scale(1.1);
          }
        `}</style>
      </>
    </Link>
  );
}
