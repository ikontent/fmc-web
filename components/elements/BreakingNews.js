import { useState, useEffect } from "react";
import Link from "next/link";
import { pathFactory } from "utils/path-factory.js";
import api from "utils/api";

const BreakingNews = ({ viewMode, slug }) => {
  const [breakingNews, setBreakingNews] = useState(null);

  // Breaking news
  useEffect(async () => {
    try {
      const frontpage = await api.getFrontpageSettings();

      if (frontpage && frontpage?.breakingNews) {
        if (new Date() < new Date(frontpage.breakingNews.expireAt)) {
          setBreakingNews(frontpage.breakingNews);
        }
      }
    } catch (e) {
      console.log(e);
    }
  }, []);

  let content = breakingNews?.breakingText;
  const url =
    breakingNews && breakingNews.post
      ? pathFactory.article(breakingNews.post)
      : null;

  return breakingNews ? (
    !url ? (
      <div
        id="breaking-news"
        style={{
          marginTop:
            viewMode === "mobile" && slug !== "/" && slug !== "custom404"
              ? "60px"
              : "0px",
        }}
      >
        <section>
          <div className="global-limiter breaking-news-stripe">
            <span
              dangerouslySetInnerHTML={{ __html: content }}
              className="cursor-default"
            ></span>
          </div>
        </section>
      </div>
    ) : (
      <Link prefetch={false} href={url}>
        <a
          id="breaking-news"
          style={{
            marginTop:
              viewMode === "mobile" && slug !== "/" && slug !== "custom404"
                ? "60px"
                : "0px",
          }}
        >
          <section>
            <div className="global-limiter breaking-news-stripe">
              <span dangerouslySetInnerHTML={{ __html: content }}></span>
            </div>
          </section>
        </a>
      </Link>
    )
  ) : (
    // need to push down page-content from behind the fixed mobile menu
    <div
      id="breaking-news"
      style={{
        marginTop:
          viewMode === "mobile" && slug !== "/" && slug !== "custom404"
            ? "60px"
            : "0px",
      }}
    />
  );
};

export default BreakingNews;
