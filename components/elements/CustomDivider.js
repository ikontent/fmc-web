/**
 * CustomDivider component. Can be used to separate sections or any parts
 * @component
 * @param {string} [className] - Added classes.
 * @param {boolean} [double=false] - Should be true if double line is needed.
 */
export default function CustomDivider({
  className = "",
  double = false,
  zIndex = "auto",
}) {
  return (
    <div className={`custom-divider ${className}`} style={{ zIndex }}>
      <div className="line"></div>
      {double && <div className="line mt-1"></div>}
    </div>
  );
}
