import { Row, Col } from "reactstrap";
import { ArticleTitle } from "components/elements/articles/ArticleComponents";
import ArticleCardHorizontal from "components/elements/articles/ArticleCardHorizontal";

const DividerTrio = ({ title, action, articles }) => {
  return (
    <section className="divider-trio global-section-py global-section-difference-px">
      <div className="divider-trio__inner">
        <Row noGutters>
          <Col className="divider-trio__title-wrapper global-box-px">
            <ArticleTitle title={title} action={action} withoutSpace />
          </Col>
        </Row>

        <Row noGutters className="with-vertical-separator">
          {(articles || []).slice(0, 3).map((article, index) => (
            <Col
              md={4}
              className={`global-item-pt global-item-pb global-item-reset-md-pb${
                index ? " global-separated-t global-separated-reset-md-t" : ""
              }`}
              key={`divider-trio-item-${index}`}
            >
              <div className="divider-trio__article-wrapper global-box-px global-separated-md-l">
                <ArticleCardHorizontal
                  article={{ ...article, showFeaturedImage: true }}
                  showTopic
                  showTag
                  showTagBoxWithin
                />
              </div>
            </Col>
          ))}
        </Row>
      </div>
    </section>
  );
};

export default DividerTrio;
