import ShareIcons from "components/elements/ShareIcons";
import Tag from "components/elements/Tag";
import { OWN_URL } from "utils/store";
import FacebookComments from "components/elements/FacebookComments";
import { pathFactory } from "utils/path-factory";

const FBscrollTagsShare = ({ tags = [], sensitivity, url = null }) => {
  return (
    <section className="fb-scroll-tags-share">
      <div>{url && <FacebookComments href={`${OWN_URL}${url}`} />}</div>

      <div className="tags">
        {tags.map((tag) => {
          return tag?.tagName ? (
            <Tag
              key={tag.tagName}
              text={tag.tagName}
              href={pathFactory.tag(tag.slug || tag.tagName)}
              hasBackground
              sensitivity={sensitivity}
            />
          ) : null;
        })}
      </div>

      <ShareIcons isBookmark />
    </section>
  );
};

export default FBscrollTagsShare;
