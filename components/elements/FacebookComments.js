import useFacebook from "utils/hooks/useFacebook";

const FacebookComments = ({ width = "100%", href, numPosts = 5 }) => {
  useFacebook({ addTrack: false });

  return href ? (
    <div className="fb-comment-wrapper">
      <div
        id="fb-comments"
        className="fb-comments"
        data-href={href + "/"}
        data-numposts={numPosts}
        data-width={width}
        data-lazy={true}
      ></div>
    </div>
  ) : null;
};

export default FacebookComments;
