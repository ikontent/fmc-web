import { logEvent } from "utils/analytics";

const FacebookPage = ({ showBadge, position }) => {
  const handleRoute = (e, route) => {
    e.preventDefault();
    logEvent("FacebookGroup", "Open", position);
    window.open(route, "_blank");
  };

  return (
    <>
      <h5 className="default-article-headline-normal facebook-card-headline">
        Csatlakozz közösségünkhöz!
      </h5>
      <a
        className="facebook-page"
        alt="Az fmc.hu Facebook oldala"
        onClick={(e) =>
          handleRoute(
            e,
            "https://www.facebook.com/groups/mindenamiszekesfehervar/"
          )
        }
      >
        {showBadge && <div className="facebook-page__badge">Újdonság</div>}
        <div className="facebook-page__text-box">
          <h6 className="facebook-page__title">Minden, ami Székesfehérvár</h6>
          <div className="facebook-page__text">
            Mindened Fehérvár és környéke? Nekünk is. Hírek, érdekességek,
            programok és beszélgetések a világ szerintünk legjobb városáról a
            Facebookon.
          </div>
        </div>
        <img
          src="/images/fmc-facebook.jpg"
          width="432"
          height="204"
          alt="Fehérvár Médiacentrum"
          className="img-fluid"
          loading="lazy"
        />
      </a>
    </>
  );
};

export default FacebookPage;
