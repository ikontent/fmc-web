import ReactDOM from "react-dom";
import { useEffect } from "react";
import { STRAPI_URL } from "utils/store";
import cheerio from "cheerio";
import ImageGallery from "components/elements/react-image-gallery/src/ImageGallery";
import Gallery from "react-grid-gallery";
import renderRelatedArticles from "utils/relatedArticles";
import { logEvent } from "utils/analytics";

const GalleryRenderer = ({ htmlContent }) => {
  let galleriesData = [];

  const galleryConfig = {
    thumbnailPosition: "bottom",
    showBullets: true,
  };

  const slideshowConfig = {
    showBullets: true,
    showPlayButton: false,
    showFullscreenButton: false,
    showThumbnails: false,
  };

  useEffect(() => {
    renderGaleries();
  }, [htmlContent]);

  /**
   * Collect all gallery info from html content
   */
  const collectGalleries = (html) => {
    const $ = cheerio.load(html);
    $(".igallery").each((i, gallery) => {
      const type = $(gallery).attr("data-gtype");
      const images = JSON.parse(unescape($(gallery).attr("data-data"))).map(
        (img) => {
          // grid
          if (type == "4") {
            return {
              src: STRAPI_URL + img.url,
              thumbnail: STRAPI_URL + img.thumbnail,
              thumbnailCaption: "",
              caption: (
                <>
                  {img.name && <span>{img.name}</span>}
                  {img.caption && <span>{img.caption}</span>}
                </>
              ),
              thumbnailWidth: img?.thumbwidth || 156,
              thumbnailHeight: img?.thumbheight || 156,
            };
            // gallery & slideshow
          } else {
            return {
              original: STRAPI_URL + img.url,
              thumbnail: STRAPI_URL + img.thumbnail,
              originalAlt: img.alternativeText,
              description: img.name,
              author: img.caption ? img.caption : "",
            };
          }
        }
      );

      galleriesData[i] = { type, images };

      $(gallery).replaceWith(
        `<div class="row"><div class="igallery igallery-type-${type}" id="igallery-${i}"></div></div>`
      );
    });

    return $.html();
  };

  /*const Images = ({ items, config }) => {
    return items.map((img) => (
      <div>
        ASD
        <img src={img.original} />
      </div>
    ));
  };*/

  /**
   * Uses the galleriesData array to render the galleries
   */
  const renderGaleries = () => {
    for (const [i, galleryItems] of galleriesData.entries()) {
      switch (galleryItems?.type) {
        /*case "1":
          ReactDOM.render(
            <Images items={galleryItems.images} {...galleryConfig} />,
            document.getElementById(`igallery-${i}`)
          );
          break;*/
        case "2":
          ReactDOM.render(
            <ImageGallery
              items={galleryItems.images}
              {...galleryConfig}
              onScreenChange={() =>
                logEvent("Gallery", "Open", "", location.href)
              }
              onThumbnailClick={() =>
                logEvent("Photo", "Open", "", location.href)
              }
            />,
            document.getElementById(`igallery-${i}`)
          );
          break;
        case "3":
          ReactDOM.render(
            <ImageGallery items={galleryItems.images} {...slideshowConfig} />,
            document.getElementById(`igallery-${i}`)
          );
          break;
        case "4":
          ReactDOM.render(
            <Gallery
              images={galleryItems.images}
              enableImageSelection={false}
              backdropClosesModal={true}
            />,
            document.getElementById(`igallery-${i}`)
          );
          break;
      }
    }
  };

  return (
    htmlContent && (
      <div
        dangerouslySetInnerHTML={{
          __html: renderRelatedArticles(collectGalleries(htmlContent)),
        }}
      ></div>
    )
  );
};

export default GalleryRenderer;
