import Head from "next/head";
import React from "react";

const STRAPI_URL = process.env.NEXT_PUBLIC_STRAPI_URL;
const OWN_URL = process.env.NEXT_PUBLIC_OWN_URL;
const FB_APP_ID = process.env.NEXT_PUBLIC_FB_APP_ID;

const HtmlHead = ({
  title = "fmc.hu – Fehérvár összeköt",
  description = "Székesfehérvár új oldala: hírek, programok, érdekességek a városról és Fejér megyéről egyenesen a Fehérvár Médiacentrumtól. Jó helyen vagy!",
  keywords = "székesfehérvár, hírek, programok",
  image = `${OWN_URL}/images/FMC_Logo-01.png`,
  url = OWN_URL,
  imageWidth = 1200,
  imageHeight = 680,
  imageMime = "image/png",
  noIndex = false,
  additionalMeta = null,
}) => {
  const imgUrl = image.indexOf("http") === 0 ? image : STRAPI_URL + image;
  return (
    <Head>
      {/* Static meta */}
      <meta charSet="utf-8" />
      {noIndex ? (
        <meta name="robots" content="noindex" />
      ) : (
        <meta name="robots" content="index, follow" />
      )}
      <meta httpEquiv="X-UA-Compatible" content="IE=edge, chrome=1" />
      <meta httpEquiv="cleartype" content="on" />
      <meta
        name="viewport"
        content="initial-scale=1, user-scalable=yes, width=device-width"
      />
      <meta name="HandheldFriendly" content="true" />
      <meta name="MobileOptimized" content="320" />
      <meta name="copyright" content={"fmc.hu"} />
      <meta name="geo.region" content="HU-PE" />
      <meta name="geo.placename" content="szekesfehervar" />
      <meta name="geo.position" content="47.2038042,18.4012645" />

      {/* Dynamics for SEO */}
      <title key="main-title">{title}</title>
      <meta key="meta-title" name="title" content={title} />
      <meta key="meta-description" name="description" content={description} />
      {keywords ? (
        <meta key="meta-keywords" name="keywords" content={keywords} />
      ) : null}

      <link key="link-canonical" rel="canonical" href={url} />

      {/* Facebook dynamic */}
      <meta key="og-locale" property="og:locale" content="hu" />
      <meta key="og-type" property="og:type" content="website" />
      <meta key="og-title" property="og:title" content={title} />
      <meta
        key="og-description"
        property="og:description"
        content={description}
      />
      <meta
        key="og-description"
        property="og:description"
        content={description}
      />
      <meta key="og-url" property="og:url" content={url} />
      <meta key="og-image-type" property="og:image:type" content={imageMime} />
      <meta key="og-image" property="og:image" content={imgUrl} />
      <meta
        key="og:image:secure_url"
        property="og:image:secure_url"
        content={imgUrl}
      />
      <meta
        key="og-imageog:image:url"
        property="og:image:url"
        content={imgUrl}
      />

      {/* Facebook static */}
      <meta property="fb:app_id" content={FB_APP_ID} />
      <meta property="og:image:width" content={imageWidth} />
      <meta property="og:image:height" content={imageHeight} />
      <meta property="og:site_name" content={title} />

      {/* Twitter dynamic */}
      <meta key="twitter-title" name="twitter:title" content={title}></meta>
      <meta
        key="twitter-description"
        name="twitter:description"
        content={description}
      ></meta>
      <meta key="twitter-image" property="twitter:image" content={imgUrl} />
      <meta key="twitter-card" name="twitter:card" content="summary"></meta>

      {/* Additional meta */}
      {additionalMeta &&
        additionalMeta.map((meta, index) => (
          <meta
            key={`head-meta-content-${index}`}
            name={meta.name}
            content={meta.content}
          ></meta>
        ))}

      {/* <link
        rel="icon"
        type="image/svg+xml"
        href="/images/logo/passover-logo.svg"
      /> */}

      {/* Handheld related */}
      {/* <link rel="manifest" href={`${OWN_URL}/manifest.json`} /> */}
      <link
        rel="apple-touch-icon"
        sizes="57x57"
        href={`${OWN_URL}/images/icons/favicons/apple-icon-57x57.png`}
      />
      <link
        rel="apple-touch-icon"
        sizes="60x60"
        href={`${OWN_URL}/images/icons/favicons/apple-icon-60x60.png`}
      />
      <link
        rel="apple-touch-icon"
        sizes="72x72"
        href={`${OWN_URL}/images/icons/favicons/apple-icon-72x72.png`}
      />
      <link
        rel="apple-touch-icon"
        sizes="76x76"
        href={`${OWN_URL}/images/icons/favicons/apple-icon-76x76.png`}
      />
      <link
        rel="apple-touch-icon"
        sizes="114x114"
        href={`${OWN_URL}/images/icons/favicons/apple-icon-114x114.png`}
      />
      <link
        rel="apple-touch-icon"
        sizes="120x120"
        href={`${OWN_URL}/images/icons/favicons/apple-icon-120x120.png`}
      />
      <link
        rel="apple-touch-icon"
        sizes="144x144"
        href={`${OWN_URL}/images/icons/favicons/apple-icon-144x144.png`}
      />
      <link
        rel="apple-touch-icon"
        sizes="152x152"
        href={`${OWN_URL}/images/icons/favicons/apple-icon-152x152.png`}
      />
      <link
        rel="apple-touch-icon"
        sizes="180x180"
        href={`${OWN_URL}/images/icons/favicons/apple-icon-180x180.png`}
      />
      <link
        rel="icon"
        type="image/png"
        sizes="192x192"
        href={`${OWN_URL}/images/icons/favicons/android-icon-192x192.png`}
      />
      <link
        rel="icon"
        type="image/png"
        sizes="32x32"
        href={`${OWN_URL}/images/icons/favicons/favicon-32x32.png`}
      />
      <link
        rel="icon"
        type="image/png"
        sizes="96x96"
        href={`${OWN_URL}/images/icons/favicons/favicon-96x96.png`}
      />
      <link
        rel="icon"
        type="image/png"
        sizes="16x16"
        href={`${OWN_URL}/images/icons/favicons/favicon-16x16.png`}
      />
      <meta
        name="msapplication-TileImage"
        content={`${OWN_URL}/images/icons/ms-icon-144x144.png`}
      />
      <meta name="msapplication-TileColor" content="#820D3E" />
      <meta name="theme-color" content="#820D3E" />

      {/* TypeForm script */}
      {/* <script
        type="text/javascript"
        dangerouslySetInnerHTML={{
          __html: `(function() { var qs,js,q,s,d=document, gi=d.getElementById, ce=d.createElement, gt=d.getElementsByTagName, id="typef_orm", b="https://embed.typeform.com/"; if(!gi.call(d,id)) { js=ce.call(d,"script"); js.id=id; js.src=b+"embed.js"; q=gt.call(d,"script")[0]; q.parentNode.insertBefore(js,q) } })()`,
        }}
      ></script> */}
    </Head>
  );
};

export default HtmlHead;
