import React from "react";
import Link from "next/link";

const Logo = ({
  className,
  children,
  href,
  target,
  sizeFontRem,
  sizeLogo,
  text,
  onClick,
  aria,
  title,
  isExternal = false,
}) => {
  const renderLogo = () => {
    const inheritedLogo = React.cloneElement(children, { sizeLogo }, null);

    return (
      <>
        <span
          className="icon"
          style={
            sizeLogo
              ? {
                  width: sizeLogo,
                  height: sizeLogo,
                }
              : {}
          }
        >
          {inheritedLogo || (
            <svg viewBox="-1 -1 2 2" width={sizeLogo} height={sizeLogo}>
              <circle cx="0" cy="0" r="1" fill="#dcdcdc" />
            </svg>
          )}
        </span>
        {text && <span className="text">{text}</span>}
      </>
    );
  };

  return href ? (
    <Link prefetch={false} href={href}>
      {/* Nextjs shows console error if <a> is inside <LogoCore /> */}
      <a
        className={"logo" + (className ? ` ${className}` : "")}
        role="img"
        href={href}
        aria-label={aria}
        title={title}
        style={sizeFontRem ? { fontSize: `${sizeFontRem}rem` } : {}}
        target={isExternal ? "_blank" : "_self"}
      >
        {renderLogo()}
      </a>
    </Link>
  ) : (
    <a
      className={"logo" + (className ? ` ${className}` : "")}
      target={target}
      role="img"
      href={href}
      onClick={onClick}
      aria-label={aria}
      title={title}
      style={sizeFontRem ? { fontSize: `${sizeFontRem}rem` } : {}}
    >
      {renderLogo()}
    </a>
  );
};

export default Logo;
