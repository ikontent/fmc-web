import { useState, useRef } from "react";
import Link from "next/link";
import axios from "axios";
import { Button } from "reactstrap";
import { STRAPI_URL } from "utils/store";
import { logEvent } from "utils/analytics";

const Newsletter = ({ className }) => {
  const [isLoading, setLoading] = useState(false);
  const [errorMessage, setErrorMessage] = useState();
  const [successMessage, setSuccessMessage] = useState();

  const formRef = useRef();

  async function subscribe(e) {
    e.preventDefault();
    clearMessages();

    if (formValidation(formRef.current, "Szükséges kitölteni")) {
      setLoading(true);
      try {
        await axios(config(formRef.current));
        setSuccessMessage("Sikeres feliratkozás");

        logEvent("Newsletter", "Subscribe", "Success");
      } catch (error) {
        console.log(error.response);
        setErrorMessage(
          error.response.data.title === "Member Exists"
            ? "Létező felhasználó"
            : "Hiba történt"
        );
        logEvent("Newsletter", "Subscribe", "Failed", errorMessage);
      }
      setLoading(false);
    }
  }

  function config(formObject) {
    return {
      method: "post",
      url: `${STRAPI_URL}/newsletter-subscribe`,
      data: {
        email_address: formObject["email"].value,
        status: "pending",
        merge_fields: {
          NAME: formObject["name"].value,
          TERMS: formObject["terms"].checked ? 1 : 0,
        },
      },
    };
  }

  return (
    <div className={"newsletter" + (className ? " " + className : "")}>
      <header>
        <h5>Hírlevél</h5>
        <p>Iratkozz fel már most hamarosan induló heti hírlevelünkre!</p>
      </header>

      <form onSubmit={subscribe} ref={formRef}>
        <input
          type="text"
          name="name"
          required={true}
          placeholder="Név"
          pattern=".{3,}"
          title="Legkevesebb három karakter szükséges"
          onFocus={clearMessages}
          onChange={(e) => {
            e.target.setCustomValidity("");
          }}
        />
        <input
          // Type has been set to "text" instead "email". Because native
          // email validation is less accurate than the pattern below, as
          // well as overwrites the custom error message declared in "title".
          type="text"
          name="email"
          required={true}
          pattern="[^@]+@[^@]+\.[^@]+"
          title="Szabályos formátumú email cím szükséges"
          placeholder="Email cím"
          onFocus={clearMessages}
          onChange={(e) => {
            e.target.setCustomValidity("");
          }}
        />
        <div className="newsletter-terms">
          <label>
            <input
              type="checkbox"
              name="terms"
              required={true}
              onFocus={clearMessages}
            />
            <span>✓</span>
          </label>
          <span>
            Elfogadom az{"\u00A0"}
            <Link prefetch={false} href={"/page/adatvedelmi-nyilatkozat"}>
              <a className="a-global">adatkezelési nyilatkozatot.</a>
            </Link>
          </span>
        </div>
        <Button type="button" disabled={isLoading} onClick={subscribe}>
          Feliratkozás
        </Button>
      </form>
      {errorMessage && !successMessage && (
        <p style={{ color: "red" }}>{errorMessage}</p>
      )}
      {successMessage && <p style={{ color: "green" }}>{successMessage}</p>}
    </div>
  );
  // ########################## END OF RENDER #################################

  function clearMessages() {
    setErrorMessage("");
    setSuccessMessage("");
  }

  // ####################### NATIVE FORM VALIDATION ############################

  function formValidation(formObject, warningForEmptyRequired = "") {
    // ###  Retain native form validation while prevent default submit ####

    // Make sure that each <input> tag has the following attributes:
    // - required
    // - title (used for displaying custom error message)
    // - pattern (except for checkbox and radio)

    const formClone = Array.from(formObject);

    // setCustomValidity() sets the displaying custom error message.
    // But once input value is valid, custom error message makes checkValidity() to fail!
    // Therefore if input value is valid, custom error message has to be emptied.
    formClone.forEach((f) => {
      f.validity.valueMissing
        ? f.setCustomValidity(warningForEmptyRequired)
        : f.validity.patternMismatch
        ? f.setCustomValidity(f.title)
        : f.setCustomValidity("");
    });

    // reportValidity() displays the error messages, but in reverse order - by default,
    // therefore array is reversed.
    formClone.reverse().forEach((f) => {
      f.reportValidity();
    });

    // The actual form validation.
    // Form elements other than <input> (e.g: <button>) are being filtered out.
    return formClone.every((f) => {
      return f.localName === "input" ? f.checkValidity() : true;
    });
  }
};

export default Newsletter;
