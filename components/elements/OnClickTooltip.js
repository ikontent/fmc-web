import { useState } from "react";

const OnClickTooltip = ({
  children,
  tooltipText = "#",
  onClickFn,
  className,
  seconds = 2,
}) => {
  const [triggered, setTriggered] = useState();

  return (
    <>
      <div
        className={
          (className ? className + " " : "") +
          "tooltip" +
          (triggered ? " triggered" : "")
        }
        onClick={() => {
          setTriggered(true);
          onClickFn();
          setTimeout(() => setTriggered(false), seconds * 1000);
        }}
      >
        {children}
      </div>
      <style jsx>{`
        .tooltip {
          position: relative;
        }
        .tooltip::after {
          position: absolute;
          opacity: 0;
          pointer-events: none;
          content: "${tooltipText}";
          top: -0.5rem;
          left: 110%;
          padding: 0 0.5rem;
          font: 500 1.1rem / 1.9rem;
          color: white;
          background-color: rgb(32, 32, 32);
          box-shadow: 0 0 1rem -0.5rem black;
          border: 2px solid white;
          white-space: nowrap;
          z-index: 4;
        }
        .triggered::after {
          animation: fade-tooltip ${seconds}s both;
        }
        @keyframes fade-tooltip {
          10% {
            opacity: 1;
          }
          90% {
            opacity: 1;
          }
          to {
            opacity: 0;
          }
        }
      `}</style>
    </>
  );
};

export default OnClickTooltip;
