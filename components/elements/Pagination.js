import { Pagination, PaginationItem, PaginationLink } from "reactstrap";

const PaginationElement = ({ _pagination, ...props }) => {
  return _pagination?.pageCount > 1 ? (
    <Pagination className="pagination-nav" {...props}>
      <PaginationItem disabled={_pagination.prev == null}>
        <PaginationLink first href={_pagination.first} />
      </PaginationItem>
      <PaginationItem disabled={_pagination.prev == null}>
        <PaginationLink previous href={_pagination.prev} />
      </PaginationItem>

      {_pagination.pages.map((page) => (
        <PaginationItem active={page.isActive} key={page.number}>
          <PaginationLink href={page.url}>{page.number}</PaginationLink>
        </PaginationItem>
      ))}

      <PaginationItem disabled={_pagination.next == null}>
        <PaginationLink next href={_pagination.next} />
      </PaginationItem>
      <PaginationItem disabled={_pagination.next == null}>
        <PaginationLink last href={_pagination.last} />
      </PaginationItem>
    </Pagination>
  ) : null;
};

export default PaginationElement;
