import SVGcam from "public/images/icons/photo-circle.svg";

const PhotoMadeBy = ({ name }) => {
  return name ? (
    <div className="photo-made-by">
      <SVGcam /> {"Borítókép: " + name.toString()}
    </div>
  ) : null;
};

export default PhotoMadeBy;
