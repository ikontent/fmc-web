import SVGLogo from "components/elements/SVGLogo";

export default function SVGInlineLogo({ className, medium }) {
  return (
    <SVGLogo
      className={`svg-inline-logo${className ? ` ${className}` : ""}`}
      medium={medium}
    />
  );
}
