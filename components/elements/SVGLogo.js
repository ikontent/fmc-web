export default function SVGLogo({ className, medium = "fmc", sizeLogo }) {
  return (
    <svg
      className={`svg-logo Ximg-optimizer${className ? ` ${className}` : ""}`}
      width={sizeLogo}
      height={sizeLogo}
    >
      <use xlinkHref={`#svg-${medium}-logo`}></use>
    </svg>
  );
}
