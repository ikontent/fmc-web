export default function SVGLogoSprite() {
  return (
    <svg width="0" height="0" style={{ display: "none" }}>
      <defs>
        <style>{`
          #svg-fmc-logo, #svg-null-logo {
            --fmc-logo-part-1: #ff7f50;
            --fmc-logo-part-2: #4db7ff;
            --fmc-logo-part-3: #ef2352;
            --fmc-logo-part-4: #332fc1;
          }
          #svg-fehervar-tv-logo {
            --fmc-logo-part-1: #bed9eb;
            --fmc-logo-part-2: #4db7ff;
            --fmc-logo-part-3: #97cdf1;
            --fmc-logo-part-4: #72c2f8;
          }
          #svg-vorosmarty-radio-logo {
            --fmc-logo-part-1: #ebcbbf;
            --fmc-logo-part-2: #ff7f50;
            --fmc-logo-part-3: #f1af97;
            --fmc-logo-part-4: #f89875;
          }
          #svg-fehervar-hetilap-logo {
            --fmc-logo-part-1: #b7b6dc;
            --fmc-logo-part-2: #332fc1;
            --fmc-logo-part-3: #8987d1;
            --fmc-logo-part-4: #5f5cc9;
          }
          #svg-szekesfehervar-hu-logo {
            --fmc-logo-part-1: #e7b3bf;
            --fmc-logo-part-2: #ef2352;
            --fmc-logo-part-3: #e9829a;
            --fmc-logo-part-4: #ec5376;
          }
        `}</style>
      </defs>
      <symbol id="svg-fmc-logo-base" viewBox="0 0 100 100">
        <path
          fill="var(--fmc-logo-part-1)"
          d="M91 25.26a27.12 27.12 0 017.63 13.28A50 50 0 0040.14 1a27.38 27.38 0 00-14.88 8 29 29 0 000 41L50 25.26a29 29 0 0141 0z"
        ></path>
        <path
          fill="var(--fmc-logo-part-2)"
          d="M85.34 85.34A49.85 49.85 0 0099 40.14a27.33 27.33 0 00-8-14.88 29 29 0 00-41 0L74.74 50a29 29 0 010 41 27.21 27.21 0 01-13.27 7.63 49.75 49.75 0 0023.87-13.29z"
        ></path>
        <path
          fill="var(--fmc-logo-part-3)"
          d="M25.26 9a27.19 27.19 0 0113.28-7.66A50 50 0 001 59.86a27.45 27.45 0 008 14.88 29 29 0 0041 0L25.26 50a29 29 0 010-41z"
        ></path>
        <path
          fill="var(--fmc-logo-part-4)"
          d="M74.74 50L50 74.74a29 29 0 01-41 0 27.19 27.19 0 01-7.65-13.28A50 50 0 0059.85 99a27.45 27.45 0 0014.89-8 29 29 0 000-41z"
        ></path>
      </symbol>
      <symbol id="svg-null-logo" viewBox="0 0 100 100">
        <use xlinkHref="#svg-fmc-logo-base"></use>
      </symbol>
      <symbol id="svg-fmc-logo" viewBox="0 0 100 100">
        <use xlinkHref="#svg-fmc-logo-base"></use>
      </symbol>
      <symbol id="svg-fehervar-tv-logo" viewBox="0 0 100 100">
        <use xlinkHref="#svg-fmc-logo-base"></use>
      </symbol>
      <symbol id="svg-vorosmarty-radio-logo" viewBox="0 0 100 100">
        <use xlinkHref="#svg-fmc-logo-base"></use>
      </symbol>
      <symbol id="svg-fehervar-hetilap-logo" viewBox="0 0 100 100">
        <use xlinkHref="#svg-fmc-logo-base"></use>
      </symbol>
      <symbol id="svg-szekesfehervar-hu-logo" viewBox="0 0 100 100">
        <use xlinkHref="#svg-fmc-logo-base"></use>
      </symbol>
    </svg>
  );
}
