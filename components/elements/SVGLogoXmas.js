export default function SVGLogo({ className, medium = "fmc", sizeLogo }) {
  return (
    <svg
      className="xmas-logo"
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      viewBox="0 0 46.8 59.53"
    >
      <defs>
        <radialGradient
          id="radial-gradient"
          cx="24.5"
          cy="36.16"
          r="25.07"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#fff" />
          <stop offset="0.34" stop-color="#fff" />
          <stop offset="0.76" stop-color="#e2e3e6" />
          <stop offset="0.85" stop-color="#e9eaec" />
          <stop offset="0.99" stop-color="#fdfdfd" />
          <stop offset="1" stop-color="#fff" />
        </radialGradient>
        <linearGradient
          id="linear-gradient"
          x1="53.77"
          y1="33.04"
          x2="19.6"
          y2="49.83"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#fff" />
          <stop offset="1" stop-color="#16171a" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-2"
          x1="-6012.24"
          y1="29.03"
          x2="-6045.1"
          y2="51.38"
          gradientTransform="matrix(-1, 0, 0, 1, -5987.51, 0)"
          xlinkHref="#linear-gradient"
        />
        <linearGradient
          id="linear-gradient-3"
          x1="-6020.85"
          y1="35.17"
          x2="-6042.86"
          y2="50.13"
          gradientTransform="matrix(-1, 0, 0, 1, -5987.51, 0)"
          xlinkHref="#linear-gradient"
        />
        <radialGradient
          id="radial-gradient-2"
          cx="41.74"
          cy="34.13"
          r="35.13"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#a5a8b1" />
          <stop offset="1" stop-color="#16171a" />
        </radialGradient>
        <radialGradient
          id="radial-gradient-3"
          cx="-1113.84"
          cy="2408.42"
          r="17.96"
          gradientTransform="translate(-83.56 -2616.25) rotate(-27.46)"
          xlinkHref="#linear-gradient"
        />
        <radialGradient
          id="radial-gradient-4"
          cx="-8073.87"
          cy="-147.23"
          r="19.58"
          gradientTransform="matrix(-1, -0.05, -0.05, 1, -8021.37, -257.99)"
          xlinkHref="#linear-gradient"
        />
        <radialGradient
          id="radial-gradient-5"
          cx="-8.47"
          cy="33.58"
          r="51.36"
          xlinkHref="#linear-gradient"
        />
        <radialGradient
          id="radial-gradient-6"
          cx="41.35"
          cy="-19.04"
          r="110.81"
          xlinkHref="#radial-gradient-2"
        />
        <linearGradient
          id="linear-gradient-4"
          x1="18.19"
          y1="19.88"
          x2="27.24"
          y2="19.88"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#fff" />
          <stop offset="0" stop-color="#fefefe" />
          <stop offset="0.01" stop-color="#f3f3f3" />
          <stop offset="0.03" stop-color="#ececec" />
          <stop offset="0.07" stop-color="#eaeaea" />
          <stop offset="0.47" stop-color="#f9f9f9" />
          <stop offset="0.79" stop-color="#f5f5f5" />
          <stop offset="0.92" stop-color="#f1f1f1" />
          <stop offset="1" stop-color="#fff" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-5"
          x1="18.19"
          y1="19.79"
          x2="27.24"
          y2="19.79"
          xlinkHref="#linear-gradient-4"
        />
        <linearGradient
          id="linear-gradient-6"
          x1="18.19"
          y1="19.7"
          x2="27.24"
          y2="19.7"
          xlinkHref="#linear-gradient-4"
        />
        <linearGradient
          id="linear-gradient-7"
          x1="18.19"
          y1="19.61"
          x2="27.24"
          y2="19.61"
          xlinkHref="#linear-gradient-4"
        />
        <linearGradient
          id="linear-gradient-8"
          x1="18.19"
          y1="19.53"
          x2="27.24"
          y2="19.53"
          xlinkHref="#linear-gradient-4"
        />
        <linearGradient
          id="linear-gradient-9"
          x1="18.19"
          y1="19.44"
          x2="27.24"
          y2="19.44"
          xlinkHref="#linear-gradient-4"
        />
        <linearGradient
          id="linear-gradient-10"
          x1="18.19"
          y1="19.35"
          x2="27.24"
          y2="19.35"
          xlinkHref="#linear-gradient-4"
        />
        <linearGradient
          id="linear-gradient-11"
          x1="18.19"
          y1="19.26"
          x2="27.24"
          y2="19.26"
          xlinkHref="#linear-gradient-4"
        />
        <linearGradient
          id="linear-gradient-12"
          x1="18.19"
          y1="19.17"
          x2="27.24"
          y2="19.17"
          xlinkHref="#linear-gradient-4"
        />
        <linearGradient
          id="linear-gradient-13"
          x1="18.19"
          y1="19.09"
          x2="27.24"
          y2="19.09"
          xlinkHref="#linear-gradient-4"
        />
        <linearGradient
          id="linear-gradient-14"
          x1="18.19"
          y1="19"
          x2="27.24"
          y2="19"
          xlinkHref="#linear-gradient-4"
        />
        <linearGradient
          id="linear-gradient-15"
          x1="18.19"
          y1="18.91"
          x2="27.24"
          y2="18.91"
          xlinkHref="#linear-gradient-4"
        />
        <linearGradient
          id="linear-gradient-16"
          x1="18.19"
          y1="18.82"
          x2="27.24"
          y2="18.82"
          xlinkHref="#linear-gradient-4"
        />
        <linearGradient
          id="linear-gradient-17"
          x1="18.19"
          y1="18.74"
          x2="27.24"
          y2="18.74"
          xlinkHref="#linear-gradient-4"
        />
        <linearGradient
          id="linear-gradient-18"
          x1="18.19"
          y1="18.65"
          x2="27.24"
          y2="18.65"
          xlinkHref="#linear-gradient-4"
        />
        <linearGradient
          id="linear-gradient-19"
          x1="18.19"
          y1="18.56"
          x2="27.24"
          y2="18.56"
          xlinkHref="#linear-gradient-4"
        />
        <linearGradient
          id="linear-gradient-20"
          x1="18.19"
          y1="18.47"
          x2="27.24"
          y2="18.47"
          xlinkHref="#linear-gradient-4"
        />
        <radialGradient
          id="radial-gradient-7"
          cx="38.57"
          cy="25.56"
          r="6.62"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#a3a3a3" />
          <stop offset="1" stop-color="#fff" />
        </radialGradient>
        <linearGradient
          id="linear-gradient-21"
          x1="44.86"
          y1="18.44"
          x2="35.05"
          y2="18.44"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#fff" />
          <stop offset="0.1" stop-color="#a6a6a6" />
          <stop offset="0.14" stop-color="#afafaf" />
          <stop offset="0.2" stop-color="#c7c7c7" />
          <stop offset="0.26" stop-color="#e7e7e7" />
          <stop offset="0.6" stop-color="#f8f8f8" />
          <stop offset="0.64" stop-color="#ececec" />
          <stop offset="0.71" stop-color="#cdcdcd" />
          <stop offset="0.8" stop-color="#9c9c9c" />
          <stop offset="0.84" stop-color="#828282" />
          <stop offset="0.85" stop-color="#646464" />
          <stop offset="0.87" stop-color="#696969" />
          <stop offset="0.91" stop-color="#797979" />
          <stop offset="0.94" stop-color="#929292" />
          <stop offset="0.98" stop-color="#b6b6b6" />
          <stop offset="1" stop-color="#cacaca" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-22"
          x1="44.86"
          y1="19.35"
          x2="35.05"
          y2="19.35"
          xlinkHref="#linear-gradient-21"
        />
        <linearGradient
          id="linear-gradient-23"
          x1="37.82"
          y1="17.66"
          x2="36.35"
          y2="17.77"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#8f8f8f" />
          <stop offset="1" stop-color="#696969" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-24"
          x1="37.87"
          y1="18.33"
          x2="36.4"
          y2="18.43"
          xlinkHref="#linear-gradient-23"
        />
        <linearGradient
          id="linear-gradient-25"
          x1="37.92"
          y1="18.99"
          x2="36.45"
          y2="19.1"
          xlinkHref="#linear-gradient-23"
        />
        <linearGradient
          id="linear-gradient-26"
          x1="37.92"
          y1="18.87"
          x2="36.44"
          y2="18.97"
          xlinkHref="#linear-gradient-23"
        />
        <linearGradient
          id="linear-gradient-27"
          x1="37.93"
          y1="19.03"
          x2="36.45"
          y2="19.14"
          xlinkHref="#linear-gradient-23"
        />
        <linearGradient
          id="linear-gradient-28"
          x1="37.97"
          y1="19.66"
          x2="36.49"
          y2="19.77"
          xlinkHref="#linear-gradient-23"
        />
        <linearGradient
          id="linear-gradient-29"
          x1="37.85"
          y1="17.99"
          x2="36.37"
          y2="18.09"
          xlinkHref="#linear-gradient-23"
        />
        <linearGradient
          id="linear-gradient-30"
          x1="37.9"
          y1="18.65"
          x2="36.42"
          y2="18.76"
          xlinkHref="#linear-gradient-23"
        />
        <linearGradient
          id="linear-gradient-31"
          x1="37.94"
          y1="19.32"
          x2="36.47"
          y2="19.43"
          xlinkHref="#linear-gradient-23"
        />
        <linearGradient
          id="linear-gradient-32"
          x1="37.99"
          y1="19.99"
          x2="36.52"
          y2="20.09"
          xlinkHref="#linear-gradient-23"
        />
        <linearGradient
          id="linear-gradient-33"
          x1="38.06"
          y1="20.91"
          x2="36.59"
          y2="21.02"
          xlinkHref="#linear-gradient-23"
        />
        <linearGradient
          id="linear-gradient-34"
          x1="38.6"
          y1="17.78"
          x2="36.44"
          y2="17.78"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#eaeaea" />
          <stop offset="1" stop-color="#908d90" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-35"
          x1="38.6"
          y1="18.45"
          x2="36.44"
          y2="18.46"
          xlinkHref="#linear-gradient-34"
        />
        <linearGradient
          id="linear-gradient-36"
          x1="38.6"
          y1="19.12"
          x2="36.44"
          y2="19.13"
          xlinkHref="#linear-gradient-34"
        />
        <linearGradient
          id="linear-gradient-37"
          x1="38.61"
          y1="18.96"
          x2="36.44"
          y2="18.96"
          xlinkHref="#linear-gradient-34"
        />
        <linearGradient
          id="linear-gradient-38"
          x1="38.6"
          y1="19.06"
          x2="36.44"
          y2="19.06"
          xlinkHref="#linear-gradient-34"
        />
        <linearGradient
          id="linear-gradient-39"
          x1="38.6"
          y1="19.79"
          x2="36.44"
          y2="19.8"
          xlinkHref="#linear-gradient-34"
        />
        <linearGradient
          id="linear-gradient-40"
          x1="38.6"
          y1="17.97"
          x2="36.44"
          y2="17.97"
          xlinkHref="#linear-gradient-34"
        />
        <linearGradient
          id="linear-gradient-41"
          x1="38.6"
          y1="18.64"
          x2="36.44"
          y2="18.64"
          xlinkHref="#linear-gradient-34"
        />
        <linearGradient
          id="linear-gradient-42"
          x1="38.6"
          y1="19.31"
          x2="36.44"
          y2="19.31"
          xlinkHref="#linear-gradient-34"
        />
        <linearGradient
          id="linear-gradient-43"
          x1="38.6"
          y1="19.98"
          x2="36.44"
          y2="19.98"
          xlinkHref="#linear-gradient-34"
        />
        <linearGradient
          id="linear-gradient-44"
          x1="38.61"
          y1="20.98"
          x2="36.44"
          y2="20.99"
          xlinkHref="#linear-gradient-34"
        />
        <linearGradient
          id="linear-gradient-45"
          x1="44.86"
          y1="19.56"
          x2="35.05"
          y2="19.56"
          xlinkHref="#linear-gradient-21"
        />
        <linearGradient
          id="linear-gradient-46"
          x1="40.86"
          y1="17.8"
          x2="38.86"
          y2="18.02"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#cecece" />
          <stop offset="1" stop-color="#8f8f8f" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-47"
          x1="40.93"
          y1="18.46"
          x2="38.93"
          y2="18.68"
          xlinkHref="#linear-gradient-46"
        />
        <linearGradient
          id="linear-gradient-48"
          x1="41.01"
          y1="19.12"
          x2="39.01"
          y2="19.35"
          gradientTransform="translate(57.09 -19.9) rotate(88.46)"
          xlinkHref="#linear-gradient-46"
        />
        <linearGradient
          id="linear-gradient-49"
          x1="41"
          y1="18.98"
          x2="38.99"
          y2="19.2"
          gradientTransform="translate(57.67 -20.57) rotate(88.8)"
          xlinkHref="#linear-gradient-46"
        />
        <linearGradient
          id="linear-gradient-50"
          x1="41.01"
          y1="19.1"
          x2="39.01"
          y2="19.32"
          gradientTransform="translate(0.32 -0.66) rotate(0.93)"
          xlinkHref="#linear-gradient-46"
        />
        <linearGradient
          id="linear-gradient-51"
          x1="41.08"
          y1="19.79"
          x2="39.08"
          y2="20.01"
          xlinkHref="#linear-gradient-46"
        />
        <linearGradient
          id="linear-gradient-52"
          x1="40.89"
          y1="18.04"
          x2="38.89"
          y2="18.26"
          xlinkHref="#linear-gradient-46"
        />
        <linearGradient
          id="linear-gradient-53"
          x1="40.96"
          y1="18.7"
          x2="38.96"
          y2="18.93"
          xlinkHref="#linear-gradient-46"
        />
        <linearGradient
          id="linear-gradient-54"
          x1="41.04"
          y1="19.37"
          x2="39.04"
          y2="19.59"
          gradientTransform="translate(0.54 -1.1) rotate(1.54)"
          xlinkHref="#linear-gradient-46"
        />
        <linearGradient
          id="linear-gradient-55"
          x1="41.11"
          y1="20.03"
          x2="39.11"
          y2="20.25"
          xlinkHref="#linear-gradient-46"
        />
        <linearGradient
          id="linear-gradient-56"
          x1="24.02"
          y1="20.35"
          x2="22.01"
          y2="20.57"
          xlinkHref="#linear-gradient-46"
        />
        <linearGradient
          id="linear-gradient-57"
          x1="41"
          y1="19.04"
          x2="39"
          y2="19.26"
          xlinkHref="#linear-gradient-46"
        />
        <linearGradient
          id="linear-gradient-58"
          x1="40.95"
          y1="18.45"
          x2="52.48"
          y2="20.83"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#fff" />
          <stop offset="1" stop-color="silver" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-59"
          x1="40.82"
          y1="19.09"
          x2="52.36"
          y2="21.47"
          xlinkHref="#linear-gradient-58"
        />
        <linearGradient
          id="linear-gradient-60"
          x1="40.69"
          y1="19.73"
          x2="52.21"
          y2="22.11"
          xlinkHref="#linear-gradient-58"
        />
        <linearGradient
          id="linear-gradient-61"
          x1="40.75"
          y1="19.42"
          x2="52.26"
          y2="21.79"
          gradientTransform="translate(58.26 -20.44) rotate(89.53)"
          xlinkHref="#linear-gradient-58"
        />
        <linearGradient
          id="linear-gradient-62"
          x1="40.8"
          y1="19.19"
          x2="52.26"
          y2="21.56"
          gradientTransform="translate(0.28 -0.57) rotate(0.81)"
          xlinkHref="#linear-gradient-58"
        />
        <linearGradient
          id="linear-gradient-63"
          x1="40.55"
          y1="20.37"
          x2="52.08"
          y2="22.76"
          gradientTransform="translate(57.76 -19.4) rotate(88.35)"
          xlinkHref="#linear-gradient-58"
        />
        <linearGradient
          id="linear-gradient-64"
          x1="41.04"
          y1="18.01"
          x2="52.59"
          y2="20.39"
          xlinkHref="#linear-gradient-58"
        />
        <linearGradient
          id="linear-gradient-65"
          x1="40.91"
          y1="18.65"
          x2="52.45"
          y2="21.03"
          xlinkHref="#linear-gradient-58"
        />
        <linearGradient
          id="linear-gradient-66"
          x1="40.78"
          y1="19.29"
          x2="52.33"
          y2="21.68"
          xlinkHref="#linear-gradient-58"
        />
        <linearGradient
          id="linear-gradient-67"
          x1="40.65"
          y1="19.94"
          x2="52.19"
          y2="22.32"
          gradientTransform="translate(0.59 -1.17) rotate(1.65)"
          xlinkHref="#linear-gradient-58"
        />
        <linearGradient
          id="linear-gradient-68"
          x1="23.18"
          y1="20.57"
          x2="34.74"
          y2="22.96"
          xlinkHref="#linear-gradient-58"
        />
        <linearGradient
          id="linear-gradient-69"
          x1="40.78"
          y1="19.3"
          x2="52.31"
          y2="21.68"
          xlinkHref="#linear-gradient-58"
        />
        <linearGradient
          id="linear-gradient-70"
          x1="44.86"
          y1="19.35"
          x2="35.05"
          y2="19.35"
          xlinkHref="#linear-gradient-21"
        />
        <linearGradient
          id="linear-gradient-71"
          x1="42.89"
          y1="18.01"
          x2="43.68"
          y2="18.04"
          xlinkHref="#linear-gradient-46"
        />
        <linearGradient
          id="linear-gradient-72"
          x1="42.87"
          y1="18.68"
          x2="43.66"
          y2="18.71"
          xlinkHref="#linear-gradient-46"
        />
        <linearGradient
          id="linear-gradient-73"
          x1="42.84"
          y1="19.35"
          x2="43.63"
          y2="19.38"
          xlinkHref="#linear-gradient-46"
        />
        <linearGradient
          id="linear-gradient-74"
          x1="42.85"
          y1="19.08"
          x2="43.64"
          y2="19.11"
          xlinkHref="#linear-gradient-46"
        />
        <linearGradient
          id="linear-gradient-75"
          x1="42.86"
          y1="18.94"
          x2="43.65"
          y2="18.97"
          xlinkHref="#linear-gradient-46"
        />
        <linearGradient
          id="linear-gradient-76"
          x1="42.82"
          y1="20.02"
          x2="43.61"
          y2="20.05"
          xlinkHref="#linear-gradient-46"
        />
        <linearGradient
          id="linear-gradient-77"
          x1="42.9"
          y1="17.76"
          x2="43.69"
          y2="17.79"
          xlinkHref="#linear-gradient-46"
        />
        <linearGradient
          id="linear-gradient-78"
          x1="42.88"
          y1="18.43"
          x2="43.67"
          y2="18.46"
          xlinkHref="#linear-gradient-46"
        />
        <linearGradient
          id="linear-gradient-79"
          x1="42.85"
          y1="19.1"
          x2="43.64"
          y2="19.13"
          xlinkHref="#linear-gradient-46"
        />
        <linearGradient
          id="linear-gradient-80"
          x1="42.83"
          y1="19.77"
          x2="43.62"
          y2="19.8"
          xlinkHref="#linear-gradient-46"
        />
        <linearGradient
          id="linear-gradient-81"
          x1="42.78"
          y1="21"
          x2="43.57"
          y2="21.02"
          xlinkHref="#linear-gradient-46"
        />
        <linearGradient
          id="linear-gradient-82"
          x1="41.16"
          y1="17.85"
          x2="46.93"
          y2="18.47"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#fefefe" />
          <stop offset="1" stop-color="#b9b9b9" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-83"
          x1="41.09"
          y1="18.52"
          x2="46.86"
          y2="19.13"
          xlinkHref="#linear-gradient-82"
        />
        <linearGradient
          id="linear-gradient-84"
          x1="41.02"
          y1="19.18"
          x2="46.79"
          y2="19.79"
          xlinkHref="#linear-gradient-82"
        />
        <linearGradient
          id="linear-gradient-85"
          x1="41.05"
          y1="18.87"
          x2="46.83"
          y2="19.49"
          xlinkHref="#linear-gradient-82"
        />
        <linearGradient
          id="linear-gradient-86"
          x1="41.07"
          y1="18.67"
          x2="46.85"
          y2="19.29"
          xlinkHref="#linear-gradient-82"
        />
        <linearGradient
          id="linear-gradient-87"
          x1="40.95"
          y1="19.84"
          x2="46.72"
          y2="20.46"
          xlinkHref="#linear-gradient-82"
        />
        <linearGradient
          id="linear-gradient-88"
          x1="41.19"
          y1="17.47"
          x2="46.99"
          y2="18.08"
          xlinkHref="#linear-gradient-82"
        />
        <linearGradient
          id="linear-gradient-89"
          x1="41.12"
          y1="18.13"
          x2="46.92"
          y2="18.74"
          xlinkHref="#linear-gradient-82"
        />
        <linearGradient
          id="linear-gradient-90"
          x1="41.05"
          y1="18.79"
          x2="46.85"
          y2="19.41"
          xlinkHref="#linear-gradient-82"
        />
        <linearGradient
          id="linear-gradient-91"
          x1="40.98"
          y1="19.45"
          x2="46.78"
          y2="20.07"
          xlinkHref="#linear-gradient-82"
        />
        <linearGradient
          id="linear-gradient-92"
          x1="40.85"
          y1="20.74"
          x2="46.63"
          y2="21.35"
          xlinkHref="#linear-gradient-82"
        />
        <linearGradient
          id="linear-gradient-93"
          x1="42.44"
          y1="19.06"
          x2="43.84"
          y2="18.99"
          xlinkHref="#linear-gradient-46"
        />
        <linearGradient
          id="linear-gradient-94"
          x1="42.35"
          y1="19.04"
          x2="44.3"
          y2="18.97"
          xlinkHref="#linear-gradient-82"
        />
        <linearGradient
          id="linear-gradient-95"
          x1="37.41"
          y1="19.05"
          x2="36.7"
          y2="19.02"
          xlinkHref="#linear-gradient-23"
        />
        <linearGradient
          id="linear-gradient-96"
          x1="37.96"
          y1="19.04"
          x2="36.62"
          y2="19.01"
          xlinkHref="#linear-gradient-34"
        />
        <linearGradient
          id="linear-gradient-97"
          x1="35.06"
          y1="18.34"
          x2="35.65"
          y2="18.34"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#fff" />
          <stop offset="1" stop-color="#181818" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-98"
          x1="46.85"
          y1="18.5"
          x2="42.21"
          y2="19.21"
          xlinkHref="#linear-gradient-97"
        />
        <linearGradient
          id="linear-gradient-99"
          x1="39.42"
          y1="19.11"
          x2="44.07"
          y2="18.97"
          xlinkHref="#linear-gradient-97"
        />
        <linearGradient
          id="linear-gradient-100"
          x1="22.79"
          y1="15.25"
          x2="22.7"
          y2="19.96"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#fff" />
          <stop offset="1" stop-color="#fbfbfb" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-101"
          x1="22.79"
          y1="15.23"
          x2="22.7"
          y2="19.95"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#fff" />
          <stop offset="1" stop-color="#f7f7f7" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-102"
          x1="22.79"
          y1="15.21"
          x2="22.7"
          y2="19.93"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#fff" />
          <stop offset="1" stop-color="#f3f3f3" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-103"
          x1="22.79"
          y1="15.2"
          x2="22.7"
          y2="19.91"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#fff" />
          <stop offset="1" stop-color="#efefef" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-104"
          x1="22.79"
          y1="15.18"
          x2="22.7"
          y2="19.89"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#fff" />
          <stop offset="1" stop-color="#ebebeb" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-105"
          x1="22.79"
          y1="15.16"
          x2="22.7"
          y2="19.87"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#fff" />
          <stop offset="1" stop-color="#e7e7e7" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-106"
          x1="22.79"
          y1="15.14"
          x2="22.7"
          y2="19.86"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#fff" />
          <stop offset="1" stop-color="#e3e3e3" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-107"
          x1="22.79"
          y1="15.13"
          x2="22.7"
          y2="19.84"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#fff" />
          <stop offset="1" stop-color="#e0e0e0" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-108"
          x1="22.79"
          y1="15.11"
          x2="22.7"
          y2="19.82"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#fff" />
          <stop offset="1" stop-color="#dcdcdc" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-109"
          x1="22.79"
          y1="15.09"
          x2="22.7"
          y2="19.8"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#fff" />
          <stop offset="1" stop-color="#d8d8d8" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-110"
          x1="22.79"
          y1="15.08"
          x2="22.7"
          y2="19.79"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#fff" />
          <stop offset="1" stop-color="#d4d4d4" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-111"
          x1="22.79"
          y1="15.06"
          x2="22.7"
          y2="19.77"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#fff" />
          <stop offset="1" stop-color="#d0d0d0" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-112"
          x1="22.79"
          y1="15.04"
          x2="22.7"
          y2="19.75"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#fff" />
          <stop offset="1" stop-color="#ccc" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-113"
          x1="22.79"
          y1="15.02"
          x2="22.7"
          y2="19.73"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#fff" />
          <stop offset="1" stop-color="#c8c8c8" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-114"
          x1="22.79"
          y1="15.01"
          x2="22.7"
          y2="19.71"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#fff" />
          <stop offset="1" stop-color="#c4c4c4" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-115"
          x1="22.79"
          y1="14.99"
          x2="22.7"
          y2="19.7"
          xlinkHref="#linear-gradient-58"
        />
        <linearGradient
          id="linear-gradient-116"
          x1="39.91"
          y1="15.28"
          x2="39.94"
          y2="18.98"
          xlinkHref="#linear-gradient-58"
        />
        <linearGradient
          id="linear-gradient-117"
          x1="22.63"
          y1="15.95"
          x2="22.44"
          y2="18.76"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#efefef" />
          <stop offset="1" stop-color="#4b4b4b" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-118"
          x1="22.63"
          y1="15.74"
          x2="22.63"
          y2="15.94"
          xlinkHref="#linear-gradient-117"
        />
        <linearGradient
          id="linear-gradient-119"
          x1="42.24"
          y1="16.76"
          x2="49.56"
          y2="9"
          xlinkHref="#linear-gradient-97"
        />
        <linearGradient
          id="linear-gradient-120"
          x1="-5961.85"
          y1="16.77"
          x2="-5954.53"
          y2="9"
          gradientTransform="matrix(-1, 0, 0, 1, -5924.55, 0)"
          xlinkHref="#linear-gradient-97"
        />
        <radialGradient
          id="radial-gradient-8"
          cx="-4771.01"
          cy="-7639.62"
          r="6.04"
          gradientTransform="matrix(-1, 0.05, -0.05, -1, -5070.71, -7399.73)"
          xlinkHref="#linear-gradient-97"
        />
        <radialGradient
          id="radial-gradient-9"
          cx="38.45"
          cy="14.8"
          r="12.5"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#c7c9ce" />
          <stop offset="1" stop-color="#dadbdf" />
        </radialGradient>
        <radialGradient
          id="radial-gradient-10"
          cx="40.21"
          cy="16.71"
          r="6.92"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#9396a1" />
          <stop offset="0.21" stop-color="#9a9da7" />
          <stop offset="0.52" stop-color="#aeb0b9" />
          <stop offset="0.89" stop-color="#cfd0d5" />
          <stop offset="1" stop-color="#dadbdf" />
        </radialGradient>
        <radialGradient
          id="radial-gradient-11"
          cx="22.69"
          cy="16.76"
          r="12.53"
          xlinkHref="#radial-gradient-10"
        />
        <radialGradient
          id="radial-gradient-12"
          cx="40.78"
          cy="39.37"
          fy="47.62662443713184"
          r="21.44"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#616570" />
          <stop offset="0.15" stop-color="#686c77" />
          <stop offset="0.36" stop-color="#7c8089" />
          <stop offset="0.62" stop-color="#9da0a7" />
          <stop offset="0.91" stop-color="#caccd1" />
          <stop offset="1" stop-color="#dadbdf" />
        </radialGradient>
        <radialGradient
          id="radial-gradient-13"
          cx="38.46"
          cy="37.64"
          fy="37.63502621698717"
          r="22.75"
          xlinkHref="#radial-gradient-12"
        />
        <radialGradient
          id="radial-gradient-14"
          cx="74.09"
          cy="18.99"
          fy="18.987517542755995"
          r="25.13"
          xlinkHref="#radial-gradient-12"
        />
        <radialGradient
          id="radial-gradient-15"
          cx="39.86"
          cy="22.58"
          r="4.16"
          xlinkHref="#linear-gradient-97"
        />
        <radialGradient
          id="radial-gradient-16"
          cx="40.52"
          cy="21.91"
          r="2.55"
          xlinkHref="#linear-gradient-97"
        />
        <linearGradient
          id="linear-gradient-121"
          x1="39.88"
          y1="12.86"
          x2="39.88"
          y2="14.76"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#731818" />
          <stop offset="1" stop-color="#fff" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-122"
          x1="40.29"
          y1="-6.57"
          x2="39.87"
          y2="12.39"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#1b75bb" />
          <stop offset="0.27" stop-color="#1b75bb" />
          <stop offset="1" stop-color="#26a9e0" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-123"
          x1="31.31"
          y1="15.23"
          x2="37.23"
          y2="9.78"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#26a9e0" />
          <stop offset="1" stop-color="#1b75bb" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-124"
          x1="46.54"
          y1="12.61"
          x2="40.45"
          y2="8.38"
          xlinkHref="#linear-gradient-123"
        />
        <linearGradient
          id="linear-gradient-125"
          x1="33.04"
          y1="9.3"
          x2="37.43"
          y2="11.37"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0.39" stop-color="#1b75bb" />
          <stop offset="1" stop-color="#2b388f" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-126"
          x1="43.9"
          y1="7.62"
          x2="39.24"
          y2="10.32"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#1b75bb" />
          <stop offset="1" stop-color="#2b388f" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-127"
          x1="38.52"
          y1="10.64"
          x2="44.7"
          y2="8.54"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#1b75bb" />
          <stop offset="0.3" stop-color="#1f89c9" />
          <stop offset="0.73" stop-color="#24a0da" />
          <stop offset="1" stop-color="#26a9e0" />
        </linearGradient>
        <linearGradient
          id="linear-gradient-128"
          x1="40.41"
          y1="10.02"
          x2="36.07"
          y2="8.15"
          xlinkHref="#linear-gradient-127"
        />
        <linearGradient
          id="linear-gradient-129"
          x1="40.17"
          y1="9.24"
          x2="40.3"
          y2="11.76"
          gradientUnits="userSpaceOnUse"
        >
          <stop offset="0" stop-color="#26a9e0" />
          <stop offset="0.12" stop-color="#1f89c9" />
          <stop offset="0.21" stop-color="#1b75bb" />
          <stop offset="0.26" stop-color="#1e85c7" />
          <stop offset="0.37" stop-color="#249fd9" />
          <stop offset="0.43" stop-color="#26a9e0" />
          <stop offset="0.54" stop-color="#25a5dd" />
          <stop offset="0.65" stop-color="#2399d4" />
          <stop offset="0.76" stop-color="#1e85c6" />
          <stop offset="0.82" stop-color="#1b75bb" />
          <stop offset="1" stop-color="#26a9e0" />
        </linearGradient>
      </defs>
      <title>FMCLogotervekMeretben_karacsony</title>
      <g class="cls-1">
        <g id="karácsony">
          <circle class="cls-2" cx="22.63" cy="39.78" r="19.25" />
          <image
            width="34"
            height="6"
            transform="translate(37.92 6.72) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAHCAYAAABtJp9HAAAACXBIWXMAAC4jAAAuIwF4pT92AAABKElEQVQoU73RO0/CYBgF4CYSoIUPaKEFQk3DpXKxYJQgooCUS6MJRjSoidFBBwcHBwcHB3VQY6KBn3wsn5Ea5bIIw9ne4XnPYQAwg9iIHQ6BBRd0wSN74FW84KM++BM8vm9mGYZx2jDIJIiUDiCckyDnw1A2IoiVF6HWFCRbUWht9V+gFLLgNhG8E6zIwRVyUwhFqALElB+h7BdiCKmYEN2C5DpJrB2lUTjVsH6moXSxgvLVKrav86jfFNC8LcK4K2H3fgvthwr2n2s4fKuj+9HESd+gj1CN3eegbbASBxIhwyYCSwKkzPg2MjvxP5DieXYiZO+pis6LjoNXHd33Bo57LQvyM0QmVhvmJMFlcSQkZcTGQjYvLYj+G/JYHTnl1O3mlakH88onT9kim5lJB0IAAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="34"
            height="5"
            transform="translate(37.92 6.96) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAFCAYAAAAg7j5MAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAu0lEQVQoU73QTQsBURTG8VmY93tnhrIgsVBSEokSKSIL5SWlFKUQ3/8TPB7jjqY0Ixunfrtz6t/RMtLEk54zYeSpQCULZpkqFjjaP2i6x4ggOcSsWrBqVKeGDbtFHRtOl/oO3AGNaOxAzJzEcP8g4B8FghOdJYKrRPYu3/sfB0ZRhaiIMCSKaFJbRfRUyFCFTF4hYu5CLmhJaxfeVsDbMWIfC7nIl1tKyK/CiOgj01jIijYu0m7jvi78ywPr7IU9ivj64QAAAABJRU5ErkJggg=="
          />
          <image
            width="28"
            height="12"
            transform="translate(40.08 4.32) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAANCAYAAAC6hw6qAAAACXBIWXMAAC4jAAAuIwF4pT92AAABbUlEQVQ4T63QS0sCUQAFYCGtfDDmY9RMzUdqPvBtaupkmoqhJCRKuaigaBNUtAgKeizaFESbfu9p7kxKlnSnmMW5q3vPx7kKAIr/JjuMonAUR/k0hcpZBtXzNdSv8mjdFNG+59B92sTucw391zpGb6ilXxNu+RDbCSDWDSLVDyO7PwluXebQvF7H9m0J7bsyOo8cei81DN4aGL63IBn0cS6sbCwj1PQi2vYLaHrAg/zC/EEMxeOEANYuxIUE7TxwmNb1K2RPWuHK2eEtOX+AyV4Imb0ICodxlE6SU8slg+aAEdYICwI60osgC/1VN1YbXoy+lXwprZwKMk4GeheD76Cn6IC/Ii4kIK1UEqhmNdDZdZNgQgTdhSVhJa1MSoRDycxi3qSG1qYD4+DBoAmWsHm8kFbyl4jHvBIqZg5kJQGNPgPYkBm2KCsrJlgEm9GqoNLzoEUjfKnBsyA7NAaF43Mh7bIcoV6QO9QLcucDZQqrIgrud/AAAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="26"
            height="12"
            transform="translate(40.32 4.32) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAANCAYAAAC3mX7tAAAACXBIWXMAAC4jAAAuIwF4pT92AAAA2ElEQVQ4T63PTQsBURTG8Vkw986dO4MNJS+FQhRSNJQFCxakJBvZKPn+n+BxmpeM0KGZxf+uzjm/rgHASJp7sOEeNdyTRu5MXTTyVwfxGfbIp/RSQa9t6I0NZ0ftP0P5m4PCPQDZo1HKU7Dn1IKQVQzaBhC3zwLWyIIaU1NCZk+I2/sZkl0LVp8ahtCE8v4HvkKiISFbVEcG0CCAuENcr0hVQtSpJkFtqpcceIPMkoCohBD9ilv8N//JFgTMIlUmrJY+4kMZRyCCuOEk+RA3lEbsQFqxA2n1AKBziuQ8xT62AAAAAElFTkSuQmCC"
          />
          <image
            width="9"
            height="11"
            transform="translate(40.32 4.56) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAMCAYAAACwXJejAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAr0lEQVQYV4WQ2RKCMAxF+QeFQnnCBRGsz+qgouiIC/7/38TclmWk4/hwuiSnTSYOETlD3uuQWnC3kvdM0ktJqlXIuxE74ZYGOlAuBOEMHitppJqXIvZ08BQLA4tn5qkaKZ+6tJ+5dJh7tJuMKeczqLKgl5AAWxCxxI/KRNB16XfN658gbaKRFo78I0q1TWsJQSTzpiTuKPU1AryChOYviW8J3ZyqtE8MBWuYv/grgA9/RdklGSGT8wAAAABJRU5ErkJggg=="
          />
          <image
            width="12"
            height="9"
            transform="translate(40.32 6.24) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAJCAYAAAAGuM1UAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAuElEQVQYV5WQzQ6CMBCEeQYRULj4ixrEo/EPFRuNWOD932bd2bSkkZOHabPtfDvbekTkWbW7hKzqPCH3zkqWzzam52YkxoaNmut3NqbGwD1ArSO6zAORWkV0T0N68K7zuAfJGLg8TH06M3BdBAKUaSSpSGodSCh03k8GLJ9Os6EIZwABNb8ACpiRcnQApJWc/mII74SvAzAWTAWbbstQuitjrnisOncACL/j/lKVxWJEZ23M3Uj/6AsH29eI/20qKQAAAABJRU5ErkJggg=="
          />
          <image
            width="17"
            height="10"
            transform="translate(40.56 5.28) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAAKCAYAAABSfLWiAAAACXBIWXMAAC4jAAAuIwF4pT92AAABH0lEQVQoU42R6VLCQBCEeQk5whGxRAVEEfD4wSUCgkoFPMCjKH3/l2i7E3ZrU2CVP7o2m0l/05lJAEi4ah/uoX+UxO1xCncnKYwraUyrGQx4n/BUXXI9MbOMBjIqpzGmVs0CPi/38XKRx6SSwZBg1We1rIVZQI8FdVZxfp7Dgnql8aPlY31VJMgPJejPzUEItBAXoMLDqYfgLItlo4D3lh+mWF8X8U3JLOmuur61EMVT9GnVw4yART2H53oeS3ZV5y+ClEhQnQIETNopOUm6vCjJqBwNMdiANIe3RgRaNSOIuus7NbaDFaSzSWO2cc/fEkyS6bHm4Ykym9najnmpNJrNwKyW3fQ85PmXObZiA+mWkmGq9g7tMscgrv5rdPULSrtJBjFle+MAAAAASUVORK5CYII="
          />
          <image
            width="12"
            height="9"
            transform="translate(40.56 5.52) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAJCAYAAAAGuM1UAAAACXBIWXMAAC4jAAAuIwF4pT92AAAA3UlEQVQYV42Q6Q4BQRCE94WEiDuI+4rjFRCxiJu4H8IVd7Cylrcs052sI/zwoyaTTH1T1S0BkN51G6Vwn2R+it6fRrUfh9IJgYBrPwFVaFt28P02SkMbJBmS6FB7MZyafhxqXuwqLqxlG1ayFZuSHduKE/uqG+d2CNow+QKODZ8wOLAqWjDLGjAVWuRNmOeNAnKJzzycwsClGxHxTmG2YlkwMzTPGRmglEPdi1MrwHUZoO5KJ8zxZNCTqBbV4aHH6dfQegrNcelGGT63gx/b0fW8aMO3dY6/jV/Av3oAl+oPoge5S+0AAAAASUVORK5CYII="
          />
          <image
            width="177"
            height="21"
            transform="translate(0 6) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALIAAAAVCAYAAADik7Q+AAAACXBIWXMAAC4jAAAuIwF4pT92AAAFVElEQVRoQ+2aeVsTVxTG/RKyI6AJEJYsrAGTYMCAiKgEjGwaICqLW6tt1ULd6GLVtlptbT/t8b5n5sxc8giXx6cQqPePn09m5mbmzszLe95z4xEiOmKxHHQSgaN0LlpNp5rKGGzrx40nsFj2m2TwKAtVmO1voKmeYyzkCx01lG4u947Jd4wntVj2El2wOteHgizg9akwPcq20/PpGD2fidHNM820MHCChkIVVsiW/QeOmovX0XRfPc2dbKAryeMsyKV0gK4NBunmSDM9vRxlsf55PU7vb8Tp37WT9GGlj35b7KHxWA2PWTwV4HNkWitJP79xAhbLboFDDjSW0WConIaV0EbDVSzAi521HA0g2PvjrfRMueuvV7tYsO9YsP3MP6v9LGCA70DUP893suCLo0QxxslZLGC7CKBTUEKFY+ZTJ2hjKkLrk2F6tdDNjvpOifZv5a4QLMSrsznbQZszTnRYyTTRjdONfD7EB9O8BOMAy/8Lkxg/BcQJloCKAijxj3MRepKLchx4c62X3irgsHBV4a/lPnZXiPilcuAXCrgxQAaGQ6eCOzvtbjEOsOwvJlFdjtdzRgQzoL+em6JZlTvnXOYTDTueI69KNcr1ZPcxL6t+fa6FvrvYRj9citBPcx30iyrpEN6bQi8D54SjIrN+WO3jz289AQtxFimcdXM2Rl+NtdAt1ZxBsGOR6v9EsNthHPAlIS/6dEsF5zws84xHq2mkrZI/I+dlu2pZLBAUhIDlILwkjEEpREac6qnbIpwkCPqkPMoopcYPFCFrpSJQEemEujauO6eECkdbyTTS2kgT3R4N0YOJdnro8jLfRa+AKuvgtbCIMt9Nvy/1eFlUwL4/hIIPRIw8C54o99V5lA17zgpwr5i/6TnvBcYBhwWTk23HPdV8oGzC2SAUNCjIevOJ4yxcCBnH4FbIbnAviAoiQp5bHXaAoACyIVwNPAY5EOUyjJf/1OXZdNRxL8WPKiMCOCFwhKiJ0BXg6wVfiC+udDq4IgMo3/iuJ+T8ViHLMSxp4T5lrhITJELooOkC8rxM76FUGAeUkmSR6FAG4XzgTDuoYkZB2OesQkosBInv4aWgzN0ebaY7ysHunA3RtxfaWKAPFPgMN8OaJbh/vpW+UWC/Pg4i2GAinhDQ1Ky7+7CNtc4NV8gQSLGY76pr3x0LcekFKOvgHlB/WADdvcxB5oHrfK9c8KGaH+YDcB7cC1wZ94Z7xP1eFVTjBdCAFf8R4/miMpjew2HAOGCvkDIkZRSle1AxFCrnEo3ynmmt4GUclHC4o95woKQtK4cUN0QDgu1lzSnXtGN4wY6QQ/xSb7kL6wDnFWctqHMX0n65xHWAdNKyvJRxl5ecX5tqeY653jrl3g0cLS71+uSEuAPiAch2OVFlQgPPQZ4FE3KeSSLgRJGD7IqlxDjgc0EWTDeVe4hggZ4H4ajImRACMiGcVMQFwUljsujugzCwnXddhh0n6bguRCS5UsQiQHA647FqXuMUd4LL7xRBTPdrKS3GASbEpVCipOGRsq+Xe3Ec6bql24Zwne7bEWBWCRpMajgRolKLFVU0orbh2HBuYIX3ZWMcoCOiRamDs6LUwdUgKMflaui8gstlpyNcfBZhem6ojUXHL2QQJ1zkt3QrSstu2PEgRCRRAFFBz2xwRF+UjjD9JsxvxLBvuK2SM6Usa+kxwzRBi2U3bNkQBxThipAh3GG3rMMppRmThkxEqruoLfOW/cQTb0oW7ANOkybxQUSb1rrohDtG/oOzFayl1LCQZT1RXw771AqDFa3loOIJufjn06QVreUQwf/YTGs57HwEeTrhVIbploYAAAAASUVORK5CYII="
          />
          <image
            width="28"
            height="17"
            transform="translate(37.92 3.36) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAARCAYAAADOk8xKAAAACXBIWXMAAC4jAAAuIwF4pT92AAABbUlEQVQ4T7XQWUvDQBQF4Ox7bJsudrfVolaL4gYuD4qKWkEQVBQRREFQRB/8/3Cc6YSkheDtYgMHwjD3fNyRAEjTTvPTxfy3i9aPR18eN/VnB/UXB403B80PBn654Ofk4Cip3NmoPNioPtqoPdkx+C6wfwOLVxZK1xbKNxY42g/OvTrov0uW/ZX8qYnCuYnZSxMRyre8F2jSDFmalODAQPbQQP6YgWcM7PaBt1YiNBaY3tKR3tER7A+ChQsB8lAdJMLjr2qYWdeQ3mTgtgD5ljmOngiU6hgadFsqvDYD10KQbZjZ1QV4ZAwNkaBdU+E0VLgLDFxmYEdDakOP0GBvdCwRNPIKrJICuxqD/ooGvxNvSZUODeoZGXogwywOgt4SA9tiS6qQSvSj2BI0n4GZEAyflMdbnBwaABVLAk8EFtizlmOUKhkl/IOsSVA9hqUEaOQESA2Pkx7Y25A/qReC2elgApQFqDpyD6QGJs0vU8K5ALch7k8AAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="28"
            height="17"
            transform="translate(37.92 3.36) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAARCAYAAADOk8xKAAAACXBIWXMAAC4jAAAuIwF4pT92AAAA1klEQVQ4T73N2QqCQACF4QNFC2FFRdKO0UbRSl0kvf9rTWdyNEVyXMaE/8LROR+EEPhHcCHwgv7HIuFBQPZkrjzime5SnnAhcGV3D4x8013OEg4EjuyswFsUMwZiy/Ed88FTHDICYsnxFdsocP8bKgRiwXGHSXDtgbo7uUCMOT5lcwU66aHMIGwCPjjLDqUG0ScwYEM2YpP8WCKIDsdlPQXaxaBEEC0CVhTUDaUt+tLgeJNJsM265qAYiCqBegi0zGMB+MFCoO5SkeQjArBWLvYFK+VDfm+a2wM7FOeQogAAAABJRU5ErkJggg=="
          />
          <image
            width="32"
            height="17"
            transform="translate(36.72 7.44) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAARCAYAAABTnsXCAAAACXBIWXMAAC4jAAAuIwF4pT92AAABiklEQVRIS73RZ0vDUBQG4JudNN172uFCFFGkoOJGUVEqOHEgCooiCv7/L68nuU1raevtNPCGkJx7zsMJY4zBi6QyyBaDGpRAF/uvMBcgc4Ck1REBCVpYgh6XYaTksYPcm6zTcJNB8UlQ/RygRZsIMyfDKipjwzQeFG94hA93ARkCOMlzhK+iwD+rjhzT9kKP0fBkHZCl4QWFA8oK7CmOCMyrCC6ODtP1gwsoKo0NtAAWVISWNYRXKFVtaIywwHYA0wSYIcBcE+AhIqsaomv6UBBhwe94vyG01AQ4iW3oiG3riO8NhhEWdIoLqNa3sE6ALQLs6EjsG0geGX1DhAV/xQVsEmCXAxIHHJE6MZCumT1jhAW9xEEkHcAhAY4JcGoifWYie9EbRFjQTxqAGgdkr0zk7y0hRNi432RoA5lzAlyayN1YyN9ZKDxYmHjydcUImw6a3DUBbglAmyg8ckTxpTNE2GzYeFuYeOaI8rvdBhE2GVVKrz6U3gjxYaPy2QoRHh51Kl82Jr9bET/HGa4Jr0mn3AAAAABJRU5ErkJggg=="
          />
          <image
            class="cls-3"
            width="31"
            height="17"
            transform="translate(36.72 7.44) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAARCAYAAAC8XK78AAAACXBIWXMAAC4jAAAuIwF4pT92AAAA50lEQVRIS73Ohw6CQBBF0TE2bFE09t5bNLZE4///F75lUTQgQ1kwOYbA7M4lwzAIP/FniOek0ScgBWnIgZZcjP2QweKsFZCHApSgEm+M84VmLS9CWQZQFfR4Qrw/vpfXoA4NaKoNYQfMId1ebupAV00IO/Az3MLith1AfRhGC2EHXA/1rOUDGMEkfAQ74HlYLB/DFGawCB7CDvhhLp/DEtbBItiBIGglA2gDO38h7EBQtJXLaQ9HPoK9MCw6yAA6weV/CHtRVHSWAXR1j2AvUEEspxvcnRHsYZVEAD3gaYewh1QTy78DXkuAyrk0Ec0JAAAAAElFTkSuQmCC"
          />
          <image
            width="34"
            height="21"
            transform="translate(35.28 7.44) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAVCAYAAAAjODzXAAAACXBIWXMAAC4jAAAuIwF4pT92AAABqElEQVRIS83RZ0vDUBSA4Zu9alvTaYeaFjcOXDhw4sKJgqKgKAjOD/r/vxxP7k06tO3tSrHwliY9uX04JYQQ8BNkAqJBAF+k1xEiIEBiCEFBiE5ACgmgRDFb6BmKvomaBzA9gJcaE0FLYmkxcFDpg2QJdBNyuLwNivAgxqAUKKbqwt+CGscfT4mgZ7xyDGIWJLBGggHVvKl6W9CzCMhLDOEgoihBaFSG0LgMfVNyV0ENvzRyNRBjiJhkkMic0jUMd8DN/0voJiYYIjzDINEFBfqXOwdxBypzNxGeRsCsQhGReYQsImRFAXtd7QjDHfhd5SaiSwzhQ2KbKsR32wNxB+rlI+xVtYzYZpDkkdYyhjvAy15DxAYCtrAdFRL7GoWkTjRIn+lNg7gDzeZuI7GHiINqyMBlcxjuQKslDxFxjIhTRFzokLnCrg3I3RoNQdyD24lu4xwhiMgiwoXk77GH+hjuoZ1EITe4jbsyZPDJrInhHtaNKMRFPJoUMvz6F8M9pJu5iKFnhLyY4LxbUPi0SiDuw0HkvFngfDBI8ZthuA8FWfHrn0Aq+wFWOl4UcB67tAAAAABJRU5ErkJggg=="
          />
          <image
            class="cls-3"
            width="34"
            height="21"
            transform="translate(35.28 7.44) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAVCAYAAAAjODzXAAAACXBIWXMAAC4jAAAuIwF4pT92AAABBUlEQVRIS8XODUvCUBjF8TNQM8lylVJRKiq9UaRFRej3/1p27p5dtrG5Z3O7m/CXsZ17+QHAPsjDnj+0lfx5AkGH9doBRQ8G0RUITthps6D0C4Pos0HYsBlQ9kuLOBMILpjvFpT/8TxEjNglu2JjNyB1EIz8EHEtENxI2rkyqYPE2CAmIeSW3dWHUQeZh2IQ3LNpdZA6yD1sEA8CwbwaRh1oBYiZQLBgy+NA6qBoFoEVe2RP5UDqoGwBwvTMXopj1MExWQRe2VsxjDqokkHgnX2wdT5IvayOLASfhzHqJXWFjUDwxX7SIPWCusO3QPCXxKgHXYRfgWAbYdRDLjMQ7Mxjy5B4/y9MtC55nh//AAAAAElFTkSuQmCC"
          />
          <image
            width="25"
            height="25"
            transform="translate(34.32 7.68) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAC4jAAAuIwF4pT92AAABkklEQVRIS7XR+0vCUBTA8b2n2zRfU5uaz94R0bsoKnpHBEVBVNAv1Q8FRf3/cDr33q5mqXfqJnxF3d35cKYkSRLwZF0C1ZYBX1KQSUqEDeeAnpDBSCuBQhRRLRlUhwEcMXPBQfSNABpBUghkEMhieQUihWCg5ge+geGyLQgQLalgVbCaOhTW9oUCZAMPKypglRlgj6vgTGoDQx1/pFsQoIpAA4EJDWLTGsTnBoO6XiCPiQPOFCKzDBlZ0PuGel7kjyk2w4D4PEMSy/1BwgNkA7oFBxYZktowfEPCAzwKLGErOiTXGJLe9gcJD/wuuYrAegtI7xjg7ptCSDj4b6lNHL7FkMwuQ7InvSHh0E5xILP3gxyZkD+PdIWEA7vFt3APW8joZWdIOKxX7gECxybkThniXYWAkChy1trEu/kPCYf4iSIXDCncRqH0EG2DhAP8RhDvmiHFu5AQEvlPOFJ5sZqQ8MZ+I8jYowXlJwuqrzaEgpCK97jJMyJvISIksknt3YbGlyM+PEz1Dxvqn7b4YBB9A2K/ZzaGbvckAAAAAElFTkSuQmCC"
          />
          <image
            class="cls-3"
            width="25"
            height="25"
            transform="translate(34.32 7.68) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAAACXBIWXMAAC4jAAAuIwF4pT92AAABBElEQVRIS7XPCUsCURTF8VMgUdkm5ZK2a+VKEEGE3/9r2Xn3vRqGGefM9oS/6Hjv/SEA7KxD7PhCjPzbAZEOO44DJR8ccuQhdNvF0l8ccMrO2EV7UPbBCYFzj+CyHSj/YfgnuGI9dtMMK/6xlyAY1If0wDWBPhuyUT1IDtjQICC3bFwdkgP/g6OATNhdNUgOpIbHAblnD+UhOZBZCID1XA6SA7lLjwSePIKZhuTBvYsvBKbslb0VQ/JY4fIsIHO22A/JQyq8e8Ba5UPySJmw9AA2ERE7tPYIPrKQXK6SA6zPNCQXq+YA6yuB5FKdHBAdscPfhH48JIeb5BBsoQebZEBs5K9fdp2wcqij6REAAAAASUVORK5CYII="
          />
          <image
            width="29"
            height="29"
            transform="translate(32.64 7.68) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB0AAAAeCAYAAADQBxWhAAAACXBIWXMAAC4jAAAuIwF4pT92AAAB1klEQVRIS8XPiU8TQRTH8XZ2uzc96AG0tKUc4glyCTEaQEEh4BE0YtRoNNFESUjw/09+vDezCynYTlt2a5PvdLb7Op9MCkCKPpCJFPg56dQiFCisFAwvnTgsFyNIQziEuoTS3sylkRlNDpeLmSWkEGL5NKyigF2hJgScqogdv9hYJSExq0zYWAhyNQG3bsQKtz1EN5OFmKxpwGvFB1/7wZ0kpBHG2JTKn6Hm4oE7vpDY9CXmzxoIbpkYuWPeGO76kiEGGQvmFThyz0T2wc1g7QBjwW3C7qqy96lFE7mlzMCwdoDj20U3zC4QuJiRaG55MFg7EMWYBB8qML9CrWZQ2Ogf1g5cjW/HWH6NwEcKHX1s9QVrB/5VYf0S44pPLJQ2e4e1A52KsOJTArcUWn5m9wRrB7pV3AzBbQKf2yjv2Ki80MPag3VJbJcw7iW1Z2NsvzusPbSXGGNIdmBj/NBJHuUk9srB+GsHE2+ot51h7WH9JLEjB1XunSpxlKu+d1D74LaVOMrVPrqY/KSqHdP353ZYe8Cg1b+4qH910fjmgfdDQaOaPzw0vnto/vQuYO2f4mjqlweu9dvH0FCu9cdH62TIKDd96mP2b6AfjLuZs/+ActqBJDoH5kUoqWRbfzAAAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="28"
            height="29"
            transform="translate(32.88 7.68) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAeCAYAAAA/xX6fAAAACXBIWXMAAC4jAAAuIwF4pT92AAABHklEQVRIS73N607CQBiE4VGMJGLSKCooHhANnlFj9Ifx/m8LZ/dbIKQt05btkryUw+w+ADDnC6lyrzl2WCcNbG+7BPdYt3109cGB+4ai1x68/qUbwAOWtYPmf8gCeMiO4qPFP2YBdM9+XLT8j34Aj9lpPFQPHHjCzthge1gO/GgBDtn5dqgcLIcDw3DBRs1ROVgbL8BLdtUMlYPcgdEKxHV9VA4KDwUMN+y2HioHpQfHxMYG4q46KgcbD0+ITQzEfTVUDlQOWjbVqLywSg7CA3tMBPqLngzE82ZUXlQnvBiI13JUXlI3vBmIWTEqL2gS3g3ERx6Vh5uGTwPdMwnoL/8iGEoCeuA7MeiRH6K/hspxrByIP+hh7OQgdnIQu38xjCV//Cnh7AAAAABJRU5ErkJggg=="
          />
          <image
            width="26"
            height="28"
            transform="translate(30.96 7.92) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAcCAYAAAB/E6/TAAAACXBIWXMAAC4jAAAuIwF4pT92AAABs0lEQVRIS73P+0vCUBTA8c1tbnPL1DQ332n0Iose9C7oAUZvev8QFVFBRET//w+ne+7RoWheX0v4jm3enQ9HAgCJ/fACeO9XHoLJqgQBwx9QkhUCZI0KmBKotgxaRB4oyC+4hWLKoFgyR9QwQcF4YGCYd6PUAVqUFSNITwbASPUPNjxwBIEEAbpDiJlRwMwrfWFNL2qI4VaRPCGhggLWeO9Yy5cIGGmG5KpIkSElBewJFexptSes7Z8ewjaxJ1Xe0IwK4XL3mPCAh0xVkVnWvArDC1pXmPAA5iFlAmpFljvHhAdqIRKeY9AiA5YIia5pEFsPdoQJD9THEQRWWKuExDaCMLIjxoTDW8URBLZY20GI7/oEYbFN2gSRxJ4OoxW9LSYc2C6O7OuQOGDQoQ7J478x4TBRHKkQ5JwY4JwbLTHhIFF8kyPaxjk1wGVQ6qoZEw7qJL7JGSHupQHpW9MfCHMvaJPUNUHp+0ZMOKCbUjcMuCMk++AjhGWqCJZ7DnmY8MNeyj4y5CnEofwrYcKPei33whDszWcIK7yHYOzD8h9CpPhpQenbEh/ut+LXP0FY6ceCX/bXhCu81hckAAAAAElFTkSuQmCC"
          />
          <image
            class="cls-3"
            width="26"
            height="27"
            transform="translate(30.96 8.16) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAbCAYAAABiFp9rAAAACXBIWXMAAC4jAAAuIwF4pT92AAABE0lEQVRIS73Nh07DQBCE4QGBQotC7x1EFTUUUd7/tczc7onIip0527Ej/ZET7+4HAJk1g4wftJV/zTqEOTbfDjh6iIi16KnlKuV/9AgsRGiJLU8PK/4zIlhh/elg5S/6jmDA1ppjk18GZJWte+pYbcgGArLBNtkW264HyoH/wYhgh+1Vx+RAbniXSGifHVTD5MDYQkRwyI7SMTlQuBQRHLPTNEwOlC4G5ISdtwzZ8plDuNSYPKbChUO4mozJQykFBNfsthyTR1LDjUO4L8bkgdQMuesAsmNE8MCexjG5XDU8OoSXPCYX64RnQq8dQHZ4SGw4wuRC3fBG6J19OiYXmoSPjiADvoj9QA82Dd8dQYb9IvsDApCMoDzGjQoAAAAASUVORK5CYII="
          />
          <image
            width="19"
            height="28"
            transform="translate(34.32 1.44) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAAdCAYAAABIWle8AAAACXBIWXMAAC4jAAAuIwF4pT92AAABd0lEQVRIS63PyUvDQBQG8KyTrYuWLna1pAcFlYJ4EQ8quKMgCFJxw4PoQQ/+/4fPmUxabZvkmbGB7xDmvR/f0wBoi8jgK6CH/pLwI8BCsP6bD4GFn//Eei8+Vl999N8DiH9yIS3dRw/dJw9jUBnrjDx07jj2wLFnCSljrRs3wjr3ElTGmtcuRNq3HBv9QLmxxrmDlUuJiXaz7yQwTu3IQf3UQeOCg1cSVMKqBwzVQ4baMcfidklzJCRS2eMYB6N2JzxnjhpW2WUQidrFYNpsJlTetrG0Y0dY1G6fpUKZWGnLQnloYxbMjRXWLRQ3ZAQ2BrOgVCwYmCisSaw0lCAFJWJez8QUtilABcxtGvC6Jvx+DMbnUsgcxmoGWN2A2+Igb+eHsh0FJGL2sg5W/YXF7ShgDrMKOiYYP9VtS5BaTsQMV8MUyDFqMRHTTQ0G02AGOqyyBJ26IsY/6BbHPH3SjlrKxkQ7cWpRHZpqJjBqmAo5kCfkQJ58A/7nml00Utp4AAAAAElFTkSuQmCC"
          />
          <image
            class="cls-3"
            width="19"
            height="28"
            transform="translate(34.32 1.44) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAAdCAYAAABIWle8AAAACXBIWXMAAC4jAAAuIwF4pT92AAAA70lEQVRIS63MCwuCMBiF4RNFFyKR7vcoKYoKwggp+v9/y87YMML0m07hhTHPHsRxjCrCG/LIJrwIVYZFhJ4VYHgQidSRZ2mcC4WE7hpywnAjFOqcMFyJqG5fqDx21ljqXnqYenAkdGIXRwwHIgeN/f0vAckwIBJoLHMjIclwa7C9I4Y1EYXtsiErDEsiK7bJh0QMMyJzjUmQjE0MtnDEMCKisKkdlIlhQEQ1tof+YvCJ+AYbumKewfrFoBSGLhGVVxz6wdAm0jFYzxVrGqxTDkow1Ik0WKs89MVqGpPGUuqLFSYNbdIYKsKkQZHEQZE+a8nlXoRnWyMAAAAASUVORK5CYII="
          />
          <image
            width="19"
            height="26"
            transform="translate(36.24 1.44) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAAaCAYAAABVX2cEAAAACXBIWXMAAC4jAAAuIwF4pT92AAABhElEQVQ4T6XQWUvDQBQF4Mm+tra1+2JbW1xQVEREER+s4L4hKooKKuKDPgj+fzjOZJqibZJpk4cDw+Tej5MhAEjSdL4ddH9c8aAo7U8HDGNn4XBUmu82GNb+SojNvNiYebXR+rDh3wmXgtJ4tNB4sjys+ZYQq91ZqN9T8NnC33vh4nAqVyaqN6aH1R8SYKVTA+VzE5VLCt6aGP4uBPwUDwyUjil21seuY2L5no7CvoHiIQVZu4tRaGxselcHA1m74hEFT4x4WG5bR26HY4We4YFhs5FQZkNDdpNmS4PXbk8PhSKx9IqKqTUNmXUNfjsGToylFlWkl2lWOcbaMTAKCsWcrgJ3gYOsHYsICsSspgJ7lmLzKlJLPLEwsyrDrMmw2xxj7RgmQkYwvSDDKPcx2szpUHBufOgfpmUl6HmK0XZWg/5qi4MiYARTXAkDrMLbsbcTLQdiskHgg0aJtxMtBmJEJpA0AjXNMRbRUjhGOKbY0gAULUVjKoFs0nZufMjDRAOT5BeajP5JHbmenQAAAABJRU5ErkJggg=="
          />
          <image
            class="cls-3"
            width="19"
            height="25"
            transform="translate(36.24 1.44) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAAZCAYAAADTyxWqAAAACXBIWXMAAC4jAAAuIwF4pT92AAAA60lEQVQ4T63MiWoCQRAE0JKgGMR4BMVzTVAUFRVRFsT8/2+N1TuDInv0amegoOmpfnDOwRpc4fAHvagFFyJXGTlrZS2IiV3+AcOJUOwhE4YjIcnZiGFP5OCxp712mIltPZbaa4epgzWhDbMzYlgSWXks818D7sUFkYXHcjsaci/+BmxpxDAjItg8HyqFYUokYn6KIRXDiMg4YJEVGwRsokOFGHpEBBuWg3IxfBMRrF8eysTQJtJhAqgBxdhXwLqvQSkMDSKCtV6HnjB8EpEI2LRitQemHRVi+CAiWP19KHH4XIJVbdADq9ghyQ2zSgRfPsf0ugAAAABJRU5ErkJggg=="
          />
          <image
            width="17"
            height="32"
            transform="translate(30.72 0.72) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAAhCAYAAAA/F0BXAAAACXBIWXMAAC4jAAAuIwF4pT92AAABjElEQVRIS6XPZ0vDYBSG4Wav2r1n1FpEQUQFRXDiwIVQnCiKIgp+Efz/8PiOBC0ZbxMDNwRyzsVJBkAmbbNfDgbfWfFgXNOfDigkHIzKfbdBEfouHA6r/2qj/2bD/bDTI90nC70XDqVCOg8Wuo8EeeZAYqR9Y6F9S7rjUCqkOTLRujYZ0rlPgTQvTdBaVxz6+0247Fc/NdA496BRCqR2ZKB2zJHGxTgwMVLZ10Gh+gnpzEiOlLd1UKR6aDAobCYWKG3qKG8RZIe0R6CDFEhxQwONYRTa1ZMhhTUNNB+hRc1GIvkVDTQGrXMsETK1oCK3xCuscigKiESyQxU+lF/mFyVC7BkFzpyC7DyBFjkUB4QiVo8gA474UCLEbMkw2zJs14OGYiCAGLVfhP0WgUTAGKJXZNDoNVZHgdWfDBhDtKIErSTBaHpQNyGiOBLUHEEIpFcJQiDRYgCRjQwoxJAy/y3RYgCRFI7414iWAgh5QBHZJJDNIdFSKMIglUOihVDEk9g1ouFY5L8JBybpB/7XsW+sdk+sAAAAAElFTkSuQmCC"
          />
          <image
            class="cls-3"
            width="17"
            height="32"
            transform="translate(30.96 0.72) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAAhCAYAAAA/F0BXAAAACXBIWXMAAC4jAAAuIwF4pT92AAAA8ElEQVRIS63O2QrCQBBE0VJxx7ivaFCRICKKKIj4/98Va5xAEJ100uPDhcB0HYI4jqEJT8Sm97d07EQengjuBB4W0CM3IncPBFcC1xTQIRdPBCcCl0+gOHIkcvZAcCBw/AZyI9gTMB18kMgizncR2BEwRT7I1iKZN5mPawJbmx4JCRhoo0Sw5NgUZgPZyCJBVkoEM45NCxlwI5MEmSsRjDg2yDQf8BsZEBiysRJBl+OeRaShGwkSpK9E0OY4sEkjN9IkYKCOEkGd4wZrFQdSpJogTR+kQqCmA957lAhU9IBF+DMo/wGRjqTEgzyJB3l6AclX+82rTOb7AAAAAElFTkSuQmCC"
          />
          <image
            width="23"
            height="31"
            transform="translate(27.36 0.96) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAfCAYAAAD9cg1AAAAACXBIWXMAAC4jAAAuIwF4pT92AAABwUlEQVRIS7XRh0vDQBgF8OxcRm20rd3LhQs3KoKC4kBcCCouFBEUVBD8/+F5l6vE2uolehZeoZDv/XipAkCRmcFXD0NvPj5+Cw+SpPnkYeDFA0P+BWg8uGg+R+VSgdqdCwY0Hl35QPXGAQPq9+3lUoDKpYPqFQVu3RCRCpRPHZTPOFC9djrK/wQUjwlKJyQEyucOKhcSgcIBQeGQoHgUId89KyzrlvyujcI+B1h+elZY9jW5LRv9OxxgS6QC2XULuQ0KbNtgK/J79o/liYDMqoXMmoUQaa0Q3SQC+lY+AXRFblMi0LtsguUzIrqJDaTnTAQLFFiMENFNbKBnykB6xkR6toUscURUGhtIjRtgCEswzxFRYWzAHzHgjxn4QNiSYE4S4A7o8IZohnUOTNJMG4nLvwWcmg63wRF/lK6Y+F15V4CUNJCyFgHsVY1KAux+DSwh0NTDeIP6r8s7ACvbAugKp0qB+t/K2wAzUGH2qmAIKfIV7L8QFYjSDgQcsPN8heg4TsIvjSjQfRVGiiJ9ESI6jpPwSzUUaLbCgdYS0WHcsA9UnQO6y1eIjpKEA3SBavHXJDpImhBQVLqA8IgOkuYdf2y3ISjZxGUAAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="22"
            height="31"
            transform="translate(27.6 0.96) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAAfCAYAAAAMeVbNAAAACXBIWXMAAC4jAAAuIwF4pT92AAABFUlEQVRIS7XO7WrCQBCF4dMWFWslilottn62iiJV1Cqh939d6Rl2USTqZJPpjzdZwsyzQZIksAy/8vJnbTgkxEj+BceBcHyGzXDsCQt+NMaxJbpjP5ewDb4hvE3DhXF8E95chwvhWBEWfG2MY0lUWt2G8+Nzh6tz2kBq4Yuw4AtjHFOigs90OAjHmOiEfWaDw/Chx6fGOAZEpVF2OBOOPtF3h2uz4fibxz+McfSICt4Ph+/ieCXaZb188H28TdhfoCFBOFpEpU5++CqOiGjD4dqyVvpD3eNNYxw1ooJHxeELHFWiz+zFXaAtZul8qBCVC2o28AlHiWjZ4dpCSO7x5PGKMY4HooKXbGGH8+fxaA+fcG0ob384drTlK7GpqgAAAABJRU5ErkJggg=="
          />
          <image
            width="23"
            height="30"
            transform="translate(29.04 7.92) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAeCAYAAAA2Lt7lAAAACXBIWXMAAC4jAAAuIwF4pT92AAABvUlEQVRIS7WOZ0sDQRBAL5e9Hk3RRNObvWHHgti7qIi9IIqIIoj+/w/jzC6JGk025S7wdvayd++tAgAK/mgB2ruNoupCTviY+xGFBXygGkJOMdbuczXCF5KyNgyZCp9a2Ad6VHUlxBe9QwUtglKcPwNmvPVIeWN0CyGFCNqbSRWsjL+lyK8HM4XCtB+MmIjR3s67GCCsLEpzfn57mnbBD05v85F//3RI2oPyrJiBfgZtg6ypSNWDQB/jch4YwMAQg/aRxiM1D0nK5cMoH0XGGATHtYYiNQ/pxlxckk9oEJpyMUDQjUkcnBRyznT9EekLBAlDM4LwrAaReR0ii3pdEekLJcJz3+LIggcBgqQdSzp0LuNEouuGNCKVVtK5goFVZM2A6IYBsa3aEamwEro1ibl804CuPZcDRGwb5Tso30X2Deg+NKtGpLJqcPGByeXxYw8CXHwk5PETE5Ln1r8RqagWCRQnTpEzDFx6ECDo5skLC1LXFqRv/0akAhmpK5TfCNJ3HgQIEmcebMjc25B9sn9FpB/XS/YR5Y8eBojcsw25Fw8D+VcH8m8OFN6dckT6UaOQvPjhYYAofn4HvgCc13moXEi6+wAAAABJRU5ErkJggg=="
          />
          <image
            class="cls-3"
            width="23"
            height="30"
            transform="translate(29.04 8.16) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAeCAYAAAA2Lt7lAAAACXBIWXMAAC4jAAAuIwF4pT92AAABCklEQVRIS7XPC2vCQBAE4FEEa7VFqiJWK7ZFig/ailoQ/P9/65y7FW1IzCbxVpg8LrvzIZxz4M+hBuefY0cudQHQiI9cHxoCoBkXSb6wHA9MKx6SPmA5Hpl2HCR9wGJ0znm+H8k+fJLykO59yO0PXSkP6VVH8j++SDn6zKAaog8MpBxDIyAMDQXAqDyiDlwGWY5XZlIOUQcug2OW+7wx0+KIOpAYPpebAWFhRsDnoxiiDmQuvVsDnwR85jqilt1cnAuAr3xELcpdZjkWlsBSAH83AULByhgIJWsi39mIulwk+DEGQtEvkU0aUReLBjsBsE0i6mKZYG8MhMI/a+DAf3G8IupClZgD/3MCmmnTI2tjqLUAAAAASUVORK5CYII="
          />
          <image
            width="18"
            height="30"
            transform="translate(27.36 8.16) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAeCAYAAAAhDE4sAAAACXBIWXMAAC4jAAAuIwF4pT92AAABe0lEQVRIS63OZ0sDQRCA4b3eYrrpzQRLUIxiBCsqosaCiqCIoogiiCDi//8wztzmUjDxkr0cvDvX9mEZYwy88GKiMReReZLGQDbFQEYbZYMDssVAsSTQotLYmLsojgRKiANeelIeC3MXNSwB5QFanE8zOzrWuXE3J2QwUlgay3DIKikjYX0PHSCH5TkiBFFmoQ2UFbCrvNCc6osNfGm3EaeGzSK0oMLU4v/Y0A8uMK9CqI7IkgrhhiDknqLOTxImaFmFyIo2FBsKUR2goUFkVYNoUxCiaHN0jSOxdQ3im/pAbDSoyYHYBs4tQYgihID4tg6JXR2S+38xX8QrsYPIHiIHOkwfGuIQnYKA1JHB53E/5gv0RptTJ1jLgPRpEIiAM+zcgMyFKQ5RmUsTslfdhKHstQm5m27CEJW/taA3Yahwb0HhgZe/CwBRxScLis+IPQaESi82lF9toBkIoirviL3ZMPPp+P/sV+XDgerXBCBCat8TgKjaz4Qg6heNSW65zcR/QwAAAABJRU5ErkJggg=="
          />
          <image
            class="cls-3"
            width="18"
            height="30"
            transform="translate(27.36 8.16) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAeCAYAAAAhDE4sAAAACXBIWXMAAC4jAAAuIwF4pT92AAAA70lEQVRIS63OCQuCQBCG4aGTTjqMCuk+iagokYj+/9+qb3dNCVtH2hFeUXfmQSKilwoXuUQfiApumLkVAZVQGVX/A5OHChBVzcQtWiH9Uo2gholbtkL6Qz2Cmo6Q/tgC1Ead/Jj9AAh1US8fZj+IEOqjAY9lH3pAPAFIDwwBqcbZGA+NDEK+I6SHfAPR1I6xSDw4MRDNf2MsEA/OgKiWjpAeXkhBK0BrtE1j7HJqYSMF7QDt0eEbYxd/YkDoKAEdDUTnBGOXrNgJ0EUCughBevkK7GYwdjgTCgAFElAIKBSANHCXgh74qyfxg3kSg1RvqU+8xqj//poAAAAASUVORK5CYII="
          />
          <image
            width="17"
            height="31"
            transform="translate(26.16 8.16) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAAfCAYAAAABZyaKAAAACXBIWXMAAC4jAAAuIwF4pT92AAABf0lEQVRIS63M50vDQBzG8ctqRlNb7UqXrbUuVFQUFMRBEUFxIKi4EBcI4hv9/+HxfknTYaxnrwa+uUvg+TBFZ2AsiD9MJqa5CnxIYVBjchCLpVVojgLVYtBsxb+LRlEko8JIKdCTnUSjCEIvM6eCsO5EwwhiFVWYBZ7Ha4Fm/u9Q+2JXNFgl1Y8AOkXjCOLUNDhVDXYrQuhbBPQgVHySQ/VWHKBvERBB3GkdYfEGR6Y0uDO6EIr8SMzpCHNn+TkvgVAjizqoxEJwJpeMX6EffyaXDbRbMZBalUAoGqbWOo2u94f6IjTy2+g0MEKlt2IY2wyiOzU4ss2HrTK7/NyRQKhMM4Zs00R2z/TP3L4ZgYQIRcOw/KEkkj8IxpR3ZME7sXogIRDmHfMxr3DKO5NEaNidFEIVzy0ULyyULm2Ub+w2JBx+r3zFgWsblbthkNsAqNzbGH90IIVQ4w8Oqk9B0kj1mQMvQdIIVXt1QA2FTLzFUX+PD4dQjU/3H5CPAPkCVX1Jixj5pbIAAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="17"
            height="30"
            transform="translate(26.16 8.16) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAAeCAYAAADKO/UvAAAACXBIWXMAAC4jAAAuIwF4pT92AAAA60lEQVRIS63MiQqCUBAF0AsRrUSEtEgLERIVEiVhQf//W3XfDBTi8spRuMNzlgMALxd+qBstDmrVh7S0FEG7HqSlrQA6BkQeXUXQ+x/6PniMAdM3IPIzJOIy+g/KN0aKYPw7lG+MFcDEgEhzogiC36DiZqAApgZEBgQwYxZ+qHwwJ+ASGhAZhopgVQ1VI0sFsDYgskAAG2ZbDnkRWdo2geyIuETFkBf4LEZNIHsiLoc85D3OLBPA0YqciLjEWch7mIPiJpAzEZfLF/IeFUIEcLUiCZHEiMjhjVCqkHe5FEmbQO5EHkZEjp+KvAFRBGNO4wNdbwAAAABJRU5ErkJggg=="
          />
          <image
            width="18"
            height="35"
            transform="translate(24.24 8.16) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAjCAYAAACZ6FpfAAAACXBIWXMAAC4jAAAuIwF4pT92AAABr0lEQVRIS63P+UsCQRTA8Znd2dv7NjU1o/u0oqKLDooyOqiIol8LiiD6/394zZtpMfFYG1f4OuPq+/AkRCNAiIy/iGpEdygQKiFqqmOERShoFkcYAc0moHtUCSMsToHFqEAwhI3k/zHxZqY1MFIUdEQSFKy8pgbZRU0MI4KZGQ3wWdBwFySwkhzG7aycvDvj+tBYxwenooO/Hd7duiLk1uQwbudW5d2bHA7reuA1+DDPqcozMs3UICwyxSTCz8gMg+hcMNYbwuFZJiA8o/MMYouDsb5f4DBuElvgLTGIrxhqEIZAbFki8VUDEs3+2EAIh0VNiSTWFCEssc6BjXapbbMnFgglNw1IbknATwnCUjsc4KX3eLsmZA66sUAEEwC2z+NI5lARwjK/QPbIguyxNQJ0ZAoAy51akD/vxAKBv2VPJJI7GxHyN8lfWFC4tNUhTEAtGwrXNozdtbHAwS6oxbe5klDp0VGHsOKNDcVbvtH9CBv5ECJY5UVuFTjUr9KDI/5aKFD5KQQIKz+HCNXeXKi/e8E/HhRuU33l0MeIEIbbTHyGAfFtGl8hQFjj24MfMqZdsqFqWJEAAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="18"
            height="35"
            transform="translate(24.24 8.16) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAjCAYAAACZ6FpfAAAACXBIWXMAAC4jAAAuIwF4pT92AAABBUlEQVRIS63OiWrCYBAE4EFEvBARtWrrgUcvJVoVWorv/1o62VUkJGbVfwMT/mPnSwDgGIcPQoILhEIYpi8iKHpARYVQeh67LkoKofIcltyUFUL1cSy5IYDaOfXHsPRBXRE0AiE5bCiE5v1Y9mFTEbQCIbloKYT2fVj+ZdsL6hDqMi82Zn+JCHrMIB+zoT6ROK+BkAwRwRszvI2ZiAwNFcEoEJLBMaE4k2zMBBLDEy9oSmjqAc0JzZhFGjPLqcLCC/og9O4ASemT2FcSM0uZ0LcTJMUlsdUVMws3oYhQ5AFtnCApr4n9KGYO50JbQlsHSICdF7TnX/3CHrSCPydIsH8v6IDjCXJ8vFy6ZUm7AAAAAElFTkSuQmCC"
          />
          <image
            width="24"
            height="30"
            transform="translate(23.76 1.2) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAeCAYAAAA2Lt7lAAAACXBIWXMAAC4jAAAuIwF4pT92AAAB0klEQVRIS7XQ6UsbURQF8MnsSxKXmFWjiVVJi3ShoKHS2lKwpa5o3XChhS5K+6H0/4fTe+dNEqnW+xJeAyfMDO+eH/dZAKz/kQe/Eiz8zssHR0n7RwIGOOLhYdP6FqN9nWD+ZwJ+FweGyeznGHNfYrS+E3JlGGheRuCkyNcYve/ioE5mTiPMnBFwTsCnQbkRoPExxPQRAScKaF5E5oD6bojGPuWQkGOF/H1GLPlXqpsBatshUuSAgMPb5SMDlXcBqh9UGKjvhXeWjwyU3waobCigthWitmMQKK37SAHaovJeIfedFwtvZnLNR+mVj6k3GbJxf/lQwETXA6f0koDXCpFmtIHx51S+ooDJF75C1g0Bxccuxp55SJHuAJHmtIDCIxfFZcoTQp5myKqnXS4C+SUXhYcKYYAjFWoD8byDZNFBPgPSqzIFRE0HcYuABQI6aosCIVKZFhDWbYTTdh9It+iMVn4L8Ms2OIykW7Qp2VVJRXpAKQMaCohmFSKVaAHeeA4cRoLsmhiQCqT0H5z4BlBVW0jDOkn/bN+CkyjAm8iQiiHAsi3kXAt2aMHN5+AWFCQN6oZ/yDkEBLQFXZNbNFfeB3gL3oCvSRoYNj3FeHEvfwDK+20H02MFywAAAABJRU5ErkJggg=="
          />
          <image
            class="cls-3"
            width="23"
            height="30"
            transform="translate(24 1.2) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAAeCAYAAADHJYVoAAAACXBIWXMAAC4jAAAuIwF4pT92AAABDklEQVRIS7XNa0vDMBjF8UNFnIx5Kc55mdVN2dTdvEEp+P2/Vj1PExhltU+SpoV/yYuTX1CWJfoIv9BHIaEg3AeOnGghR561sU/4IZwbOCqOL8Lfezgajg/CfeDYEN2xzzrcGceKqODbQ7gTjjei72zdDAfjWBJdGrx1p0GNl14s/hoZx5yo4It22BvHE1HBn3XYC0dGVPCZG+yMY0r0gT26w0447ojeWzyLjd9YfOoHqziuiQp+6w+34rgiOmaTMPhfHCnR1OLjiDjOCV5YPA2Hm/GRxS+7wQc4hkQFP+sO13CcEh3aRrHxE6L2Ae2Sa+Z3TFQaxIMrFwnRI4NrY9/kq3BtGJLBkx5xbRTaH/r3cBpK38gXAAAAAElFTkSuQmCC"
          />
          <image
            width="27"
            height="32"
            transform="translate(25.2 0.96) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAgCAYAAADjaQM7AAAACXBIWXMAAC4jAAAuIwF4pT92AAAB4UlEQVRIS7XQ6UsbURQF8MksmS2auGRPTIyiUttq6Upp7aKt4Ip2QUsrlNIF6gfx/4fjfcu0sVVuXuILnGGSvHt/nOcAcGxn7izF/HmBPzhqer9TCEy8s4dHSfdnAoH1Ti1jnW8Juj8SzP5SkDVs5msCkc73BP2/s4OmaX+JIZKB1rDWcQyR9mfKSYx//2cXDJrGuwiND5HEWp8UaAWr70cQabyP0PwYo3n0P3QjWHU7RG03Qk1ghwq87iy7jEtlM0R1K5SYbHhgCSuvh6i80dgOtdu7HhoJm36RR/l1iDJhWTtuhl16VSaf5TG1lsf0KwI3CHvLQ0NhE08CTD4l7DlhLzW4bgErPQgw8YjyWINrCuTmjLHiaoDivQCl+xrUDbk5Y2z8to/xuz6KKxp8qEBubiissORjbJnAO77EMpCbM8bSeQ+FRcJuaUy34+aMsaTrIZ0jbEFhsh1dJ7fUGItaLuIZD0lPNcvacQuNsbDqImoqTDSTV7kwGnQllp9yEVYIqxPW8hB3VDtu0SC59CUo5SAiwbpu1yasawHzx/qwGmENhXFLBs2fFzdycAnTV8ktMIl85HwH/VgGcsOmUY/cXywDucFhIj7ImnmpwrihYSMxxyUwICyxB0mMO3CTuQBydg2e2xZsQAAAAABJRU5ErkJggg=="
          />
          <image
            class="cls-3"
            width="26"
            height="31"
            transform="translate(25.44 0.96) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAfCAYAAAD5h919AAAACXBIWXMAAC4jAAAuIwF4pT92AAABIklEQVRIS7XNiU5CMRCF4RODKCHigqAoIu4oKiBxIfH9X6ue3k5CbuDa22Wa/EnTTOeDMQaa4RemyDeYElYOKe6+4djwTWTlEDUIn0S+2I82tHRQ6c33KTQsiCzLSHYIMyIW+lCE8EbAQvNNJBuEKYFX9r4dyQJhQuCZTauRPNCTQC+KEB4IWGjyP5IE4Y6AhR79SDSEGwK37L4eEgXhmsDYQb7ZaAhXBEYCjZUgXHL5UKBRGBIGDQQahiO1IZwRsNBFHFILQo9An53HI14IXQKnAvWVIBxz+YlAvTSkEkKHyw8F6qYj1dCBQEd5kK0Q2gQs1MmHbEBoEWi7fB9DW1+aBPZYSxFCg8t3BdrPj6yhHYGaOkhh8JgCaughJcg3mNofAmO4P2htuGUAAAAASUVORK5CYII="
          />
          <image
            width="29"
            height="33"
            transform="translate(21.12 0.72) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAiCAYAAABIiGl0AAAACXBIWXMAAC4jAAAuIwF4pT92AAAB7klEQVRYR8XQ7U/TUBQG8G59v3tzgw3GGGwEVAgYUQkoAVEzAtEYNQYMaIwBQvCDH/z/k8d77i2tkJGztmtZ8izres/55bkGAOM+wh4YVxb+lLD4t4xc4f5VCQRTcoN7FwIE939HaObw3C+B+XOB3qXA7XfscNJ0f/hQ8JnGc4FnT3zMnkr4p1D4sDPskrjpHPnoHGu4+90fio4dbn/xMPPVU3Dn293oWOHpjx7anzWs8KMc4Kn3LigK/yRh2ZybYZdyaQ1ctA4C+IOncG4mNTz5xkHznYT3JXyocW4mNTyx42ByL4AHGqfm3FwquPHSQWPbwcSuxN9qvDkYHU0E1zds1DdtNF4F8J7GublUcG3dRu2ZjQfXOLV+HR+NBVfXLNSeBvCLCObmUsOVZQvVJ5aGn2u4vmVnC5eWTJQfS3j1v9YbydGRYNE3EcJrQev1dCgLi54JCsF01ZUVjXNLR8mdL7yZIvxuBJcfWqo1t3DUDP3TbRXhtSNYLEh8UeKPMoTtRgFOswh3KsDngtYS5pbFyY0Hq1qAXZeReAgHrblFcXPjwRTDYW5JkoQ/iq4Bgq9b03UTzC1IGvVVcAxQzJKEa1FrbjhN6CPrGlCNCa5omBtMGwVT2/CqK9mjIUyNFe4ZuaAK5g5kFfZAVvkHRgvUdrVZ7hsAAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="29"
            height="31"
            transform="translate(21.12 0.96) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAfCAYAAADwbH0HAAAACXBIWXMAAC4jAAAuIwF4pT92AAABNElEQVRIS73NaU/CQBSF4WPcMKhBxd0QECSGEBfcg///b9VzvYP9YOGW6Z1p8iZt5kwfFEWBHGGOAt/yGr6tCx7hk+hcywbjneAH+yrR5DBeCb4p/O/MuhwbZgRfFK48t34QG56IzqrRZDAeiAr8nBHGlKDAj8tRdxgTglPN3FqDuuGeoDSxUTcYY4Jjha2tG4wRwTuFra0bjFuCQ4WtrRuMPsFBgIeZYPSI9QI8WB+Nh28C3I9Do2BcEQywtXWDcUFQ4Otm6FowzggKfNkcrQ2jS1Dgc8WtfZ3swQkx6VRxa1+31YcdYkcB7vqhNnwY4GNfdCWMfYICd/zRpTDaBAU+SINWwmgR3GPtjDB2iO2WsHW5SeXLFrEF3EqL/sHYILbJthW3LnkkT7GArbFnv7A1StEPMVIjazK/EjcAAAAASUVORK5CYII="
          />
          <image
            width="38"
            height="33"
            transform="translate(17.76 0.72) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAiCAYAAAAzrKu4AAAACXBIWXMAAC4jAAAuIwF4pT92AAACSUlEQVRYR83S92/TQBjGccfxiO00o0nT7NUyxSyUJcQeLUMMgYgQiC2EEEP8/9LDe76ziZKgNzi+hB++aZq87+mjcwwAxv/U1s8A27/y/OAyG34LMPweYOtHwA8vq/5nH4MvAQZfA4j/2QXd9T756H300ae/Ahd9zi7qrPuOUO+pDxI3/h27rKvOaw/dNz4i3OT37AE6ao88tF966LxSuLcrhjWf5dB67qH1gmAjCRM3N2uWPSytGo9zaD6lBI5grdFsUBR7YBrVH+TQeEQ9kTARt8Meumi1PReb91zUH/7BcTsidiBpG7dc1O7KQpi6NW4vih1IUvWag+oNByHuDsH2qfvu3CjogFUuE+qqgt2WMHFr3N5k7MC8rV90IBKwioBdJ9hNN8Rxu9pgpV0b5XPUBRvrAnZF3RrhuF1tsOJpG6Uz1FkFEzencNyuNljhmIXiKRvFHYkrn1e4S4uhEsPyhy2sHbUgYIUTEhfe2q7EcfvzxA5MFmxnkT+kYMcJddKWsJ10QIlg/jCLEHbQQnxrCsft/mvsgMhrZ+F1s/D71HAaxu0niR1w6yZyTRNeh1A9CQsOqMd5RA+KhTlVE+6mgokbGyiYepzc4Yv01y/sUgZOZQzWUY+TYNyhaTT1gZXPwFrLIIbVCNag2oTrLQc1BTNzBrKBhFlFCXM2CKd+Z9xhaRa/ydgGTJdgPqHyYzD1OLmD0k6+mAYimOkZ8Y3ZZYnjDtGRfDFmwAqZlYCiQtTkjYnfGbeou0gX47iFZcUOrCp2YFX9BvfzzALnz4bMAAAAAElFTkSuQmCC"
          />
          <image
            class="cls-3"
            width="38"
            height="33"
            transform="translate(18 0.72) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAiCAYAAAAzrKu4AAAACXBIWXMAAC4jAAAuIwF4pT92AAABo0lEQVRYR82Oh1LCUBRErxWxl7Eggg0VERsWRsf//624mzwTMwSWQF6AmUNyb3bvHAuCwOYJ++EDTxUsC/uywL7BPIlZH0L9SCzeqZJv7ANCn5FYaq+KPrE3CDmxgW+q7AvrQYhi7xwzvqsDPrAXCL2CHschGXWkSOwJMsSJjcyqY0VhXcg8OrFnrkReBYrAOpB5AF2OOh92VGBarA2he9DhqPNxTwUmxW4hQ+5AmyvdSfVVYBKsBZlWIqbymTdUIC92CZlrJ3bDle5k3lGBPNg5ZCh2Fcmp/MhbKjAO1oBIE1DsYjqh+KYKKKwOmTMn1uRKd8ZBBkaWa5A5dWINrnRnXGRgaPEYMidOrM6V7uRBBjJLh5D5E6txpTt5kYFU+AAihGJHkZzqTIoMxME9iOwnYio/LTIQhnYg809M5YtAB7YgQ7HdSE7li2L4hw2IEIptR3LqWJFkL6sQWXdim+UKxQ4Diwpk1hIxdcAX6WEFMqtOrMqVPuCL5GUJMstOrMKVLvsk+luAjBNThbLgLwjFFjnrQlmEYio0C2RgVsjArPgFG5I/W9RjfswAAAAASUVORK5CYII="
          />
          <image
            width="36"
            height="33"
            transform="translate(16.32 0.72) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAiCAYAAAA3WXuFAAAACXBIWXMAAC4jAAAuIwF4pT92AAACL0lEQVRYR83RiWsTQRTH8c3eR5K2SZu0aZqkaVUUFbWKIipYj9YDFG9EvIsKIuL/Dz/fm9nNJr2eaXZSC99Ct/OGD28sANb/0NrvBOt/yvJB0/V/Juj/SsAg/lscMNnq9wS9bzFWfyQqHCeo+yVGbydWIC77Lg4WXedjjM6nGN3P1FdqJ8dMHbTyLsLK+widDzlq9xnxkqJqv4nQfkugIdR+58SLiqj1IsTyKwINoQ46K142aUtPQ7SeUy8J9VqjDjsvXjhJS09CcK1nGepwjDFQcztA8yH1KFAgtSVCSXNGQAv3AjS2qG0NWnystyTNGQHN3/axcCdAhmo+0ChpzgioftNH/ZYPRjUYdF9vSZozAqpd91G7kYI2aUubtKW742MKAc1d88AxikHZlqQ5I6DZDQ+zl4dA6ZakOSOg6jkXMxcJdIm6kqOkOSnxwO4qZ1xw1fMpaCMFXfUmxowNKp90UT6lQZWzBLrgDVDS7L8mHsiK+w6SdQeMykDZlqTZcRIPcFHHwQjodI6SZsdNPBC2bERtAvVS1Aln8GzS7FE68B9+w0ZAMShcJlS2pTW9Jenio7bvR69Wgl/PQbwhBUq3JF06SXs+uNUS3JlR0ODZumYxe0BORJjKKChYTFFt2zhmBGQHFhjklDVIPdu8RkmXFJn6VfIs2L4GqQ3Rs3lzekvSBUXHP7SeHOQk+bNJwyZSIN5QiUB2qFHSkMkGG1LPFljHiuHEA9NOPDDt/gLB6Qkmfq/W6AAAAABJRU5ErkJggg=="
          />
          <image
            class="cls-3"
            width="36"
            height="33"
            transform="translate(16.32 0.72) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAiCAYAAAA3WXuFAAAACXBIWXMAAC4jAAAuIwF4pT92AAABh0lEQVRYR83QiU7CQBSF4SuiqEEEVyS4gIg7IhhDiO//WtczCzaR5dB2pkjykwJnJl8QVZX/kHyL2tiwiGTqMPaZjWMnE2Cm5tF/ZgdiJV+AmCYJxn7PDsZIxoB8OtDcb+xw6GQEyNi18Hd2QchkCMgHGi3G2A27JFQyAOQdDZdj7I5dFCJ5AeQNDVZj7JYN8ibPgBjQK8fYPRtkTR6BMHkQ2/+eY4MsSR+Ivgc9rY/RGCDpAdHzoId0GA0Nki4QXQ+6T4/RkCC5BaLjQXfZMBoKJNdA3HhQJztGQ4CkDcQVMqjbfBjNC5IWEK0ExPbrRAdLDzaBuPSgdhiMZgXJORAXCYjt00QHcwdOgTjzoGZYjKYByTEAphMHYvus0YEd1YFoJCC2zxMf1IA48qBGXIwykFSBOPSgenyMrgLJPhAHHlQrBqPLQFIBYgaqFofRvyDZAWAX7SUgdkHokocyAOUExA7Gyr1tAVFC5h+qbA5jLXipBW07EDsQOwcqbR4yiw6Kjg6K7gepJXPRW4gJnwAAAABJRU5ErkJggg=="
          />
          <image
            width="38"
            height="32"
            transform="translate(14.16 0.96) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAgCAYAAAB+ZAqzAAAACXBIWXMAAC4jAAAuIwF4pT92AAACOUlEQVRYR83R6W/TQBDGYceO7xzN1TQ4TZO2XBUgQCBA3EXlPoS4CwgQIBAgIfH/Sy+zXl9NSadx1wkffqmTzqwerTUA2v/S2m8f638qEM/s8Cwa/fCx+suHgMW/sUtFN/zqYfSdYD9TlIhdLKrBJw8rnz0I2PCbh/H/swcU0fJ7F4MPHgYfCfdlN0rEHqKy/raL/lsXy+9S2KRZ9jBVBS9cBK8I9kbCRHvNsweqqPfUwaHnDoKXhHtNuO29USL20IO09MhB7wklYM9SHLeHImHdeza6D2yM47i9OHYgT50tG4t3CBbjHksct5eNHZi29k0LnVsEux3B7hPs4XQoqIa1rlpo30hhYXftqVFQBWtestC8bCGBbdoSt5UPpQS2cN5E4wJ10QxhrWuEu05tWrlRB4YtnDUhakS48NauSBy3y8UOTKp2qoz6GRN1gTsXwaJXyu3uJ3bgX1U3yqidoCJcDBOvk9vdb+zAeJUjZVSPUzHstHyd3N60sQPZvFUDMax2Mr0xbi9P7ECCGhkQJTe2IXHcXt7YASfQ4fYNuAOCDSUs7FhxKBZmd3U4PaqvSxjdmL9OHSbg0TnBrJYOq60jxAUS5q5EuDWjUNREmFkvIYEtRbDM6+QOVdGuHwy/hDLBzIbEZV8nd5jKdnzRHQ2GR7CqxIWwRQnjDlJd8lAyNej2TpjZJFxn9qgEpukaRDHMqKQw7oCikh/0J3tjIaw2P1RkkrclYCLDnS8oTn6UCGWIR35hVv0FxnLzCCCaSKgAAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="36"
            height="31"
            transform="translate(14.4 0.96) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAfCAYAAACPvW/2AAAACXBIWXMAAC4jAAAuIwF4pT92AAABc0lEQVRYR83OB08CQRiE4ZEAEUuwICIW7NgbFqL+/7/1OcstcBfKHNeQ5E3I3uzmgZnhP4QfGH6hh0WEPiHf7i//q3Ge4ZOQL9YPMIMzdSmv8E6IB0XO1cWswxsRPeZAH1GMFQ3CCxGvHtSbxFiRIDwR8RyA5u7UQ1mEB0IeA5DcqkGacEfEPfMgtbc8QbghwuVRaj+6pwZJwjURXQ+6jY+xPEC4JOLKg7qLYSxrEM6J8CC1nfmGGsQNp4ScsYvkmME7aqDCCRGuTgBSe5UczL18RMSxr5MeY2lAaBPRHoPUPm5yMPVSi4gDDzrMDmNJQGgSsc9aAUjtF00OIuMGEXtjkNonSQ5Gwx0idj2omQ/G4oKwRcQ2c6hGfhiLA0KdiBBI7dM2/+MGEZusHoDUY1k0+8MaESGQeiirph/WiAiB1CNZNnlQJWLVg9aLxVgYhDIBFTYE1YrHjEAoEVD2mOpyIMPcz7DiQZXlYiwCKi0f4/oDClIxBArbiUwAAAAASUVORK5CYII="
          />
          <image
            width="37"
            height="34"
            transform="translate(12.72 0.24) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAiCAYAAAAzrKu4AAAACXBIWXMAAC4jAAAuIwF4pT92AAACZElEQVRYR82RCW8SURSFgYFZoSD7Xop1TbUatxjj3sQtaqxrjLsxNsYl8f8nx3fnzcjQQg+UGZTkIzBzz82Xc1MAUv8Dw58e1n/nEf6ngaRZ++ZBGP7wcPiXh/A5DSbJ4IuLwY6rxb6PpAQaToL+Rxern5WUiH1VYjvjUgJdEje9dw767130P4zkJs3RRXHSe+NACMX6nyZLCXRZHHReOOi8dNB9pcV6b52pQiF06aK0t220n9kQORHrvuZSAh1YhOZjG60nSuypEnuuW2OZEDpwEBoPLAjNhzZ8uW0tx3JR6MC81O9YqN9VYvcCuUdajuV2QwfmobZlQWiI2P1Rayw3CTowK9XrJqq3TNRuq8YirbHcNOjALJSvmKhcVWI3tJi0JnIstx90gFG+bCIqVr2p5ViOQQemUTqfw6GLiks5iJyIVa5pOZadBTowieLZHErnFBdGYmFrLDsrdGA3K6eyWNnMonhGy0VbY9l5oANRCiezKGwosdNazm8tOCnLzgsdCMkfzSJ/XIkpuZWNoLXgpCx7EOiA4A0NeEeMcTF10uJmMlIziTmrBtw1JbauxFRrhRMjOZZdhH1f2t0MnL4SG2g5/5zHtBxbvChTX1jNDOy2EusaWi7SGlsaBxMfmrUMrIZC5DrjrbGFcbHnQa6chlnJwJcTMTlnT7fGlsXJuFQxjVwpEKsrsZYSa+lzskVxs1esGGksOCdbkgR/f2ScFIx8GlmRC85p1f+NFEKxtJmCL+YGYkFrLJwk+stQYmYgVtBiLJg08oEvZinslC/GQsvAF0sHYtIYCyyLMTE2vEz+AOKPyHDrq+9sAAAAAElFTkSuQmCC"
          />
          <image
            class="cls-3"
            width="37"
            height="34"
            transform="translate(12.72 0.24) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAiCAYAAAAzrKu4AAAACXBIWXMAAC4jAAAuIwF4pT92AAABqElEQVRYR82Qh07DQBAFF2RCCT0gemgBIapCjQj//1vmbfZiK7g8O76ziDSR7zy7GlniOJb/gIwllh99dGc20AYyQtQ3GOvR3bGhkMgnYr7ASI9/3rHhUMg7gj4sLPc9WxACeUXQm4UVOmyJT2SIGMWFlbpsmS/kGTEvLmyoV8Rngg/kETFPFsbcZIYJTZF7BD1YGHNn5pjQBLlDkAtjbmaWCfMgt4hRXBjzc3cwoS4yQMxNGsb8wj1MqINcIeY6DWN+6S4mVEUuEDMNG+gVnyndx4QqyDliNOzS4phfBSowpI+YvoUxtw5UKB0+QdCZhTG3LlQoHDxCkIad6pH7daFC7tAhgjTsWI/cnwcqZAb2EXRgYcxtAhUSsYeYPeDCmN8UKiTiLoJ6FsZcH1BhIm0jyIUx1xdc2ESQhu3okS/0RfnLDQS5MLbIN8Uvugiahm3pFV/mk/zLVcSsgXWLY0tCkL1YRsw0rKtXfEkIZg8dxHTSMDYckvQhQsySC1vRKz4ckvQhclEdPfLB0NjfAoIW7YuxgbbQXzwJi/TMB9rCwiYfjstt8guEc0D4ysYCwwAAAABJRU5ErkJggg=="
          />
          <image
            width="30"
            height="36"
            transform="translate(10.56) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAkCAYAAACe0YppAAAACXBIWXMAAC4jAAAuIwF4pT92AAACG0lEQVRYR73Qa08TQRQG4O6le2+L7bZIaUsrKFEECYkauUgkGkCjBlBj8J5gvCR80P+fvM7sTLfdID1lZ9oP73a3O+c8e04BQGGaWfwTYulvRB/Umd7vEBzmIQ/ryMJZgO6PABy+cR6C/0cWqabzNQCHez9D9H4JdOJw69RH+7OfwN3vAYbfkcV5M//OR+uDj86XAJ1vWXQicPPYQ/ONBw63P7J88i+g2uHrrzzMHQm49Z5NfPp/VCs8+8JFCr8VE486TzYcJ40DFwn8UsIn3khUC1x/5qKxz+DnEn5No8pwvOeg/nQA99dN1SnB8a6D+MkQfChgqk4Jrm07qHF4V8J83Qfjo7ng6qYDntqOgOM9N8GpOiX42sMiqo8kzKd+LNZN1WmBU3xLTE3VKMOVjSJ4Zu5ncapOCS6v2qisD8EPBE7VKcGlOzbKd1nWsjhVR2Xky2jZRum2SILfEx9ANR0nI1+GixaiWxLn8OoU4KBnoQ9HcurSih70UthvW/A7FhJ8ieUm+4BlfeilsNc0BbwgcTk51ewqufCHO2vCm2NpSbwrcKrRVZN5cGITTsNEH++vnGqSJ5mHYtUATzr1vMCpJnkyQCsMrUh4aGqqQd6kN1Y4gJ0aW3ld4FSDvElvTK8AKxrgPFSxSpKLUSyAw3bJgC1XThWqRlxMBrts4sAAX7ldnhbMfkxH4tHk0Qxs2AKmCnQlA1OHdeYftpNizZp4XLkAAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="30"
            height="36"
            transform="translate(10.8) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAkCAYAAACe0YppAAAACXBIWXMAAC4jAAAuIwF4pT92AAABUklEQVRYR73PaU/CQBSF4eMWREVxwQVcUBb3FUkI+v//Vj2XGW0aG247vZ0Pb0onZ/oEJEmCmOFbHnxqQ6swR4IvFhPGjODc9XemXaoapgRnrsy5djE0TIh9smkWrBXGO8EPNslHa4HxSvDNwUt32ofKhGeCLw5Wt9qgaHgk6GFtawbjnqDAT8VQExh3BKWH4mhlGGOCYwdrWzMYQ4IjD99GgnFDbMA8ru1NYPSJ9T08CEPD4EsPX4ejpWFcEJSuqqGlYPQI9hysbYukDhajM4JdD59HgnFC7Bfu2qDF4A5BwU/tUBXGEUGBj23RpTAOCB56uBMTbhP0uPaRkPIP9whK+/WguTBaBHc93I4Jb6ewdrlK2ZctggK36kX/w02igu9EhNEguOlg7ZJF6Y8NDzcjwlgnKDXioCm86mBtbBmwQlTgtdgw/7Tg2tC6BayN6ugHK1zO//wLOp8AAAAASUVORK5CYII="
          />
          <image
            width="33"
            height="34"
            transform="translate(7.2 1.2) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAiCAYAAAA6RwvCAAAACXBIWXMAAC4jAAAuIwF4pT92AAACCklEQVRYR8XSCUsbURQF4GSS2WOMZje7C7XaEkwVRehOq7iiYqlUsaUtiKWg/n84fW/eLC7R28m8ZAJnmAzvXj7OTAJAIo7MXNmYvcnA+08OyE7n0sb0HxscMnNtjx7S/m2Bp3MhINN/A8RIIM3vFpo/LLR+Ckj7wkK/c+SiKGmcmmicmT6k9as/YmiQ2lcTdZ5vLuRctPLUDLk0bKaODEwdG6h9uYuh5sjFYVLdN1A9YJAjAXGaOaER0iDlbR2VHQOVXQbZFxDeCjUnFVL6rKO0qaO8pfuQ6mE4RGRI8YOO4icG2RAQp5m98IhIkPwbDYV3GnyM2wo1JxUyua4h//oeZGNwxECQiVUVtyGF9wzzMRoiNCT3SgWHTKwFmPxbLTIiFGR8SQVPbllgPAg1978hD/BkX6SR7aYdyHiPQVYEhpoLE/JAZj6NsUUGeRlAcityESTEnksh84xBFlxIV3WS640YYnVS4Jix5wLiYailg+RJBI/XiPd6qIWDpu9Do6bAbAYQ5ztZGB6iL0QvKzCqDNIQEO87oRZFzYMHWuEeZDYGiDqZhA9pBY1QS2TkcUhdQKgFsuLfpOwkOEQvCYhRV2C1Y4AoRsKH6O43Qg3LjHNJqgmkMqIRHt4INSg74qIwiJVEOuu+nnIMEPYDhyhm0Ao1NIwICIuiMYgdD8KBuJrYAF7IA6PKP2A5UWY1kWbHAAAAAElFTkSuQmCC"
          />
          <image
            class="cls-3"
            width="31"
            height="34"
            transform="translate(7.44 1.2) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB8AAAAiCAYAAACnSgJKAAAACXBIWXMAAC4jAAAuIwF4pT92AAABQElEQVRYR8XOi07CQBSE4dEQQVRQEIJGFEQEBKMiakh8/9daZ+3R2gietnuxyZ/0MpuvMMYgdniH+Uwb+g5rgWPieCX2xtb2Ud5ph1zDiuALEzzzTTvsEpYEn9kqiwbH8UD0iS03w0Fw3BO0WfxxO+wdx5zgIsG1rVccM6J3bJ4P9oLjluCETdksP+yM44bgOMW1vTcc1wRHgo+Lw6VxDAgOBR+Vg0vhuCR4JfiwPFwYxwXBvuADN7gQjnOCX3jfHc6No0fwjMkPaPu86YMuwR+4ti+SPjgVvOcXVnG0iVq86x/+E0eLaEvwTkQcTYLHgrfDwNvxhuAn4eCNOI6I2pph4V846kQPBG/ExveJ2h84DA9ncFSJ1hJcO+Sr9GZP8FpkHBWitmo8OMV3E1wb+85e5n/xnfjwN66NQqUOQvYBaHGDLqQuMYwAAAAASUVORK5CYII="
          />
          <image
            width="31"
            height="34"
            transform="translate(5.28 1.44) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAiCAYAAAA+stv/AAAACXBIWXMAAC4jAAAuIwF4pT92AAACIUlEQVRYR73RWW/TUBCGYTve7TRpnLVJE0JaJBBlEwIEZUcUxE4rWrEjLlgkEBL/X/qYswSnQDJJfPDFG7W2Z/zo2AJgFdXoe4KNHwk2f5YxvsYO5W34OYbsS4yjXxOMviWYvM8uyNPgY4wj1PBT1p/PsEuWqf82Qv9dhMEHBRBNe5Zdtki9gwjrL6nXGvA+lohZM+zSeevuhei+CCERrzTiTTTz5UYAnSch1p5RzwlAiN5+JBHcnBFA+0GAzqMQnccasasQ3JwRQGsnQOt+AIkQgKcKwc0ZATRvB2jeVQlA+6E6CW7OCKB+3Ufjpo/mHQVo3SMAnQQ3ZwSQbvuoX5sAaAQ3ZwRQu+QhvUKAqwS4oRCNW36ul88NqF30IBOIbYUQn4Kbmyf2geo5D6LV8wqRXvYlgpubt5k3K6dcVM64qJ7ViAsKwS1dpKk3Vk64WDlJbWmEPglu4aJNvVE+7kIkAacpOg1u2TL982JyzIFIAugUKlsFAuKhg3hEgM0MIeIWLdtfF6I+AQiRbCiAgHBL8nTon7BbQtgr4RCiKEDQLiFYywBjBLcgb7//8BsEaGnEukYMCgJ4NRteasNvKsD4JLhhE8kft0qAVYWQp6A/BzdsIvnjlG1IRJqdBDdoKqsUWXBiG5MIv14kICBApAEVheCGTCYBEpFkCG7IZJZlW7AdQohPUS725QpgaUCgENyA6SRAnoJb/MslgHvgf/cLct5/H0JB2oMAAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="30"
            height="34"
            transform="translate(5.52 1.44) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAiCAYAAABIiGl0AAAACXBIWXMAAC4jAAAuIwF4pT92AAABTUlEQVRYR73PiU7CQBSF4eNuBAXFDbSuBMU9CNGo7/9a45neiYZUe0t7Z0h+SOHMfAHOOaQIn3D4kvJn7UDT8E7sjfnPD0GjwpgRmrKZwIXftQvqhAmx1wBPi6g5jGdCvolUutUuqxoeiD0F+KUcNYFxR8jn4UcdNIFxS2wc4PvqaCMYI2KjAI8XQ2vDGBIbBvhmcbQWjCti1wJrWzMYFwQvBda2WurgZ3hG8FxgbVsldZCPMoKZwNq2avpgQPAkwFkiGH1i/QCf2qE6fBTggS1aCuOAoIeP7dF/YfQIevgwDvonjD2Cvn3BtQvqVvyiS3CX9eKhBRgdgh2BtYNNm3/YDnA3IYwWwTbbiY/Ow1sBbieEsUnQw6006C+8QTTg2gGr5G1NYG1sGbBK1MPrqeFloiuCa2PLgCWBtaF1/uX8v9aG1uWwNoqROojVNz6wIWrHTJ5mAAAAAElFTkSuQmCC"
          />
          <image
            width="26"
            height="32"
            transform="translate(2.88 1.92) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAgCAYAAAAMq2gFAAAACXBIWXMAAC4jAAAuIwF4pT92AAAB5UlEQVRIS7XQa08aQRSH8WGXvaMoyqIUKBS1XhKN0TRNW62madRekmrTNvXSqC80XhLTfv/k75mdhRWrnh3CvnhggTnnl0EAEFnW/luAfGcP9tuL6wDtmwBT/zKCmuc+mmc+WhcBWlfUZTB4qHHi4/mpgmR3f2OH01Q/9FA/8tA4VpDs/hl2CdezXx5qv6kDwv74EfbQOXbRU1X3XFR/uIiwfYU9dpZd9lgTX11MfiPoO0E/vQh76jy78KEqOw4qnx1E2K7CuBl26f3KHx2E2wR9UpCMm9GGyh8cRNAWQXSriS/pEC1obM3G+IYNiYUxxs1oQ6U3NiLoPbWpMG5GGxp9bUE29o6w9QTj5rSgkRULI6sEvVJY6a26GbdUCyouWSguE7QSQzHGLewPiutg3DJtqDCbx9A8tZDH3Ztxy7SgYNpEYYaQOQUNLyqMW8T13xd+y0QwRdjLGKObcUvS1PPBa5jwmwS11a06GLckTb1Q3YSsg8mbcQvS1n1wKgbcSapqdDEZtyBtCRQakEVQzYRbMwaGdCGrlIPMLidYNlCRoFGFScgO1d/IDeuUQJ3kzcLBIl3IcAXMQi/GDeoWveQsBeWHFcQN9ZMQhoAwBQyPMJ+woawgoaCcTZgjMkESiMqZ2UK322qmQ3MZROgAAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="25"
            height="31"
            transform="translate(3.12 2.16) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAfCAYAAAASsGZ+AAAACXBIWXMAAC4jAAAuIwF4pT92AAABMUlEQVRIS7XPbUsCQRTF8WNiiQ+VkdiDlaVmpSaEWYTf/2tt53IHFpHduzM78+LIiDP/HyLLMqQY/pBhL0eercu+ww/jsl8FoiPYMv7N7XIgGoIvhmVb3dHvVsAaNgxvFCm8Y0VKgTXjn4qU3rNChQ8/GF9x63IgGMEb44IsbSAIwSvjgrxXA7wRzBgXZFEd8ELwwviUm/sBlRFMGH9WxLobhOCR8SduEgaYCMaMyxxkxcKQO4c8hAOlCG4YF+S+HlCIYMS47LY+UIxcO2SUCMEV44IM4wBHCAaMyxxkPa66wy8XDhnEAw4Q9Bk/5y511kOf5Ycu4z2FrEe+yw8dh/QTIWgzLkg3PpAjZwQcZD0ImX60FLEuh04/mkROEyI4IdBMByjCPyOQdbHOFGmkRf4B5/Rg97qGTVMAAAAASUVORK5CYII="
          />
          <image
            width="25"
            height="28"
            transform="translate(0.96 3.6) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAdCAYAAABfeMd1AAAACXBIWXMAAC4jAAAuIwF4pT92AAABtUlEQVRIS7XP208TURDH8bP37pbegVZa2tKokIpGDeiD3B4I6INiDBoTjWCAEBLi///2Y+YcT7GSMrtlS/I9acLOfDIKgMqzlasiuME19cckDqWt+ztB7yxB/4K6TGAx/p84LNX5EWP5J/UrRvfEQL3zBP9+Iy6Z1NJxAe2vMdrfYmjoL9I9HQemRlqfCnj0mfpS0Ej7u4EmfS8u/L/mhwitjwQcGcRedN+MuNS2sB9h8YA6jGCh1pG5SJoVl3PzeyE4CzXfG0iaS43U34VobFE745A0lxqpvQ3AaWibkF0DSUtTI9XXAbja5jgkLcyElF/4qLwKUGHojcGkZZmQ0tBHad2HhaobOSNzaz64skVeBhqSFqVGkoGH4mPqiaeh0jOCnvsPAu4ifQ8aYmSVLnpK0DBHJO540PUI6puLGJEWpGn0I2q5KCy5GtIXUdJw2vQTzruIFimGOgbipOG06SeoO+AsxEmDWdKPXyGkStVuIWkwS8qbc8BpiK4JGy7ChZwRN1Lw4luIk4ayppxAgfOKBuKkoawpx1NQjoH4KmlgmvhvhHDSwDRZiZ+ZACNk1okf5NENgiV3TgIGctAAAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="25"
            height="28"
            transform="translate(1.2 3.6) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAdCAYAAABfeMd1AAAACXBIWXMAAC4jAAAuIwF4pT92AAABCElEQVRIS73N207CQBSF4aWoIVAVKKRUFOUYGgiEkkZ9/wcrazJjSAO4O+0MF6sXzZ7/Q57ncDn8Ilcr/JMelR0yxr+5Hw8I9oym3IHLinEnCLYM/yHpZaAygjWjagrZXY9XRpAwnBhkIwPWCBYMrzQi3VojmDI84xSytANKIfhieGKQuT0gIhgzPDbItBogI+8G+awO/IsgZnzEfdQDriKIGB9yb/WBiwgGjA8MEvtCQoNEboAzBF3Ge1zfHXCOvBJQUOgJwTPjCum4BYpIQOBFQ9Ij2+lPi3G1wD1wQpoGaftEngg0/QAn5IHIo0cE9wQavhFoRDqsM43c3QCRjupOPHAx8cDFjlFX2oUAdswfAAAAAElFTkSuQmCC"
          />
          <image
            width="19"
            height="32"
            transform="translate(22.56 8.64) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAAhCAYAAAA74pBqAAAACXBIWXMAAC4jAAAuIwF4pT92AAABqElEQVRIS63O90/CUBDA8dfdvioggsoGV9S4414Yd5wxzhh3jCZqYvz/fzjvWhAHUOgryZd7KX0fjjHGoBx+mEhMNhCSMYmBrIuBTDZdRNJw4lmxJd8go8sKL9UigRqSQIv4A50vuqyG3egshOkxGbSoBDTLGZ1y0+D3QY8j0OEiRsKdZqo58PtgImAm3ayM4kA8p/jDKEKsbCmEeEEBu7dx8N8DAni3m421DKj+MdrE7kOkX3VqHcSGGwOrPqRtHGRIhRBCoVEVwuOaJ1jzB9omNFKCxjQITwhgDlBCIpPYlAZts/XBuv9EQGQakTlsxp2+MYqA6LwO0QVsUYf2Fb0m6IlR7UuILOsQK+IsimK4TWzVLb5mQMeWURX0hMrF1w2Ib2CbiG0b0Ln7H/REfuYgO9ieAV0HphhG2zjQvgmJI+z4N+gJ/K3rsAKlziwxjEqemJA8NSF9aUH6qgJ6XqxW6tyC1IXlYJlrQYyijQjK3ASAUZlbDtk77J6DMJZ94JB7xJ4CwKj8M4f8C4fCm+39ciMVXm3ofg8Io3o+gsQ+A8Qozxea6QsHbGh9o9yLdAAAAABJRU5ErkJggg=="
          />
          <image
            class="cls-3"
            width="19"
            height="32"
            transform="translate(22.56 8.64) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAAhCAYAAAA74pBqAAAACXBIWXMAAC4jAAAuIwF4pT92AAABAElEQVRIS63Mi2rCUBAE0KEgokGttrY+WhERCaFUalER/f/f0slua5A8No8NTLh37+wBgGscfmga/GN4ag7qjxBaXlhLMbSbgcmhrRg69cHk0FEIXSaoBz5eAoVcMBn0iPWZ5+pgejBQSPJSDcweDgmNnDB5eCU2Zt7Kg/kPfxDemWk5sPhxQijOzAGTAiHMmU8btLEPQnEWDpiUFophWQya0L24VAyrfNBE7sWVQlg7YFJeK4ZNNmgCqQVCCL2wUDFEadBczgQjT+yLWJztI2gu5oLfjpgs/xDcJaC5UIgRwq8TJsCe4FFBs2wFB0dMkBPBM+xi2fhiF0dMQKtQJTdyc7duQzmrxAAAAABJRU5ErkJggg=="
          />
          <image
            width="18"
            height="34"
            transform="translate(20.4 8.4) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAiCAYAAABStIn6AAAACXBIWXMAAC4jAAAuIwF4pT92AAABrUlEQVRIS63OeUtCQRSG8bn7apaaqZWathctUET7QhsURNECFRRRVLTR9//jdM7MbbHSsbkJz4Wrvj8OY4zBe/hhqrGvkGapY0x3BaCZDHSHgeFrShgzPA10j/GMQAMzgSX/jvGHEUZAhFitGthp/U8Yf7yPrZTIzug82fgHRPFxO5bVwclhHTq4heaxmhc+zkch4nUZ4BWNprCaF7czGncb4JcE4lcUIMpDwC9jPaKg14Cw35Riv34ZVAUQ9IkSw4oQFQ6YEA5iQyYkRkxoGWuM1f2BrkiMIjAqkOSEBclJqy7W8FwOjFsfSOuUIkTR+KNpC1Kz9q+YHMJx24wAeHOKEEXj1LwN6UVRZuUnJkWo9EKELCGybEP7uqMGUQRkVhFZcyC7FQOi6JLsBrbtQMdOLSYdfy27GSFYbtdVhyi6hMrtuVA4+MSkw+/RJXlE8vsudB556hCVx0sKhwidxIQKR4gcexzqPheYdFSvrlOBxIfOPChe+rxYUPFCIKWrmBBVuvahfONDz30g/3OjyreI3AVQeYwJUZUHhJ7+A3oOoPryDxBVfQ3lf2q2N40RMYaG8J+UAAAAAElFTkSuQmCC"
          />
          <image
            class="cls-3"
            width="18"
            height="32"
            transform="translate(20.4 8.64) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAhCAYAAADUIPtUAAAACXBIWXMAAC4jAAAuIwF4pT92AAAA+ElEQVRIS63OCQuCQBAF4AkkCInolBC7CLuJLoL+//+q50wH4jHajvDE3X3zuUREzyR4yCXyagDz3DB5eQJR83/s99EUiFr/YelFSyDy62PphS8Ip10Py260BXGGeLMDKEm3OlZ8AIR6SL8aVnwwAPKOhpRCfDgCNEQCHdP/FAhE43JMh8aCUOgIcSkUiCbFmIpwKQISGUBcnAJKMs/HVOBbnAniDHF5AWhhAPHAElicxdTBzEBsBa0BrZBNGlMHczEgtLWAdkYQD+6BHX6YOlAIHQEdDaAPZgOdcKuzYGq5FLoBuhhADFytoDtu9SC9WCVmEGNaoWpeW+lgBSDa4GIAAAAASUVORK5CYII="
          />
          <image
            width="17"
            height="36"
            transform="translate(18.48 8.64) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAAlCAYAAACkhgJBAAAACXBIWXMAAC4jAAAuIwF4pT92AAABnElEQVRIS63NZ0vDUBSH8TSjSZO26d677lUVB27FvRUVEXyhKIqC4Pd/czwnt8Nq2zQ3DTy5Tej/FwEABLzAShSAnp0miJoAHkkAOkUV8zqHLORvkuFxBFk3GjXTPSCbHIgcwCGmhPAMspOyG7chFI28MdFKibDfakLsC2p7sJA4lmCAluFA1CSO0ziu58tL4CtIttC/F1oWxzkcF1s5RujLNNRLWAUrS2AM94Y6vqSxUa03JIF/THaOUP4RGfyjrMA4Ntkd6o7g1wMTrYI1DoQKTuF4GpuRwawpYM4pHaGeiDWcZWNzXoHQAgdC0bDZIicSXlIgvKxAZMXbzDFCRVZxjEXX8VzjRGgc3fBCbAvPTU6Eim0jssOK76ttkO24UXxXhfgeK3HEiSQOcHzISp5yIlTyGIETFVJnGqQutCZkO/ydNT5nQPqSE6HSVwhQ1y6QzI0GVrcukOydD7L3rPyjD7gQKvfAAFcIjQtPOhSedX6Exo24Ear4okPx1SVSetOh9O4SKX8YUP403CFU5WsASPV7AEgj2z/00w9MumNU8ydb3QAAAABJRU5ErkJggg=="
          />
          <image
            class="cls-3"
            width="16"
            height="36"
            transform="translate(18.72 8.64) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAAlCAYAAACkhgJBAAAACXBIWXMAAC4jAAAuIwF4pT92AAAA+klEQVRIS63OiQrCMBAE0EEQFamISPG+UKQiHhRFxP//LZ1sxFJsm7abwIQku/sIALxNuFA3EKShRGQjgmZ9yG5Ni6BVD0oOBNBmOtWh5PAFVIhcukRMgmpQ+hJYQIXIQ4+ISb88lP3Y94EMiJgMy0H5haFFELqh/EJoAYwUiBRHFsGkGCpGxgRMpgpEGghgxszzITcytwAWCkSalkSWWmRFZM1ssiEn8GskgK0W2fpAdkRM9v+QczjVTACRFol8IAciJsc05Bz8gwjgpEVOPpAzkYsSkaGrDyTmb+IEcg5kIjciNy1yJ3JXIjL48IE8fSAvD0gKdDWUyQeNXb1xEwx/uQAAAABJRU5ErkJggg=="
          />
          <image
            width="13"
            height="31"
            transform="translate(17.04 8.88) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAAfCAYAAAA89UfsAAAACXBIWXMAAC4jAAAuIwF4pT92AAABUklEQVQ4T53NWUvDQBiF4clM9qRNN7rY3bohiiuoKCKCgheiBQWhoKAiInjh/7/4nJPBQGk1MwbeJAPnYRj3GDGmkg/TidmxRZZgaTzQg8wuWiQii7jLSIQW4ZyL3Conp2wpkFjk1ng+wstvcfIaPEM456O2GuJWr66Jgp4ghNsAgq6gcCB+hdlPOJRDGW4FiEYaCEVLEvYViJYFxSv2XDh1iFdtBWT4L6xrIIRhvKZAcVMTYVjYUKC4ZVOy7czAGYRRsuOk42TXodKeBkIYlvYVQOXDaTgXlQ/kUFY5crNyEaqcyDE6Vl8tVD11CdXO1FcLodq5RyoDVL/wqH6palx5GfwTYfhT89rXQwjj5o1vhlq3PqWNDdDCnU9p9wYItR8C6siMUGci0cQQdR8D6j2F1H8OSRuh/otEr4Zo8BbS4N0QDT8iQkYILX7+A42+FPoGeY/u9XamMzkAAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="13"
            height="31"
            transform="translate(17.04 8.88) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAAfCAYAAAA89UfsAAAACXBIWXMAAC4jAAAuIwF4pT92AAAA0ElEQVQ4T53OiQrCMBAE0BUpeKFUKSJVq1LvA0UUBf//t3SyK5bS1mQTmJBs5kGIiN4mWOQaohqQSd0dygZAgRYFgqjhBrMDADWRlh1mhzbAN86ILx0gk95/mL90AUxCBeJBKIgG1bA46AtQIR5GQJEWDYFMRuWwFPEDAMVaFAuiSRFWo7EAmioQPyZAiRbNgWZKxIUFYJqHdpT6oBXQUos2QGsl4tIWcJdBK+DSHuigRUcPxMWTDzr7oAu+eBVoLf/QzQc9gO5KxOWnD3oJ+gCc8zIwc15wIAAAAABJRU5ErkJggg=="
          />
          <image
            width="21"
            height="34"
            transform="translate(14.64 8.64) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAiCAYAAACwaJKDAAAACXBIWXMAAC4jAAAuIwF4pT92AAABrElEQVRIS7XOB0vDQBTA8aZJM7tHavdSFBcuFAfi3iAu3CguFATF7w/P9+60Vjuu2jPwvzuS3I/nAQAPPrQAnWXkqUUVTQ7MFq/JQUq1lI5hthBEMKU6CmjBzmCO+hWOBTiohRTwhf8OVw+EUHrcC74o3/WE909w9aDHEIlxyHCxpASUIsjo4plpLOUFK6f+Gq57wbAsz8qqYJckoFZeBauAWPGjsgpOz+/ghi9pOqfylb9P6xylaDp/r8YK9GsQHGwfbvqBpiMsMIDgkAahEV/nKEXTERgc5mhotD1Y+APDxnwQHscmJKEEUdFpHSKTuM/oQliIUpEpjrFmJaGfWGwOm9chvtQaFoKfMWwBW9TBXTXkoIQllg1IrBjgrhmQ3GwOC7HaEqscdNcN6Nox5aDuBp8wuSURpZLbBqR2TVb6oDEsRH7GwD0E903IHFlyUIomTB8iemxB9qweFgKNogkzJwhiuQtJKJU9RfQc0UuJKJW/soEq3NrfYOHFlug1gjc2FO8lolThDtEHG8rPThUWXmqn0qMDpSfZKIKVFwe63/wgDaUqr/+A1vYOcO9onkz7TxEAAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="20"
            height="32"
            transform="translate(14.88 8.88) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAgCAYAAAD9oDOIAAAACXBIWXMAAC4jAAAuIwF4pT92AAABA0lEQVRIS7XMC2sCMRAE4BFbEexDrVjbHkc5RdEiohVB7P//W3aSFfW4x0bZBCbksfMBwBENHLlgFeBBUDTtYNkeBUXLBpatQ7At0QohuRyeiXYkWklL/mIE5y89gq/ME/NyP1x8eCPYlWjlYPQMu/Tvg8sfhwQHEg0IRv3H+wke3g7Xf45ioB9EHfx5G6wPfAmKJBzWBxIBkRqifiiNgX4TdcnCYHXgPJjFQMdEJ8xUh1UsN0wQM2t0Jijm9bAKFQoLogtr9Ieoy7IaVpHSEkGsrNFVBNQX14Q35bBarkQJ4tcY9eUt4V0RVou16C4C6oE94UMeVkshcSj+LrBaCM01+g8IekBRaOf0QQAAAABJRU5ErkJggg=="
          />
          <image
            width="18"
            height="31"
            transform="translate(12.96 8.88) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAAfCAYAAADqUJ2JAAAACXBIWXMAAC4jAAAuIwF4pT92AAABfklEQVRIS63MZ0vDUBSH8WbPDtuk0zYd4kC0WFFwgRMFRUVBQVFURH3h+P6v/t6TtBVN2ytJA889WeeXAJBgFx2g+6gFhxBAghId8w9RS4AiSDKESJh/yCkBclKAZAeTnnmLAyHVEaHmRChZwY/uKd5yCKK0ggi/PENclhM884AQROkVEXq5WyUGZNQk+FWDSZjpSf/CQi/MhoRehhdMq8XHBr60ptgyy56WfcSekaNB9qyMfgxJzrHmR2NDP9Bir9SCjHRbiQZRqUWGtFlsppcUZJaHYyMhWk53GNAJkMgQlVlRMLGqILum9mckiKLl7Hq3jRhQblNFbusnZyeMcZFezjYDWO6uFg9y9zS4+90OtOgQlT/UkD8KKp7ovzDu8t8Kxxqo0nlMqHiqo3Smo3wRE6IIKV/qqFwZfYy7NCgC/K5jQtTkjYHJ2zFA1TsD1XsDtUcTsaDag+kj3lNMiPKeTdRfxgDVX0003iw03y3+z7wIaX6MA/q00Pqy8A110q9treOhpQAAAABJRU5ErkJggg=="
          />
          <image
            class="cls-3"
            width="17"
            height="31"
            transform="translate(13.2 8.88) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAAfCAYAAAABZyaKAAAACXBIWXMAAC4jAAAuIwF4pT92AAAA7klEQVRIS63OiwqCQBAF0KEHIhISWESJiCFFT3pR9P//ZbMzkIiuo84Kd1l37xwWiqIAbcxXwMjs5bIdGTMCk+EQL1NGwBsG8eIxQPH7Q+XGZ4AS9IOqPwEDKoQOZoiYhN2h+kHIgAqhwzkiJlE3yH4RMQJLGbJfLBiAlQKhyxUjsG6HWhEqIAAbTGyHZCRmABIFQqWEEUibIRGgUsoAZAqEipkLZIuISV6HxOFKOXeB7BHZKZE/dKhC4lANOTpAaOiE0LmExIFG5ILIVYnQoBPkhq+5MySWrcgLkYcSoeEnQh+Qi63IG4GvEiEIkR/VpZepGLsRnQAAAABJRU5ErkJggg=="
          />
          <image
            width="24"
            height="30"
            transform="translate(11.04 9.12) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAeCAYAAADZ7LXbAAAACXBIWXMAAC4jAAAuIwF4pT92AAABz0lEQVRIS7XP+U8TQRjG8dnd6R6zvVtKD3tziAIKGMEzAjFyxEjCHQlojFwhIf7/P7y+70xLoz2mLbtNvtt2u/t8uowZDBhT4YuFETMdBEwMMYMzoO+6m0aNmS4OUzYimOkxsKJGoJA8WMKAdhwBHjcgkgoOkgcew2EqgeNJBVB21gwEevggx9Nq2M6Y8t3JB4xQcjiHTZrgFlRe2Xo01HVCjhexkkpULRD1x0E9T9K/9yoYAQ2VPz0+1POkqLWGmxZEZ7gEYnM8WITypxQQncWecog95xBfGA8a+KMcf6aA2DwiixwSS5GRIe0F8dZ4/AUCLyOQWA4BoWg8iePJFexVBNJv7JEg7QUUDVOp19iqQjLvh4e0F7RLranx9FsEPoSEUOl3OP5Rlf1kw8SmMxSkveD/aDy7jsCGA7nP2Bc9pB3tFT3BRAuY3AkJofHcFgLb2K4DhW/uQEg72C8az391ZYW9kBCKnoCA4r4LpWOvL6Qd0lU8QODQgycn2PfekHZEV+moBZx6UD4LCaHoCQgon3tQ+Sm6IO3AsFUuBFR+iHARqvpLQO23gPq1/w+kvXGUapeIXCFyEyJC1W99aNz50LzvQNqbxomA5p8O8hcxX8w0ffWOgAAAAABJRU5ErkJggg=="
          />
          <image
            class="cls-3"
            width="24"
            height="30"
            transform="translate(11.04 9.12) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAeCAYAAADZ7LXbAAAACXBIWXMAAC4jAAAuIwF4pT92AAABDklEQVRIS73P62rCQBQE4EEQ661Y8S5tLWJLtEVUpJT2/V/Lzu78kGDiiWs2gQnZZM75CACcXHghVnRzUC0epFtNCOpxoPNDXQgeyofSh4YQNMuF0oemALSYdnnQ5Yu2AHSYx3Kg7JddAT5P90P5H3oEXPoREf+xLwTD+yC7MBCCcThkF0YERkIwDYPMgi9NBGAeEfHFmRA83w6ZhVSZAF4UqxuOOOBVsbrBiB9YEHljlsUhs5A5tBSCVTHILOQOrqpA3ol8MIkNmcuuDidCsL4OmYusYENkExv5JPLFbPMhc0mROMBnnw2ZC4oGuyoQAjgwx0vIHL4lDoiO+IXfhH7SkDkUkmqQX/7N3xn6B9MLI3dtHMkhAAAAAElFTkSuQmCC"
          />
          <image
            width="25"
            height="30"
            transform="translate(8.88 9.12) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAeCAYAAADZ7LXbAAAACXBIWXMAAC4jAAAuIwF4pT92AAABwUlEQVRIS73Q+S8rURTA8Tv7Vqq6M7TKe0FqL7ETYl8iL2/JI4IQIeEH//8Pxzn3Ti3R9rb0muQ7SWfunE9OGWMMeBoDvJiKWBXRLAaGrymBmO4y0G1EbIFYXViivRjTPRzuaWAEGpidEdKtgZ3W2wbxmxlDIC42sJM6B5xcmxGKNngBsjq4eSxsD/Tuh50SgBMBXp8B/oDxZejDA/qb3N4IKGAlA4IfX4NqPvRCHF4UWwSDBsR+mtAxYn4aqvuCbzAktqginWOfgxq+pOGxYQRGEShj4ybEJ62WIekB2qCjLLaIT1gQn7agq9IaJD1AvQBTCMxgswoQigMVUWLOgu5Fu2lIeuBtNDwxj8CCzZHkanOQ9MDb+PAlbFkAShAquSKGp9ZsSG84kNl2pJB0aK1S6whsOpDeQmTHgexeY0g6sF6ZCMjsOpA7ctUgVHYftzgQSP6kPiQd1KjcocuB3LFAev7UhqSDZNHw/C8EfrvQ+89Tg1Ac+OtxpO/8IyQd0Gzhfw/CU4H0X/rvIOnHzRaeRcCFD/1XihCqChSufSjevkLSD1uNgMKNYoQq3vkwcB9A6TEAZQhVevgGhBp8EsgzRm7SlIAFVkQAAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="25"
            height="30"
            transform="translate(8.88 9.36) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAeCAYAAADZ7LXbAAAACXBIWXMAAC4jAAAuIwF4pT92AAABF0lEQVRIS73ND0vCcBDG8QfT0gorVNRG/0VSREUlNXr/b2vd756VyOZum/tN+Ipud/cBgBA16QKhfOAjKBAhaPiB+FUngEupWT50+OGAKyJolQsd/5HjuI66KQ+KP5DjuI26KwdKftgWoE0ED+dDp1/cE9A650HpLzsE0JV6xSF7oEdA6xeDzAEd6hPAQHrMD5kD/4NDAVyBR0SHAyJ4zgeZA7GFJyJ4zQ6ZA4lLL0Twng0yBxKX3ghg5BHRxQ8iGNuQeSx1eUwEn+mQecjKAZj4RgTAVJqdhswjWXKANk+GzANZcwCWvpEFEazikLmcJwdom2PIXMwb1hUgevRLoN0BMheKhC0RfBMyF4qGfQWIHv+pAPnrFzV8GDh7c16YAAAAAElFTkSuQmCC"
          />
          <image
            width="30"
            height="25"
            transform="translate(6 9.36) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAZCAYAAAAmNZ4aAAAACXBIWXMAAC4jAAAuIwF4pT92AAABuklEQVRIS8XPB0vDUBDA8SQvO1Fba9Xa2NoqKm4cOMCNiKg4ceAWEUUF8fvDeXkv1ta2XkeqhX8I6eX9cpIkSfCVrEmg6BLgT2p04sIQVgTMLBlUV244zi98UxNRRwZmI9wkgxbBWhv3AfzCUQSZm4dGZdDbFB51SC3lbnJgsKkeE6jRoYDVzULHix5wMC5AswtLIOwxsNIM7Gx4H1DyoY8aCJpJsS1HMwycPgbugBoKXvYP01MEmI/2q+AOqtA0Uj9ODuRvytEhFZpHVWiZ0OrCyQG/3KbDiI4F8KQGkenacXLgK44i2Dwuto1MCbh1Xq8JJwd+xlEEIzMaRGcFHFusHicHSsXROUQXBBpb0qFtrTqcHChXAboi4PYNo2KcHPit2DKCqzrE1wyO+nVsV4aTA1Tx9QDdRHTLgM4dExJ7JomTB1eaj/rbcnjfhK7D33HywGrq3A3QI+zYBO/MKouTh1Vb4kCgyRMLvHMLuq9K4+RBtZQ8DdDLP4b9vAtEry1I3dqQurOLcPKAekrdIHpvQ/rBhsyTU4CTL4dRzyPCzw5kX79x8qWwyrwg/PYPsF/vuwN9Hy7HPwGZHs2wAPMMlgAAAABJRU5ErkJggg=="
          />
          <image
            class="cls-3"
            width="30"
            height="25"
            transform="translate(6 9.36) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAZCAYAAAAmNZ4aAAAACXBIWXMAAC4jAAAuIwF4pT92AAABEklEQVRIS8XOiU7CQBSF4RM1Ai5IBDdUDGCMQFyjEoLv/1r13M4E0rRwukyR5Cel3DvfIIoi8GNfkT3vKqzgPXawO3z9sO9gHLJG/RdI/vAomqxVL55+4VEcsWN2Ws8Fsl8aeOJrs7Pw+PY/PRp3HhbXAx2Host64XA5EA95FBfskl1Xv4AcSAxfEbVuWL8aLgdSCx7FLbsvj8uBzCVD7xyMQTlcDmxcJIgH36g4LgdUGDoYj8VwOZAnjB2Mp/y4HMiboXHP+XA5UCRDMWFTjcvDioYZYetlOy4PKpOheGVvm3F5SNkMxTv7yMblAVXCJ2HrO43L5arhy8GYJ3G5GCL8/BMcQwviyzUuF0JmMH4d/gfFgSGidrTR9gAAAABJRU5ErkJggg=="
          />
          <image
            width="30"
            height="24"
            transform="translate(4.08 9.84) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAZCAYAAAAmNZ4aAAAACXBIWXMAAC4jAAAuIwF4pT92AAABsUlEQVRIS73P+UsCQRTA8dnZe7c0U7PytqjoTiqifigKiYoO6KIiIigKKuj//+X1Zna9QH3q7iZ8ZXXfzGeGMcZAxhkoOgP8sP+IcRNB1YsbDFRHAW1UifwAjFsI1rMRdhGOKaAnosXll8BaQdm4AkaagznJIzlA40GLe6CR5BKUaIaDNc3Bzquh420/6qgAzSlEc1gW4aIKTiVcvOOfFqJ2TgW7gKBAyyq4CLuz4eFdXzj+Ld0ZDxyZ12SjixrElrXAB+j5UoJzmteCj65gazrEN/RAODkgkuiSd9PYqgbxdR3GqtjW8Dg5UE/eVKAbPrqpQ2Jbh/FdYyicHGgtXm3eVICyPQNS+4Pj5ECnEjt6G5o6MGCiZg6EkwPdEmjSR9NHJkwcm5A57R8nB3ol0UMT0rUmPHluwdSlRR6A3LyfJHqCnfnwlQXTN71xctN+k+iFj15bkL23Ifdod8XJDQdNwrdNOP/cGSc3GqbsHaIP2JMHF14dKL45bQcgNwmSgAsvjoRL7w6UP90GTi4OWv22pQ8M4cq3h5MLw0rctvKF/bgw8+vSC6KKHIgqciCq/gACwNBEV4oxbwAAAABJRU5ErkJggg=="
          />
          <image
            class="cls-3"
            width="30"
            height="23"
            transform="translate(4.08 9.84) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAYCAYAAADtaU2/AAAACXBIWXMAAC4jAAAuIwF4pT92AAABCUlEQVRIS8XOi07CQBSE4cEIBJGgIHgHIyFcDJeIYPT930tm9zQQUmDaskWSnzTt2f0OAPy5+MM5AwoG44Jdnm8B+yOIosEosXL+C2wfygb6KuwqXzz+IkJRZbX88P0vHXptMOrsJvwCxz9GqK8ZFtcDt0QbBqMdDpcDfuiOaMtgPITB5cBm0KH37JE9sefTFpADsQMRihfWyY7Lgb2HHPrKuuwtGy4HDh7sbGH00uNyQIV3g9FPh8uBJDnUN2CjZAvIgaR5dGgwPjQuL0yTR8cGY3ocl5dlCRODMT+My0uyhpnB+GRf8QXkBaeGhcFY7eLyYIiwNBjf7NcWkIdChp9/gneWUAN5tQac18exYIw4+QAAAABJRU5ErkJggg=="
          />
          <image
            width="30"
            height="21"
            transform="translate(2.4 10.32) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAVCAYAAABR915hAAAACXBIWXMAAC4jAAAuIwF4pT92AAABmElEQVRIS73P+UsCURDA8be365rrqut92wGREdEd0X0QRRFFRVQUQUUQ/f+/TLM7u0ppPo814Suyzr7PG8YYAz9RZYAf9h8xFxUZCBKGsBhiIBkCyKYw0kuwJqYjFhEIjQqgWAKotjgy3P2SwghGPNAkVIkTrGVECOWDv0Dzh+yBakIkMI1lCdWLEoQrUqB42wM16W2ZI9QB/Yzx4PCOD120gJuWEawiWCc0MiXD2LQM0YY89AW6/ulvaUwgOkmoD8fmlaFw7oCTj0ZnsFkZzDnFhWOLg+PcAT8XbRBqOuiCAtayAvE1dSCcO/A7B7WWPHRVhfi6CslNbKe/C3AHOmWt0KaJDQS3vBBOHWg949yBbrmbbqtg72pg72sunDkJ9YRzB3jZewSmDjVIHxOcPefj3IN7KX3koaeEOuWuuuPcQ/spc4bgBZW/1qFwq/+Jcw/rt9ylh97oULzHHnQoPYXbLsA9aNAKdy20/ByGyutPnHvAMJUeCS2/IPxmQO3DaOLcl4PI2bb6bkAV4foX4dyXgqz22YK/AVHic2ypT77HAAAAAElFTkSuQmCC"
          />
          <image
            class="cls-3"
            width="30"
            height="21"
            transform="translate(2.4 10.32) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAVCAYAAABR915hAAAACXBIWXMAAC4jAAAuIwF4pT92AAAA/ElEQVRIS72Pi2rCQBREhyKNYlXUorWV+raU0lpEKdL//y6duxuREHUSszFwAklm9kwAYG/wwj2BEz+QikcVQuFvlZMYj6Ra/oDkQ0Rp5MWokXp5A9IvYqHjibTKkV/+0KC06cVok07YATrQjsVd8hxOLgMuFEvRI30yKD5ABhJhk76QVzIsJpeBVMGkb16M0e1yGThbMum7F2NMpvkHyMDVskknZEYW+eQyoLC/xZwsyUd2uQxkwf72KMZnNrkM5MGkjm8tl4flBV9ejBX5vTxAHnQL+DmJsT4vl4cUwaSOTVouy0UxKbbkLymXxVCYGDvy7wfIQkhMehQfAN8mwJ2Xpvp5AAAAAElFTkSuQmCC"
          />
          <image
            width="34"
            height="6"
            transform="translate(35.04 7.2) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAGCAYAAACmekziAAAACXBIWXMAAC4jAAAuIwF4pT92AAABWElEQVQoU7XRS0sCURyG8YFMzSnvppYp02g2mpq3xnSazEspVkgllVJBgUSLLhAtIqhFLVrUIvDzvs2cyRHLTZGLFw5n9eP5UwCo7zN6jBj0P8xRlF4DeZoJLXTWMRicNGSImTHDFrBiMmSHO+aEJ+mGj5/GrDCDwJoP82UGXIX9NzCBjNCj0Fr0KsLkMxGIPWiDM+zoQTJfkHwPEtmeQ7QeRLzBIdUMgz+OIne2CPE88SskpTHqSAm93QDaNa7WsLIWOCRIF+Hlp8BkPWBF70BIcj+ENIFEkD2NYaWdwOpFCoWrJZRvM6je51B7ELD1JA4Eqg/apdSQZ/MriL4a0lm6kGCJIZBwzU8g8T1OgbQWkDmJIisV6UKK1z8h9Zc8dl8LaLyXcfCxgT7IX6aeRYKkDqUaR0oNoR1XalymUbzhsX63TCCbj6ICec5jR4a8ldDsVNDqVPEJu5q6ps6xwWUAAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="34"
            height="5"
            transform="translate(35.04 7.68) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAGCAYAAACmekziAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAwklEQVQoU73OzQsBQRzG8T2ws7Mz+4ISUiIkUhKRt1IiSilFShRFOfj/bz9PzSIlcyBTn8PUTM/XCDmMwj7EGJlxSDJiaYtYBnIWWQUoQZkTr0KNk92Alk2iA30Y2IRjfOPlYiYQkgpCshjPQ1GF8IqK4HVENKGNgO4zRAxtkiOYCHJmMIeFIHcpyV1Lbai29BPRC0LuEWOMT9+ErCR5G9hK8vdwdChygovzCNSO/ZK3C0IOiDirkOhV0X7+F+2Df7kBxGn1XIdANrkAAAAASUVORK5CYII="
          />
          <image
            width="42"
            height="9"
            transform="translate(33.36 7.44) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACoAAAAJCAYAAABE+77DAAAACXBIWXMAAC4jAAAuIwF4pT92AAABwUlEQVQ4T83S6UsCQRgGcDss8yyPNG/T9UhN1/JIrdSstLI7uwiioiCIDiI6vgQFQURR1N/7tDuma1GZhNGHZ2BnhuHH8y4PAI8naESDiA++tBnNcgHYvf8Wgqz7AG1RCSHSiCHRS9BqlkFua4PKoUClx2oZXr2Qj0ZJE5paOSAbsVYMqUFKoAoW6lRA7Vahw6eGPqCBMaSFOar/MzxZWKRA2QKhWsQ1aZIRpNIuR7tLCY2HQXa3Q0drYOjtgCmiI1BrvxFU0gRH2gJXxgrPBFUT/LuPIrI4cgUl55osIoMFJJvOuAFU4g052kmg3kk7uqfs8M85Ech3IbjiRnjN+2v8t4dK5r8stqllRq6j1aU2LTGmzQEj7ClzqU33uI1A/bNO0PMu9CyyUA+BRjd8iG/TGNztQXIviPRRuCp8xQufxRItINmUj92bo7g2F1zoXS602bfuQ2zTj/6dQAk6dBDC8HEEmbMYxi7iyF0OYOo68SW+Iqra+KYdoBlooc0y6Bb9Dpo+DGPkpA/Z8zLoVQIzN0nM3Q4hfz+MpccRrDxnUBPoT5LaD5HRs9DMaZRgOWgK83dp5B8Y6NMoVl+yBPoKEuKOoE8cUX0AAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="42"
            height="9"
            transform="translate(33.36 7.68) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACoAAAAJCAYAAABE+77DAAAACXBIWXMAAC4jAAAuIwF4pT92AAAA9klEQVQ4T83O3UsCQRTG4b3QdnZnZltDkChasCIqRFKJQIyCUqQPIiqKLiQS6QP8/+/eXrchWEpOwYYOPDfDmTM/r2B9TBRLVPbB480jrxi5SBe6UKFlWlXw1yihdQW1oSAt+0/fLtLQFReaZEPVFu0ECGpUp0YA6YO8iAN+1YVu0jbtZkPDFu3TQQjdpg4dhpD2/pU4ME0ausfA5s+h+jiEOaGuhu3TGV1oSHtzD50mDT1yoacapudCz+mSrjSia4Po1mDxnh7o0UDaK36cp6/QG4beZUPjAT1ZxM80sii90JvFTEJ/Ix660Fd6t1gaf8aKD+fFB06211TNg/GWAAAAAElFTkSuQmCC"
          />
          <image
            width="31"
            height="18"
            transform="translate(33.84 3.84) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB8AAAATCAYAAABobNZCAAAACXBIWXMAAC4jAAAuIwF4pT92AAAB1UlEQVRIS73Q2UsCURQGcIssnUzLNXfLNTWXXFKzLCvLVjNDyyAogqCgICKIIHoIKnqIoL/3a+Y6TkTL1cwePpiBe87vnCMCIPqvFB+y2HqaQ/k5h+prnl7QShYuU1i6TmPlZhJrtxkU77Mo8XjlZYHeoNlMn0SRPYtj/jyBOr7MhsMLdzPYfJxF/S21WSNJ7AXAJX0YxuTRGLgB6nj+aoIM8FUdtfFPCW16ECl7Eav6CT5xEKrhx+z2p3HkLpJfoi3h3iUHAgU3ghtuRCs+xHdreIrHMyxO69E07pyxwpMbxsiiHf5VJxkgwuKxHR7fDzaENoXbkkYMp81wTFvhmrWRAQLrLoKHSx5Et31NoQ3jxrAO5pgBtqQJ9ikLXNka7uNPT6v/Fa7xqDA4qoUxPAgLh6dMcGSsAu7N21uCv8UHhvqhdimh9aqhD+pgiuhhTbyfnta00Xz46TP2QW6Wo99Ww3U+DQwh9uxRPSzjhj9DP+FSDYNevYzg3OYqJ7v5iJqcntuc1ug3ET7E8h5ItYyAk7NzuF/TFljAOyRdECtYXM1AZpBBYVEQnFbcakQiFubSPSCBRCWFjD89rfAvQvBORowepRSMrvdfUAGnPWhnqA/amTc08yjH5E9ygAAAAABJRU5ErkJggg=="
          />
          <image
            class="cls-3"
            width="30"
            height="18"
            transform="translate(34.08 3.84) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAATCAYAAACHrr18AAAACXBIWXMAAC4jAAAuIwF4pT92AAABD0lEQVRIS73N3ysEYRjF8blg39+7Zotysa2iSBKtSORClCKRG4SUckH5/++OM7s7s1vbeGbMzF58a2rO834iANE8i78Cuj9BHlat8+qx9M4+AuJPRjj+bghuP3h0ntgLe5vA0xvxkaKFa4dw69C+Zwn8yJ498vbig1L+0sFfEU3hO5eL1QK7Mwt/zlL4phj4b9geW7gTlsIXthRYGrYDA3vIEvh0BEs3lWCza2D2CCbwkR0m3RTpz596S8PsEE7wfcIHphY0F1brGnqDTcHSQ2WbRfsaam0Mb7Lt+tEZWPWI9iawdFyl7KO1otBaVSO43yyawYtdomNYOqiraCEoDOHl+aEZLI2aSBw0lThoql+5eCmB6y6E4gAAAABJRU5ErkJggg=="
          />
          <image
            width="28"
            height="21"
            transform="translate(31.68 3.6) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB0AAAAVCAYAAAC6wOViAAAACXBIWXMAAC4jAAAuIwF4pT92AAABxklEQVRIS73Q2UsCURQG8CGsnMospyltLNPcyi3T1CxN3EIp2omKiuohooeIgiCCiIKIeumhv/dr7h2VFuk6Lj18cJk55/w4hwPAtSIbLxlsvirZfs9j76OA8j9ms9oU7+ax/JDEylMK689pBX7L4msNc0gtyVxGkbuOYfEmjgr6uIBVGa5Wzxz4VxKn00idh5G+iCB3paCF2zkKL90nq4INobFDP+ZOplCG6bYlmNXLHP4zoe1JRPa8+IounIUozOpVjfpXnAisuRDcmkB4x4PIvg+zRwHEjxWY1a8anVi0YbIwDt+yg8IEndn1InrgV4XVjNpTFrjz39HgprItq7cudGzWjPHEKBzpMQp7inaKBtbddYN/ouZpIywxCTYZdZZQEtbAWlL145BXxPDUEEajw3RTcmJX1toU8BcqOAwYcAowllBLVEL5xKxBalJ56C169Fv7ZNQAsqkUNIKcmGzLGqI2lUfvSC8ITFGPCJN/EOaQqelgBeUHutBt6gGByYlFt3xi32BLQIq26zuhFXjwYhdFyYlFl9AykKJtvAYEJqhO0lGY1dRoOE6rQUeflm5LUFZDM0JRja4DnQb+X0CKsgpakU9BbG1ovmlTkAAAAABJRU5ErkJggg=="
          />
          <image
            class="cls-3"
            width="28"
            height="21"
            transform="translate(31.68 3.6) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB0AAAAVCAYAAAC6wOViAAAACXBIWXMAAC4jAAAuIwF4pT92AAABJ0lEQVRIS73P7ysEQRzH8X1gb37v7V08UC4ikq6rQyQlJT+SSCFJiVKk/P/PPr6zu04bd7OmmXvwfjLzmV5NAiCJWefNoPNh0P00+D5zPvIpf9LIXwzyVwIt+v4DBkfb9xrtRwIt+lyHgqPZtUJ2S6BFH/RYLAhqLhTMJYEWvdHI7tygN6pPFPQpgeclaq5UI8wbVYcS+oiy8Nn/MC9UHUjYCvTYD2yMyl0JuUfgfom69q6cA7klILfFCHXtmzTxUgwExLBCd0QQcCIq+gT2S1RshgPHonyVg6/zEh2EBf9E2RIHWylRvhEe/IWyHoGLFbrGo4A1tDXP0Fqo0OV4YB2dq9BeXHCEpl0CLUq/dT0IUZJmDBZNZ6cDFuiMITCfHligrkGMvgBjpppxWC2TRgAAAABJRU5ErkJggg=="
          />
          <image
            width="33"
            height="12"
            transform="translate(30.24 7.68) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAANCAYAAADMvbwhAAAACXBIWXMAAC4jAAAuIwF4pT92AAABoElEQVQ4T83R2UsCURgF8IFyy5jcRsc9JzU1tWwqLXfFzGhHWqiHIqLlIQqih3ooqCgIeij6e08zVxgthIth0sOBy+Xjuz/OZQAw/yEMo+2HnD69CipWA41JB719EKybhcFnAG1Bt8LIgH5WDbVBCx03QBCDjibEEjTBNsbBMWH7UxSjNmpJCwM2fbOJ4SGY/SZwITOsEQvscStcIg9P0gFf2gV/3tN1lHJo/Q6jIDUxKiHCFvAxK5wJHq4pO7wpJ4SMm0BCFR/CCyOIrQS7gmp7afYbSRt8lCMIdwtCyLoRKHoJJFJrQBL1EMStCGb2or9GUQdcooRIOhVIoDiMcFVQ2oivBgmiAYlh9mAcmaME8mdiRyjqwM8Ey80via+NSm2EG4jdKFL7caQPJ5A9niSQ8sUM5q9nsXiboaKoD9MiQ6Z2xkgbc1IbMiR3KqJ0Po3yZZJAajdpLN1lsfpYwMZLqS2K+lCnyZ2IpA0ZUrlKKY0s3+cIZP25hPprGTsf1W8g6uJuhUAe8lh7KmDzrYLt9yp2P2sKhrqgV6EO9CpfCxB8aqCWS5EAAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="33"
            height="10"
            transform="translate(30.24 7.92) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAKCAYAAADRuIyZAAAACXBIWXMAAC4jAAAuIwF4pT92AAAA7UlEQVQ4T8XOW0sCURiF4blI93kcL0ISpKAoRDBIE0RCiECiogilIEIE8fD/f8BqzQw0TEXb0mzgvZjN4uMJdpxAXKEswC/4r4JCKUUk7QoU9wRETULsy63Ccj/FCiHVDCIOJeQxq0uohvpT2JePCeKAgCN2EkNUAlGn7ExBn28e5R28I5opRLdSiO5qmIs0341V8g4+pttEdBRML4PYS3ZlYAcG7tr8CuYd+DL9GJGHuDv2YBAO7coo7+AnuRsCbtk9EY8W4cii9Mxe2Ov3KO/xdQqfMkg0ZhOHaOpQnrOly8G8xzZdNCNi8RnyBvETft4rIlb4AAAAAElFTkSuQmCC"
          />
          <image
            width="34"
            height="7"
            transform="translate(27.12 7.68) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAICAYAAACccC2SAAAACXBIWXMAAC4jAAAuIwF4pT92AAABbUlEQVQ4T73R2UsCURgFcEHNZRy3bMzcUrNS03FfxjU101ZbpMUoKigoqAgiqoeC6sGHHnro7z3NjEbZJoL4cL6Hy73cH+cTABAIpCJwEZEDkGhlIPQElGYlNDY1Bp1aUG4dDD4KppAB1rgR9pQZzmkruLe9iuADIlSwCI0UcooAaSShsqh4COXSQe8ZwohfD3O4BUk3IZMzdrjLDniXxkFXJxCsuRDZnkJ8z9c1kh9CQgyxSgKpjm1jWAHSREL9tQ36sw0bY4IjY4EzP8pDPPNjPCSw7kJow41IvQlhDv1IHweRPQ2jcBFF+Sb5L44f3DpklBwKg6J9JWwbv62Fg7S1sdJsI7zlQWzXC+aA5iEcIn8W4SGlqwQqt0ks3GdQfc5hrVHAD0g3+WstgRYkuuNFYp9G6iiA7AkLOY+ieBnD7DWDubskFh9YyFMOq408ai9FbL6WUH+rdP641+Egy485fD/v+LBf6XihX3kHXZCIhGWNzOIAAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="34"
            height="5"
            transform="translate(27.36 7.92) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAFCAYAAAAg7j5MAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAuElEQVQoU73O3wvBUBjG8V2Ys7PtzIaIkhJRiohIKVJKyQVRKD9KKRf+/7vHkynSaomc+pyrt/f9ahEloHuUFIimBUTOgMg/FAwYJaoYkFUJWaemhNmmrgk+7Vfun55gRIoylBXPiCKVX0Jq1GBEizomrB71aWDCHtHYgprQlGbWR6GhA+8CQ4bBIc7ChrOyEVvTlnY23IMKDAw9/A1nzpglAzZ+iLuno4J7UvDOdFGIX/2w0GX/cgPK55CEHi7lVwAAAABJRU5ErkJggg=="
          />
          <image
            width="34"
            height="14"
            transform="translate(26.88 8.16) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAOCAYAAABKKc6PAAAACXBIWXMAAC4jAAAuIwF4pT92AAABs0lEQVQ4T73R2UsCURgF8CE0d9wtLZs0x7TUXEqt3Fs0sgWLViqCogUignooiOohCIIeov/3NPdOMxQio2UOnGG49+POj3MZRq0AicLQDaVRBbVVA/5hOh2mS6ukABKVRQNdjw46px5G1thREEOa6DaraRNah4Aw9BlgGjTBylngCNo6AqIvgtDYtdD1CgjShsVnhm3YSiHOiAP9Cee/gqQPvUtAmL18EzzCHvhChB1wRXsohE31wZt1Yyg30HZU3QJpggDEJn4gMm4JMjzrQbAy1DZQww3xOkgGki54pvopgiuyFCFCwst+RGuBP4NkB0jYtHAlvgILrjQoIMpehKochcTWAhjfHkVqP/JrkOzA9/hFBN8EQURW+DZ4RHxjRIDshTF5OIbMcQy5s0RLKNmBRiFNEEhsPYjE5giSuyGkDyISJH+eQOkyifmbyaZAsgPNZGInJLUxfRRF7jQuQeau06jcTmHxPoOlx1xDlOxPWk32REAULyYwe5WijSzcTVMEyepzEeuvM3Ug2YP/GhFSfchi+SnPQwqovZQoZuu9LIFkD2p3SCMEsvE2h52PigT5BOUm0IHF2u1jAAAAAElFTkSuQmCC"
          />
          <image
            class="cls-3"
            width="33"
            height="14"
            transform="translate(26.88 8.16) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAOCAYAAABKKc6PAAAACXBIWXMAAC4jAAAuIwF4pT92AAABFUlEQVQ4T8XQX0vDMBSG8V7YJmmSbkVlzL+geKGgbsrGBEVQREFRVEREUGEgQ/3+t2dv00pRqse5bis8hbSHw494U1aQHwnC400yL0H4MZoRFNQmB3IvfxqQ2RQSzAkSC5LEkhwr6sshqAMyD8QiWkarkuTaeECFHx1iJYfIdUVqU40U9OvPT4TcAGQLNdCOorBVPoodSEpuQ22jJhC7qI32QtL7YWkgduB7DtEJU8hBmjkaHsQO/FRyGw5ymELMiSZzpsme63+h2IG/ZI4BOc0h9lJTdD0YiB0YNHsByBUgN+jWUOUePRgWxS4epugugzwaqj5ndW0hil1WVpUnIF6sg1RfLcU99J6j2AWjKn4D5COH9AG5HgzdCsZV6gAAAABJRU5ErkJggg=="
          />
          <image
            width="39"
            height="22"
            transform="translate(26.16 3.36) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAAWCAYAAABDhYU9AAAACXBIWXMAAC4jAAAuIwF4pT92AAACNUlEQVRIS82SaWsTURSGg4R0mbYT0zTbJJOkMWkmncY0+9I2SbO12qrUWq0bKoIgoqLiAqIIfhAERcHf+3rvTdPSRD02nal+eIaZO+e8PJx7LAAs/xM7X1u49r2DGz/W6WKzufChiksfa9j6tIrtzw3sfGFy39rYZYJksxmsvS7j3NslnH+3jI33K+CCXK4r2ESvjgwyivrjHBpP82g9L6L9soS1NxUhuLknx6fX30OGHpflB2lwao+yWH2SR/NZAe0XRXRe7U2P8bteMnxYCrcWULpzFpX7qQE5Pj2q33C5xW0NmasJ5K7rEHJ3D+SqDzNCkMowXE7fjCJ5MYbUVhzpKxqyu/PoTa58rytHZRgup3VmoXUi0DcG5fI3dRRvJ4eSOpZctB5ErBHCXCsMLtgvx6+WyvgbyIJ+wpUAIisqonUm1+zKJdYjWOhd7eW4IWJHklPzPoRKCmaXmFx1UI7vHZVxVMgCX8oNf8aLQNYLtaAwQX9XkE2PX22cyVEZw/LHn+75GXiTLiiLHiGo5nwIFhUhdqYWNE2qxy8Pp6MOOGMOzMxNw6MfCCppD8Jlv5Cjgo1g4EBWp2AP2eGInBZwQVfCiUCOTY7tHRVoJIc+xt0SJnwTjEnIqizk+M7xqXE5Ksxo9l9s9lGMOMYw7pKEHJ8Y3zkuR4WYhXicGrPCOmXbl+M759KcQo4KMBOLZdQKLmezj0DySJCDMpxsalTjSSDkrJM2SGzf+J5RDScJWfAv+QlGnCC27PL+pAAAAABJRU5ErkJggg=="
          />
          <image
            class="cls-3"
            width="39"
            height="21"
            transform="translate(26.16 3.36) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAAVCAYAAADFEfeTAAAACXBIWXMAAC4jAAAuIwF4pT92AAABbUlEQVRIS83QWUvDQBSG4V7YJpnJpAsuKHXBfUFxQUWEiitSEUVUUFwQFRHB/393/CbJ1FhDT/f04gkhOWd4mRQRpXpN/ktR4Vvxg92Se1WUe1eU/wDE5T8Tjss+upR7hheE6bg3RdH/7AHt5t245N26lL2HB3gKAuNm2cPaxbuU5F0h7DqMu4sPimIPbYUqS1JncA4XOk76cdyewQ40wz2W5J7AKaLKv3HcXjV2oBFyT5B7AEeiEsft1MIO1EPuCpIlQJzcR9ihIG6nHuxALWJbkNhBkIkrtSfKYAfiOOsOiQ3YhEggt9codqCas+KQsxqJ23KI22kWO2DYC4haAh23FuB2WsUO2DM22XNg4pY7H2XU/GlN2GRNIWwW5mGxe2FafNQoosZgHEHTgNvjDuqEfx8ywxZlikGYf3OTyYRpf8MGETYUxhWTizIqL+mCRen+MG7EIm6xG/xH2kOYjhvojSgj1acs0rjBJPwAStfpSZM8TjQAAAAASUVORK5CYII="
          />
          <image
            width="36"
            height="9"
            transform="translate(24 6.72) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAKCAYAAADcpvzeAAAACXBIWXMAAC4jAAAuIwF4pT92AAABY0lEQVQ4T73SzUvCYBwHcCGz6Ww5Q5f51maamamYiqlLTacZ2jsWeejQoUNBhw4dgl4OBRFEBP293zY3ZVG2JNnhO3b48fw+fJ/HAMDw38S2Q0jshZFsR5A+jiJ7EkPuNAH+LInSeQrrlxkIV1nUr3PYvCmgdb+GQWdpLlPHn3WDK3gxX/JjocpicSOASCOA5a3hQM07Hq2HInafyjh4qeLwrYbORwO/ghzhaTBLDrjiDDzJGfgys5hb9YDjvQiWv4KGbah5K4N2HsvYf66g/Srg6L3eRX2DkC4rKB8Fe4CGhHLFnXBLoLQCUhoKVURQnUO0GeyDViRQRwble6CL1MDr+Sn9nzFyHCYbAbPDAquImlKjYgpKbInNK1cmyA31QFqL/hr5QxhhnDRhwm6GhSFBeSjQnK2LkSBeqZ3e+yn6R7Z8IMhoFTE0AYuTBM3KEPX70TpklNEc0DuaA3pHc0DvfAID0Yj9MDZtOQAAAABJRU5ErkJggg=="
          />
          <image
            class="cls-3"
            width="35"
            height="6"
            transform="translate(24.24 6.96) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAGCAYAAABJuCfcAAAACXBIWXMAAC4jAAAuIwF4pT92AAAA1klEQVQoU7XQ4YcCQRzG8X3RNjM7M3vbcinuSqlzlHKkSHIc5zhHUiR6k1OOrv//3dOzTC9S2ZRefF4M4/f7zngAvGvpfgA9pPcA5oM+NcyXhv2mHxpphGODcGrwMKM5LQyipUX0a5Fb0x9tLJJ5qQv3VFNBtelNIehQjxGXxkzOxKxczL9FvLWnF4uShChTVULW6VVCNRjScjHdJIaSmEGAtIdc6uCQzQtkiwLiycXUGPLiYvgzacNu5fmhgB8z4pEKlIQ8U0XefflRTMYyJkeMSbt8bzsJ+PfvMAvE5AAAAABJRU5ErkJggg=="
          />
          <image
            width="42"
            height="13"
            transform="translate(24 5.52) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACoAAAANCAYAAADfavzVAAAACXBIWXMAAC4jAAAuIwF4pT92AAAB6UlEQVRIS8XSWU8TYQCFYaJQulC773sLLV3oNrTQUkpbKopiAG3EiEai0RiXGE1MNDEmxmhcrrzw977OfB0LmJIRsHJx5mIyc+bJ+WYMGDurXHnXQMm1D002P7bY/tzm+tcOve9dkd2f6yLKs5pl/yKNhyWaj8u0ns7TeVGl+2qBtdc1Lr+pC+jG+2WB3fokQ790uPFtVUB3fqzxu0PzI8dNYTtFqZdGupWluptj8V6e+oMiy4/KrDyRaD+vsPpShb7tQ68eWPWoXs0Pa2X2Yoz0pTiZ9QT5zST5rRTSzQyV21kW7s5R2yuwdADaenZ4Va3+U0HjjRDTK2GSnSjDoKXeLNKOjFUW3ZMXvV+kf/zSX8NOBQ1KPsJVP7F6kEQzzEwrcgia25hRoerR38mdGHZsqDvjxDvnxl/0ECx7CVf8RGsB/lx1AJVX1eo8SYbetMWs2KdtOFMOBDTnwl9QoVUFGhRQsWo7QqobGwnuSKjJa8IcNGONWLAnZGjSjju9v2ig5CU07yOyGCC2FBo5bih04sIkeocBk6ePVVZ1qKt6sq7BqlqFo0r/oh9n3Kxj0q7HqEIt8qq2uHWwqlbRqCOQ56d06Kx69E4jBpcRk2+q/wtELWgV/K8I6DnjhFjTIEOVRbVeOov8AhoVhf4vZayjAAAAAElFTkSuQmCC"
          />
          <image
            class="cls-3"
            width="42"
            height="13"
            transform="translate(24 5.52) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACoAAAANCAYAAADfavzVAAAACXBIWXMAAC4jAAAuIwF4pT92AAABIElEQVRIS83O7SsEURTH8Xlh5t65c2d2UEqUhzxF1OYxIiWPUZSkkNqUpPz/735+M3dH01hO1lj74lO32zmnrwfA6yfpU4zBZ3qNMfTmZP/iYt2SqwjJtUVyY9G4pTt6sEgfqRUjD31xynvi4d+wRxHiUzqjCwZeUjX03uK7GwVx4CeiPYPcvoE9oMMI9qQdeu5CpRtfEQckZiOE2TIw2wzcrYQedx9WJQ50Eq6ECJsMXKV12myH7rhQab8b4kBBz2voBY1wiZFF6Joj7dZBHFCTGmqakbMuVC8yctmFSrt16vgZjCmocQZOUBY6w8A5Fykd/CufPoIRhWC0FDqlIR3phY+Hnyr4w4wshUrLveQNxAqZIlRa+C95qN/o38DCO2Gsy2oBjW9tAAAAAElFTkSuQmCC"
          />
          <image
            width="30"
            height="13"
            transform="translate(23.28 8.16) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB8AAAANCAYAAABRsLWpAAAACXBIWXMAAC4jAAAuIwF4pT92AAABkUlEQVQ4T8XQy0sCURgF8Amk1BxNzTEb329NTZ3SSlPMshK0KDHpAUUFQRDRokVtatGmINr4957mfpYYVCM9Bw4Mw+X+zhmOU6vAouIHMWTSQGPRQn64vwg3oFER+hqdTQe9Qw+jZwTmgOlXi3C02KimxVrrMHiRh8FpgMlvhBAZxVhcgF2y/UoJTj2qpbUsvJ3vLmawLSEQLqbH4MqK8OYdCJTcP1ak+8J+NQuDLWEzrDEL4Qx2ZsbhnrPDV3AisOBGuOJFvB5EYi34rSLvfhSindViykqwa6az2l90ERxZlvFaAJPrIaSbUWR2Y18qoXiAwZ6cA76iE6FFD+HRqp+WpxoRSK0OPrufQO4ohcKJ1HcRxQO9odUrPoKTG2FaLbUmkN2LE54/TqF4OoXyeQbLl7OKJRTBz5JuRjqrDyYxd5iUV6e7eEXGV69yqN0UPiyhCPSb+Re4dDaNxYss4dXrPOHrdyVs3pfRfFx6U0Tx0q+GrWZ4/bZIeOOhjK2nCnbaq90Cipf8dLbbK/+H9+YZ0zEPKbeuO20AAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="30"
            height="12"
            transform="translate(23.28 8.4) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB8AAAANCAYAAABRsLWpAAAACXBIWXMAAC4jAAAuIwF4pT92AAAA6ElEQVQ4T8WO3QvBUBiHd2HO2ceZDRcoH+HChZISEqWERHFD4kKRCxf8/3evt23Zlo+Dbaye1Vm/necRIoyAaCBJAtEUAXyEX2G+xIQtTxMgWQokjxRo6CGeQzSD8pwtLyFlCrRCQapKoYQ8/EiKXrFJXQK5gbTkwEK4A1Nc88qVDtJD+v5CuAM3chPlbZR2Hbk6QEYKsInycQh38IybeKiAOrbkbIYs3o/gDt6FTVE8t+TaUoXYGtmqL0O4l36DtkLxxpLrO2TPQD+yuxDuRX4x5QdLbpwYxC9OBPfnoDHOf5S74Q7C5AqfsUW6ZgB/5QAAAABJRU5ErkJggg=="
          />
          <image
            width="31"
            height="25"
            transform="translate(23.52 8.16) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB8AAAAZCAYAAADJ9/UkAAAACXBIWXMAAC4jAAAuIwF4pT92AAACJUlEQVRIS72P2W8SURhHebACZRi2YRu2KftSkEXWAqVQSKmtYtrGauMea2NiNHF5cIkmakw0Mb707/059xJIawOXAgPJIbN8c8/5VCrNFQxYMqgh/1SLQqU2a7DEqylXjRqQ+2W7biERslwLjUULrbAMrbWPzsmBd/OKB9A/sqnOwYGTpZzIQe/Sg/fwMEpGmIMmxSKGF5yop9safAZZaoDJL4sDJlijFthigiIB527oprLQEjJToRDpi21xAWLaPveACw+EsHkotCescCRtcKZscOccMs65Box8QYRkWyIk+EoueAsi/DXP3ALGviTbevMifEUXpIqbQuTBhg/hljRzBHOAbEylVQ8VB9a9CDUlRNoriG0FZgpgDgyg0g2JbkzE0Y6fypM3w1MHMAfOEm6tUHG8G0BiO4jVnRCVZ/ZjUwUwB/5nIE7uhpDqRZDeiyJzEMP1u4lLBzAHRnFWnDuMI3+0iuKDJMqPr00cwRwYR5ZsfJig4tLDFJVXnqRRO85OFMAcmITifXnjRylUnqZRfZZB7SSL9Rc5ZgDz4Emh4mNZ/Lwv3niZR/tNaWwA89DLUj/pi1uvC1TeeV8eGcA8bBqarwrYJOK3JXQ/rKH7cQ07X+oXIpgHzULnXZnKb3yuYfdrHb3vjXMBzANmZftTlYpvfWug96OBvV+tYQDz43lB5Ld/NrH/exN3/nawUPmAgz9tKj867bKHleLe6RZ7SEn+AbNTOkqXUMYvAAAAAElFTkSuQmCC"
          />
          <image
            class="cls-3"
            width="31"
            height="25"
            transform="translate(23.52 8.16) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB8AAAAZCAYAAADJ9/UkAAAACXBIWXMAAC4jAAAuIwF4pT92AAABW0lEQVRIS8WOWUvDQBRG+2Azk8lkExVccEWhiqgo7huKiooLFRcQ1BdRH/z/j9c7uVAsTbxpOomFU9L0mzmn1ucLMNQjAfipVUnyZcT1fgHOYLUBrYf6AMqHkOHqAtp+GLEzIkCMSRDjsvSIjhfOKInFBDJVbkDqSzFJYjEtQc6VF5D5h5hB8SzJZUOCu+Baj2AHsuGCnHcTubtkN4AdGBLxIsnVqr0AdtAKWEb5CsnVup0AdvAbtUZitaFAbSnwdlRPEewgNWITxdvIHnJQPIAdZOHtonif5Pq4WAA7+DPgEMVHJNcnHugzr6sIdpAHfUpi/xy5zB/ADvKSiC+QK+QmXwA76IZEfI3cehDcaTaAvbAIQVMn8uBeQ/iUHcFeVJTgAeWPJA+f0wPYS3rFiA3Ra2cAe9gG4QvK33yI3v22APagTYw8/kC+KII9YJv4k+Txt8+Py+Jf5YYfMC928JWCm6MAAAAASUVORK5CYII="
          />
          <image
            width="39"
            height="21"
            transform="translate(21.6 3.84) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAAWCAYAAABDhYU9AAAACXBIWXMAAC4jAAAuIwF4pT92AAACPElEQVRIS8XR2U8TURQG8EZLaTuUpTvdKXS1nXZK981uIEUUikSiROIWNVExakw0JsbEGGJ88cW/9/PeO4UWk3qxzNCHb9JmzjnzyzkaAJpJp3fcwu4POXs/O9j/tY6D311+oxrpfqpg83MVW19quPW1jp3vDfSOm31cmwFpHXeQUmm8yqL1Jo/OuwLWP5Sw8bHMgBS3/a1BgE383cMdepFUnqRRfSah/iKD6y+zaB7l0Hl7Fke3N6qf+4FxkjtIoHCYROlRCpWnEmrPZVzrKI82wa29L44EqYJL70Yh7cWwuh9H7j7BPSC4hyLK/e2xzb3OnQulGC7eXUZiKwRxJwLpTnSAOxRRpLjHMo43R1FcqBVAZG0J8Y0BLk1wmbtxZO9dg3xacSzU2LiligfLdR9CzSHczRCSt8NI9SLyaQmQN+c84RYMx5tdhL/oRrDqJTg/w8VuBAfb2w4rgvovnDNph1tywrNKcAUZt9LwI9yWtxcjON6McfLPl9awGbaoBc6EDa6Ug+F8eRcCZQ+CNS87Le8DF8nIF/OBOVhWFga4NMGRs/oKBFdyq4oaiTN5TKCZ81OcGfaYFfSsi6IdnoyTbY43VKmc+WOwGSE4ZxiObs5MNmeNWOCIy0DeMKVz+kM3Pw291UBwwinO0sfZCY43SI2wxxXjFLSzOkyb9TDaBcy4yFl9s2xzvAFqRqPRa0GjNemgtxgwfFpes9phuKvCFMPRzQkOAbymywp7nMB4xZcdbsEkwy2YZP4Ari8b83/FJhIAAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="38"
            height="21"
            transform="translate(21.84 3.84) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAWCAYAAACsR+4DAAAACXBIWXMAAC4jAAAuIwF4pT92AAABWUlEQVRIS83QS0vDQBSG4SxMm05m0kZFEIqCoCKCCN4Q7xcQEVRERAQVpXhB8P/vjl/Sjp1i7VfbtHHxZJGcc3iJJyLefxC/Gonf4cPI6KfhC4NSftBSeYQnI5UavCDqzYj9Tg9kKbrRUr6FO7hvhD03Y1z0WL+iy1CiK7jW4oaxPXq4V+Y0FHMGF4hK43Qax/YsOvAX+kiJPg5FnyDIhp2HXce46EA3wh0l4T6iDlvD2F4ndKATtYmgLUjC9uAAYfhrbK8bdKBt0GpJ1DrYsG3YzSbIogOu0nJJEmoF1mBDpXFsrxd04DtqEVFLrWFspx90IJgLJFgAJ4ztZOHXD8WZQBI2LPlj7FiW2kdNIWq6ETYL84GwQ1n78aJQRZATxg4MSmvURFEKk1Cth7HlQUof/lhR/HEENcLY0jB4fhlRcTOMLQyLN2LqYWxw2NIwNpQHOpAXOpCXLzhGH1dWOOsCAAAAAElFTkSuQmCC"
          />
          <image
            width="34"
            height="9"
            transform="translate(18.96 6.48) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAJCAYAAABXLP43AAAACXBIWXMAAC4jAAAuIwF4pT92AAABb0lEQVQ4T73Q2UsCURgF8KGsTM3MNXPXnHLFZZoxHdwzK1EkycQWKAqiIAp6iIKIoB566aH+3pPjuCRDDoX1cB4u3Pt9v3sIAMRv4i26EKyQCG0vI7LjBdXwgd4LIHYYRPw4BPY0jORZFOkLCtlLGvlrBlv3LMqPKVSfM6i95lF/K6D5sQFunuhCLhbKCFvMBCdrgTtjw9KaA551JwJlEuEWJFr3gm76ecRRC3EiRGzesSg9JFF5SqP6ku1BGu/F7yFq1xy0pAYGrxbGoB7maAvC8JDFNA/hGvGX3O1WOMgKBznoQFptpM55RO6KQeFmFcXbBIZ9duAg08uhNCuhsqugcauh70Ii87DSC3AkzHClrCBz9gFIuOYBtesDsx8YukwUIpmZxJRaij5kVghheMjXRsSG/yQEIZVgXNGBGPqNaEk1DH5dG2LqNGKPm0e6XADptiE3KgQQsQGjCjEmm2hDpnWyHkTs0V9E9MJ/5RP4CuXNSp5wvgAAAABJRU5ErkJggg=="
          />
          <image
            class="cls-3"
            width="34"
            height="9"
            transform="translate(18.96 6.48) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAJCAYAAABXLP43AAAACXBIWXMAAC4jAAAuIwF4pT92AAAA50lEQVQ4T73Q30vCUBjG8V24nR87Z7nwQlRExIjCoAgxAoMQBEEEQSys6CIK8qL//+7xwW3goHGI2C4+g7GX9/0yD4BXtmhjcPJML/RmUH+nT4v42yKbcS4pou80wgk90KOGmdIshJ3TgpYhohWtDaInBmzpNY34YMQX7SxOf5IY58GMGiqoawV9S2Mevy8Oce36TeEH0ZMQAwl5TpeMuKIbRoxS/COu5X+RewmaAkFLQHQYwRCZhVwkIa5l/3F4+LGA32BEM43oUp8BZ7LU47mQmmXEUUjQFpUdz4X49STCNVg250BV9m9VH5KBU65uAAAAAElFTkSuQmCC"
          />
          <image
            width="40"
            height="27"
            transform="translate(20.16 2.64) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAcCAYAAAATFf3WAAAACXBIWXMAAC4jAAAuIwF4pT92AAACgklEQVRYR82SaU8TURSGiUIppbV032lL943ue6ELLSBCEEFA0Wg0kkg0LjEmxg+aaDREo8bE3/s6d6YtLWAPhU7xw3PTzpz3zZNzZwTAyP/E1rc6tn82sPt7CXt/rtMBsVn7OI/1TxVsfKni1mENW985wR8N7PxaBHtPFojFyvsSGKsf5sAkb37mJL9WsXm4gM45smiQ1F9l0XiTw9LbPJbfFZqC8xC2WMZpGbJ0EJQPUqg+T6P2IoNuySIv2StLll+E4uMYSk/imNtPoHyQbAvWX2exyAlSeYglmL0fQe7BLAqPomCSgmAKlWfcFl9mziTWghzol+ROEOm7YTDJ/ENBsLTf3OLTZF9yGKRgdMOH+FYAye0gUndCyNwTtsgkC5wklf8X5ABFcMWN8KoHs+veLsH0XpiXpPIU5EAvfHUngsuutmBs0484R+J2AKnd0IXlcF5BV3kanpoDTNC/OMNLRtaOJKl8P5ADx3HkrZgp2eDukGSCoRtuXpLK9ws50IktZYI9a4GzaIVr/kgwwAlS2fNCDjBMUQMsCSOsLcGCIOiu2OFdcIgmh7MI6oNaGCN6mGMGXtCRFwSdRZuoYi16vtS4VdD5jwTZFdvSZthzFlDFg+LUh1N2JaYcSqiZoE8DQ0gHM7vmuJEXpEoHyYkHCqsCyulrgqBLBS0T7LhmqnDQnBS0KMBgW9R41NB61dAHOMGwDlSZGHT9kWomIDNMCoLNDTJJds1UkVi0f4wpx9EpqJwWvkOqQGz444psDKMKCcbVUsj0k5g0yXlJKjwMhEM6iqtyCSQqKSa0Msg5QSo4LHg5Br9BJqiTgQoNE+HgBNk3SA1fBuTAZUMOXDZ/ARE1E8sclHznAAAAAElFTkSuQmCC"
          />
          <image
            class="cls-3"
            width="39"
            height="27"
            transform="translate(20.4 2.64) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAAcCAYAAADiHqZbAAAACXBIWXMAAC4jAAAuIwF4pT92AAABnUlEQVRYR83QWUvDQBiF4VzYZp0mFbWKQnHfl4oiUkTFBUVxQ0XEKi6IIPj/7z7PJJlWq+3XJWl68ZQ2PTO8RCMirZtkPwRlPwX1fgl+3AneiyDvDd4Rhjj1nD0YF7fkkPvokPcEzwh7rUQp7CVRc+8QdQ8PUAriam3Zy6KSuXIocw23iAoDuTPspe0S5zZlLkDF3fBRCjtolTi2SZzAGci4S5u4M9XYQbOcA5ucQwSpuNPmoxR20Ch71yJH2rf8OOeo9SiFHTTC3rLI3gYZt2cRt28UO6jH2kRQEVTcTnRhEjuoxVo3ydowg7hitFEKO6hmFkwyVxG2FsTJt8edaRU7+MlcQthKJY7bt4sdKMY8ohbCuEL8YRI7MKYNMmZhDlGLsNyZMKl+2ASipmAmiOMui1rNP/RRg/TxMA5vj7soDv8+1PMIy4dxk8mESX8epEcMkvy4seTCpF8/0jmd0kN6OY47HLfyl1QfogbCuOHkwyT/I5XVyY8bDOK4Q52ipVyEeWFcrnvCJK1HhHH93RUm+XHcKCnsIEnsIEnfYmQHkMBHuAcAAAAASUVORK5CYII="
          />
          <image
            width="28"
            height="16"
            transform="translate(18.48 8.4) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAARCAYAAADOk8xKAAAACXBIWXMAAC4jAAAuIwF4pT92AAABrklEQVQ4T73P20vCYBjH8UFWnjNdpluaWlNn5jzWzFoldrAT2oEoqCgiii6Cii4iqIuILrropr/3196NxOjwZmmD7xjbu+fDwzBGA0gGWwc6nSaYesxQL6ZVMW2WdhjsOkayeKyweq3oDjpaAjMEMbJmmN0WFbPAxttg99k1kA074U24mwprNwKRrex9OuRQcw12wy2y8MR7wEm96Jf5psC1B7KVI9ClgS7BCXeM1SCSL8chkO/DgOKHUAz8Cf7wgo240Dukb8UlVSzrVTEewYIPg5N+hFUwOhtCfEn4FfztRz7lgX+EQ3BM3U7FhGkdE+dCGF4JQ6pGkN4QG4KpB0ihcbJdP6IzQcTKA4gvCxqYWheR3RqCvJf4MUo9UJ+GLQqQKhEk16LIbMaQ245jdDeBsQMJynGGClORz0qu1mE7w8jvSxo4cZTG5GkWpfPRL2HqcFoEKxymMHGsY8WzEZQuZMxd5bFwM/4Bpg78acpJprYdwcrXBQ1cvlPeodRBjTZzKdfApVsFlfsprD4WsfFUQkvA+sh21YfpGrj9Mk//qVltPs/+L/gW9UCzewWBDOZMV7DOQQAAAABJRU5ErkJggg=="
          />
          <image
            class="cls-3"
            width="28"
            height="16"
            transform="translate(18.48 8.4) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAARCAYAAADOk8xKAAAACXBIWXMAAC4jAAAuIwF4pT92AAABBElEQVQ4T73N30vCUBTA8T2Y9/e2ICQEUSTIoCiICgwFCVFENHowhOqhIgj6/19P5+6MMBw7/pgOvoO7nXs+QckJ8OET7KPgIBKQdCSgfLx7OHklWIXAclWAqEkQdbkT/N9hERMNCfIEaxULZ34UzRQ7JVCdK1AXqhA496c8U4T5LhXoa+x2O5gd8KmrFLsh0LQ1mI7eCGYHFtN3GrTH7gm0Paxv1oLZgaxMF0GPPRBoBwbceDWYHeCyQ8RGBLqJgfApH2YXrpqbIvhIYDizEM1tJswuWjePhc8IvmBvyyi7YNOiVwLjdwfxp/uD2YvbFn8g+OXg8Bv7cfyFIts76GMHiu4XypQXCXMjBdIAAAAASUVORK5CYII="
          />
          <image
            width="26"
            height="25"
            transform="translate(19.2 8.16) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAZCAYAAAAv3j5gAAAACXBIWXMAAC4jAAAuIwF4pT92AAAB90lEQVRIS7XP/08ScRzH8fuBKelhcoDAEQQoIh6CRycI8sWAQYhYzpyaa31b6qyt1VprfVtffmhra21t/b2vPu8P4jSJDyCwPe/geN/7sY8kmU1oZbKMgH2kYSSNKmaMKldgkkf4nRK91E8SLTbbWHZqDLJb5ole7DV+GXOOY9wlY8I7AVm18Ls1MDlQjF8sHguoFkApQSumIvaBYadfaPmk/ypsM1aWAnvYxiE17hwIdu6HLaTAcQI4ow6OUN4l96WxCw+cmgMuhrgZ4NEJUeFLqghmvZfC2j706C4O0El8KRX+9DVM530IFf19Yx3/JCDATkKnaUFzlWBfmHCAAzevY7bkR7gc4NB8bbpnTDhAERCpNgFtbQbR9RDim+GeMOFAK63OgEYICxuziN0JQ9+aw41drWtMOHA2OsUiA/S7ERgMMfY0LD+IdYUJB/4tsd1Elu5Fkbq/gPSjOLJPdSEmXNyu5P4J8jCGzGMGHegoHBsdMeHSTmWeLHIkd5hA4ZmB0svUfzHhMlH5owRWnxsovkhyqPom3RYTLuomQsqvmghV/5C9gAmXdFvl9TJuvc1g7f0K6h9z2PhaOIcJF/RS7d0K1j/l0PicHy5ENb7kcfvbKja/F7H9s3yKCV/st60fJQ7t/a5iqBC186vCof0/NfHwoPoLTIjcL8uitI4AAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="26"
            height="25"
            transform="translate(19.2 8.16) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAZCAYAAAAv3j5gAAAACXBIWXMAAC4jAAAuIwF4pT92AAABQElEQVRIS7XPyUrEQBCA4Rw0vWcyCu4biqKoiLu4gYqCgqLCKHgQRMSD4PvfyupuHB0mSWUN/CHpVPdHggHDwIZX0GTBYIuBa7hZzN0sYgtHsPFmwO5DOIrIGDbBgE3z2rGel3ASkSkObAabqxfrW2CzHmHzHPhSfVjiIltAZNFDfFXUgqV+4MuIrAgQa9hGdSzzo8C/EeseElvVMHLAIZsC5Da2Vx4jB2xyB5FdbF+COpKlMHKgix1IkIcIHWMnxTFy4H+/iDqVoM+LYeRAUvoMoQvsSuXGyIFU7FI5yNzkw8iBrMy1h8wtjZGHUVnE3CmI7rMx8qA8RQ8IPWqInnQqRh6St6iD0LOG1ksyRh5QJIu4XvsxcnPRLGKL300PRm4sU/xmHNT+/MPITWWLPzzU/vIYuaFqFhr6NvRgXf0AM88OOSPYmQMAAAAASUVORK5CYII="
          />
          <image
            width="35"
            height="13"
            transform="translate(18.24 7.92) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAANCAYAAADBo8xmAAAACXBIWXMAAC4jAAAuIwF4pT92AAABv0lEQVQ4T8XQ6UsCQRjHcQlLs9pWU8vSPNIyzfXWLO880O77piiKiIoiIijogCJ6UQTR3/vLmS27pO0wevEd2OFh9sMjAiCqqBajipVApq4Bo2XA6BiQ+/9IJJKKQRIzEkiVsiJIbmKhbFdAbVNCw6nR4mlCa6D5z6EUVFnASOTSIog11ENhlkNlbUCjXUVBWq+GggwhLdoirbAkDX+Co0cVK0W1Skarba6D3MiioQRIH2zhQdECKKFHR9qIzqwJtlxb2XBvPgiGboiCFC8gZyO0Pg3FmMI6mGP6Iohg7HkzuOF2uMas8Ex2/gonOEC2Q9K9AxGMNcNvh2BI7gLGO2ODf64LwUXHj2CCA6WyxF9tp98Mx9DTdqZ4TGDBge4lDj0rToTX3N+CCQ58JW6kA+4JK3wzdh6zzKF31UUx0Q0PYps+JLb9X4IJDny3wEIXQstOCoqsE4wX8S0fkjt+9O0Gkdrv/hQm+IPfFClsh0BSezwke9iDzEEIuaNeDJxESsIEHy1XBJM/DlPI0HkMA6dRjF4lP6AEHyp3g2dRDF/EMXKZwPh1H6Zu05i9z+LfQK+bvElh+i6D+Yccnu8eAbQwtiJu/eucAAAAAElFTkSuQmCC"
          />
          <image
            class="cls-3"
            width="35"
            height="13"
            transform="translate(18.24 8.16) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAANCAYAAADBo8xmAAAACXBIWXMAAC4jAAAuIwF4pT92AAABAUlEQVQ4T8XO3ytDYRzH8XPBnp/HzlYrU5IUEa0wolFSFgkXSkPkzpX//+7j8zw72cmm72rnzFPvm6fv99srWUgVQotNhdqyglrVUGsafMl/lERMloNarJ2j1tmGht5i2wZm18wFOfZRW/kF2gwgHUFmj3UM7CE7qgYoDoQCSO8MMbF9grrs2MKeWrgeO7elAMWBSdmDHHRCSAHkLiz85WwwcWCa3NkIE7ty8NcO0l5loGK+T0x/CEpv2C27mx4nDsxSgMTuHZYe2KOHtCMeLaMA+enJoz74GyYeK7P6CzHP7JW9eWTv4zDxSFVlH2kENT5TFP/FxaoLoObXCCUuzLtvzczzL1RAj2EAAAAASUVORK5CYII="
          />
          <image
            width="38"
            height="19"
            transform="translate(16.32 4.08) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAATCAYAAAD8in+wAAAACXBIWXMAAC4jAAAuIwF4pT92AAACE0lEQVRIS8XS2U8aURQGcBKrIEhHURDZESg7AjqsKgKCS7St4tKqaaMxpmni1iWa+GIaY7okJj74936dcwlgY+gNyuDDRyaZew+/fGcUABTPkeXLabz+kcObqxlUfhWw9mcWGzclvLstg95zB7Qj+UMRheMEZr8kUf6exvx5BosXkyAcwVZ/FlD5XWQ4gm3fLfCHPjbp3TFMHsQw/SmO3OcJFE8k2NfUA1ittZXrPO7f5/5BKxnfDEDcDiH1MYLMXvQfWEGCFU+TKH1LYe4sg//NaRss8taLaMUHBtsKIfkhDGosux9jOILlj0Qupi0wX3kU/jkXgotuhJc9DBbfCGDifRCJnTBrLbsfbQnzZJg7Z4e35ATharDYmp+FWiMYbwYv3AP340iZMTplhSffgIWWPAxG64yt+58MaglmjhthS5jgSFskmK0Oo9A6Ccab0Wq4B0YiBpjGhmEVTbBLjTmzVrhyDRzv/mPT9MWgRwe9bxDDQT0IR60RzJGxwEWtzdhlQzWF9TsE6FwDGPJKsMBQHWaTWnNmLbKCmsK0Fi0EuwRzD7DGjKFGY1ZxpCOoB7Begxp1GDX2SgeDv9oYb1C7U3/o6VdBbdBAa9aCrdLdgPGGyBH206XpRregRK9ezWCCTUDtO+MNkCsKheoFajBqTGPsY+vkXZQ7VVhfD5Q6FWuMYLxLnUh1jS+VDMY73Mn8BYu9jRPTwDUbAAAAAElFTkSuQmCC"
          />
          <image
            class="cls-3"
            width="38"
            height="18"
            transform="translate(16.32 4.32) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAASCAYAAAA31qwVAAAACXBIWXMAAC4jAAAuIwF4pT92AAABM0lEQVRIS83O7ysEURTG8XnB3jv33tkRIRF5QUKLhNKKlPyWhCTi//8nHs/d2bvNZnbPtmZ2vfjWNPec0ycCEI2y9M0hfWefDlNf7DtB0Zx46K8lDxb1J/bsUH8lKMA+MlivPfHwMLkri+SG3THCkkePI+ylN6RSmDs3cBcW7pKYa3bL7u3AmHzigJRtGtgTdkZUDibtSYkD/TJHBua4DTvNYNLOoIkDhaD9GOaABVizPFBIHMgXN2LEuwR52GEGk3aGTRzw6c0YeouobbbD9uLKQCFxQK9p6A3dBZN2yqjvo1olKAeTjpVZMWhZQ62wAFvXkA6V3a8ftSUNX4BJB6qqGzWvUFtQHZi0XGWdj8kZgubasMXxonwZKlVowWYVpIVRFU0kRHnY9P9B+VowaWgc/QDs3GtJYbofKAAAAABJRU5ErkJggg=="
          />
          <image
            width="39"
            height="22"
            transform="translate(14.88 3.36) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAWCAYAAACyjt6wAAAACXBIWXMAAC4jAAAuIwF4pT92AAACLElEQVRIS8XR2W8SURQG8NFYC7RYllKWoeyUKYVSilCgLZTVkraGhiZN3I2J0Zi4RRON0RijMeqLL/69n3PvMGMnNhyLM/Thm8Bw78cv5wgAhPPIwccarn+qo/95B4dfGxh8a+LoRxvHP7u4+asH9RxZZFRaLzbQfV3B7psqeu82sf9hGwzZ/8KATQy+t2RgiyNP3iOLx83Wwzy2H62j9riA5rMS2i/1wL33CpBNcVQP+UdnTelWFuW7q6g+WANDMmDjaZEDO6/KHNl7uzkSZTgwf7SMwnEaxRsZbNzOonLvD5BNsf7kKprPS/+MMgyY2U8ie5BE7jCFk0g2wcr9nIakegwHSp0Ylq/FsbKXOAW4Ar7mO6v/BRsbmKiHsNSKQOpEOZID+ymsDSQNSXWcJeQBNZGKiNjWIuK1EJKNMEfqpigjqY5xQh5gCRb8CJUCiFSCHJnYUYBSN4b0btwUmJqRP/qyCwjkvBDXfVgsMqCoA6baUVNxI4EeyY2F9Dx8GQ/EPAP6ES6LiFaVKVLFRuXUl664E+6kCwzJgfIEWULDKVKlRuavF47wHJwxhw6orpkqMyO6L7MBO+yiHQypAr3DNVNFZkX7YPXYMOObAUMyIFvz/JKCpErMDH9cdlow7bJqwLmQvOaosmaqwOzwx9SVaTAkB/pnYQ8qU6QuTyLCBeslqEC+Zq8yReripCIIliHQYYHFbYVNBlKXJhkOvGib0qZIXZh0yAPnnd8+G1NGH+fhOwAAAABJRU5ErkJggg=="
          />
          <image
            class="cls-3"
            width="39"
            height="21"
            transform="translate(14.88 3.36) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAVCAYAAAA0GqweAAAACXBIWXMAAC4jAAAuIwF4pT92AAABaElEQVRIS83QW0vDQBCG4VyYZnPYJC2KCEVFxRNa0aJgQQQRTyiKiCgiRRAREf//3fhtN1ttrRl7SOLFE5pkZvsSi4isosUvksqv8Abvkiofksw7dnnUovuAogd4DChuwpOk+BmSyO559sBhySufwmu4CSi8RdjdV2CEQG6f/YNByVOf5DlcdgZye93YgX4Ehx4FRz4FJwhTgRc6kNtLww78hb+PsAMwgccIOxsuzGAH0vgNj/w9MIFJJLfXD3agF2/bJW8HdpPAJJLbGwQ70M2rI6z+LbCRTZjBDhhuzSV3AzaTQHxFbmcU2AFFrCJsDWo60N3KJ05hB8SSILEidOB6fmFG6ktnHnGLOlB9Re6wLPz6wpkV1A5cFsQdlJWeD51pxM3AHOIWiotTfjwoVRGWBHLLeei4KU06VJqC6v+IU9o/7HGETehAbilPrYtddsiu6EBuIW+tix3qQG64CNaYdEjhBovyCen7Kb7erZV4AAAAAElFTkSuQmCC"
          />
          <image
            width="47"
            height="12"
            transform="translate(13.2 6.24) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAANCAYAAADv5u30AAAACXBIWXMAAC4jAAAuIwF4pT92AAAB4klEQVRIS82SaUsbURSGFRWzaPbMaJLJ6phoEhMTE7Ob3WjcN4KioohiaakilNKiFFoKpR9Esb/37TiTTJIhcBVc8uEZ5g5nzj0P7+kB0PNaBDY8mN6eQLjmRWTPh9jhFOJHAaTPQsich5H9OIP85ygKl7MoXcVQ+ZrAwrckqtdpLP/IYO1XFuu/c9j4k8fW3wJqd2XsPlTQegdxiKfA5u1wFx3wlJ2YrLjgq7Lwr4wjyAmEWgRmD/y8QPIkyEvMfQijcBFFkRu+/CXOCyx+T6F6Iwis/pxrEShi57aE2v089v4tiBLE4TrBREZhi5nhSFrgyljB5mxPFkgcB5A6nRYFcp8ibRKku6UQCxrQkwaM+CmYgjQs4RFYoybY4xY40wzYrERgqS6wKRHY9z97QBLEAo1DAz2rAzXRFDCHWgRSDMbqAu6Soy0BUu+XoOPHYfMwVIwKGrsaOpcWhnE9KEkCTIQTSFi4NWLqa2R/k4GltB3kBgWUtBJDJkFAbWsI6GD06EF7jZIVMoN0wWsjvgyoBjGolUFBCQKPKYgJuJsJjAZokJq+JcJD1o+GgJxSiAJiApwAqdF7wQ/fWxeQ6eWQG5trpLaqQWrw3vACfcoBIQGdjBcg/dRNEAu6HWJBt/Mf9NlZYBlSCmgAAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="47"
            height="12"
            transform="translate(13.2 6.24) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAANCAYAAADv5u30AAAACXBIWXMAAC4jAAAuIwF4pT92AAABM0lEQVRIS9XR6ysEURzG8X1h55w5M2ftbiRySa4RUuSWklBC5JYWSXJL+f/fPZ65mYnZfojd8eKzbbtnzz7f3RKAUivZfQ/2gA49VI7p1EflzEfnBTXokq59VG/o1qJ6Rw8WtSeLvPvEL/wJb90gtGHgb9IW7XD0Lu2lAfaIASdRQOU8jrjKCbhnwCM904tF/TWNEcd8hVlwYRZpiaNXae1DwLYXSgKk+75DPJDHnXbhztAczXN4ErBsYFbSAOme3yAeyNJjGnqSmgRIn/8L4gE1pKFGODoYPx4HTMUBs+0ZndX0DaefwwcoCRiNIyY0pEtb6dMLTreC00N9CmpQR4aLNTrr/Um5rlDu4vAgoDcKCP4F6YJ2Cx86LMcnAfz1pQ8VSTg+DKj9r+EJ8UDRiQeK7g0Ro5nXa3T7ogAAAABJRU5ErkJggg=="
          />
          <image
            width="35"
            height="21"
            transform="translate(14.16 8.4) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAVCAYAAAAuJkyQAAAACXBIWXMAAC4jAAAuIwF4pT92AAACG0lEQVRIS8XR/08ScRzH8VtFfBH5evJFEDiEQ74dokgcIMouwDS/NJ2Wa2Wr+Utrq61aW6tVq7XWD7XW//vqPm92zZn2ET2E7cm4uzefPfY+QbBdg5HFZYX+EUaZwCBXHBbK4rbC6rPDERwD74/DSrjqvE6bYRCr1wab6MBYyAln2Al33H3pMPpiGJtohz3gIAhrPDIOT8IDr+SBKPsvDfb3B3tNBsQVc+kYN2F8KS/EjB+BnIiwEhg67J8b7DUZEH/aRxADw4pWwkNFnXiTtiLrmKyIUGECoWIAkXKIMKyEGkGyOTUU2H8fBvMTtJXJ2SBBYtVJxGsRSI0pTLdiyGiS6SjuACs638ck1CikepQw6eU4ZC2BmY6E/FrKNBh34GjGZtLtODI3JcJke0kCKZuyKSjuwPEYhjbTTSJ3axr51RQK62kCze3mUNnPXwjGHTgthmEVN2SUtjIo72Qxv5dD9X4B6kHp3CjuAC+FYbZnCNMHFVF7qKD+eBaLh3MDw7gDZ41hFvYLqD1QoD4qofGkD1p6WhkIxR0YNPVAQUPfTvOwTBiW9uIGOi9rZ4JxB86bAWo/WyBQ95WKlTd1Lop78EUizPMqbaf3WsXq2yZuv1/E+ofWqTDuoWbEtmOAGIa19aV9Iop7mJmtvdO387GFjU9LBNr+pmH3RwcjAxltfl7Gna990M53DXd/9TBS0NH2fnZx7/cKjOs/w335g/dzWQsAAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="35"
            height="21"
            transform="translate(14.16 8.4) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAVCAYAAAAuJkyQAAAACXBIWXMAAC4jAAAuIwF4pT92AAABTklEQVRIS83OWUvDQBiF4VyYZCaZSRMUrIgoIkVxQVzBFXdEcUP0RhEFEdH/f/t5MtNYpbVfl6S18BaSnBkeZ0D7lIaf8x9y3MiC3BgN9h9m/twhYKp5w2ikf7BfD14ZmLIF+WOC/HFB3AV5V/fCGxUWkzaBJgWJqd7B/vyQYTKQmO4Nih2ICjAVC5JzkuS8JO5MN7GDNDEDyKz8BslFScFyMTB28DO5YDFyCaAVtJY/ih00ymBW0XpA4QbaCog702rsoFkGs4l2bGqvexg74Aq3gdlFwKgDdBQSd6ZQUJbaB+YQHYekTkPSZ53B2EG7qRNgqiB92T6KHXSSPgfmwoKiK3SriDtTKCgrugbmRhlQ6b41FDvIo+jOgkoPtmZb9rI8M6BHRfETetHUaMNekncG86wNKHmrR7EXFFX8akHJO/qowdiDRZdiks8a6Auy8DUvllcqjgAAAABJRU5ErkJggg=="
          />
          <image
            width="31"
            height="24"
            transform="translate(11.76 8.4) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB8AAAAZCAYAAADJ9/UkAAAACXBIWXMAAC4jAAAuIwF4pT92AAACM0lEQVRIS72Q6U8TQRiHGwK03dL7oNe23ZYeCy1l6/aApaXlKFRFIWBAxKiJwcTjg1ETj5hoojHGD8YP/L0/Z6ZKxNAObXf58Jvd2X3nfZ53TABMJss4aMaECdD9VYUtY1YCJhm3T2LSZYHFa70SCbaYPVZMOM0EbIbFJ8AWnIIj6jBcgC10UiEgwOoXyNOGqRCBiw64Ei64JZdhEmcvtqCNTWyP2LvguJOB3UkX/FmvIQLnNvSqnX+gnpQbvowHAdkHf86LUDGgu8CFHymYTkvBwbyfJaJMQyyHdRXo+YOBCwEy8TQipSBLrBKGpIm6CfT9GaZgJQhRDbGp49UIpKUoUo0Y0q3EyBLcAgqNVcNIEKikRZFcFgk4jlw7CXkzNZIAt+BvKHRmJY70agLZdYnBZ6/PoHArM7QAt+Df0KumYHmTgDsp5G+mUdzNQtmXhxLgFvwfBiYTU/D87UwXfkdG+Sg/sAC3oFfodTPwXg7q4RwqxwUsPiwOJMAt6JcFAr52MAv17hyq9wn8URH1k9KlBbgFvFBw5TiP2oN5BtceK6g/KaH5XOVKcJtfNl3wApZPFDSfqQy+8arWV4DbdJBQ8MrTLnjtZRXtN4vovNN6CnAbDpPWizLWydQM/l7DjY/1CwW4jYbNxusatt4uMfj2pwa2Pzew+611ToLbZNR0PmgMvPOlyeD7P9bOBLiH9crO1yb2vq8y+MHPNo5Ot/iH9A6FH/5q495ph19sZLgFRuY3P3dADwKUtu0AAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="30"
            height="23"
            transform="translate(12 8.64) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAYCAYAAADtaU2/AAAACXBIWXMAAC4jAAAuIwF4pT92AAABS0lEQVRIS73O20vDMBTH8T64JWmbrp2CeEFFUVFUVFS8oSIoCIqKoqjIGHh98f9/O57kSEFXd9ou3eALW/drPvEGtIBaExsUgB+vX3m15AceElAf7h+efjGobVSAGJeVX+DXj/oIwmPSwmKqWrzjgZhAdJJgMSNBzlZzgcyHYppQC89jC8o53vVPOWdQCWpJgVp2i7MDuUioWsHW3OHswGTRVWxdgb/pBmcHKW7QDWwL2/Yh2PV7ugA7+JtB/R2Cg4PyODvIKthDdJ/g8Lgczg7+xQ8RPiI4PAkK4+yAy6DhaQD6rBjODvJkUNs5dpnvAuwgbxa9IDi6CVmcPbBI+grRa+w2hOi+O84eVqbojuDGI/acfQH2kLI1HhB9IjhudeLsAb1m0LitIX7RkLzp9ALsi66KXxF+xz4JZ19wWfJBcPNL8+OqYgdV9Q3xctXG0PEtXwAAAABJRU5ErkJggg=="
          />
          <image
            width="34"
            height="21"
            transform="translate(7.92 9.12) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAVCAYAAAAjODzXAAAACXBIWXMAAC4jAAAuIwF4pT92AAACCklEQVRIS8XQ+U/TcBjH8cbsYje7u3Vsc7Kj7GBldQcwmIMhOlQEUTQe0ag/eCQmHokxGn5QE01MjH/wx36fRpw6+I5RZpN307RPvn3lEU45zBBsJsrsscLitUG7hHEnmN1WmFwWmJwWglh9E5gI2seOEdjPWTa/BgjYCWEPOeAUnfAkPGMD0c0W0LfgiOgAV8wFd9yNyZQX/mnfWDD7DwSIagBJB3iTHvjSkwQJyQFECsETBf3zoh8QyPgQzPkJEi2HEa+KJ4YZ+JIQWQ2Q1wFiKYRYJYK4KiJRiyHZlAwHHfghPBMkRHQ2DEmJQNK2MXU2ikQ9hpQGObOcMBTDHZDmxP1NMAAr3ZoiSK572jAMd+BXycZvQKaTRHYlRRB5PY3S5cyxQdyB/hiiH8Aq9KYJUtnOY+66PDKIOzAo+byOKG7oiPJmFsq1PNSbBdTvlkbCcAcOizaxlYOyI6O6OwP1lg5p3p/F4iPlSCDuwDAxCEPUbhfRuFfG/AMN8ljB8tPq0BjuwLDV7hRpGwyx8LBCCNbKixq6rxpcEPcHR40h2DaWnlTRfqbqkNcNrL+dR+/94oEg7sGj1n6uoqMhVl/WCXLh3QIuapBLe0sDMdwDjWjtTZMgvQ8tgmx+amPrS+cPEPcQI9vYa+HKxzaufj6H7a8d7Hxbxe6PNYwd0h+D3Pjepf4r5O9+AgFAgrP7iBthAAAAAElFTkSuQmCC"
          />
          <image
            class="cls-3"
            width="34"
            height="21"
            transform="translate(7.92 9.12) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAVCAYAAAAjODzXAAAACXBIWXMAAC4jAAAuIwF4pT92AAABN0lEQVRIS83Q/ysEQRzG8f1ld2dmZ2731BEhIZFcJImEfEuISJHkRIr//w8Yz8y2LW7bz97e7p6t99bNPTe9OseNfJ2ExxlVjjuWQtzO6DD25Y372mQg3gSaah7064M3GSNM/gxrFNN34E0zi/Bn0RxabAaUeWgB82iBaQYIW64fk/ulRSzFEL7KNV/jtYHIAVvhmhmEqYs26sGQgySLWI8hYgtti0pB5OBvYjOFiB2hg71qQOQgE5MgdtE+OhgeQw7yMv+GhRwKLY/RaVAaRA6KJI+AOEFngZbn5TDkoGgJQl2gK3QzGIgcDJq6BOI6hrRu0b0sBCIHZbOIO2kh4SN6ygeRFw5b6yGFhM9SR71sEHlRVRlE+ALIK3pXfRjygqqL3pSFRB9Ktz9TEPnDOmt//RPIz74BxF2/bRoul70AAAAASUVORK5CYII="
          />
          <image
            width="32"
            height="11"
            transform="translate(7.2 8.88) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAALCAYAAADx0+Q/AAAACXBIWXMAAC4jAAAuIwF4pT92AAABbUlEQVQ4T8XQy0sCQRwH8CUM02R9FIq14qNM23y7aa2vtUQzNDIsiaIgyktRdCooog51qUMd+nu/7azsGkpMmdThOwwzw/w+fBkADDOmAwnZ/0c0wIhBh1GzHnqbASanCSzH/hmKUYazeg1gtBs7CBcLq9cCOz+JqZhDCe2zQaMsBGBQh8sNWDxmBTDht2kITnDCszwNb4bDbME9VJC2GZcBKuIzwBmx9yBc8BfdCFZ8CNX8CG/M/RrUd2D1dQGO3hZEuQWpA1AR0UYAie15CLsLSB+EBwJRH3DJDsAjA0gDgZIXfHVGa4EgCEBFZE/ikM4EOYvfBlEf9CZY9mmI2FYQiR1eA4hHUeTaCRROBaxcpFC6TGPtWqRiqENpSbZ4pPZDWDqMIHMcR7YdVxAEULkSsX6TQe0uh/pD/ksMdchPI5EWzlNdxG1WATSeimg+r6L1Wu7DUD8dRur3eWw+Smi+lNB6K2PvvapEvf8AYNiUK39JZYcAAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="32"
            height="10"
            transform="translate(7.2 9.12) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAKCAYAAAA6jzeaAAAACXBIWXMAAC4jAAAuIwF4pT92AAAA60lEQVQ4T73QS2sCMRSG4dmMSSaJjjcoatGFC21dCN5ASqEggkILRREFURDb7vz/u+M3M0HwgrHVGnghhEPyEMf1GbkpU4YRlnPvHDeJx9OmLKNYDhU4sUd+N9BuE3swgDwLAazIiZejxLP4V9DJwwDASgZRQU+CRA3VBXkN1L4tyjrAqweAZoSQXY/kK3rzrgZZBw7zWkB08PhLhFBAqB7qS9ID+SeQdcBW8BMBIEQM0buk+CcaKYpP1EUo68Bv0x8GMVaUmKI5WpzHWC+9psQsQvhLtNLkr9G3PgJZL7pl/pem5I+m1GYfsgWYimc5q2EuSAAAAABJRU5ErkJggg=="
          />
          <image
            width="36"
            height="23"
            transform="translate(9.6 3.6) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAYCAYAAACSuF9OAAAACXBIWXMAAC4jAAAuIwF4pT92AAACK0lEQVRIS8XS6W8SURQFcGJapNChFKhDoVO2Ah2gspRlCoW2FGtJq3ax1mhc4hI1MTFqTDRGY2JiNNH4Jx/fe9MZNaK3pcP44RCWe29+OYMDgMPObL9v4dKHNq58XMHupzXsf+ng4GsXh98vgP9OHrAiG6+WwLP5uoGtdz9BOwy095mBvnVhzJLHBk37ySJWn1bQeVZD90XdBPXeNmG01G+PPHySaHfPo3G/iOWHJbQelcFRAvRSg9FS702zL8RSUOV6DtWbedRvLUC7w1D3CgJltLT+vP5PhGWgwk4Gxb15lA9UcFSNobTbOqj5oIj24/KxIacCZXtJ5LbmsHA5jcJuBqWrKhYPs6jeOGqJPTrqhmWg9HoM6sUE1M0k8tspgSrts5au6SBqnwo5YCSxrCDZnkVqLQoDZYDEo2Mo6sZxQg7wzNbCiC3NgKPmVnTQ/EbCRFH7Jwk5ECmFoFTDiGoRHbTKQJ2YQFG7g+SvP8i5KYTyUwgXZSiVaUTrEcQbrKWWjqIOD5q+XwbTfpxTgwI0XZDxa0vxpjI0TF+QL+6DPzmJYDpgojiIh/+XqIOnzW8fpBkJE9EJHZQJQM7qIB7qkFUx33hkDzwhD7yKV6D4YxOonH2YP0Es4+Fxs6VAym8rxgQ5J10YC7qPQJLZErU8jIiXUe9ZOH0uuM2WpP+CESCHawQCxFpyBcYEiFoaZgRoRHKaKGph2BGgM+5RgaKG7Qg5YHfIAbvzA5z3Lw7hM2JXAAAAAElFTkSuQmCC"
          />
          <image
            class="cls-3"
            width="36"
            height="23"
            transform="translate(9.6 3.6) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAYCAYAAACSuF9OAAAACXBIWXMAAC4jAAAuIwF4pT92AAABVklEQVRIS83Q7SsFQRTH8X1h7cPM7F5CIok8J5THdCUpJaKUPJUbkfz/f8DPmZ0dS4xzsbO8+Na2e87p0wYAgr+oda9Q9KjQ90Q9K+j37GJd5VcS+TV1I9HqUHcl6MFAbOyh35adSWTnBLmsQPmthGuePfiT1LGAOqFOBSwou3AjvIHkgYA8JMhRBeJ2vIDEXgq5TxFIlSBuxwtItFOIXeoNiNvhYgdcpVsE2aZ2DIqb7zZ24ANkLUG6QVlQuz7Mt0HJCkFWS9BmvRAbO1BAFhMkS5QFrSdeMF2B4rkY8QJhNGrZH8T2NWaaMLMViDtWR84P0SRBLGg+bgQDFygaj1GApqiZ5jD4DNQ7ShgNmjAo7kDdvccMRyhAYwbFLfvo9SEcJMwQNWJQ3KKvKtBACaK/xC35zGDyCGG/AXELvgt6VASdBnHDTVSAwux/YHTsQNOxA033AlBrc36rkKsoAAAAAElFTkSuQmCC"
          />
          <image
            width="37"
            height="13"
            transform="translate(8.4 6.24) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACUAAAANCAYAAAAuYadYAAAACXBIWXMAAC4jAAAuIwF4pT92AAABh0lEQVQ4T8XSy0sCURgF8AmkLEdKM9PGZ6am+UhNbXzkIzULE0nLQIIiatGiaFEEQUTQIoJa9feedGx8zChXI3NxFjPc+c6P7w4FgPqPZG7DyN2xyD9EsPcYw/7zFoovCRy8pSE8Sxw2THwVB4I1F0L1sKdeRM/XEb/0I3G1gfRNCA0Yjyo8xTlY6TWFch1Wec/g6COL2tcuuahf7Bkz7FkznHkL3EUbPCU7/IcOBI7XOJgQlboOcqide1aASrZQ1c8cGrOJ5Z0xbjIwR3RYjhtg2zaJUL7yKgJVZxN15kXswidCkToGRmk9augCGhhCSzCxTH9U/fp4VPjEPRBgKJTSooDKpoTaoYLGrQbj60ZZ022Uq2DlYKSyQdPzpVwnx5xpFvMrTRS/KWOYaaJieliTRm5bpILfpOtBqpqBTENDhHJ3XF/jv4rqR4IRoaYUUkwvCFDW9qYYv2akEBFqQipBT9TPpkhD/joUVQdRnahFWQtF+nhU4VASerKForX02DB8iAfGkW8z4tQ4tZfznAAAAABJRU5ErkJggg=="
          />
          <image
            class="cls-3"
            width="37"
            height="12"
            transform="translate(8.4 6.48) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACUAAAANCAYAAAAuYadYAAAACXBIWXMAAC4jAAAuIwF4pT92AAABCklEQVQ4T8XQbSsEURjG8XnBnDlPY5NYeYzIpiWxEVHyUB6KEklJKd//I1yus3NWG6N7XuzsvPjVeXGfc/7dCYCkLv7ewj9Y5I/05JC/OEy90hu9O7Q+6NOj9eUxfE98uCp7ZmDPDdwlXTPmln5HPf+NKntL/Ow/pqdhjhhyTKfEKHdBV7aIuqE7W/qpRBwYprsaeof2GHQQo04ihkn3qxIHgmwzQ9ah7SLK7EeHowupHKVWM6h1xmzQVoza1bWEiFHpImOWaC0G9TdVf0xpVDqrkLZpgUErFDclPTJqP4fJGYUgnQtRCmp5/DEDRVDOoGnGhE3Nq8ZiBpIJrxCELUnD4yIONEEcaMI38lsVS/XoaKcAAAAASUVORK5CYII="
          />
          <image
            width="37"
            height="27"
            transform="translate(6.24 2.88) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAbCAYAAAAQ2f3dAAAACXBIWXMAAC4jAAAuIwF4pT92AAACaklEQVRYR82SaW8SYRSFUVtKgbIXKVCQvUCxBdnKWrpCqbVaNcVqTFyjxmg0GpfUGKNG4yej/b3HeeftTCxaL9IZ9MNDmOGemyfnogGg+R/ofGtiZ68F6ZkMqMXmhwY2PzZw4dMCLn5ZxPbXVXS+NyH9Ti5QirXXFbR3K1h/U8XZtzWcez8vi219XkD3PLnwqCw9KWDl2RyaL0povSrLYhvv6qLcYTlycb/U72fReJjD4uM8lp8WudzLsijX3q0eKqSaWOV2GtW7GdTunZHFpNZWn8+RQoqLFa6lULx+GqUbM2ByTKz+gLe28Cjfs5CiYtlOErkr08hfTYli5VtCa3d4a1RWFbGZ8zGkt6aQuZxAdjspiomt3ZztW+jIYtPrEaQ2ogfEcju8NSrbC+RAN1MrQcSbISTXwmByrLVZJncpLspR+V4hB34m0vAjuhhAbDkgi7HWmBiV/VvIAYlAeRKhmg+SXKIltNbmclS2H8gBhi/vRrcYOymTo7L9Qg54Mi5M5tzwFz2iXLi+f86lgGpSpJgn7QKDNSaKlbwIVX0Iz/tVlcKfxJxxB1wpJ9yzJ7lYgTfGoJYqwW9f2sNWjMfscCa4HGttMjshylELleKXF5ZTZjAxR5eYV/ivUcuU5MCDyWuC2W+GNWiBPWLDuHBOSY5apDTyF8OEEWOeMZh9Ji4WtsmtUUvUQP4yOq6HwWXgckJrtqBwzqjtn0hBEtNadNA59NA7uZjJx09KhdVE/BgyaqG1jIhiRjc/KRVUG80x3RBO7ItJrVGhQaDRCGJDY1oMm0cw6tSDCgwKUey4fhgjVh2o4UHyA/DeAXfnEb+uAAAAAElFTkSuQmCC"
          />
          <image
            class="cls-3"
            width="37"
            height="27"
            transform="translate(6.24 2.88) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAAbCAYAAAAQ2f3dAAAACXBIWXMAAC4jAAAuIwF4pT92AAABk0lEQVRYR83QWUvDQBSG4V7YbDPTpor7QkVREaWKKCoWFUUs4oaKoogKLoj4/y+P56SZNto2p7VJmosndPlmeEkGADJpUfhWoD+z4zi5bwoKH+gTfaFehuUfJeSfJLgvCtxXjHmvxwSxF0Uhf4cx5AHpsOfmQRp7aTdyVxJy1+gG3WLQvQyNCWIH/6HOBKhzAblLUQvjzvzFDjqlKhh1gk7RhQBu3wo7aJc8EiCPMSYQxp0Jww7aIfcdkIeI4irdBWnsIIwoOyB2MYjCDhzg9p1gB62IbYzaQXtV3L5T7KAZZ8MGZ8sPK0cfRdhBkLOGQeuIwjbjCdLYgWav2GCvYpAfx+27xQ6IvYRRpXoYt48CO7AWMUiHlZKJIuFRcxZYC4jilpOLIi3/MGcwaLYexl0UteZRRQvMaT9s3gLukjg0Rk1i1FQ1jN4ad0FcGn4wJvywYu+iyK8vxqgJxpgJ9Na4g3GrfTCGMGq4GmaMpygsO+CHjZjAHUqC98i6Jnhhg+mIIt6jT2FYf3qiiBdFuGHSfgBE4UsemG8mMgAAAABJRU5ErkJggg=="
          />
          <image
            width="34"
            height="13"
            transform="translate(5.04 6.48) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAANCAYAAADMvbwhAAAACXBIWXMAAC4jAAAuIwF4pT92AAABp0lEQVQ4T73R3S9CcRgH8GOaovdSodSRkjpUXjsdolJ69Vpj08y8zIUL5mUYm7kwY27c+Hu/zu/EKRt+i7j4np2Lc57ns+/DAGCalcjmMISdEGL7o5g9GEfycAKpkwjmz6PIXU2hcBPDwu0Mlu7iWH1IovyUwvrzPCovOfrwr8IVPBheHMBIaRBj6wFMVIbAbwUh7IZrkKNJpE95ZC4E5K+nUayDrNwnUHqsYsg86sL69M844U2y8KX7EMj2Y6joRZhA1kTIBgfSSHQ7hKm9sARJiI3MHUeQPuORvRTw3WzqchLHWBdcETvc073wxF0yhDQSXPZhpOyXGqlvhTazIYg10InuoBWfQfwZt9QIwYTfzkNb1jDEwBpg9prwAcKLkJh4mkStES7v+dXyLyHqbg10Dh0MrL4K8dcgbNQhNUIgtKE/ifyiNLVD3aWB1q6FwVWF2DgLekK2KkRshDbsN5EeCk0blEYVOmxquRGLzyw3QhvSjDCMSoHWNwhphED0YiMmj1HC0AY0KxKEIMhp3hvRO/X/BpAhLSJEoa2dhvbDX4X6wX/lFRPTbW7Xl1SHAAAAAElFTkSuQmCC"
          />
          <image
            class="cls-3"
            width="34"
            height="13"
            transform="translate(5.04 6.48) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAANCAYAAADMvbwhAAAACXBIWXMAAC4jAAAuIwF4pT92AAABDklEQVQ4T8XOXUsCQRTGcS/SmZ2X1coCwYgoirLQCEIDIbKCXigqepMkJKTv/wmenmV2TTKbytYu/jfDOWd+GQCZNAqvNMIbg/DOIP/A2qxjUHhmXYvCC+tZTL9aRPPeg9/JtBTMkYY51rAn7JxdEnJNyC27J+KRPQ1DkhveT0alGgq66TIH7FA5yCk7YxcaX+1/zDswWLAdQO2wXbb3DtH7DuLbHxsiKwGCLVYbgNTH+/hHELkiIdfYRgypOojv6G/69FEsSoglAhLIOhGb6QBGQnJlImKIWCZiVaYKGIJk5wRy8yyCLDiIb/kvc4gZgWwxhpTERAF9yJQlIoLMOohvIa0cJP9/gCTvwKR6Aye2oBeUWcXOAAAAAElFTkSuQmCC"
          />
          <image
            width="37"
            height="8"
            transform="translate(3.36 7.92) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACUAAAAICAYAAAB+rDbrAAAACXBIWXMAAC4jAAAuIwF4pT92AAABcUlEQVQ4T8XRy0sCYRQFcINBx0fiM/P9mjEdzRi1fDRjapr5qqAUQ2pXLYKCgmoRhYsWBREt+ntPzoyJQmpR0eJ8q497f5wrAyD7bvxZF+iCB8ENH5gqhXCNQnQ7AHYvhHibwXIngtRhFNwRC+6YRe40gcLZCooXSWzeZFC95VC/z2Lc/KmAOcYES9gMO2uBM2GFO2WHj3eCzvdRFf9EFH8Sk1DnEqp8/YHi0ehmsfOQx+7TOlovpQHyU4jWoYXeq4OR0sMcNIoo25KE8q46ITQVGGoq0qBFVLzFTESVLlMoX6VFVO2Ox1Z3TUI9rqP5XEL7tYzOW2UUozAoobaoMYwSmrL2Qa6kDZ60Az5OaMqNhaJXQtUlVKwZQmI/jOTB4tjTfCXiM6MkQGgVII1KqIZRtGGAcsR7p0va4cn0ULwLVM79o8UTUTKSgBBiVg6FgRyLEpqaNuy3IqIIjRxyHQnSpIJ6XjOCmjbgLzL1w3/kHQ4xvRPpKqvXAAAAAElFTkSuQmCC"
          />
          <image
            class="cls-3"
            width="37"
            height="6"
            transform="translate(3.36 8.16) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACUAAAAGCAYAAABEplebAAAACXBIWXMAAC4jAAAuIwF4pT92AAAA4klEQVQoU8XO32dCcRjH8S7qfH+c7/es1YopRZM1kdKYlChRF2UjmzGTzURi///dp8/5sUo7JLJdvHlunud5JQAkTk21FPQ9e9DQHQ23y/psoGFGbOwG2Sl7dOE9sbmB92xw8cre2LtBesE+LdJLi/37RwGyKiFv2Z2CqrMGO0T1QpQZ+qgQZCYRauYGDwPUywHqg6CvEHW5YmuLzLeNhzgFAVGUEGVWIciH1eQO1SSorRC3e462QyorkLoScPIR6Ad1s0MdO3auQpBHUCZCXUeo0t8hfqGSVsDPRzk58W+Q/TZhthOuP79t2AAAAABJRU5ErkJggg=="
          />
          <image
            width="33"
            height="12"
            transform="translate(3.84 9.6) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAANCAYAAADMvbwhAAAACXBIWXMAAC4jAAAuIwF4pT92AAABl0lEQVQ4T73Ry0sCURgFcCEx8xEq5qjl+5U6PstSM1OzRNGwBxkJRkS1iYiCWrSQFkFEBC36e09zrzlZEjezXJy7mPnmzo/zSQBIJHIpSKRqGcZ1E1BwSqjMKpB3o4qIIIBuCIJE69RA79WBC07BHOH+FUaPMZUMMo0ccr0CSqMS6hk1Ji2TIsTIC5AoB8u8CfbUDFwr1j9H0YOuw9BZBwGQ6FzajzYExCdI1gpvwQ5/yYlAxY3QhndoWN+D3iYMfj1MEQNFWBNm2NMdhDtng6/oECHhug9zjQAWmvyvQcyBbhO25DQcAsS9YoMn/95GuYOIbPoQ3w0gsc8j2Qpj6SiK5dP4QCjmwNd4hJV02+Crnh5EEIutEIWk3yG5s3kULxd/BGIOsBLdnsXcngAR1pI8CCN1GEHmJEYRhfMFCildp1C5zaDaXv4WxfzRoMkcx8Q2Vi8EyFUSpZs0hdTaWdTvc9h6KPSBmBcPG9IGgZA2ancEksf24yp2n9c+YZgX/XVIIztPRTRe1tF8LYsY5oejCnNgVHkD7+t4adFemWkAAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="33"
            height="11"
            transform="translate(3.84 9.84) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAMCAYAAAAH4W+EAAAACXBIWXMAAC4jAAAuIwF4pT92AAAA9ElEQVQ4T73O30vCUBjGcS/Uc3Z+zDkTIREKgwgkiBIMQQlDBaVIpB8QiRdC3vT/37098xRLHLyVusF3F+PZyyeTtYKiciE6EJSvoKokPJk0W71yARAlVP6GCBI1SeII1SXJU3Tm7RW38SF/+ANxDMBJDPEa6BxdeKSau4Wxg0TEpYOoliLdRh21NYodJKWuHEJdO0SU6aG+JjPU/0KxAy7dBeQGiFs0cBA7Rvea/Iffo9jBX7MjIO6+IBP0aKjwbFgQe3ib/Kkh/wmQF/RqKJihuU1Escd2VeEthgQLS8XlOog9sI+K7w4SfsQY9qe0Ygdp9Qk5gkheSqstXQAAAABJRU5ErkJggg=="
          />
          <image
            width="36"
            height="11"
            transform="translate(1.2 10.08) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACUAAAALCAYAAAD4OERFAAAACXBIWXMAAC4jAAAuIwF4pT92AAABrElEQVQ4T8XR2UsCQRwHcCnzykxdV7c8y/s+0lLR0u3SpCK7L4oeCioKeoggehCCgl566e/9tjOLS1CxdlgP32GZ/THz4TsKAAqFRgmSXn0fVEY11GYtyP5/hS49uj4JpDJqKEo/pIfBaYBpxAi5Q347dFEaVFCbNNAwWmhZHfq5fgzYBySUJcjAFmUxnLL9CVD6IBidVUcbGnQZYBQwBGQNW8CGGHAxFva0Dc7sENx5O0ZLzq4B322022H8ZrACyBqxgItbKciR4eCaGIan4KAoP+9BuOZFpO79VeCnPywBM21JBHG0IQIimDYoNDtCUdGGD4nlAFKrIeR2Yj8Gyg6Q2DMiqg3yVdxiS/OjtCUCSq4EkdkII7sdxfh+HIXDJMonGUydjn0ZKTvwUfxV8dlI4ksBikqvC6AtAbQXo6DiUYqiKmdZ8Bc5zFxNoHZT7AgoO9BJUs2Q0FJEail/kEDpOE1R1fMc+MtxCdW4K2GxNYnl+wqaj/yHSNkLvxuCIk/XRs1dF7BwWxJQZSy1pkTUA4/N5zmanZeaBJQ9vBt5i1p7mqao3Ze6hHoFbg4G0pckvN4AAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="34"
            height="11"
            transform="translate(1.44 10.08) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAALCAYAAAAa5F88AAAACXBIWXMAAC4jAAAuIwF4pT92AAAA/ElEQVQ4T8XQ/0vCQBjH8f3QvN3dbjNBI7BICyUD8RtFYgUFgV8gIhBFCUEkCPL//+3xM0+UiXGV2gavwbGH597MOlCMbI8RHitKlu3rEDsBSUaxo2ii5q9lREqHsBNHO3P+LSp0iB0jJL2IOIUMnDvk5KHA9xr17QeWxeUXkINLuOLEi1DiJCq7jzIOBIK/MY8IlBFSgxtB4laQbMCD2DrMOLBOVBFxzXVEHe50iPsIz5JUU/4pyjhgIu8XEU+rENWBF0neK7y5PwozDvyWaiGiHQ7xui75fRjC++Yw4+Jd8HsIGOiQ+AjGiuITRYcf8Alfyrxk34KQxFTRDBcG28BLdpnlAAAAAElFTkSuQmCC"
          />
          <image
            width="29"
            height="16"
            transform="translate(1.92 6.48) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAARCAYAAADKZhx3AAAACXBIWXMAAC4jAAAuIwF4pT92AAABnElEQVRIS7XRWUtCURQFYAMp09TUnHLiOg9pOeWQmqlZlmaZJBERBRVERAVFEERQUNRLL/3elec4hJQcrOvDuk/77I+9rgCAgI9kzyPIX8VQvE1g7T6F8kMa1ecsaq851N8LaHwUsfdZQmeeubBfYvt+xA8CSB7NIn0awuJZGIXrb7jymEH1KYutlyVsv+Up/mc4UHUisOlCaMeLyK4PBCdw6iRI4dzlPIo3CazeJbt4v11MjMS1zMGzYoNv3Q7/hhPBuqcLJw6b8HEbvoj2hQaCuQUTrCkT7FlLE7bCW7LBX2nB4Ya3C7OQgWBjWA9L3AAuaaSwq8A1YTuF52puWjdr+cCwPqCBIaSDKTpNcVvGTGFS90zZ8S/wV1jlUELtVkHrU/fApG5nnuMF/AFPWuQg6cCdq2ndzX/NWjRo6Eeik0BmlEFulkFpV1CcwCSmiJ53tAcmkRqlUHVgv2YoYBcWSkcxphBBrG1dTeqeciqHilJ4RCSkML3YIKU46xEfEQja8LhaTHHWA75CYeFEq27WMJ9hDgwrzIFh5Qvpl0Wi/E6PqQAAAABJRU5ErkJggg=="
          />
          <image
            class="cls-3"
            width="29"
            height="16"
            transform="translate(1.92 6.48) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAARCAYAAADKZhx3AAAACXBIWXMAAC4jAAAuIwF4pT92AAABBUlEQVRIS73PQUsCURSG4VnkzJ259zoaFkjUosggIkFCKQyMoCCqRaG0ESEIgqD/v/v6xpl0hOrkONfFuzvnPBwPgFd28digNmGvFrU3i/o7+7DY/LT4nhGPSNkHDfuoUX0yqA4N4hc2TisdNtcRzI2GuSV6n4Of2cjgr91CsL6MkGSucvCd/hdUCI7OQkQ9ohcprAeE+bW0txIctkOEHcJddk60vxoowuqY4AnLwdKxZfoZPVRQRyzBT8sFf4WDA4I5WDpQtEV0T2FagreUM3QB9psB/J0M3neLzmB/i+h2Bu+6R2dwpZ7B/FpaKCtvwwZI4EpjfegcjteLTmFpwFXigKu+AN0AcqySRSO4AAAAAElFTkSuQmCC"
          />
          <image
            width="31"
            height="25"
            transform="translate(0.48 4.56) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB8AAAAZCAYAAADJ9/UkAAAACXBIWXMAAC4jAAAuIwF4pT92AAACJklEQVRIS72R+0tTcRjGD6Fr2tylLdfW0d03t7mLm2s3N7eOTpdDV5kmRhRRBEEQFEUQQlQQ/VAR/b1P5/sez4FKfXc59sPnsNvzfp73nQRAOk/6n1rof27jzhcFu18V7H1bx70fHRz+6vLhYem8rmLzbQ3dd3VsvV9B76iBnY9aAV2+/72Dg58b/LBBaT1fxo0XJay9LGP9VYXkgt5RE9sfVqnA3xl2KMfKkyU0nhbQfFYkuXIs33ijbX9Wlh1+GqXDRZQfZFF7lIMoIORi+/ZxAS4/knxpN4nCXgrLB2lVnkFVldcf540CXH4keWY7jlw/gfztBRTuJkl+/X4GFXX76sPcUNKh5KnNCNI3o1jsxYwCxX1tey47ljyuBJFYCyHVjZA8u6Ne4JZWgMtynPlluDGPaCuAWDuIhU4YKbH9VgziL+AGD8KpXwQq1xCqy4g0tQJie/0C3NBBOfFDuejDXMmPQFkrEF3VCsSVkGniE+X+vBcCvUCoJhsX4IYNyx9vZpMeeNNX4MvOUgEhFwSrsunif+SehBt6Adq+cJUuwA0ZFeOFK+yEO+qCKCDkegFuwDjQwz5nh2PeDmfQQXK9ABceF3rY/DbY5RkqQBeIXaYCXHhc6DHtvQSbz0ZyvQAXNAPJ4rLC6pkCFVAvIBAFuKAZSJOOi7A4rSTXC3AhsyC5wOqeMi7AhcxCkqwT0AsIuICZkPzC9CQmZiz/VUxy7gfnyW9TYSh2CP4OXwAAAABJRU5ErkJggg=="
          />
          <image
            class="cls-3"
            width="30"
            height="25"
            transform="translate(0.72 4.56) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAZCAYAAAAmNZ4aAAAACXBIWXMAAC4jAAAuIwF4pT92AAABSklEQVRIS8XNWytEURTA8fNgZt+PmaM0EZKIhojcEmJIuYRci5AXt/L935a1LzVpTvaZc9ke/vtht9b6RQAQhaj5rqD5gX0pSL6VfyFPg48STM8SGi8KGq8Ivjn4U4Ge8R7JWnwlIb7B7hB8cPCThdPmvQd9qTMB6lxAfIldI3yL4L1MxUqB5RGCx9ipgy+EFysEyw4HeYAdOvikPzAXLHYR3MM6FvbNlwKLLQ5iB9PwPi+EZoL5Oge+gaCGt4uDmWC+woCvMQPzzfLQP2G2xIAtI7pqcd+hfktH5xFdsLDOdyRPPR90jgFtO3ixGjQdnqGgcdauDu2ByRSiGp6llaK/YDJJQUenLe5bLFoXHrewzrdURuapjxIw8EQYtAu3CBh8LCBcH0ZUN0KCoQauDTm4FRpOCGjcN1h2US1GOPkHeECFRw3sG6iqH4wKJSk7CHQdAAAAAElFTkSuQmCC"
          />
          <image
            width="37"
            height="12"
            transform="translate(13.2 8.4) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAANCAYAAADFVhxbAAAACXBIWXMAAC4jAAAuIwF4pT92AAABp0lEQVQ4T8XR+ytDYRwG8FOYXezCYRezu7OLjd0vzNbYmBNjZjWj+IGklEgplJKU4hfJ3/s478vOrKajtdmp59Q5fXvfT8+XYeSDIBlQDWFIM4zhUTmEh+l3GAKiGZGJMMW4EpopDbRWLaQO6FUYgpHp5JCzChqlXgWVaYSiSNjpUYx7WBj8E5gMGv4NSl8ERFoiILVZTUE6exM14WVhDAiwkAFTURMcaQucGUtPkS0fjfXpHDqw3JjYlHFWD5PQljlihDUxSWGurBXckg2+ohMza66uI9v+/ImiK/xuqoEiIShPwfEF412Y3XQjuO1BrO7vClJygISgbElzS1PuvB3eVSd8QluBDQ5zZQ9CFS+iu37E9wJIHc4hfRRC5iTSEVRyoF0aKyQJlDjaVnjHi3DVJ6IasOxpBLmzGPKXSaxcpf6MlBz4S8gKCSpam6Gw5IGAOg5h8SRMUUvncRHG36Sxfp9B6SGL8lPuV6jkpZ1m4agJy18kKKx4PQ/+rgnbesyh8ryM6kse9fdiC1Lygm6Hv20Dey2g9raK/Q9exEke1K9IDvQrn5hKAI7zCgtGAAAAAElFTkSuQmCC"
          />
          <image
            class="cls-3"
            width="37"
            height="12"
            transform="translate(13.2 8.4) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAANCAYAAADFVhxbAAAACXBIWXMAAC4jAAAuIwF4pT92AAABBklEQVQ4T83Q2ysEcRjG8bkw+zuPnd2QkJwKRSuUnIpSEikiLqSQK/n/7x7PHLKmVq91mp36TE3zzvv7NtFQUOAVDZooC8vEKbUVGqODEZrf4lYRFY8wbIwmNdQUTWtIC/5K5aExzqgJXQlTcxp6gRb/N/LTF2qGUbMMmi+icssGZoXWDOy6gbT8J8SBjF4yRdQqdbphdpO2LNwO7VlIe/ohDvRiNz5EbdMu7Vv4Qzpy8McO0g6JOPAV2d9yB9WwcEKndO6QXPQfKg58x3vYWRl26ZFcewzf0C3deUg7xEN+S3JVRpVhzXt6CGg+BaTPAbWF9ZKHPRZh6UtA67UbKH5cF3GgLm81Jj1kVbUTHQAAAABJRU5ErkJggg=="
          />
          <image
            width="34"
            height="8"
            transform="translate(14.88 7.68) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAJCAYAAABXLP43AAAACXBIWXMAAC4jAAAuIwF4pT92AAABXklEQVQ4T73Qy0sCURgFcCExUxsmG03JfDQ55pSD+Uib8pGjNkYPyaxUMKhF0KJFiyCCalFQUdAi/96TjikNMzFG4eJs7v2+e38cHQDdoPEkpuHjXaDTbgQKPrAlGtwug3BlHpEDFsuNRSSPOayehpE6iyBzHoPWm738eDExS4JirLCzFJycHa6oEx1IB0Fn3GAEL4IijdAOg6X9oASJ19uQJge+D4kidxFH/jKBjasVbN6sYes2pYpTHJimzCBcBEgfiUm/HOJNdiH+da+skQ4keqTeiAJyl0b5MYvKiwBViH7cgFHrGMwDQJicHMKVAxImVltAohkCf6KEiNe8BNm+b0Mesth7yqH6msfhexH1lvilMerRg3xvxBak4AjZFJC5jEe13t+k8iyg+lZA7UNEo1XqIkYsBhhIYx9BzBBSG12I/c+fDhIZxEiZYHZYJIzW4n9Hc2BY0RwYVj4B8ZDfTapiDoYAAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="34"
            height="6"
            transform="translate(14.88 7.92) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAGCAYAAACmekziAAAACXBIWXMAAC4jAAAuIwF4pT92AAAA2ElEQVQoU8XPX0sCQRSG8b1wd2Z2ZvZPICRIBopQQVAEQWAgoSgJBSKGKEEQFILf/+7tzd1sYYtZhOjiuTrDOb/xAHhVU+cK6kIhvGLXIfQN67Fb1g9h7thQw47ZhD1qRFODaG7g2v3rQBxLiLaE7LITCXVGhAsy0DAjAu7ZAxFfkJlB/MRWBskze7ElWAkQHAqIJhGtIkRBnuaQS+X83U/FC0KW35Dk1SJ9Y+sMtXvopwJ+XSBo5JCjHNKRex2uUvqeQQ42Fl7NCnxWhLgW/EVbiB/9z/FiHwWS6FJs0OM5AAAAAElFTkSuQmCC"
          />
          <image
            width="29"
            height="5"
            transform="translate(23.28 7.44) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAFCAYAAABSIVz6AAAACXBIWXMAAC4jAAAuIwF4pT92AAABQ0lEQVQoU43R20vCYBgGcAkxtbVmTufUqTky85SmzVnz0JTUIovqQpEORBBFEF3UTQQFUUFXQf29T+6bLEuFLh74rt7f+7yfCYDpb5jgDNjILNwxFt40B2GFR6jgh1gKIFINYbEhItmKILMfRa4Th3yUwtpZGuXLLEbNGxXjYaYtsDptoHgKtECDXXAacEDyIqwImFd1NNYUCapF6iYgn/zA6rWEjdsCNu8VbD0oYxcxTdjM0FACs3ZQ3mkwcwy4HsovcfBneQTzPgIPtW3HkT9MQj7uwxe/4e3HEnaeK9h7U4cWILDFYcVkvy05c6+tJ+GCL+PR0WIPHWg7CGtDVk/TKJ4vo3KVQ/Umj/pdgbRtPZV1+FXFwXsNnc+GsQA5sQbbuSnS1hFm4Io6wafcEHL8v/9sXHZf1knj9kcd3a+mMe8bYx0xL8AHFGYAAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="29"
            height="5"
            transform="translate(23.28 7.44) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAFCAYAAABSIVz6AAAACXBIWXMAAC4jAAAuIwF4pT92AAAAv0lEQVQoU6XP3wvBUBjG8d3Mdnb2kwuJUpQiSSgloRRFRJQSpVwoF/7/u8czW1srLLn41Ol0Ot/3VQAov5JDA3JEEwNpbz95e6mVdWgVHXqN6gKiSW0Bo0t9BgdB2JzSXMJa0ErC3tDOhH0wUweKDmpOQyZPJUb9cDUMNxhtUYfRnoiichyGZ4wuac3olvZB2DnS2YR7eT+Eonoa/OgrXKBivG3a1N84Jz9qwb1a8G50txL/JbZN++wffjj7iONPAHldHnsdio8AAAAASUVORK5CYII="
          />
          <image
            width="31"
            height="5"
            transform="translate(1.44 9.36) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB8AAAAFCAYAAAC94zfEAAAACXBIWXMAAC4jAAAuIwF4pT92AAABHUlEQVQoU2P4//8/Awiz8rP/Zxfm/M8lxv2fR4rnv6CSwH9hVaH/4lqi/6X0xf/Lmkj+V7SS+a9sJ/df3UXxv4ab0n8db5X/+oHq/41CNf+bRun8t4jT+2+bZvTfLsP4v1O+2X+XYov/7mWW/71qbP77Ntj9D2xz/B/U6fQ/fILb/6gpHv8ZGDhY/oMwKx/EcpDFfDJ8WC1XsIRa7qz4X8tTGW65cbgWiuUO2Saolldbgy33b3b4H9LtDLY8eqrnfwZGTqDFQF+DMLKvQRjZYnlzaRSfgyzX9VMDW24SoQ222CrJAGF5nilWy2EhDcIMzNys/2FBziMJ8jXvf0FF7EEOwioO8nDL9fzVUILcMkEfxXBCGIWDHuSENFOKAU1eL6c6tagRAAAAAElFTkSuQmCC"
          />
          <image
            class="cls-3"
            width="31"
            height="3"
            transform="translate(1.44 9.6) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB8AAAAECAYAAAB2v+RhAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAgElEQVQYV8XNTQsBYRiF4VkYz/s1I4milKKUbCxsLCQrSVko+f9/5HZMpHw0K1lcZ3XqzoDsJu8YzZ4MDBs6bCRjh5vKzOPnsvCE5d0qENeyCaSt7CLFXg5yjJSnRHlOtC6JR+NVNY3CyNsKd6X/jLtJfbwKf4h/C77F/6X28EtX0pgnA8rcDCoAAAAASUVORK5CYII="
          />
          <image
            width="11"
            height="13"
            transform="translate(0.48 10.08) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAANCAYAAAB/9ZQ7AAAACXBIWXMAAC4jAAAuIwF4pT92AAAA3ElEQVQoU43Qz4sBcRjH8TmIKRRm/RwZm8HkR4xGa8TuqF3lR0kIOXHgwJGTo+seXPy9Hz1fKVPydXg/p9fn8ggAhHcTBNEByukT4Y564P/0QVL9kPUInmJXQITnAUaKISS+ZKRbim3Ajlf2MviRCTCo1GQW4VwnBRumgpqEmB5G0oxD/bkhqtBToY812PC91HfihvpplIZZlEcajHn+Ob5H0JjlUV0UUF+VYe2M168ylyU01jqamwofU9bWQPfYwN++xsftg8kwxcXU4GRhem7zITX5/8Xi0uHDx643blvRO0+gYwAAAABJRU5ErkJggg=="
          />
          <image
            class="cls-3"
            width="11"
            height="12"
            transform="translate(0.48 10.32) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAsAAAANCAYAAAB/9ZQ7AAAACXBIWXMAAC4jAAAuIwF4pT92AAAAm0lEQVQoU2Ng4WX/D8KsIuz/2eU4/gMBAy7MAFYohFDMrsjxn0OT8z+nLieGRjiDXR6qUIMDrBCEuSy4/mNVjIw5jYAKzTj/c1tx/ee2R2jA6T5ua6BCOyB2IEIxCPM4c/3ncef+z+vH/Z+gYhDm9YUo5gvjwa8QhvlCef7zxxKpmD+GBMUCibz/BZJ5CSuEYcFMEhSDMEEFyBgAJ5hskRtlYScAAAAASUVORK5CYII="
          />
          <image
            width="28"
            height="18"
            transform="translate(29.52 4.8) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB0AAAATCAYAAABsmQZ/AAAACXBIWXMAAC4jAAAuIwF4pT92AAABoklEQVRIS7XQyUtCURQGcIsGBzQz59Q0c57NIUXFUitLaTRphIJo0SKooEURtQgiilb+vV++q+81IN6e+hafyOOe8+M7IgAiobLzVkDto4j65zIOG2Ww36mD/03pZgErd2ms3WdQecyi+pTD9usSdt9b6EFjdTBo5jyC7EUU+csYlq4S4OCHDKrPOWy+5MG0/TtHXdwpsX0fkicBpM/Cv9Hb77bd5qnAzwQ3nAhtuRDd8yJx5EfqNERQFi5eJ7tivFBX0Qb38iz8FQcYOFLzgLQ9DnAwbQcvdDZjhmNxhqDesp2g4R035utegtLmeaOmmAHWtAn2nIVDA1UHOTFtMW9U59PAGNbBHG+iqRbqKrVPvD7XF9gRVdknofWqYQw10XZT5sTOgrVvrCOqMCugtCmh9ahhCGphiuphSRhha8K0RXzC/ZFqZSCodQJqpwrMiQ1M27hxoCCHjinFkOpkkE/LSVONawoad6stbUEvIT/D0lGMqyQEZU+sbsK04V4jEolHwKLsiWlD/YagQ5IRgsr0MsFBgtIeCBHqAyFCfSBEvgC/osxr6OlHagAAAABJRU5ErkJggg=="
          />
          <image
            class="cls-3"
            width="27"
            height="17"
            transform="translate(29.76 4.8) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAASCAYAAABIB77kAAAACXBIWXMAAC4jAAAuIwF4pT92AAABCElEQVQ4T73N4SsEQRjH8XvhdmZ2Zpa9FydFvECRuDonUl4oopQi8kJ5obwQ/v93P7+dXTrO9rg168V3aup5nk8HQKfN8heP3pvHx19cmKbZe4eiuQf26JE/sWf2GhHMriyya4fshtjdV/CnefFgXf7cwl+wywq8LUFpTzz8PXecwp0QGgOlnUagPUxhj4gV4KmFO5sO+jWY7hmk+8TGQGmnMWgGxHZZgR6UqHRQqh7bMjA7hIZsZP4M1YJ6TUOvs02C22wQD5sA1YqGWq3ADR0VmgDVErGiCpUWmxaeZF4hWVAluNwe9gl2eyWYLLaLBbCbKQSwr1rHAjjjCeb/gwVQGoidOBC7d/f2aym27nEoAAAAAElFTkSuQmCC"
          />
          <image
            width="31"
            height="6"
            transform="translate(36.72 6.24) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAGCAYAAACij5zfAAAACXBIWXMAAC4jAAAuIwF4pT92AAABMUlEQVQoU53QzUvCcBwG8B1MS8l8WVula23qcsMcudQsnU1DM4ykJEE7dIsIPHTo0qEOdaguXfx7n9wGm2PMXg4P/PjBl+fDQwAgfhN2PwG+yiCjsci2eOS6GeTPBRSuJJSud/DTvVdcH2EmjCgXQTwdBS2R2JBpJJV1GICaG6AMdEAOlZs8arcFqHcKGuMSjh/KaD9WcPpUnYuzHv5IAEEqhFkANQPYOkgiXd80yqVOyiiXe9vmAiMbUL/fcwG6zyp6r0e4eG+4MASx6IOehRUdELQApBBzLKADUqoJENu8BdjtiygOdYBsAbRx0QE4e5kC3jRcfjQx+GphNOnAAfAt+xGILSG0Zi+wmo1bALacAHfITBdgITS5uZN6RQf0P03AcHLiAaBtACWS/yr6a74BN72jaEZ5704AAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="31"
            height="5"
            transform="translate(36.72 6.48) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAFCAYAAAAkG+5xAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAwElEQVQoU7XPzQsBQRzG8T1gZ3Z2126UC6EcFIoopRwUCZGD5OAlBwfx/18fz77gsFu7Dg6fmqZmft+fBkBLItsSskNdCWNAQwNqRGOaGDCnNFewlrRWsLcKSX++RS70ioCvRg0B0RSQrTCgFw0wvYAZBy9oRZsgwN6ZyB/oSGcTztWKjfocsgUduRKVObgaE+Bt35epN7P3YcCJwy9BgHOz4N7p+Y3RMpaOrEvFIMDf3guoi9TDfuE+YgKSHv3TC7+FcmjC0iG8AAAAAElFTkSuQmCC"
          />
          <image
            width="33"
            height="13"
            transform="translate(28.56 8.16) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAANCAYAAADMvbwhAAAACXBIWXMAAC4jAAAuIwF4pT92AAABo0lEQVQ4T8XR2UsCURgF8IEy933fl9RU3NLctXItpZASMpQoiCgKCqIiCHrpIeglevDvPc29U9qCTFTWw5lhZr7h+3EuA4BhRNMg9/8MRZBMyQQQKIUQ6yXg+2kSYchykhm1CEKNGFKTFFKzDCqn8k9BDFku0okhMUggMXIIuVUOlUsFjVcNQ1D3JyB6IYhXgMKhoG2oPSpofRoKMUcNsCXNEwW9e1A6lFwTs1wTJMawHpaYkUKcGSvcBftEQJ9eaL0jhIkg4kZYEyY40ha4slZ4Snb4Ki4EVzwItby/hhr7wRQxUARpgiBIE54ii1h2wl8dQaJtP5Ld0I9BvAMk3JHY4Cu7Rm00ZxFZ9yHeCVBIqh9Gbi+GwsH8t1C8A2/jZxFzNTdtIkwQmwEktlhEL/wCiaLIQhaPEiifplC/yH4ZxTswLuRISBsUwiIyOxHk9+MUsnScpJDaeQaNqxxaN0Ws3ZbQvlsaC+Nd+NVkd7k2SocJLJ8soHKWRoNt5CNk476M7mMd208N9AarQxjvgp+kcZnD6nX+E6TzUKWQ/qA5hDwD2PFy9v6WBoMAAAAASUVORK5CYII="
          />
          <image
            class="cls-3"
            width="33"
            height="13"
            transform="translate(28.56 8.4) scale(0.24)"
            xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAANCAYAAADMvbwhAAAACXBIWXMAAC4jAAAuIwF4pT92AAABA0lEQVQ4T8XQ7UvCQBzA8b3Z7mF3a43ApFAhSiiMQjEMAiWJxCgYRYIvRCHEN/3/7379tpvMhXU+7GHwHez2u92HGaZHYBleRlEZqxDziIB1XAwovFklBQgrEyCnFEiV5gpKPFgnVCEqCkLOKNCLfEBrF0ktQpwrCLtiwBosU9C/L2kdIZdMQa6xWwa8mQ1IOxAU/A12E0Fa2B0Hfs/BfuCpobQDv+NthHRiiN3lIB6xJ3svlHbgr0JEUA8RfQWRA+wFe9sepR3YNPGMgCH2qiCOL8D5EHAwwsZCC9MesEuOj5B3hHzGEHeCTSW4XxIO59hCJnDaj6aZO0tCvO8Yo92cVz8s/6ch+8QtaQAAAABJRU5ErkJggg=="
          />
          <g id="Layer_2" data-name="Layer 2">
            <g id="Layer_1-2" data-name="Layer 1-2">
              <path
                id="Path_443"
                data-name="Path 443"
                class="cls-4"
                d="M53.29,31.89a9.22,9.22,0,0,1,2.6,4.52A17,17,0,0,0,36,23.64a9.31,9.31,0,0,0-5.06,2.72,9.86,9.86,0,0,0,0,13.95h0l8.42-8.42A9.86,9.86,0,0,1,53.29,31.89Z"
                transform="translate(-17.2 -0.64)"
              />
              <path
                id="Path_444"
                data-name="Path 444"
                class="cls-5"
                d="M51.37,52.33A17,17,0,0,0,56,37a9.3,9.3,0,0,0-2.72-5.06,9.86,9.86,0,0,0-13.95,0h0l8.42,8.42a9.86,9.86,0,0,1,0,13.95,9.26,9.26,0,0,1-4.51,2.6A16.92,16.92,0,0,0,51.37,52.33Z"
                transform="translate(-17.2 -0.64)"
              />
              <path
                id="Path_445"
                data-name="Path 445"
                class="cls-6"
                d="M30.93,26.36a9.25,9.25,0,0,1,4.52-2.61A17,17,0,0,0,22.68,43.66a9.34,9.34,0,0,0,2.72,5.06,9.86,9.86,0,0,0,13.95,0h0l-8.42-8.42A9.86,9.86,0,0,1,30.93,26.36Z"
                transform="translate(-17.2 -0.64)"
              />
              <path
                id="Path_446"
                data-name="Path 446"
                class="cls-7"
                d="M47.76,40.31h0l-8.42,8.42a9.86,9.86,0,0,1-13.95,0,9.25,9.25,0,0,1-2.6-4.52A17,17,0,0,0,42.7,57a9.34,9.34,0,0,0,5.06-2.72A9.86,9.86,0,0,0,47.76,40.31Z"
                transform="translate(-17.2 -0.64)"
              />
            </g>
          </g>
          <circle class="cls-8" cx="22.78" cy="39.41" r="19.81" />
          <path
            class="cls-9"
            d="M24.42,28.91a17.64,17.64,0,0,0-1.58,2.51,18.84,18.84,0,0,0-1.14,2.72,18.27,18.27,0,0,0,0,11.6A20.69,20.69,0,0,0,23,48.9,19.05,19.05,0,0,0,36,59.1a19.81,19.81,0,0,0,3.36.42,18.9,18.9,0,0,0,3.39-.18.1.1,0,0,0-.09-.05,19.12,19.12,0,0,1-3.44-.14,19.77,19.77,0,0,1-3.36-.74A17.84,17.84,0,0,1,28,53.5a19.21,19.21,0,0,1-2-2.44,18.43,18.43,0,0,1-2.53-15.35,18.81,18.81,0,0,1,1.11-3c2.88-6.1,9.05-11,15.94-11.15-5.66-.77-11.34,2-15,6.19C24.82,28.44,24.42,28.91,24.42,28.91Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-10"
            d="M55.81,28.79a19.48,19.48,0,0,1,1.55,20,17.27,17.27,0,0,1-1.08,2A17.87,17.87,0,0,1,55,52.64a20,20,0,0,1-19.1,6.71c7.67.86,15.74-2.92,19.46-9.78a16.51,16.51,0,0,0,1-2.26A19.83,19.83,0,0,0,50.82,25.9a18.63,18.63,0,0,0-2.41-1.79,18.34,18.34,0,0,0-3.06-1.56,16.63,16.63,0,0,0-6.81-1.17,17.5,17.5,0,0,1,6.07.17,18.23,18.23,0,0,1,2.92.85,19.32,19.32,0,0,1,2,.94A18.68,18.68,0,0,1,51.5,24.5a16,16,0,0,1,3.29,3.06C55.43,28.31,55.81,28.79,55.81,28.79Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-11"
            d="M46.7,24a17.23,17.23,0,0,1,5,3.58,18.31,18.31,0,0,1,2,2.39,18.81,18.81,0,0,1-2.55,24,23.36,23.36,0,0,1-2,1.73,26.29,26.29,0,0,1-2.21,1.53,12.89,12.89,0,0,1-8.58,1.89,1.75,1.75,0,0,1,.44-.07A14.54,14.54,0,0,0,49,54.25,22.45,22.45,0,0,0,50.95,52a19.33,19.33,0,0,0,1.9-21.11A21.64,21.64,0,0,0,51,28a21.34,21.34,0,0,0-2.25-2.54A19.82,19.82,0,0,0,46.7,24Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-12"
            d="M37.07,57.36a13.41,13.41,0,0,1-1.24-1.07,10.94,10.94,0,0,1-1.07-1.22c-.16-.21-.32-.43-.47-.66s-.3-.45-.44-.68l-.38-.66c-.25-.44-.48-.9-.7-1.35s-.43-.92-.62-1.38-.42-1-.6-1.57a9.92,9.92,0,0,1-7.17-3.31c-.26-.26-.51-.54-.74-.82a22.36,22.36,0,0,1-2.91-4.72,16.3,16.3,0,0,0,1.09,6.18q.2.55.42,1.08A19.31,19.31,0,0,0,26,53q.48.5,1,1a21.72,21.72,0,0,0,1.77,1.47c.31.23.62.45.94.66a18.29,18.29,0,0,0,3.86,2l.82.29.71.21,1.2.27a25.75,25.75,0,0,0,3.88.59q.66,0,1.31,0a10.49,10.49,0,0,0,5.15-1.38,9.46,9.46,0,0,1-5.9.88,8.67,8.67,0,0,1-1.42-.39A8.44,8.44,0,0,1,38,57.92,8.69,8.69,0,0,1,37.07,57.36Zm7,1.39a12.17,12.17,0,0,0,1.47-.38Q44.85,58.6,44.1,58.76Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-13"
            d="M28.84,36.14c0,.13-.05.25-.07.38s0,.24-.06.37-.06.48-.08.73a20.86,20.86,0,0,0,1.19,7.24,7.13,7.13,0,0,0,2.45,3.94,3.54,3.54,0,0,0,1.14.55,3.9,3.9,0,0,0,.62.12,4.31,4.31,0,0,0,.63,0,4.39,4.39,0,0,0,1.3-.25A1,1,0,0,0,36.66,48a3.57,3.57,0,0,0-.19-.54,27.82,27.82,0,0,1-1.82-7.59q0-.41-.07-.81-.06-.82-.1-1.63l0-.54c0-.74-.13-1.47-.15-2.21a.9.9,0,0,0-.92-.8,1.47,1.47,0,0,0-.36,0l-.48,0a8.4,8.4,0,0,0-2.74,1,2,2,0,0,0-.75.9Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-14"
            d="M48.53,29c.16.2.32.41.47.62l.21.32a7.35,7.35,0,0,1,.53,1,20.89,20.89,0,0,1,1.74,8,7.84,7.84,0,0,1-.2,1.9,6.35,6.35,0,0,1-.29.91,5,5,0,0,1-1,1.58,4.88,4.88,0,0,1-.7.63,6,6,0,0,1-.73.45,2.88,2.88,0,0,1-.49.21.85.85,0,0,1-1.2-.93A20.37,20.37,0,0,0,45.51,35c-.08-.21-.17-.43-.26-.64a28.76,28.76,0,0,0-2.14-4.2,1.1,1.1,0,0,1-.12-.89.89.89,0,0,1,.19-.34,1.11,1.11,0,0,1,.62-.33,8.28,8.28,0,0,1,3.36-.18l.41.1.32.13a1.71,1.71,0,0,1,.3.19Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-15"
            d="M24.73,46.12a16.19,16.19,0,0,1-1.06-6.44c0-.55.07-1.09.14-1.64a16.74,16.74,0,0,1,.85-3.45,17.9,17.9,0,0,1,1.53-3.19,19.19,19.19,0,0,1,12.75-8.93c-4.87,2.31-9.54,5.67-11.83,10.72a16.85,16.85,0,0,0-1.18,3.31A15.44,15.44,0,0,0,25.49,40c0,.52,0,1,.05,1.56a17.82,17.82,0,0,0,1.57,6.05c.25.55.52,1.1.81,1.63A20.62,20.62,0,0,0,34,56l.76.52a13.38,13.38,0,0,0,3.67,1.71,2.39,2.39,0,0,1,.5.23,11.61,11.61,0,0,1-3.44-.81c-.35-.14-.68-.28-1-.43a17.88,17.88,0,0,1-5.91-4.54,20.66,20.66,0,0,1-2.94-4.48q-.24-.51-.46-1C25,46.84,24.86,46.48,24.73,46.12Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-16"
            d="M43.45,58.28A23.66,23.66,0,0,0,56,47a17.93,17.93,0,0,0-.33-17.29,17.37,17.37,0,0,0-1.34-2,20.41,20.41,0,0,1,1.41,1.91,21,21,0,0,1,1.18,2.06,21.23,21.23,0,0,1,2.26,8.24,18.25,18.25,0,0,1-4.26,12.76,21.37,21.37,0,0,1-9,6.08,17.25,17.25,0,0,1-3.6.88,13.94,13.94,0,0,1-3.71,0,18.8,18.8,0,0,1-4.06-1,17.3,17.3,0,0,1-2-.88c.74.21,1.5.42,2.26.59s1.54.33,2.31.43a17,17,0,0,0,2.35.16,11.21,11.21,0,0,0,2.33-.26C42.78,58.47,43.45,58.28,43.45,58.28Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-17"
            d="M38.68,16.81a3.23,3.23,0,0,1-.18,1.29,3.76,3.76,0,0,1-.29.61L38,19l-.19.29a5.66,5.66,0,0,1-.42.54c-.16.17-.31.35-.48.51l-.25.24-.26.24a5.52,5.52,0,0,0-.46.51,4.36,4.36,0,0,0-.92,2.38,4.15,4.15,0,0,0,0,.51.09.09,0,0,0,.09.09.09.09,0,0,0,.09-.09,4.26,4.26,0,0,1,.62-2.4,5,5,0,0,1,.88-1l.26-.23.25-.25.24-.26.23-.26c.15-.19.29-.38.42-.58s.24-.4.34-.61a3.63,3.63,0,0,0,.39-1.28,3.22,3.22,0,0,0,0-.54.1.1,0,0,0-.09-.09.09.09,0,0,0-.09.09Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-18"
            d="M38.73,16.79a3.32,3.32,0,0,1-.18,1.3,4,4,0,0,1-.29.63l-.17.31-.19.29a5.68,5.68,0,0,1-.43.55c-.16.17-.32.35-.48.52l-.26.24-.25.24a5.68,5.68,0,0,0-.47.52,4.33,4.33,0,0,0-.91,2.54c0,.1,0,.2,0,.3s.05,0,.05,0a5.08,5.08,0,0,1,0-.7,4.34,4.34,0,0,1,.15-.7,4.07,4.07,0,0,1,.59-1.23,5.48,5.48,0,0,1,1-1l.12-.12.13-.13.24-.26.23-.26.21-.27a5.3,5.3,0,0,0,.38-.58,6.46,6.46,0,0,0,.32-.62,3.39,3.39,0,0,0,.28-1.26c0-.1,0-.19,0-.29s-.06,0-.05,0Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-17"
            d="M40.94,16.53a3.36,3.36,0,0,0,.19,1.36,4.7,4.7,0,0,0,.7,1.25l.23.29.21.26a4.08,4.08,0,0,0,.46.45,8.24,8.24,0,0,1,1,.92,6.64,6.64,0,0,1,1.08,2.36c.08.27.16.55.25.82,0,.11.22.06.18,0a16.72,16.72,0,0,0-.88-2.48,4.23,4.23,0,0,0-.8-1.1c-.31-.3-.66-.56-1-.87l-.21-.24-.1-.13-.12-.14a6.22,6.22,0,0,1-.42-.58,3.79,3.79,0,0,1-.54-1.26,3.12,3.12,0,0,1-.06-.86.09.09,0,0,0-.19,0Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-18"
            d="M41,16.54a3.35,3.35,0,0,0,.19,1.34,4.74,4.74,0,0,0,.7,1.23l.22.28.21.26a4.11,4.11,0,0,0,.46.45,8.16,8.16,0,0,1,1,.92,6.73,6.73,0,0,1,1.1,2.39c.08.27.16.55.25.82,0,0,.06,0,.05,0a16.62,16.62,0,0,0-.87-2.47,4.2,4.2,0,0,0-.79-1.08c-.31-.3-.66-.56-1-.87l-.21-.24-.1-.13-.12-.14a6.09,6.09,0,0,1-.43-.59,3.86,3.86,0,0,1-.55-1.28,3.23,3.23,0,0,1-.06-.88s-.05,0-.05,0Z"
            transform="translate(-17.2 -0.64)"
          />
          <rect class="cls-19" x="18.3" y="18.26" width="8.98" height="3.23" />
          <rect class="cls-20" x="18.3" y="18.18" width="8.98" height="3.23" />
          <rect class="cls-21" x="18.3" y="18.09" width="8.98" height="3.23" />
          <rect class="cls-22" x="18.3" y="18" width="8.98" height="3.23" />
          <rect class="cls-23" x="18.3" y="17.91" width="8.98" height="3.23" />
          <rect class="cls-24" x="18.3" y="17.82" width="8.98" height="3.23" />
          <rect class="cls-25" x="18.3" y="17.74" width="8.98" height="3.23" />
          <rect class="cls-26" x="18.3" y="17.65" width="8.98" height="3.23" />
          <rect class="cls-27" x="18.3" y="17.56" width="8.98" height="3.23" />
          <rect class="cls-28" x="18.3" y="17.47" width="8.98" height="3.23" />
          <rect class="cls-29" x="18.3" y="17.39" width="8.98" height="3.23" />
          <rect class="cls-30" x="18.3" y="17.3" width="8.98" height="3.23" />
          <rect class="cls-31" x="18.3" y="17.21" width="8.98" height="3.22" />
          <rect class="cls-32" x="18.3" y="17.12" width="8.98" height="3.22" />
          <rect class="cls-33" x="18.3" y="17.04" width="8.98" height="3.22" />
          <rect class="cls-34" x="18.3" y="16.95" width="8.98" height="3.22" />
          <rect class="cls-35" x="18.3" y="16.86" width="8.98" height="3.22" />
          <path
            class="cls-36"
            d="M35.5,20.33h.1l.24,0,0,0,.67,1.17a.23.23,0,0,0,.14.11.2.2,0,0,0,.28-.05l.67-1.05a.1.1,0,0,1,.09,0l.58,0a.11.11,0,0,0,.07,0,.12.12,0,0,0,.08,0l.58,0a.11.11,0,0,1,.09.05l.67,1.11a.21.21,0,0,0,.36,0l.67-1.11a.11.11,0,0,1,.09-.05l.58,0h0a.12.12,0,0,0,.09-.06.1.1,0,0,0,.1.06h.58a.1.1,0,0,1,.09,0L43,21.64h0l0,0h0a.19.19,0,0,0,.11,0l.07,0h0l.05,0h0l.67-1.17a.12.12,0,0,1,.09-.06h.14l.19,0v-2.7h-9Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-37"
            d="M41.61,17.58a.11.11,0,0,0-.11-.11q-1.52.06-3,0a.11.11,0,0,0-.11.11v2.73a.12.12,0,0,0,.11.12l.58,0a.11.11,0,0,1,.09.05l.67,1.11a.21.21,0,0,0,.36,0l.67-1.11a.11.11,0,0,1,.09-.05l.58,0a.12.12,0,0,0,.11-.12Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-37"
            d="M38.41,17.57a.12.12,0,0,0-.11-.12q-1.52-.07-3-.26a.1.1,0,0,0-.11.1V20a.13.13,0,0,0,.11.13l.58.07a.12.12,0,0,1,.09.06l.67,1.17a.21.21,0,0,0,.36,0l.67-1.06a.1.1,0,0,1,.09,0l.58,0a.11.11,0,0,0,.11-.11Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-37"
            d="M44.84,17.31a.09.09,0,0,0-.11-.1q-1.51.19-3,.25a.12.12,0,0,0-.11.12v2.73a.1.1,0,0,0,.11.11l.58,0a.1.1,0,0,1,.09,0L43,21.49a.21.21,0,0,0,.36,0l.67-1.17a.12.12,0,0,1,.09-.06l.58-.07a.13.13,0,0,0,.11-.12Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-38"
            d="M44.86,20.07a38.31,38.31,0,0,1-9.78,0V16.5h9.78Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-39"
            d="M38.34,17.55a.12.12,0,0,0-.11-.12q-1.52-.07-3-.26a.1.1,0,0,0-.11.1V20a.13.13,0,0,0,.11.13l.58.07a.13.13,0,0,1,.09.06l.67,1.17a.21.21,0,0,0,.36,0l.67-1.05a.1.1,0,0,1,.09,0l.58,0a.11.11,0,0,0,.11-.11Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-40"
            d="M35.82,17.61a.16.16,0,0,0-.15.17.21.21,0,0,0,.17.21h0a.16.16,0,0,0,.15-.17.21.21,0,0,0-.17-.21Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-41"
            d="M35.82,18.28a.16.16,0,0,0-.15.17.21.21,0,0,0,.17.21h0a.16.16,0,0,0,.15-.17.21.21,0,0,0-.17-.21Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-42"
            d="M35.82,19a.16.16,0,0,0-.15.17.21.21,0,0,0,.17.21h0a.16.16,0,0,0,.15-.17.21.21,0,0,0-.17-.21Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-43"
            d="M36.34,18.88a.09.09,0,0,0-.08.09.11.11,0,0,0,.09.11h0a.09.09,0,0,0,.08-.09.11.11,0,0,0-.09-.11Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-44"
            d="M37.37,19a.09.09,0,0,0-.09.09.1.1,0,0,0,.09.11h0a.09.09,0,0,0,.09-.09.11.11,0,0,0-.09-.11Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-45"
            d="M35.82,19.62a.16.16,0,0,0-.15.17.21.21,0,0,0,.17.21h0a.16.16,0,0,0,.15-.17.21.21,0,0,0-.17-.21Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-46"
            d="M37.83,17.8a.17.17,0,0,0-.16.18.19.19,0,0,0,.17.2h0A.17.17,0,0,0,38,18a.19.19,0,0,0-.17-.2Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-47"
            d="M37.83,18.47a.17.17,0,0,0-.16.18.19.19,0,0,0,.17.2h0a.17.17,0,0,0,.16-.18.19.19,0,0,0-.17-.2Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-48"
            d="M37.83,19.14a.17.17,0,0,0-.16.18.2.2,0,0,0,.17.2h0a.17.17,0,0,0,.16-.18.19.19,0,0,0-.17-.2Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-49"
            d="M37.83,19.81a.17.17,0,0,0-.16.18.19.19,0,0,0,.17.2h0A.17.17,0,0,0,38,20a.19.19,0,0,0-.17-.2Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-50"
            d="M36.8,20.81a.16.16,0,0,0-.16.17.2.2,0,0,0,.17.2h0A.16.16,0,0,0,37,21a.2.2,0,0,0-.17-.2Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-51"
            d="M35.69,17.6a.16.16,0,0,0-.15.17.21.21,0,0,0,.17.21h0a.16.16,0,0,0,.15-.17.21.21,0,0,0-.17-.21Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-52"
            d="M35.69,18.27a.16.16,0,0,0-.15.17.21.21,0,0,0,.17.21h0a.16.16,0,0,0,.15-.17.21.21,0,0,0-.17-.21Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-53"
            d="M35.69,18.94a.16.16,0,0,0-.15.17.21.21,0,0,0,.17.21h0a.16.16,0,0,0,.15-.17.21.21,0,0,0-.17-.21Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-54"
            d="M36.21,18.86a.09.09,0,0,0-.08.09.11.11,0,0,0,.09.11h0a.09.09,0,0,0,.08-.09.11.11,0,0,0-.09-.11Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-55"
            d="M37.25,19a.09.09,0,0,0-.09.1.11.11,0,0,0,.09.11h0a.09.09,0,0,0,.09-.09.11.11,0,0,0-.09-.11Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-56"
            d="M35.69,19.61a.16.16,0,0,0-.15.17.21.21,0,0,0,.17.21h0a.16.16,0,0,0,.15-.17.21.21,0,0,0-.17-.21Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-57"
            d="M37.71,17.78a.17.17,0,0,0-.16.18.19.19,0,0,0,.17.2h0a.17.17,0,0,0,.16-.18.19.19,0,0,0-.17-.2Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-58"
            d="M37.71,18.45a.17.17,0,0,0-.16.18.19.19,0,0,0,.17.2h0a.17.17,0,0,0,.16-.18.19.19,0,0,0-.17-.2Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-59"
            d="M37.71,19.12a.17.17,0,0,0-.16.18.2.2,0,0,0,.17.2h0a.17.17,0,0,0,.16-.18.19.19,0,0,0-.17-.2Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-60"
            d="M37.71,19.79a.17.17,0,0,0-.16.18.2.2,0,0,0,.17.2h0a.17.17,0,0,0,.16-.18.19.19,0,0,0-.17-.2Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-61"
            d="M36.67,20.8a.16.16,0,0,0-.16.17.2.2,0,0,0,.17.2h0a.16.16,0,0,0,.16-.17.2.2,0,0,0-.17-.2Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-62"
            d="M41.61,17.55a.11.11,0,0,0-.11-.11q-1.52.06-3,0a.11.11,0,0,0-.11.11v2.73a.12.12,0,0,0,.11.12l.58,0a.11.11,0,0,1,.09.05l.67,1.11a.21.21,0,0,0,.36,0l.67-1.11a.11.11,0,0,1,.09-.05l.58,0a.12.12,0,0,0,.11-.12Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-63"
            d="M39,18a.18.18,0,0,1-.19.18.2.2,0,0,1-.19-.19.18.18,0,0,1,.19-.18A.2.2,0,0,1,39,18Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-64"
            d="M39,18.71a.18.18,0,0,1-.19.18.2.2,0,0,1-.19-.19.18.18,0,0,1,.19-.18A.2.2,0,0,1,39,18.71Z"
            transform="translate(-17.2 -0.64)"
          />
          <circle
            class="cls-65"
            cx="38.77"
            cy="19.37"
            r="0.19"
            transform="translate(1.16 56.97) rotate(-88.46)"
          />
          <circle
            class="cls-66"
            cx="39.33"
            cy="19.16"
            r="0.1"
            transform="matrix(0.02, -1, 1, 0.02, 2.15, 57.45)"
          />
          <circle
            class="cls-67"
            cx="40.47"
            cy="19.16"
            r="0.1"
            transform="translate(-17.51 0.02) rotate(-0.93)"
          />
          <path
            class="cls-68"
            d="M39,20a.18.18,0,0,1-.19.18.2.2,0,0,1-.19-.2.18.18,0,0,1,.19-.18A.2.2,0,0,1,39,20Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-69"
            d="M41.17,18a.2.2,0,0,1-.19.19A.18.18,0,0,1,40.8,18a.2.2,0,0,1,.19-.19A.18.18,0,0,1,41.17,18Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-70"
            d="M41.17,18.7a.2.2,0,0,1-.19.19.18.18,0,0,1-.19-.18.2.2,0,0,1,.19-.19A.18.18,0,0,1,41.17,18.7Z"
            transform="translate(-17.2 -0.64)"
          />
          <circle
            class="cls-71"
            cx="40.98"
            cy="19.37"
            r="0.19"
            transform="translate(-17.71 0.47) rotate(-1.54)"
          />
          <path
            class="cls-72"
            d="M41.17,20a.2.2,0,0,1-.19.2A.18.18,0,0,1,40.8,20a.2.2,0,0,1,.19-.19A.18.18,0,0,1,41.17,20Z"
            transform="translate(-17.2 -0.64)"
          />
          <circle class="cls-73" cx="22.65" cy="20.5" r="0.19" />
          <path
            class="cls-74"
            d="M40.57,18.56a.16.16,0,0,0,0-.2L40,17.69a.11.11,0,0,0-.17,0l-.57.67a.15.15,0,0,0,0,.2l.46.55a.09.09,0,0,1,0,.11l-.46.54a.16.16,0,0,0,0,.2l.57.68a.11.11,0,0,0,.17,0l.57-.68a.16.16,0,0,0,0-.2l-.46-.53a.09.09,0,0,1,0-.11Zm-1,0a.08.08,0,0,1,0-.11l.28-.33a.06.06,0,0,1,.09,0l.28.33a.08.08,0,0,1,0,.11l-.28.33a.06.06,0,0,1-.09,0Zm.64,1.28a.08.08,0,0,1,0,.11l-.28.33a.06.06,0,0,1-.09,0l-.28-.33a.08.08,0,0,1,0-.11l.28-.33a.06.06,0,0,1,.09,0Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-75"
            d="M39.06,18a.18.18,0,0,1-.19.18.19.19,0,0,1-.19-.19.18.18,0,0,1,.19-.18A.2.2,0,0,1,39.06,18Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-76"
            d="M39.06,18.69a.18.18,0,0,1-.19.18.2.2,0,0,1-.19-.2.18.18,0,0,1,.19-.18A.2.2,0,0,1,39.06,18.69Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-77"
            d="M39.06,19.36a.18.18,0,0,1-.19.18.19.19,0,0,1-.19-.19.18.18,0,0,1,.19-.18A.19.19,0,0,1,39.06,19.36Z"
            transform="translate(-17.2 -0.64)"
          />
          <circle
            class="cls-78"
            cx="39.43"
            cy="19.15"
            r="0.1"
            transform="translate(2.77 57.78) rotate(-89.53)"
          />
          <circle
            class="cls-79"
            cx="40.57"
            cy="19.14"
            r="0.1"
            transform="translate(-17.47 -0.06) rotate(-0.81)"
          />
          <circle
            class="cls-80"
            cx="38.87"
            cy="20.03"
            r="0.19"
            transform="translate(0.53 57.66) rotate(-88.35)"
          />
          <path
            class="cls-81"
            d="M41.27,18a.2.2,0,0,1-.19.19.18.18,0,0,1-.19-.18.2.2,0,0,1,.19-.19A.18.18,0,0,1,41.27,18Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-82"
            d="M41.27,18.68a.2.2,0,0,1-.19.2.18.18,0,0,1-.19-.18.2.2,0,0,1,.19-.19A.18.18,0,0,1,41.27,18.68Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-83"
            d="M41.27,19.35a.2.2,0,0,1-.19.19.18.18,0,0,1-.19-.18.19.19,0,0,1,.19-.19A.18.18,0,0,1,41.27,19.35Z"
            transform="translate(-17.2 -0.64)"
          />
          <circle
            class="cls-84"
            cx="41.08"
            cy="20.03"
            r="0.19"
            transform="translate(-17.76 0.55) rotate(-1.65)"
          />
          <circle class="cls-85" cx="22.75" cy="20.48" r="0.19" />
          <path
            class="cls-86"
            d="M40.66,18.54a.16.16,0,0,0,0-.2l-.57-.67a.11.11,0,0,0-.17,0l-.57.67a.16.16,0,0,0,0,.2l.46.55a.08.08,0,0,1,0,.11l-.46.54a.16.16,0,0,0,0,.2l.57.68a.11.11,0,0,0,.17,0l.57-.68a.15.15,0,0,0,0-.2l-.46-.54a.08.08,0,0,1,0-.11Zm-1,0a.09.09,0,0,1,0-.11l.28-.33a.06.06,0,0,1,.09,0l.28.33a.09.09,0,0,1,0,.11l-.28.33a.06.06,0,0,1-.09,0Zm.65,1.28a.09.09,0,0,1,0,.11l-.28.33a.06.06,0,0,1-.09,0l-.28-.33a.09.09,0,0,1,0-.11l.28-.33a.06.06,0,0,1,.09,0Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-87"
            d="M44.86,17.27a.1.1,0,0,0-.11-.1q-1.51.19-3,.26a.12.12,0,0,0-.11.12v2.73a.11.11,0,0,0,.11.11l.58,0a.1.1,0,0,1,.09,0l.67,1.05a.21.21,0,0,0,.36,0l.67-1.17a.13.13,0,0,1,.09-.06l.58-.07a.13.13,0,0,0,.11-.13Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-88"
            d="M42.17,17.8h0A.19.19,0,0,0,42,18a.17.17,0,0,0,.16.18h0a.19.19,0,0,0,.17-.2A.17.17,0,0,0,42.17,17.8Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-89"
            d="M42.17,18.47h0a.19.19,0,0,0-.17.2.17.17,0,0,0,.16.18h0a.19.19,0,0,0,.17-.2A.17.17,0,0,0,42.17,18.47Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-90"
            d="M42.17,19.14h0a.19.19,0,0,0-.17.2.17.17,0,0,0,.16.18h0a.19.19,0,0,0,.17-.2A.17.17,0,0,0,42.17,19.14Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-91"
            d="M42.66,19h0a.11.11,0,0,0-.09.11.09.09,0,0,0,.08.1h0a.11.11,0,0,0,.09-.11A.09.09,0,0,0,42.66,19Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-92"
            d="M43.66,18.87h0a.11.11,0,0,0-.09.11.08.08,0,0,0,.08.09h0a.11.11,0,0,0,.09-.11A.09.09,0,0,0,43.66,18.87Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-93"
            d="M42.17,19.81h0A.19.19,0,0,0,42,20a.17.17,0,0,0,.16.18h0a.19.19,0,0,0,.17-.2A.17.17,0,0,0,42.17,19.81Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-94"
            d="M44.11,17.61h0a.21.21,0,0,0-.16.21.16.16,0,0,0,.15.17h0a.21.21,0,0,0,.16-.21A.15.15,0,0,0,44.11,17.61Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-95"
            d="M44.11,18.28h0a.21.21,0,0,0-.16.21.16.16,0,0,0,.15.17h0a.21.21,0,0,0,.16-.21A.15.15,0,0,0,44.11,18.28Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-96"
            d="M44.11,19h0a.21.21,0,0,0-.16.21.16.16,0,0,0,.15.17h0a.21.21,0,0,0,.16-.21A.15.15,0,0,0,44.11,19Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-97"
            d="M44.11,19.62h0a.21.21,0,0,0-.16.21.16.16,0,0,0,.15.17h0a.21.21,0,0,0,.16-.21A.15.15,0,0,0,44.11,19.62Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-98"
            d="M43.12,20.82h0a.2.2,0,0,0-.16.2.16.16,0,0,0,.15.17h0a.2.2,0,0,0,.17-.2A.16.16,0,0,0,43.12,20.82Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-99"
            d="M42.28,17.78h0a.19.19,0,0,0-.17.2.17.17,0,0,0,.16.18h0a.19.19,0,0,0,.16-.2A.17.17,0,0,0,42.28,17.78Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-100"
            d="M42.28,18.45h0a.19.19,0,0,0-.17.2.17.17,0,0,0,.16.18h0a.19.19,0,0,0,.16-.2A.16.16,0,0,0,42.28,18.45Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-101"
            d="M42.28,19.12h0a.19.19,0,0,0-.17.2.17.17,0,0,0,.16.18h0a.19.19,0,0,0,.16-.2A.17.17,0,0,0,42.28,19.12Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-102"
            d="M42.77,19h0a.11.11,0,0,0-.09.11.09.09,0,0,0,.08.09h0a.1.1,0,0,0,.09-.11A.09.09,0,0,0,42.77,19Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-103"
            d="M43.77,18.86h0a.11.11,0,0,0-.09.11.08.08,0,0,0,.08.09h0a.11.11,0,0,0,.09-.11A.08.08,0,0,0,43.77,18.86Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-104"
            d="M42.28,19.79h0a.19.19,0,0,0-.17.2.17.17,0,0,0,.16.18h0a.19.19,0,0,0,.16-.2A.17.17,0,0,0,42.28,19.79Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-105"
            d="M44.22,17.6h0a.21.21,0,0,0-.17.21.16.16,0,0,0,.15.17h0a.21.21,0,0,0,.16-.21A.16.16,0,0,0,44.22,17.6Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-106"
            d="M44.22,18.27h0a.21.21,0,0,0-.17.21.16.16,0,0,0,.15.17h0a.21.21,0,0,0,.16-.21A.15.15,0,0,0,44.22,18.27Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-107"
            d="M44.22,18.94h0a.21.21,0,0,0-.17.21.16.16,0,0,0,.15.17h0a.21.21,0,0,0,.16-.21A.16.16,0,0,0,44.22,18.94Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-108"
            d="M44.22,19.61h0a.21.21,0,0,0-.17.21.16.16,0,0,0,.15.17h0a.21.21,0,0,0,.16-.21A.16.16,0,0,0,44.22,19.61Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-109"
            d="M43.23,20.8h0A.2.2,0,0,0,43,21a.16.16,0,0,0,.15.17h0a.2.2,0,0,0,.16-.2A.16.16,0,0,0,43.23,20.8Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-110"
            d="M43.33,19.06a.09.09,0,0,1,0-.11l.4-.59a.18.18,0,0,0,0-.2l-.49-.62a.09.09,0,0,0-.07,0,.1.1,0,0,0-.08,0l-.49.71a.17.17,0,0,0,0,.2L43,19a.09.09,0,0,1,0,.11l-.4.57a.18.18,0,0,0,0,.2l.49.63a.08.08,0,0,0,.07,0,.1.1,0,0,0,.08,0l.49-.73a.18.18,0,0,0,0-.2Zm-.45-.65a.1.1,0,0,1,0-.11l.24-.35,0,0,0,0,.24.3a.09.09,0,0,1,0,.11l-.24.35,0,0,0,0Zm.56,1.34-.24.35a.05.05,0,0,1,0,0l0,0-.24-.31a.1.1,0,0,1,0-.11l.24-.35a.06.06,0,0,1,0,0l0,0,.24.3A.09.09,0,0,1,43.44,19.74Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-111"
            d="M43.44,19a.1.1,0,0,1,0-.11l.4-.58a.17.17,0,0,0,0-.2l-.49-.62a.09.09,0,0,0-.07,0,.1.1,0,0,0-.08,0l-.49.72a.17.17,0,0,0,0,.2l.4.51a.09.09,0,0,1,0,.11l-.4.58a.17.17,0,0,0,0,.2l.49.63a.08.08,0,0,0,.07,0,.1.1,0,0,0,.08,0l.49-.73a.17.17,0,0,0,0-.2ZM43,18.39a.1.1,0,0,1,0-.11l.24-.35a.05.05,0,0,1,0,0l0,0,.24.3a.1.1,0,0,1,0,.11l-.24.35a.05.05,0,0,1,0,0l0,0Zm.56,1.34-.24.35,0,0,0,0L43,19.78a.1.1,0,0,1,0-.11l.24-.35a.06.06,0,0,1,0,0l0,0,.24.3A.1.1,0,0,1,43.55,19.73Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-112"
            d="M37,19.1A.09.09,0,0,1,37,19l.41-.51a.17.17,0,0,0,0-.2l-.51-.71a.11.11,0,0,0-.09,0,.09.09,0,0,0-.07,0l-.51.62a.17.17,0,0,0,0,.2l.41.58a.09.09,0,0,1,0,.11l-.41.5a.17.17,0,0,0,0,.2l.51.72a.1.1,0,0,0,.09,0,.09.09,0,0,0,.07,0l.51-.63a.17.17,0,0,0,0-.2Zm-.47-.74a.09.09,0,0,1,0-.11l.25-.3,0,0,0,0,.25.35a.09.09,0,0,1,0,.11l-.25.31,0,0a.06.06,0,0,1,0,0Zm.58,1.45-.25.3,0,0a.06.06,0,0,1,0,0l-.25-.35a.09.09,0,0,1,0-.11l.25-.3,0,0,0,0,.25.35A.09.09,0,0,1,37.16,19.81Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-113"
            d="M36.92,19.08a.09.09,0,0,1,0-.11l.41-.51a.17.17,0,0,0,0-.2l-.51-.71a.11.11,0,0,0-.09,0,.09.09,0,0,0-.07,0l-.51.62a.17.17,0,0,0,0,.2l.41.58a.09.09,0,0,1,0,.11l-.41.5a.17.17,0,0,0,0,.2l.51.72a.11.11,0,0,0,.09,0,.09.09,0,0,0,.07,0l.51-.63a.17.17,0,0,0,0-.2Zm-.47-.74a.09.09,0,0,1,0-.11l.25-.3,0,0a.06.06,0,0,1,0,0l.25.35a.09.09,0,0,1,0,.11l-.25.31,0,0,0,0ZM37,19.79l-.25.31,0,0,0,0-.25-.35a.09.09,0,0,1,0-.11l.25-.3,0,0,0,0,.25.35A.09.09,0,0,1,37,19.79Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-114"
            d="M40,12.44a1.48,1.48,0,0,0-.33,0l-.16,0-.2.08-.18.1a1.85,1.85,0,0,0-.7,1,2.25,2.25,0,0,1,.77-1l.18-.11.2-.08.2,0Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-115"
            d="M35.6,16.5h-.51v3.57h0a.13.13,0,0,0,.09.05h0l.06,0,.33,0Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-116"
            d="M43,21.45l0,0a.19.19,0,0,0,.06.06.2.2,0,0,0,.2,0,.24.24,0,0,0,.1-.09l.67-1.17a.12.12,0,0,1,.09-.06l.27,0V16.57H43Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-117"
            d="M42.22,16.5H39.67v4.9l.13.21a.21.21,0,0,0,.36,0l.67-1.11a.11.11,0,0,1,.09-.05l.58,0a.12.12,0,0,0,.1-.06.1.1,0,0,0,.1.06l.52,0Z"
            transform="translate(-17.2 -0.64)"
          />
          <ellipse class="cls-118" cx="22.77" cy="16.14" rx="4.89" ry="0.52" />
          <ellipse class="cls-119" cx="22.77" cy="16.12" rx="4.89" ry="0.52" />
          <ellipse class="cls-120" cx="22.77" cy="16.1" rx="4.89" ry="0.52" />
          <ellipse class="cls-121" cx="22.77" cy="16.08" rx="4.89" ry="0.52" />
          <ellipse class="cls-122" cx="22.77" cy="16.07" rx="4.89" ry="0.52" />
          <ellipse class="cls-123" cx="22.77" cy="16.05" rx="4.89" ry="0.52" />
          <ellipse class="cls-124" cx="22.77" cy="16.03" rx="4.89" ry="0.52" />
          <ellipse class="cls-125" cx="22.77" cy="16.01" rx="4.89" ry="0.52" />
          <ellipse class="cls-126" cx="22.77" cy="16" rx="4.89" ry="0.52" />
          <ellipse class="cls-127" cx="22.77" cy="15.98" rx="4.89" ry="0.52" />
          <ellipse class="cls-128" cx="22.77" cy="15.96" rx="4.89" ry="0.52" />
          <ellipse class="cls-129" cx="22.77" cy="15.94" rx="4.89" ry="0.52" />
          <ellipse class="cls-130" cx="22.77" cy="15.93" rx="4.89" ry="0.52" />
          <ellipse class="cls-131" cx="22.77" cy="15.91" rx="4.89" ry="0.52" />
          <ellipse class="cls-132" cx="22.77" cy="15.89" rx="4.89" ry="0.52" />
          <ellipse class="cls-133" cx="22.77" cy="15.87" rx="4.89" ry="0.52" />
          <ellipse class="cls-134" cx="22.77" cy="15.86" rx="4.89" ry="0.52" />
          <path
            class="cls-37"
            d="M36.87,16.71l-.37,0h-.28c-.2,0-.24,0-.09.06l.37,0h.28C37,16.76,37,16.73,36.87,16.71Zm.32-.45.39,0h.25c.22,0,.28,0,.14-.06l-.39,0h-.25C37.11,16.21,37,16.24,37.19,16.26Zm-1.2.15h.24l.4,0h0c.13,0,.06,0-.17-.06h-.23l-.4,0h0C35.69,16.37,35.77,16.4,36,16.42Zm.44.22a2.37,2.37,0,0,0-.43,0h-.18c-.24,0-.35,0-.24.06a2.42,2.42,0,0,0,.43,0h.18C36.42,16.69,36.53,16.66,36.43,16.64Zm-.72-.13h.13a2,2,0,0,0,.45,0c.07,0-.07,0-.32-.06h-.13a2.15,2.15,0,0,0-.45,0C35.32,16.48,35.46,16.5,35.71,16.51Zm.09.1c.25,0,.43,0,.4-.05a1.65,1.65,0,0,0-.46,0h-.07c-.25,0-.43,0-.4.05a1.62,1.62,0,0,0,.46,0Zm.7-.28h.66c.18,0,.17,0,0-.07h-.66C36.31,16.28,36.31,16.31,36.5,16.33Zm7.36.09c-.25,0-.39,0-.32.06a2,2,0,0,0,.45,0h.13c.25,0,.39,0,.32-.06a2.16,2.16,0,0,0-.45,0Zm-.66,0,.4,0h.24c.22,0,.3,0,.17-.06l-.4,0h-.23C43.14,16.35,43.07,16.38,43.2,16.4ZM42,16.27h.25l.39,0c.14,0,.08-.05-.14-.06h-.25l-.39,0C41.71,16.23,41.77,16.26,42,16.27Zm-.79,0h.16a2.34,2.34,0,0,0,.44,0c.09,0,0-.05-.28-.06h-.15a2.29,2.29,0,0,0-.44,0C40.84,16.2,41,16.23,41.21,16.24Zm1.46.09h.66c.18,0,.19,0,0-.07h-.66C42.49,16.28,42.49,16.31,42.67,16.33ZM38,16.21a2.37,2.37,0,0,0,.44,0h.16c.24,0,.37,0,.28-.06a2.31,2.31,0,0,0-.44,0h-.16C38.07,16.16,37.94,16.19,38,16.21Zm-.55.56h-.29l-.36,0c-.16,0-.13,0,.07.06h.3l.36,0C37.7,16.82,37.68,16.79,37.48,16.77Zm4.76.07a2.83,2.83,0,0,0-.42,0h-.2c-.23,0-.33,0-.21.06a2.62,2.62,0,0,0,.42,0H42C42.25,16.89,42.34,16.86,42.23,16.84Zm1.37-.14h-.28l-.37,0c-.16,0-.12,0,.09.06h.28l.37,0C43.85,16.75,43.81,16.72,43.61,16.7Zm-.6.08-.36,0h-.3c-.2,0-.23,0-.06.07l.36,0h.29C43.14,16.83,43.16,16.8,43,16.78Zm1.15-.26h-.06a1.65,1.65,0,0,0-.46,0h0s.14,0,.4.05h.07a1.59,1.59,0,0,0,.46,0S44.41,16.52,44.15,16.52Zm-.15.1h-.18a2.36,2.36,0,0,0-.43,0c-.1,0,0,0,.24.06h.18a2.43,2.43,0,0,0,.43,0C44.35,16.65,44.24,16.62,44,16.61Zm-2.66.27a1.89,1.89,0,0,0-.45,0h-.1c-.25,0-.41,0-.35.06a1.9,1.9,0,0,0,.45,0H41C41.24,16.93,41.4,16.91,41.34,16.88Zm-.89-.76H40.4a1.51,1.51,0,0,0-.46,0s.16,0,.41,0h.05a1.55,1.55,0,0,0,.46,0S40.71,16.13,40.45,16.13ZM39,16.85h-.1a1.87,1.87,0,0,0-.45,0c-.06,0,.1,0,.35.06h.1a1.9,1.9,0,0,0,.45,0C39.45,16.88,39.29,16.85,39,16.85Zm-.83,0H38a2.83,2.83,0,0,0-.42,0c-.11,0,0,.05.21.06H38a2.62,2.62,0,0,0,.42,0C38.54,16.86,38.44,16.83,38.21,16.82Zm1.7,0h0a1.25,1.25,0,0,0-.46,0,1.21,1.21,0,0,0,.46,0h0a1.21,1.21,0,0,0,.47,0A1.25,1.25,0,0,0,39.91,16.86Zm0-.69a1.51,1.51,0,0,0-.46,0h-.05c-.25,0-.44,0-.41.05a1.55,1.55,0,0,0,.46,0h.05C39.73,16.22,39.91,16.19,39.88,16.17Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-135"
            d="M36.51,16.29h.66c.18,0,.17,0,0-.06h-.66C36.32,16.25,36.32,16.27,36.51,16.29Zm.69-.07.39,0h.25c.22,0,.28,0,.14-.06l-.39,0h-.25C37.12,16.17,37.06,16.2,37.2,16.22Zm-1.39.35c.25,0,.43,0,.4-.05a1.62,1.62,0,0,0-.46,0h-.07c-.26,0-.43,0-.4.05a1.59,1.59,0,0,0,.46,0Zm.19-.19h.23a2.76,2.76,0,0,0,.4,0h0c.13,0,.06-.05-.17-.06h-.23a2.73,2.73,0,0,0-.4,0h0C35.7,16.34,35.78,16.36,36,16.38Zm2-.21a2.25,2.25,0,0,0,.44,0h.16c.24,0,.37,0,.29-.06a2.32,2.32,0,0,0-.44,0h-.15C38.07,16.12,37.95,16.15,38,16.17Zm-2.31.3h.13a2.06,2.06,0,0,0,.45,0c.07,0-.07-.05-.32-.06h-.13a2.13,2.13,0,0,0-.45,0C35.33,16.44,35.47,16.46,35.72,16.47Zm7.49-.11a2.75,2.75,0,0,0,.4,0h.23c.22,0,.3,0,.17-.06a2.73,2.73,0,0,0-.4,0h-.23C43.15,16.31,43.07,16.34,43.2,16.36Zm.66,0c-.25,0-.39,0-.32.06a2.08,2.08,0,0,0,.45,0h.13c.25,0,.39,0,.32-.06a2.11,2.11,0,0,0-.45,0Zm-7.43.22a2.49,2.49,0,0,0-.43,0h-.18c-.24,0-.34,0-.24.06a2.38,2.38,0,0,0,.43,0h.18C36.43,16.65,36.54,16.62,36.44,16.6Zm4.77-.4h.16a2.24,2.24,0,0,0,.44,0c.09,0,0,0-.28-.06h-.16a2.29,2.29,0,0,0-.44,0C40.84,16.16,41,16.19,41.21,16.2Zm.79,0h.25l.39,0c.14,0,.08,0-.14-.06h-.25l-.39,0C41.72,16.19,41.78,16.22,42,16.24Zm.68.05h.66c.19,0,.19,0,0-.07h-.66C42.5,16.24,42.5,16.27,42.68,16.29Zm.33.45-.36,0h-.3c-.2,0-.23,0-.06.07l.36,0h.29C43.14,16.79,43.17,16.76,43,16.74Zm-2.55-.65h-.05a1.54,1.54,0,0,0-.46,0s.16,0,.41.05h.05a1.53,1.53,0,0,0,.46,0S40.72,16.09,40.46,16.09Zm.89.76a1.9,1.9,0,0,0-.45,0h-.1c-.25,0-.41,0-.35.06a1.91,1.91,0,0,0,.45,0H41C41.25,16.89,41.41,16.87,41.35,16.84Zm-4.47-.17-.37,0h-.28c-.2,0-.24,0-.09.06l.37,0h.28C37,16.72,37,16.69,36.88,16.67Zm5.36.13a2.64,2.64,0,0,0-.42,0h-.2c-.23,0-.33,0-.21.06a2.64,2.64,0,0,0,.42,0H42C42.26,16.85,42.35,16.83,42.24,16.8Zm1.92-.33H44.1a1.63,1.63,0,0,0-.46,0h0s.14,0,.4.05h.07a1.61,1.61,0,0,0,.46,0S44.42,16.48,44.16,16.48Zm-.15.1h-.18a2.53,2.53,0,0,0-.43,0c-.1,0,0,.05.25.06h.18a2.37,2.37,0,0,0,.43,0C44.36,16.61,44.25,16.58,44,16.57Zm-.4.09h-.28l-.37,0c-.15,0-.11,0,.09.06h.28l.37,0C43.86,16.71,43.82,16.68,43.61,16.66Zm-3.69.16h0a1.23,1.23,0,0,0-.47,0,1.21,1.21,0,0,0,.47,0h0a1.21,1.21,0,0,0,.47,0A1.22,1.22,0,0,0,39.92,16.82Zm-2.43-.09h-.3l-.36,0c-.16,0-.13,0,.07.07h.29l.36,0C37.71,16.78,37.69,16.75,37.49,16.73Zm.74,0H38a2.64,2.64,0,0,0-.42,0c-.11,0,0,.05.21.06H38a2.65,2.65,0,0,0,.42,0C38.55,16.82,38.45,16.79,38.22,16.78Zm.83,0h-.1a1.89,1.89,0,0,0-.45,0c-.06,0,.1.05.35.06h.1a1.91,1.91,0,0,0,.45,0C39.45,16.84,39.3,16.82,39,16.81Zm.84-.68a1.55,1.55,0,0,0-.46,0h-.05c-.26,0-.44,0-.41.05a1.53,1.53,0,0,0,.46,0h.05C39.74,16.18,39.92,16.15,39.89,16.13Z"
            transform="translate(-17.2 -0.64)"
          />
          <ellipse class="cls-136" cx="22.63" cy="15.86" rx="3.1" ry="0.15" />
          <ellipse class="cls-137" cx="22.63" cy="15.85" rx="1.52" ry="0.07" />
          <path
            class="cls-138"
            d="M41.16,16.5a3.08,3.08,0,0,0,.5-1.73c0-1.38-.8-2.5-1.78-2.5s-1.78,1.12-1.78,2.5a3.08,3.08,0,0,0,.5,1.73h.26l-.07-.09a3,3,0,0,1,0-3.28,1.24,1.24,0,0,1,2.18,0,3,3,0,0,1,0,3.28l-.06.09Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-139"
            d="M41,16.49H41l0,0a3.11,3.11,0,0,0,0-3.38,1.32,1.32,0,0,0-2.3,0,3.11,3.11,0,0,0,0,3.38l0,0h-.07a3,3,0,0,1-.51-1.72c0-1.33.76-2.42,1.7-2.42s1.7,1.08,1.7,2.42A3,3,0,0,1,41,16.49Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-140"
            d="M38.23,14a3,3,0,0,0-.09.48,2.75,2.75,0,0,0,.07.9c0,.07,0,.15.06.22l.06.16c.08.16.14.33.23.48a4.78,4.78,0,0,1-.23-.71l0-.2a3.71,3.71,0,0,1-.06-.6c0-.07,0-.14,0-.2s0-.13,0-.2a2.56,2.56,0,0,1,.28-1,2.78,2.78,0,0,0-.25.54Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-140"
            d="M41.25,13.55a1.72,1.72,0,0,1,.11.28c0,.07,0,.13.06.2a3.54,3.54,0,0,1,0,1.46,2.57,2.57,0,0,0,.1-1.23c0-.07,0-.13,0-.2l0-.2c0-.07,0-.14-.08-.21l-.1-.21-.12-.2L41,13a1.48,1.48,0,0,0-.77-.54,1.59,1.59,0,0,1,.78.7l.11.18Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-114"
            d="M38.82,16.5l-.09-.13a2,2,0,0,1-.16-.27,3.09,3.09,0,0,1-.31-1.59c0-.13,0-.19,0-.19A3.26,3.26,0,0,0,38.49,16l.07.15.08.14.12.17Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-141"
            d="M41.13,16.5a3,3,0,0,0,.52-1.6c0-.09,0-.19,0-.28s0-.18,0-.27a3,3,0,0,0-.11-.52l-.08-.25a3.23,3.23,0,0,1,.18,1c0,.09,0,.17,0,.26a3.63,3.63,0,0,1-.53,1.65Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-142"
            d="M41.4,16.08l1.43.06h.24l.24,0,.22,0,.21.06a.55.55,0,0,1,.15.1c.11.11,0,.26-.13.3a5.39,5.39,0,0,0-.56.09c.17-.09.53-.06.46-.28a.25.25,0,0,0-.16-.18l-.18,0-.19,0-.19,0c-.46,0-.9-.1-1.36-.13Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-143"
            d="M38.14,16.09l-1.43.06h-.24l-.24,0-.22,0-.21.06a.59.59,0,0,0-.15.1c-.11.11,0,.26.13.3a5.13,5.13,0,0,1,.56.09c-.17-.09-.53-.06-.46-.28a.25.25,0,0,1,.16-.18l.18,0,.19,0,.19,0c.46,0,.9-.1,1.36-.13Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-144"
            d="M44.77,16.61a13.23,13.23,0,0,1-3.13.54l-.71.06a20.73,20.73,0,0,1-5.34-.41,24.83,24.83,0,0,0,5.19.32l.41,0,.55,0a15.73,15.73,0,0,0,2.81-.38Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-145"
            d="M35.49,20.82a.32.32,0,0,0-.06.24.51.51,0,0,0,.1.23,1.55,1.55,0,0,0,.55.42l.33.12.39.09.39.06.62.05,1.07.06.54,0a15.44,15.44,0,0,0,3.75-.28,3.91,3.91,0,0,0,.51-.17l.49-.22a.8.8,0,0,0,.37-.38.21.21,0,0,0-.07-.27c.5.16,1,.27,1.52.45l.35.13a12.51,12.51,0,0,1,1.69.79l.37.22a3.73,3.73,0,0,1,.35.26l-.42-.11-.25-.06a4.84,4.84,0,0,0-2.93-.11l-.4.11-.39.11-.39.11a19.79,19.79,0,0,1-5.12.37l-.52,0-.54,0L37.25,23l-.57-.1-.36-.08c-.35-.09-.69-.18-1-.23l-.51-.07a6.12,6.12,0,0,0-1.55,0l-.35.05-.42.08c-.22,0-.44.11-.66.18l-.33.11-.57.21-.57.22c-.52.21-1,.37-1.55.61.42-.28.85-.54,1.29-.78l.39-.22.79-.43.46-.23.46-.21.33-.14c.55-.22,1.11-.43,1.66-.63l.23-.07a4.62,4.62,0,0,0,1-.39Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-146"
            d="M39.24,22.44l-.71,0-.35,0a8.72,8.72,0,0,1-1.73-.26L36.14,22l-.3-.11-.29-.13-.14-.08c-.19-.09-.41-.27-.35-.51a.67.67,0,0,1,.15-.28L35,21a.54.54,0,0,0-.26.34c-.07.35.29.53.54.69l.15.08.31.14a4.52,4.52,0,0,0,.49.17l.3.06a16.9,16.9,0,0,0,1.91.27l.39,0a21.66,21.66,0,0,0,2.77-.05l.47-.05.47-.07.46-.08a6.94,6.94,0,0,0,.7-.18l.17-.06.22-.08A2.43,2.43,0,0,0,44.6,22l.15-.1a1,1,0,0,0,.32-.35.56.56,0,0,0-.26-.65.47.47,0,0,1,0,.56,1.28,1.28,0,0,1-.76.45l-.3.09-.31.08-.18,0a8,8,0,0,1-.89.14l-.47,0c-.59.05-1.18.07-1.77.09l-.52,0Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-147"
            d="M23.44,29.49a19,19,0,0,1,1.2-1.72l.44-.54a15.21,15.21,0,0,1,1.67-1.77c.3-.27.61-.53.93-.78L28,24.4l.37-.27a19,19,0,0,1,4.26-2.32l.52-.2a13,13,0,0,1,1.61-.46,3.81,3.81,0,0,1,.5,0c.07,0,.18.26.32.4l.21.17a2.71,2.71,0,0,0,.75.35l.43.12a12.07,12.07,0,0,0,2.09.29,12.1,12.1,0,0,1-2.21-.24l-.47-.09L35.85,22l-.29-.06a3,3,0,0,0-.51-.08h-.52l-.56.07-.56.11a14.64,14.64,0,0,0-5,2.35l-.33.23-.32.24q-.47.35-.9.75a13.68,13.68,0,0,0-1.6,1.71l-.51.63a21.33,21.33,0,0,0-3.16,5.69A18.58,18.58,0,0,1,23.16,30Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-148"
            d="M30.17,57.14a24,24,0,0,0,2.22,1,19.5,19.5,0,0,0,13.71.53A20,20,0,0,0,57,49.7a16,16,0,0,0,1.07-2.41A15.78,15.78,0,0,1,57,49.6,20.71,20.71,0,0,1,32.35,58C31,57.51,30.17,57.14,30.17,57.14Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-149"
            d="M21.3,33.72a20.3,20.3,0,0,0-1,7.71q.07.79.2,1.58a21.35,21.35,0,0,0,3.68,8.74c.31.42.63.84,1,1.24a16.33,16.33,0,0,0,3.54,3.12,17.58,17.58,0,0,1-3.27-3.19c-.32-.4-.62-.81-.91-1.23a24.72,24.72,0,0,1-3.78-8.43q-.16-.75-.27-1.52a17.47,17.47,0,0,1,.4-6.49C21.13,34.31,21.3,33.72,21.3,33.72Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-150"
            d="M47.23,22.15a16.42,16.42,0,0,1,5.72,3.26A19.05,19.05,0,0,1,54.49,27a20.57,20.57,0,0,1,4.78,9.71,19.39,19.39,0,0,1,.27,2.09,18.27,18.27,0,0,1-.17,4.19,17.14,17.14,0,0,1-.57,2.35,17.21,17.21,0,0,0,.42-2.13A17.49,17.49,0,0,0,59.38,41a24.47,24.47,0,0,0-.3-3.46c-.09-.57-.2-1.14-.33-1.71a21.5,21.5,0,0,0-3.9-8.23q-.56-.72-1.2-1.38A15.5,15.5,0,0,0,48,22.46Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-151"
            d="M38.94,21.86a2.29,2.29,0,0,1-1.12,1.42l-.24.12a1.19,1.19,0,0,1-.25.08h-.21a.52.52,0,0,1-.3-.09c-.16-.1-.07-.29,0-.41A4.66,4.66,0,0,0,38,21a.38.38,0,0,1,.24-.25.42.42,0,0,1,.28,0,.63.63,0,0,1,.39.46.84.84,0,0,1,0,.21,1.25,1.25,0,0,1,0,.26A1.12,1.12,0,0,1,38.94,21.86Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-152"
            d="M42.22,21.92a1.08,1.08,0,0,0,.15.25,1.56,1.56,0,0,1,.32.4c0,.09,0,.22-.09.24a1.28,1.28,0,0,1-1.24-.74,2.11,2.11,0,0,1-.13-.33c-.11-.36-.12-.78.25-.93a.3.3,0,0,1,.38.15,1.3,1.3,0,0,1,.17.47C42.08,21.58,42.15,21.75,42.22,21.92Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-153"
            d="M38.31,14.7a2.82,2.82,0,0,1,.48-1.61,1.24,1.24,0,0,1,2.18,0,2.83,2.83,0,0,1,.48,1.61h.21s0-.06,0-.08v0c0-.09,0-.18,0-.27s0-.18,0-.27a3,3,0,0,0-.1-.37l0-.13,0,.08a1.83,1.83,0,0,0-1.6-1.39c-1,0-1.77,1.1-1.78,2.47Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-154"
            d="M38.34,6.48c.1,10.75.18,6.76.18,6.76A1.59,1.59,0,0,1,41,13s0,4.12-.08-6.51Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-154"
            d="M38.67,6.48c0,10.82-.05,6.89-.05,6.89a1.67,1.67,0,0,1,2.48-.06s.06,4,.15-6.84Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-155"
            d="M38.69,6.35c0,7.76,0,1.8-.05,4.28h2.5c0-2.47.09,3.49.14-4.28Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-156"
            d="M41.13,10.75c-1.88-1.24-3.9.29-5.58,1.26-.19.11-.38.17-.55.27l-.5.24a39.07,39.07,0,0,1-7.81,2.08,8.86,8.86,0,0,0,8.12.33c.23-.12.45-.24.67-.38a16.31,16.31,0,0,0,2.68-2.42A2.91,2.91,0,0,1,41,11.19C41.13,11.18,41.15,10.94,41.13,10.75Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-157"
            d="M52.21,13.79l-.92-.2-.92-.23a32.46,32.46,0,0,1-4.89-1.45c-1.54-.68-3-1.58-4.71-1.69a6.86,6.86,0,0,0-1.06,0,.93.93,0,0,1-.23,1,3.9,3.9,0,0,1,1.17,0,2.06,2.06,0,0,1,1,.43c2,1.61,4,3.28,6.61,3.5a7.68,7.68,0,0,0,.9.06,7.26,7.26,0,0,0,.9-.05,6.33,6.33,0,0,0,3.29-1.35A5.61,5.61,0,0,1,52.21,13.79Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-158"
            d="M31.48,7.61a2,2,0,0,0-.24.42,2.22,2.22,0,0,0,.37,2.28,4,4,0,0,0,.36.38,5.35,5.35,0,0,0,3.65,1.26h.56l.48,0a17.55,17.55,0,0,1,3.63-.35,6.23,6.23,0,0,0-3.2-.91,2.2,2.2,0,0,1-.46-.11l-.67-.21a13.39,13.39,0,0,1-3.13-1.63l-.51-.36a3.31,3.31,0,0,1-.39-.45.88.88,0,0,1-.17-.6A1,1,0,0,1,32,6.71a1.92,1.92,0,0,0-.29.45C31.62,7.31,31.55,7.46,31.48,7.61Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-159"
            d="M46.81,5.15A3,3,0,0,1,48.18,6.4a3.86,3.86,0,0,0-2,.47l-.41.22-.36.25a4,4,0,0,0-.34.28,3.16,3.16,0,0,0-.31.36l-.29.3-.29.29-.31.28c-.2.18-.4.36-.61.53a4,4,0,0,1-3.63,1,4.15,4.15,0,0,0,.72-.36A5,5,0,0,0,41,9.59c.15-.14.3-.27.45-.43a23.07,23.07,0,0,1,2.67-3.08l.39-.3a3.78,3.78,0,0,1,2.08-.71A.85.85,0,0,1,46.81,5.15Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-160"
            d="M48.95,7.71a1.26,1.26,0,0,1-.25.7,5.6,5.6,0,0,1-.42.53A6,6,0,0,1,46,10.61q-.32.14-.66.25l-.64.19a11,11,0,0,1-4.72.16c-.12,0-.15-.16-.13-.27a.94.94,0,0,1,.42-.72c.8-.44,1.7-.62,2.52-1L43.23,9l.57-.34a6.28,6.28,0,0,0,3.13-3.39,1.22,1.22,0,0,0,.21.09l.23.12.21.13.21.16a3.76,3.76,0,0,1,.47.48,4,4,0,0,1,.66,1.17A1.18,1.18,0,0,1,48.95,7.71Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-161"
            d="M31.35,7.67a5.65,5.65,0,0,1,1.2-1.87A1.28,1.28,0,0,1,33,5.42a1.9,1.9,0,0,1,1.64,0A5,5,0,0,1,36,6.4l.39.4.49.56.48.57c.16.19.33.37.49.55l.49.55q.24.27.5.52c.17.17.35.33.54.49a4.62,4.62,0,0,0,1.23.69c.16.09.38.32.27.43-.56.55-1.5.35-2.22.16-.26-.08-.52-.18-.77-.28-1.26-.52-2.46-1.24-3.67-1.84l-.62-.36q-.61-.37-1.24-.77a1.05,1.05,0,0,0-1.07-.17c0-.07,0-.11,0-.16Z"
            transform="translate(-17.2 -0.64)"
          />
          <path
            class="cls-162"
            d="M38.73,9.37a3.32,3.32,0,0,0,.2.27,1,1,0,0,1,.08.15.8.8,0,0,1,0,.16,2.15,2.15,0,0,1,.05.26,2.2,2.2,0,0,1,0,.27,2.13,2.13,0,0,1,0,.36,1.84,1.84,0,0,1-.45.94l.26,0a15.44,15.44,0,0,0,2.15-.1l.25-.07a.73.73,0,0,0,.25-.11.7.7,0,0,0,.18-.18,1.7,1.7,0,0,0-.27-1.87l-.14-.08-.15-.06a17.64,17.64,0,0,0-2.48,0Z"
            transform="translate(-17.2 -0.64)"
          />
        </g>
      </g>
    </svg>
  );
}
