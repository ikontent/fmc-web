import Logo from "components/elements/Logo";
import SVGfacebook from "public/images/icons/facebook-circle-transparent.svg";
import SVGtwitter from "public/images/icons/twitter-circle-transparent.svg";
import SVGemail from "public/images/icons/mail-circle.svg";
import SVGclipboard from "public/images/icons/link-circle.svg";
// import OnClickTooltip from "components/elements/OnClickTooltip";
import { useRouter } from "next/router";

import { logEvent } from "utils/analytics";

const ShareIcons = ({ title, slug, path = "" }) => {
  const router = useRouter();
  const asPath = router.asPath;
  const url =
    title && slug && path
      ? `${process.env.NEXT_PUBLIC_OWN_URL}${path}`
      : asPath
      ? `${process.env.NEXT_PUBLIC_OWN_URL}${asPath}`
      : null;

  return (
    <nav className="share-icons">
      <div>
        <Logo
          className="facebook-icon"
          children={<SVGfacebook />}
          href={url ? `https://www.facebook.com/sharer.php?u=${url}` : "#"}
          sizeLogo={36}
          aria="Megosztás facebook-on ikon"
          isExternal
          onClick={() =>
            url &&
            logEvent(
              "ArticleShare",
              "Share",
              "Facebook",
              `https://www.facebook.com/sharer.php?u=${url}`
            )
          }
        />
      </div>
      <div>
        <Logo
          className="twitter-icon"
          children={<SVGtwitter />}
          href={
            url
              ? `https://twitter.com/intent/tweet?text=${title}&url=${url}`
              : "#"
          }
          sizeLogo={36}
          aria="Megosztás twitter-en ikon"
          isExternal
          imgFilePath="/images/icons/twitter-circle-transparent.svg"
          onClick={() =>
            url &&
            logEvent(
              "ArticleShare",
              "Share",
              "Twitter",
              `https://twitter.com/intent/tweet?text=${title}&url=${url}`
            )
          }
        />
      </div>
      <div>
        <Logo
          className="email-icon"
          children={<SVGemail />}
          href={url ? `mailto:?subject=${title}&body=${url}` : ""}
          sizeLogo={36}
          aria="Megosztás email-ben ikon"
          isExternal
          imgFilePath="/images/icons/mail-circle.svg"
          onClick={() =>
            url &&
            logEvent(
              "ArticleShare",
              "Share",
              "Email",
              `mailto:?subject=${title}&body=${url}`
            )
          }
        />
      </div>
      <div
        onClick={() => {
          navigator.clipboard.writeText(location.href);
          logEvent("ArticleURL", "Copy", title, location.href);
        }}
      >
        <Logo
          className="clipboard-icon"
          children={<SVGclipboard />}
          sizeLogo={36}
          aria="Webcím másolása vágólapra ikon"
          imgFilePath="/images/icons/link-circle.svg"
        />
      </div>
      {/* <OnClickTooltip
        className="clipboard"
        tooltipText="Webcím vágólapra másolva"
        seconds={2}
        onClickFn={() => {
          navigator.clipboard.writeText(location.href);
        }}
      >
        <Logo
          className="clipboard-icon"
          children={<SVGclipboard />}
          sizeLogo={36}
          aria="Webcím másolása vágólapra ikon"
          imgFilePath="/images/icons/link-circle.svg"
        />
      </OnClickTooltip> */}
    </nav>
  );
};

export default ShareIcons;
