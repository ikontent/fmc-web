import { useState } from "react";
import { Row, Col, Button } from "reactstrap";
import { ArticleTitle } from "components/elements/articles/ArticleComponents";
import ArticleCardHorizontal from "components/elements/articles/ArticleCardHorizontal";
import CustomDivider from "components/elements/CustomDivider";
import SVGdown from "public/images/icons/chevron-down-circle.svg";
import ArticleButtonRandom from "components/elements/articles/ArticleButtonRandom";

const SimilarArticles = ({ article }) => {
  const [expanded, setExpanded] = useState(false);

  const similarArticles = article.similarArticles
    ? expanded
      ? article.similarArticles
      : article.similarArticles.slice(0, 5)
    : [];

  return (
    <section className="similar-articles mt-50">
      <Row noGutters>
        <Col>
          <ArticleTitle title="Hasonló cikkek" />
        </Col>
      </Row>

      <Row noGutters className="mt-3">
        <Col className="middle-part">
          {similarArticles.map((similarArticle, index) => (
            <div key={index}>
              <ArticleCardHorizontal
                article={similarArticle}
                showTopic
                showTag
                showTagBoxOnTop
                showPublishedAt
              />
              {similarArticles.length - 1 > index && (
                <CustomDivider className="global-box-py" />
              )}
            </div>
          ))}
        </Col>
      </Row>
      <div className="global-box-py article-buttons">
        <Col tag="nav" md={6}>
          {!expanded &&
            article?.similarArticles &&
            article.similarArticles.length > 5 && (
              <Button
                tag="a"
                onClick={() => setExpanded(true)}
                block
                className="btn-outline btn-lg btn-void"
              >
                Továbbiak betöltése
                <SVGdown />
              </Button>
            )}
        </Col>
        <Col tag="nav" md={6}>
          <ArticleButtonRandom />
        </Col>
      </div>
    </section>
  );
};

export default SimilarArticles;
