import Router from "next/router";
import Logo from "components/elements/Logo";
import SVGfacebook from "public/images/icons/facebook-circle.svg";
import SVGinstagram from "public/images/icons/instagram-circle.svg";
import SVGtwitter from "public/images/icons/twitter-circle.svg";
import SVGyoutube from "public/images/icons/youtube-circle.svg";
import DarkModeSwitcher from "components/header/DarkModeSwitcher";

import { logEvent } from "utils/analytics";

const SocialIcons = ({ className, hideDarkModeSwitcher = false, position }) => {
  const handleRoute = (position, name, route) => {
    logEvent("SocialLink", "Open", position, name);
    Router.push(route);
  };

  return (
    <nav className={"social-icons" + (className ? ` ${className}` : "")}>
      <div>
        <Logo
          className="facebook-icon"
          children={<SVGfacebook />}
          href="https://www.facebook.com/fmc.fehervar/"
          sizeLogo={36}
          aria="Facebook ikon"
          isExternal
          onClick={() =>
            handleRoute(
              position,
              "Facebook",
              "https://www.facebook.com/fmc.fehervar/"
            )
          }
        />
        <Logo
          className="instagram-icon"
          children={<SVGinstagram />}
          href="https://www.instagram.com/fmc.hu/"
          sizeLogo={36}
          aria="Instagram ikon"
          isExternal
          onClick={() =>
            handleRoute(
              position,
              "Instagram",
              "https://www.instagram.com/fmc.hu/"
            )
          }
        />
      </div>
      <div>
        <Logo
          className="twitter-icon"
          children={<SVGtwitter />}
          href="https://twitter.com/fmcfehervar"
          sizeLogo={36}
          aria="Twitter ikon"
          isExternal
          onClick={() =>
            handleRoute(position, "Twitter", "https://twitter.com/fmcfehervar")
          }
        />
        <Logo
          className="youtube-icon"
          children={<SVGyoutube />}
          href="https://www.youtube.com/channel/UCsW_aZkayDIJePnDv-2Cygg"
          sizeLogo={36}
          aria="Youtube ikon"
          isExternal
          onClick={() =>
            handleRoute(
              position,
              "Youtube",
              "https://www.youtube.com/channel/UCsW_aZkayDIJePnDv-2Cygg"
            )
          }
        />
        {!hideDarkModeSwitcher && <DarkModeSwitcher />}
      </div>
    </nav>
  );
};

export default SocialIcons;
