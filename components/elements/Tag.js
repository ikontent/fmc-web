import Link from "next/link";

const TagCore = ({
  text,
  hasComma = false,
  hideUnderline = false,
  hasBlackText = false,
  isHighlighted = false,
  sensitivity = false,
  hasBackground = isHighlighted || sensitivity || false,
  className = "",
}) => {
  return text ? (
    <span
      className={
        "display-as-tag " +
        (hideUnderline ? " hide-underline" : "") +
        (hasBlackText ? " has-black-text" : "") +
        (hasBackground ? " has-background" : "") +
        (isHighlighted ? " is-highlighted" : "") +
        (sensitivity === "sensitiveContent" ? " is-sensitive" : "") +
        (sensitivity === "verySensitiveContent" ? " is-very-sensitive" : "") +
        (className ? " " + className : "")
      }
    >
      <span>{text}</span>
      {hasComma && <span>,</span>}
    </span>
  ) : (
    <></>
  );
};

const Tag = ({ href, ...propsToTag }) => {
  return href ? (
    <Link prefetch={false} href={href}>
      {/* Nextjs shows console error if <a> is inside <TagCore /> */}
      <a>
        <TagCore {...propsToTag} />
      </a>
    </Link>
  ) : (
    <TagCore {...propsToTag} />
  );
};

export default Tag;
