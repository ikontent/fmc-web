import Tag from "components/elements/Tag";
import { getWeirdFMCFormat } from "utils/dateStuffs";
import { pathFactory } from "utils/path-factory";

const Tags = ({
  className = "",
  article = {},
  showPublishedAt = false,
  showTopic = false,
  showTag = false,
  hideUnderline = false,
  hasBlackText = false,
  isHighlighted = false,
  sensitivity = article.sensitive || false,
  hasBackground = isHighlighted || sensitivity || false,
}) => {
  return (
    <span className={"tags " + className}>
      {showPublishedAt && article.published_at && (
        <Tag
          text={
            article.published_at
              ? getWeirdFMCFormat(new Date(article.published_at))
              : ""
          }
          hideUnderline
          hasBlackText
          className="timestamp"
        />
      )}

      <span className="tag-n-topic">
        {showTopic && article.topic?.name && article.topic.slug && (
          <Tag
            text={article.topic.name}
            hasComma={showTag && article.featuredTag?.tagName && !hasBackground}
            href={pathFactory.topic(article.topic)}
            hideUnderline={hideUnderline}
            hasBlackText={hasBlackText}
            hasBackground={hasBackground}
            isHighlighted={isHighlighted}
            sensitivity={sensitivity}
          />
        )}

        {showTag && article.featuredTag?.tagName && (
          <Tag
            text={article.featuredTag.tagName}
            href={pathFactory.tag(article.featuredTag)}
            hideUnderline={hideUnderline}
            hasBlackText={hasBlackText}
            hasBackground={hasBackground}
            isHighlighted={isHighlighted}
            sensitivity={sensitivity}
            className="featured-tag"
          />
        )}
      </span>
    </span>
  );
};

export default Tags;
