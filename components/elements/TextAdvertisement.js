import Link from "next/link";

export default function TextAdvertisement({
  advertisement = {},
  isSide = false,
}) {
  return (
    <Link prefetch={false} href={advertisement._URL || "#"}>
      <a className={`text-advertisement${isSide ? " side" : ""}`}>
        <div className="text-advertisement__inner">
          <h5>{advertisement.title}</h5>
        </div>
      </a>
    </Link>
  );
}
