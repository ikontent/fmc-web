import Tag from "components/elements/Tag";

const TimeStampAlert = ({ time }) => {
  const days = Math.floor(
    (new Date().getTime() - new Date(time).getTime()) / (1000 * 3600 * 24)
  );
  return (
    days > 29 && (
      <div className="mb-2 mb-md-3">
        <Tag
          text={
            (days < 366
              ? Math.floor(days / 30) + " hónapnál"
              : Math.floor(days / 365) + " évnél") + " régebbi cikk"
          }
          hasBackground
          hasBlackText
        />
      </div>
    )
  );
};

export default TimeStampAlert;
