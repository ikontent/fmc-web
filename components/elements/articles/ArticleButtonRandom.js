import { Button } from "reactstrap";
import SVGright from "public/images/icons/right-circle.svg";
import { pathFactory } from "utils/path-factory";
import api from "utils/api";
import { useRouter } from "next/router";
import { logEvent } from "utils/analytics";

export default function ArticleButtonRandom({ topicId, tagId }) {
  const router = useRouter();

  const handleClick = async () => {
    let article = null;
    if (topicId) {
      article = await api.getRandomArticleByTopic(topicId);
    } else if (tagId) {
      article = await api.getRandomArticleByTag(tagId);
    } else {
      article = await api.getRandomArticle();
    }
    if (!article) {
      return;
    }

    const url = pathFactory.article(article);
    router.push(url);

    logEvent("RelatedArticle", "Open", article.title, url || "");
  };

  return (
    <Button
      onClick={() => handleClick()}
      block
      className="btn-outline btn-lg btn-void random-button"
    >
      Ajánlj nekem cikket
      <SVGright />
    </Button>
  );
}
