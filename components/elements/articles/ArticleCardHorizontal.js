import Link from "next/link";
import { imagePathHandler, getImageUrl } from "utils/helpers";
import { pathFactory } from "utils/path-factory";
import { logEvent } from "utils/analytics";
import { OWN_URL } from "utils/store";
import { cropLead } from "utils/wysiwyg";
import Tags from "components/elements/Tags";

const ArticleCardHorizontal = ({
  article = {},
  showPublishedAt = false,
  showTopic = false,
  showTag = false,
  showTagBoxOnTop = false,
  showTagBoxWithin = false,
  hasConstantImageSize = false,
  hideImage = false,
}) => {
  const articleUrl = article._URL || pathFactory.article(article);

  const handleRoute = (e, title, route) => {
    logEvent(
      "SimilarArticle",
      "Open",
      title || "Cím nélküli cikk",
      `${OWN_URL}${route}`
    );
  };
  return (
    <div className="article-card-horizontal">
      {showTagBoxOnTop && (
        <Tags
          className="tags-ontop"
          article={article}
          showPublishedAt={showPublishedAt}
          showTopic={showTopic}
          showTag={showTag}
        />
      )}

      <Link
        prefetch={false}
        href={articleUrl}
        onClick={(e) => {
          handleRoute(e, article.title, articleUrl);
        }}
      >
        <a>
          <div className="article-card-body" lang="hu">
            {!hideImage && article.featuredImage && article.showFeaturedImage && (
              <div
                className={
                  "img-box" +
                  (showTagBoxWithin && article.sensitive
                    ? " sensitive-tag"
                    : "") +
                  (hasConstantImageSize ? "" : " enlarging")
                }
              >
                <img
                  className="img rounded Ximg-optimizer"
                  src={imagePathHandler(
                    getImageUrl(article.featuredImage, "thumbnail")
                  )}
                  alt={article.featuredImage.alt || "Fehérvár Médiacentrum fotója"}
                  loading="lazy"
                />

                {article.videoURL && (
                  <div className="play-box d-flex align-items-center justify-content-center">
                    <img
                      src="/images/icons/play-icon.svg"
                      alt="Play"
                      className="play-icon"
                      loading="lazy"
                    />
                  </div>
                )}
              </div>
            )}

            {showTagBoxWithin && (
              <Tags
                className="tags-within"
                article={article}
                showPublishedAt={showPublishedAt}
                showTopic={showTopic}
                showTag={showTag}
              />
            )}

            {article.title && (
              <>
                <span className="gap-title" />
                <span
                  className={
                    article.isHighlightedContent
                      ? "default-article-headline-highlighted"
                      : "default-article-headline-normal"
                  }
                >
                  {article.title}
                </span>
              </>
            )}
            {article.lead && (
              <span className="default-article-lead-normal">
                <span className="gap-lead" />
                {cropLead(article.lead)}
              </span>
            )}
          </div>
        </a>
      </Link>
    </div>
  );
};

export default ArticleCardHorizontal;
