import Link from "next/link";
import SVGLogo from "components/elements/SVGLogo";
import ArrowRight from "public/images/icons/arrow-right.svg";

/**
 * ArticleTitle component.
 * @component
 * @param {string} title - The title of the SidebarArticles components.
 * @param {string} [medium] - The medium (possible values: "fmc", "fehervar-tv", "vorosmarty-radio", "fehervar-hetilap", "szekesfehervar-hu").
 * @param {object} [action={}] - The action object should contain a text and a routing url.
 * @param {string} action.text - The text of the action.
 * @param {string} action.url - The url of the action.
 * @param {boolean} withoutUnderline - Without underline (border-bottom).
 * @param {boolean} withoutSpace - Without space (margin-bottom).
 */

const ArticleTitle = ({
  title,
  medium,
  action,
  withoutUnderline = false,
  withoutSpace = false,
}) => {
  return (
    <div
      className={`custom-article-components-article-title d-flex justify-content-between align-items-center${
        withoutUnderline ? "" : ` ${medium || "default"}-border-bottom-color`
      }${withoutSpace ? "" : " mb-1"}`}
    >
      <div className="d-flex align-items-center">
        {medium && <SVGLogo className="mr-2 icon-left" medium={medium} />}
        {title && <h5 className="mb-0 default-section-title">{title}</h5>}
      </div>

      {action && (
        <Link prefetch={false} href={action.url}>
          <a className="d-flex align-items-center action">
            <span className="mr-2 action-text">{action.text}</span>
            <ArrowRight />
          </a>
        </Link>
      )}
    </div>
  );
};

/**
 * ArticleButton component.
 * @component
 * @param {string} text - The text of the button.
 * @param {string} url - The path of the route.
 */
const ArticleButton = ({ text, url, target, className }) => {
  return (
    <div
      className={
        "custom-article-components-article-button" +
        (className ? " " + className : "")
      }
    >
      {url && (
        <Link prefetch={false} href={url}>
          <a className="btn d-flex align-items-center" target={target}>
            <span className="mr-2">{text}</span>
            <ArrowRight />
          </a>
        </Link>
      )}
    </div>
  );
};

export { ArticleTitle, ArticleButton };
