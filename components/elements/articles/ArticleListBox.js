import Link from "next/link";
import { getElapsedTimeText } from "utils/time-text";
import { imagePathHandler, getImageUrl } from "utils/helpers";
import TextAdvertisement from "components/elements/TextAdvertisement";
import { ArticleTitle } from "components/elements/articles/ArticleComponents";
import SVGInlineLogo from "components/elements/SVGInlineLogo";
import { cropLead } from "utils/wysiwyg";

// TODO: THIS COMMENT NEED TO BE REFRESHD
/**
 * Replacement of SidebarArticles component. Reusable component with 3 permutations.
 * Permutation 1: without numbers, just headlines.
 * Permutation 2: numbered headlines.
//  * Permutation 3: headlines with live-feed options.
 * Permutation 3: headlines with bullet points.
 * @component
 * @param {array} articles - The list of articles.
 * @param {string} title - The title of the box.
 * @param {boolean} [isNumberedContent=false] - If the articles are numbered, like "1.", "2.", ect.
//  * @param {boolean} [isLiveFeed=false] - If the articles are in live-feed mode.
 * @param {boolean} [isListedContent=false] - If the articles are listed with bullet point.
 * @param {date} [showPicture=false] - Permission to show the picture of the article.
 * @param {string} [medium] - The medium (possible values: "fmc", "fehervar-tv", "vorosmarty-radio", "fehervar-hetilap", "szekesfehervar-hu").
 * @param {object} [action="{}"] - The action object should contain a text and a routing url.
 * @param {string} action.text - The text of the action.
 * @param {string} action.url - The url of the action.
 * @param {boolean} withoutUnderline - Without title underline (border-bottom).
 * @param {boolean} withoutSpace - Without title space (margin-bottom).
*/

const ArticleListBox = ({
  articles,
  title,
  withLastSeparator = false,
  isNumberedContent = false,
  isLiveFeed = false,
  isListedContent = false,
  showPicture = false,
  medium,
  action,
  withoutUnderline = false,
  withoutSpace = false,
  customPositioned,
  showLead,
  className,
  hasSidebarSponsoredPost,
}) => {
  return (
    <div
      className={`article-list-box${
        withLastSeparator ? " with-last-separator" : ""
      }${customPositioned ? " border p-20 my-25" : ""}
      ${className ? " " + className : ""}
      ${isListedContent ? " useful-info" : ""}
      ${title === "Kedvencek" ? " favourites" : ""}`}
    >
      <div>
        <ArticleTitle
          title={title}
          medium={medium}
          action={action}
          withoutUnderline={withoutUnderline}
          withoutSpace={withoutSpace}
        />
      </div>

      {(articles || []).map((content, index) => (
        <div
          className={`article-list-box-item${
            isLiveFeed ? " live-feed-item" : ""
          }${isListedContent ? " listed-content-item" : ""}`}
          key={`content-${index}`}
        >
          {(title === "Legnépszerűbb" || title === "Kedvencek") &&
          !content.id ? (
            <div className="article-list-box__text-advertisement-wrapper">
              <TextAdvertisement advertisement={content} isSide />
            </div>
          ) : (
            <div className="d-flex">
              {isNumberedContent && (
                <div className="number">
                  {index + 1 - (index > 1 && hasSidebarSponsoredPost)}.
                </div>
              )}

              {isLiveFeed && (
                <div className="live-feed-item__left-side">
                  <span className="live-feed-item__time">
                    {content.created_at &&
                      getElapsedTimeText(
                        content.firstPublishedAt || content.published_at
                      )}
                  </span>
                </div>
              )}

              <div
                className={`d-flex ${
                  isListedContent ? "align-items-start" : "align-items-center"
                }`}
                style={{ width: "100%" }}
              >
                {isListedContent && (
                  <span className="d-inline bull mr-2">&bull;</span>
                )}

                <div
                  className="d-flex justify-content-between"
                  style={{ width: "100%" }}
                >
                  <div>
                    {isLiveFeed ? (
                      <Link
                        prefetch={false}
                        href={
                          content?._URL
                            ? content._URL
                            : `/?externalId=${content.id}`
                        }
                      >
                        <a className="d-block text-decoration-none" lang="hu">
                          <h6
                            className={`mb-0 cursor-pointer ${
                              showLead
                                ? "default-article-headline-normal"
                                : "default-article-headline-small"
                            }`}
                          >
                            <SVGInlineLogo
                              className="live-feed-item__logo"
                              medium={
                                content?.source == undefined
                                  ? "fmc"
                                  : content?.source == "tv"
                                  ? "fehervar-tv"
                                  : "vorosmarty-radio"
                              }
                            />

                            {content.title}
                          </h6>
                        </a>
                      </Link>
                    ) : (
                      <Link prefetch={false} href={content._URL || "#"}>
                        <a className="d-block text-decoration-none" lang="hu">
                          <h6
                            className={`mb-0 cursor-pointer ${
                              showLead
                                ? "default-article-headline-normal"
                                : "default-article-headline-small"
                            }`}
                          >
                            {content.title}
                          </h6>
                        </a>
                      </Link>
                    )}

                    {showLead && content.lead && (
                      <Link prefetch={false} href={content._URL || "#"}>
                        <a className="d-block text-decoration-none">
                          <p className="default-article-lead-normal mt-3">
                            {cropLead(content.lead)}
                          </p>
                        </a>
                      </Link>
                    )}
                  </div>

                  {content.featuredImage &&
                    showPicture &&
                    content.showFeaturedImage !== false && (
                      <Link prefetch={false} href={content._URL || "#"}>
                        <a
                          className={
                            "d-block text-decoration-none img-box" +
                            (customPositioned ? " mr-20 " : " ml-3 ")
                          }
                        >
                          <img
                            className="img rounded Ximg-optimizer"
                            src={imagePathHandler(
                              getImageUrl(content.featuredImage, "thumbnail")
                            )}
                            alt={
                              content.featuredImage.alt ||
                              "Fehérvár Médiacentrum fotója"
                            }
                            loading="lazy"
                          />

                          {content.videoURL && (
                            <div className="play-box d-flex align-items-center justify-content-center">
                              <img
                                src="/images/icons/play-icon.svg"
                                alt="Play"
                                className="play-icon"
                                loading="lazy"
                              />
                            </div>
                          )}
                        </a>
                      </Link>
                    )}
                </div>
              </div>
            </div>
          )}
        </div>
      ))}
    </div>
  );
};

export default ArticleListBox;
