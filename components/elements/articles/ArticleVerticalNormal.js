import Link from "next/link";
import { imagePathHandler, getImageUrl } from "utils/helpers";
import Tag from "components/elements/Tag";
import { cropLead } from "utils/wysiwyg";

/**
 * ArticleVerticalNormal component. The normal vertical article type with column (flex) layout.
 * @component
 * @param {object} article - The article object.
 */

const ArticleVerticalNormal = ({ article = {}, isNewsSectionOne = false }) => {
  const showImage = () => {
    if (!article.featuredImage) return false;
    if (isNewsSectionOne && article.frontPageDisplayStyle)
      return (
        article.frontPageDisplayStyle == 3 || article.frontPageDisplayStyle == 1
      );
    else return article.showFeaturedImage;
  };
  const showLead = () => {
    if (!article.lead) return false;
    if (isNewsSectionOne && article.frontPageDisplayStyle)
      return (
        article.frontPageDisplayStyle == 3 || article.frontPageDisplayStyle == 2
      );
    else return article.lead;
  };
  return (
    <div className="custom-article-vertical-normal">
      {showImage() && (
        <>
          {article.featuredTag && (
            <Tag
              text={article.featuredTag.tagName}
              href={
                "/tag/" +
                (article.featuredTag.slug || article.featuredTag.tagName)
              }
              isHighlighted
              sensitivity={article.sensitive}
            />
          )}
          <Link prefetch={false} href={article._URL || "#"}>
            <a className="d-block text-decoration-none img-box mb-3">
              <img
                src={imagePathHandler(
                  getImageUrl(article.featuredImage, "small")
                )}
                alt={
                  article.featuredImage.alt || "Fehérvár Médiacentrum fotója"
                }
                className="img rounded Ximg-optimizer"
                loading="lazy"
              />

              {article.videoURL && (
                <div className="play-box d-flex align-items-center justify-content-center">
                  <img
                    src="/images/icons/play-icon.svg"
                    alt="Play"
                    className="play-icon"
                    loading="lazy"
                  />
                </div>
              )}
            </a>
          </Link>
        </>
      )}

      {article.title && (
        <Link prefetch={false} href={article._URL || "#"}>
          <a className="d-block text-decoration-none mb-3">
            <h5
              className={`default-article-headline-normal mb-0 ${
                article.isHighlightedContent
                  ? "default-article-headline-highlighted d-inline"
                  : ""
              }`}
            >
              {article.title}
            </h5>
          </a>
        </Link>
      )}

      {showLead() && (
        <Link prefetch={false} href={article._URL || "#"}>
          <a className="d-block text-decoration-none">
            <p className="lead default-article-lead-normal">
              {cropLead(article.lead)}
            </p>
          </a>
        </Link>
      )}
    </div>
  );
};

export default ArticleVerticalNormal;
