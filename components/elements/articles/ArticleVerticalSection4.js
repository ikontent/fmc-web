import Link from "next/link";
import { imagePathHandler, getImageUrl } from "utils/helpers";
import Tags from "components/elements/Tags";
import { cropLead } from "utils/wysiwyg";

// TODO: THIS COMMENT NEED TO BE REFRESHD
/**
 * ArticleVerticalSection4 component. This article was created only for section 4.
 * @component
 * @param {object} article - The article object.
 * @param {object} [advertisement={}] - The advertisement object is advertisement is available.
 * @param {number} [advertisement.position=1] - The position of the advertisement. Counting should start at 0 (like index), ect.
 * @param {number} [advertisement.data] - The data of the advertisement.
 */

const ArticleVerticalSection4 = ({ advertisement = {}, article = {} }) => {
  return (
    <div className="custom-article-vertical-section-4 d-flex flex-column">
      <Tags article={article} showTopic showTag />

      {article.title && (
        <Link prefetch={false} href={article._URL || "#"}>
          <a className="d-block text-decoration-none">
            <h5
              className={`default-article-headline-normal mb-0 ${
                article.isHighlightedContent
                  ? "default-article-headline-highlighted d-inline"
                  : ""
              }`}
            >
              {article.title}
            </h5>
          </a>
        </Link>
      )}

      <div className="d-flex">
        {article.lead && (
          <Link prefetch={false} href={article._URL || "#"}>
            <a className="d-block text-decoration-none">
              <div className="lead default-article-lead-normal mt-3">
                {article.featuredImage && article.showFeaturedImage && (
                  <div className="img-box ml-4 mb-1">
                    <img
                      className="img rounded Ximg-optimizer"
                      src={imagePathHandler(
                        getImageUrl(article.featuredImage, "thumbnail")
                      )}
                      alt={article.featuredImage.alt || "Fehérvár Médiacentrum fotója"}
                      loading="lazy"
                    />

                    {article.videoURL && (
                      <div className="play-box d-flex align-items-center justify-content-center">
                        <img
                          src="/images/icons/play-icon.svg"
                          alt="Play"
                          className="play-icon"
                          loading="lazy"
                          loading="lazy"
                        />
                      </div>
                    )}
                  </div>
                )}
                {cropLead(article.lead)}
              </div>
            </a>
          </Link>
        )}
      </div>
    </div>
  );
};

export default ArticleVerticalSection4;
