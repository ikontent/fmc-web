import Link from "next/link";
import Tag from "components/elements/Tag";
import Tags from "components/elements/Tags";
import { cropLead } from "utils/wysiwyg";
import { imagePathHandler, getImageUrl } from "utils/helpers";
import { getYYYYMMMMDDFormat } from "utils/dateStuffs";
/**
 * ArticleVerticalWithHeading component. The normal vertical article type with headings.
 * @component
 * @param {object} article - The article.
 * @param {number} index - The index of the mapped element. Required.
 * @param {boolean} [onlyFirstImage=false] - Should be true if only the first image of the element is needed to be shown.
 * @param {boolean} [isItFehervariHetilap=false] - Should be true is this component used in the "Fehérvári Hetilap" section.
 */

const ArticleVerticalWithHeading = ({
  article,
  index,
  onlyFirstImage = false,
  isItFehervariHetilap = false,
  showPublishedAt = false,
}) => {
  return (
    <div
      className={`custom-article-vertical-with-heading${
        isItFehervariHetilap ? " fehervari-hetilap" : ""
      }`}
    >
      {article.featuredImage?.url &&
        !(onlyFirstImage && index) &&
        (article.showFeaturedImage || isItFehervariHetilap) && (
          <Link prefetch={false} href={article._URL || "#"}>
            <a className="d-block text-decoration-none img-box mb-3">
              <img
                className="img rounded Ximg-optimizer"
                src={imagePathHandler(
                  getImageUrl(article.featuredImage, "small")
                )}
                alt={
                  article.featuredImage.alt || "Fehérvár Médiacentrum fotója"
                }
                loading="lazy"
              />

              {article.videoURL && (
                <div className="play-box d-flex align-items-center justify-content-center">
                  <img
                    src="/images/icons/play-icon.svg"
                    alt="Play"
                    className="play-icon"
                    loading="lazy"
                  />
                </div>
              )}
            </a>
          </Link>
        )}

      <div
        className={`mb-2 tags ${
          isItFehervariHetilap ? "fehervari-hetilap-date" : ""
        }`}
      >
        {isItFehervariHetilap ? (
          article.created_at && (
            <Tag text={getYYYYMMMMDDFormat(article.created_at)} />
          )
        ) : (
          <Tags
            article={article}
            showTopic
            showTag
            showPublishedAt={showPublishedAt}
          />
        )}
      </div>

      {article.title && (
        <Link prefetch={false} href={article._URL || "#"}>
          <a
            className={`d-block text-decoration-none${
              article.isHighlightedContent ? " mb-3" : ""
            }`}
          >
            <h5
              className={`${
                isItFehervariHetilap
                  ? "default-article-headline-small"
                  : "default-article-headline-normal"
              }${
                article.isHighlightedContent
                  ? " default-article-headline-highlighted d-inline"
                  : ""
              }`}
            >
              {article.title}
            </h5>
          </a>
        </Link>
      )}

      {isItFehervariHetilap && article.lead && (
        <p className="lead default-article-lead-normal">
          {cropLead(article.lead)}
        </p>
      )}
    </div>
  );
};

export default ArticleVerticalWithHeading;
