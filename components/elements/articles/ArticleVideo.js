import Link from "next/link";
import { imagePathHandler, getImageUrl } from "utils/helpers";
import SVGInlineLogo from "components/elements/SVGInlineLogo";
import Tags from "components/elements/Tags";

const getMedium = (article) => {
  const mediaDivisions = [
    { tagName: "Fehérvár TV", slug: "fehervar-tv" },
    { tagName: "Fehérvár TV Híradó", slug: "fehervar-tv-hirado" },
    { tagName: "Vörösmarty Rádió", slug: "vorosmarty-radio" },
    { tagName: "FMC", slug: "fmc" },
    { tagName: "Fehérvár hetilap", slug: "fehervar-hetilap" },
    { tagName: "Székesfehérvár HU", slug: "szekesfehervar-hu" },
  ];
  if (article?.tags == undefined) {
    return "fmc";
  }
  let medium = article.tags.reduce(
    (acc, tag) => mediaDivisions.find((md) => md.tagName == tag.tagName) || acc,
    { tagName: "FMC", slug: "fmc" }
  );
  if (medium.slug == "fehervar-tv-hirado") {
    medium = mediaDivisions[0];
  }
  return medium.slug;
};

/**
 * ArticleVideo component.
 * @component
 * @param {object} article - The article object.
 */

const ArticleVideo = ({ article = {} }) => {
  return (
    <div className="custom-article-video">
      {article.featuredImage && (
        <Link prefetch={false} href={article._URL || "#"}>
          <a className="d-block img-box mb-3">
            <img
              className="img rounded Ximg-optimizer"
              src={
                typeof article.featuredImage == "string"
                  ? article.featuredImage
                  : imagePathHandler(getImageUrl(article.featuredImage))
              }
              alt={article.featuredImage.alt || "Fehérvár Médiacentrum fotója"}
              loading="lazy"
            />

            {article.videoURL && (
              <div className="play-box d-flex align-items-center justify-content-center">
                <img
                  src="/images/icons/play-icon.svg"
                  alt="Play"
                  className="play-icon"
                  loading="lazy"
                />
              </div>
            )}
          </a>
        </Link>
      )}

      <Tags className="mb-2" article={article} showTopic showTag />

      {article.title && (
        <Link prefetch={false} href={article._URL || "#"}>
          <a className="d-block">
            <h5 className="mb-0 default-article-headline-normal">
              <SVGInlineLogo
                className="mr-2"
                medium={article.medium || getMedium(article)}
              />
              {article.title}
            </h5>
          </a>
        </Link>
      )}
    </div>
  );
};

export default ArticleVideo;
