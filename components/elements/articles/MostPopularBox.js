import { useState, useEffect } from "react";
import ArticleListBox from "components/elements/articles/ArticleListBox";

const MostPopularBox = ({
  articles = [],
  withLastSeparator = false,
  isNumberedContent = false,
  className,
  exportedArticleId,
}) => {
  const hasSidebarSponsoredPost = !!articles[2]?.sponsoredId;
  const [mostPopular, setMostPopular] = useState([]);

  useEffect(() => {
    const sponsoredPostId = hasSidebarSponsoredPost && articles[2].sponsoredId;
    setMostPopular(
      (exportedArticleId
        ? articles.filter(
            ({ id }) => id !== exportedArticleId && id !== sponsoredPostId
          )
        : articles
      ).slice(0, hasSidebarSponsoredPost ? 6 : 5)
    );
  }, []);

  return (
    <ArticleListBox
      articles={mostPopular}
      title={"Legnépszerűbb"}
      withLastSeparator={withLastSeparator}
      isNumberedContent={isNumberedContent}
      className={className}
      hasSidebarSponsoredPost={hasSidebarSponsoredPost}
    />
  );
};

export default MostPopularBox;
