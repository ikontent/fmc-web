import { Container, Row, Col } from "reactstrap";
import Link from "next/link";
import Logo from "components/elements/Logo";
import SocialIcons from "components/elements/SocialIcons";
import SVGLogo from "components/elements/SVGLogo";
import Newsletter from "components/elements/Newsletter";
import FacebookPage from "components/elements/FacebookPage";
import { STRAPI_URL } from "utils/store";
import CustomDivider from "components/elements/CustomDivider";

const medium = [
  {
    medium: "fehervar-tv",
    name: "Fehérvár Televízió",
    url: "https://www.fehervartv.hu/",
  },
  {
    medium: "vorosmarty-radio",
    name: "Vörösmarty Rádió",
    url: "https://www.vorosmartyradio.hu/",
  },
  { medium: "fehervar-hetilap", name: "FehérVár hetilap", url: "/hetilap" },
  {
    medium: "szekesfehervar-hu",
    name: "szekesfehervar.hu",
    url: "https://www.szekesfehervar.hu/",
  },
];

const pages = [
  {
    url: "/page/rolunk",
    title: "Rólunk",
  },
  {
    url: "/page/kapcsolat",
    title: "Kapcsolat",
  },
  {
    url: "/page/impresszum",
    title: "Impresszum",
  },
  {
    url: "/page/mediaajanlat",
    title: "Médiaajánlat",
  },
  {
    url: "/page/adatvedelmi-nyilatkozat",
    title: "Adatvédelem",
  },
  {
    url: `${STRAPI_URL}/posts/rss`,
    title: "RSS",
    target: "_blank",
  },
  {
    url: "/page/panaszügyintézés",
    title: "Panaszügyintézés",
  },
];

const Footer = () => {
  return (
    <footer className="footer global-separated-t">
      <div className="footer__bg">
        <div className="global-limiter global-limiter-difference-px">
          <Container fluid className="global-section-px">
            <Row tag="nav" noGutters className="footer__body">
              <Col className="footer__logo-and-social" md={{ order: 1 }}>
                <Logo
                  className="fmc-logo"
                  children={<SVGLogo />}
                  href="/"
                  text="fmc.hu"
                  aria="fmc.hu logója"
                  sizeLogo={40}
                />
                <SocialIcons hideDarkModeSwitcher position="desktop-footer" />
              </Col>
              <Col className="footer__media-divisions" md={{ order: 2 }}>
                <h6>Fehérvár Médiacentrum</h6>
                <ul>
                  {medium.map((media, index) => (
                    <li key={`medium-${index}`}>
                      <Logo
                        sizeFontRem={0.875}
                        sizeLogo={50}
                        href={media.url}
                        isExternal
                        text={media.name}
                        aria={`${media.name} logója"`}
                      >
                        <SVGLogo medium={media.medium} />
                      </Logo>
                    </li>
                  ))}
                </ul>
              </Col>
              <Col className="footer__informations" md={{ order: 4 }}>
                <h6>Információk</h6>
                <ul>
                  {(pages || []).map((page, index) => (
                    <li key={`page-${index}`}>
                      <span className="d-inline bull mr-2">&bull;</span>
                      <Link prefetch={false} href={page.url}>
                        <a
                          target={page?.target || "_self"}
                          className="a-global"
                        >
                          {page.title}
                        </a>
                      </Link>
                    </li>
                  ))}
                </ul>
              </Col>
              <Col
                className="footer__facebook-and-newsletter"
                md={{ order: 3 }}
              >
                {/* <h6>Csatlakozz közösségünkhöz!</h6> */}
                <FacebookPage position="footer" />
                <CustomDivider className="global-box-pt" />
                <Newsletter className="newsletter-footer" />
              </Col>
            </Row>
            <Row noGutters className="footer__copyright global-box-py">
              <Col>
                Copyright &copy; 2021
                {new Date().getFullYear() > 2021
                  ? `–${new Date().getFullYear()}`
                  : ""}{" "}
                Fehérvár Médiacentrum, fmc.hu
              </Col>
            </Row>
          </Container>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
