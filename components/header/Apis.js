import LastnameApi from "components/header/LastnameApi";
import WeatherApi from "components/header/WeatherApi";

const Apis = () => {
  return (
    <div className="apis">
      <LastnameApi />
      <WeatherApi />
    </div>
  );
};

export default Apis;
