import { useState, useEffect } from "react";
import Logo from "components/elements/Logo";
import DarkModeIcon from "public/images/icons/contrast.svg";

import { logEvent } from "utils/analytics";

const DarkModeSwitcher = ({ className }) => {
  const [isDarkMode, setIsDarkMode] = useState(true);

  const handleDarkmode = () => {
    const isDarkMode = localStorage.getItem("isDarkMode") || false;

    if (isDarkMode) {
      localStorage.removeItem("isDarkMode");
      document.querySelector("html").classList.remove("dark-mode");
      setIsDarkMode(true);
    } else {
      localStorage.setItem("isDarkMode", true);
      document.querySelector("html").classList.add("dark-mode");
      setIsDarkMode(false);
    }

    logEvent("Darkmode", "Switch", isDarkMode ? "Day" : "Night");
  };

  return (
    <div className={`dark-mode-switcher ${className}`}>
      <Logo
        className="dark-mode-icon"
        children={<DarkModeIcon />}
        sizeLogo={24}
        aria="Dark mode icon"
        title={isDarkMode ? "Dark mode" : "Dark mode kikapcsolása"}
        onClick={handleDarkmode}
      />
    </div>
  );
};

export default DarkModeSwitcher;
