import { useState, useEffect, useRef } from "react";
import { Container } from "reactstrap";
import LogoFMC from "components/header/LogoFMC";
import SocialIcons from "components/elements/SocialIcons";
import Apis from "components/header/Apis";
import MenuDesktop from "components/header/MenuDesktop";
import MenuMobile from "components/header/MenuMobile";
import Banner from "components/customPositioned/Banner";
import { isBannerVisible } from "utils/banner";

const Header = ({ mainTopic, viewMode, slug, headerFeaturedTags, banner }) => {
  // ###################### setSticking #######################
  const [isSticking, setSticking] = useState();
  const stickyRef = useRef();
  // needs for SearchBox, MenuDesktop and MenuMobile
  const [inputIsFocused, setFocused] = useState(false);

  useEffect(() => {
    typeof window !== "undefined" &&
      (window.innerWidth < 769
        ? window.addEventListener("scroll", onScrollMobile, { passive: true })
        : window.addEventListener("scroll", onScrollDesktop, {
            passive: true,
          }));

    return () => {
      window.removeEventListener("scroll", onScrollMobile);
      window.removeEventListener("scroll", onScrollDesktop);
    };
  }, [viewMode]);

  function onScrollMobile() {
    setSticking(stickyRef.current.getBoundingClientRect().top <= -59);
  }

  function onScrollDesktop() {
    setSticking(stickyRef.current.getBoundingClientRect().top <= -99);
  }

  const menu = [
    {
      name: "Székesfehérvár",
      slug: "tag/szekesfehervar",
    },
    {
      name: "Fejér megye",
      slug: "tag/fejer-megye",
    },
    {
      name: "Hírek",
      slug: "hirek",
    },
    {
      name: "Kult",
      slug: "kult",
    },
    {
      name: "Életmód",
      slug: "eletmod",
    },
    {
      name: "T-T-Z",
      slug: "ttz",
    },
    {
      name: "Multimédia",
      slug: "multimedia",
    },
  ];

  // #########################################################

  return (
    <header id="header" ref={stickyRef}>
      <section className="top desktop">
        <div className="global-limiter global-limiter-difference-px">
          <Container fluid className="global-section-px">
            <LogoFMC sizeFontRem={2} /* sizeLogo={60} */ />
            <Apis />
            <SocialIcons position="header" />
          </Container>
        </div>
      </section>
      <section className="menu-desktop desktop">
        <div className="global-limiter global-limiter-difference-px">
          <Container fluid className="global-section-px">
            <MenuDesktop
              sticking={isSticking}
              mainTopic={mainTopic}
              inputIsFocused={inputIsFocused}
              setFocused={setFocused}
              tags={headerFeaturedTags}
              menu={menu}
            />
          </Container>
        </div>
      </section>
      <section className="menu-mobile mobile">
        <div className="menu-fixed">
          <div className="global-limiter global-limiter-difference-px">
            <div
              className={
                "menu-fixed-content global-section-px" +
                (isSticking ? " sticking" : "")
              }
            >
              <LogoFMC /* sizeLogo={44} */ />
              <SocialIcons
                className={isSticking ? "sticking" : ""}
                hideDarkModeSwitcher={true}
                position="sticky-header"
              />
              <MenuMobile
                className="menu-mobile-modal"
                inputIsFocused={inputIsFocused}
                setFocused={setFocused}
                tags={headerFeaturedTags}
                menu={menu}
              />
            </div>
          </div>
        </div>
        {viewMode === "mobile" && slug === "/" && isBannerVisible(banner) && (
          <div className="global-limiter global-limiter-difference-px">
            <div className="global-section-px">
              <div className="banner-wrapper">
                {banner?.content && (
                  <Banner banner={banner} position="Fejléc hirdetés" />
                )}
              </div>
            </div>
          </div>
        )}
        {viewMode === "mobile" && (slug === "/" || slug === "custom404") && (
          <div className="global-limiter global-limiter-difference-px">
            <div className="global-section-px">
              <Apis />
            </div>
          </div>
        )}
      </section>
    </header>
  );
};

export default Header;
