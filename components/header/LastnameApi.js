import { useState, useEffect } from "react";
import axios from "axios";
import { getNevnapAPIFormat } from "utils/dateStuffs";

const NameDayApi = () => {
  const [names, setNames] = useState("");
  const [errorMessage, setError] = useState("");

  useEffect(() => {
    getNames();
  }, []);

  const getNames = async () => {
    try {
      const response = await axios("https://nevnap.ikontent.app/today");
      setNames(response.data);
      setError("");
    } catch (e) {
      const error = e.response?.data.error;
      console.log(error);
      setError("Error message: " + error);
      setNames("");
    }
  };

  return (
    <div id="nameday-api">
      <div className="date">{getNevnapAPIFormat(new Date())}</div>
      <div className="name">
        {names.length ? names.join(", ") : errorMessage}
      </div>
    </div>
  );
};

export default NameDayApi;
