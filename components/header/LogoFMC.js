import Logo from "components/elements/Logo";
import SVGLogo from "components/elements/SVGLogo";
// import SVGLogo from "components/elements/SVGLogoPassover";

const LogoFMC = ({ sizeFontRem, sizeLogo }) => {
  return (
    <div className="logo-box logo-fmc">
      <nav width={sizeLogo} height={sizeLogo}>
        <Logo
          children={<SVGLogo />}
          sizeFontRem={sizeFontRem}
          sizeLogo={sizeLogo}
          href="/"
          text="fmc.hu"
          aria="fmc.hu logója"
        />
      </nav>
      <div className="slogan">Fehérvár összeköt</div>
    </div>
  );
};

export default LogoFMC;
