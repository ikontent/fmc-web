import Router from "next/router";
import LogoFMC from "components/header/LogoFMC";
import Tag from "components/elements/Tag";
import SearchBox from "components/header/SearchBox";
import DarkModeSwitcher from "components/header/DarkModeSwitcher";
import { OWN_URL } from "utils/store";
import { logEvent } from "utils/analytics";

const MenuDesktop = ({
  sticking,
  mainTopic = { slug: "" },
  inputIsFocused,
  setFocused,
  tags = [],
  menu,
}) => {
  const handleRoute = (e, title, route) => {
    e.preventDefault();
    logEvent("Menu", "Open", title || "Cím nélküli link", `${OWN_URL}${route}`);
    Router.push(route);
  };

  return (
    <nav>
      <div className={"logobox-textlinks-tags" + (sticking ? " sticking" : "")}>
        <div className={"logobox" + (sticking ? " sticking" : "")}>
          <LogoFMC sizeFontRem={1.5} /* sizeLogo={44} */ />
        </div>
        <div className="textlinks">
          {menu.map(({ name, slug }) => (
            <a
              style={{ cursor: "pointer" }}
              key={slug}
              onClick={(e) => handleRoute(e, name, `/${slug}`)}
              className={
                "a-global" +
                (slug === mainTopic.slug ||
                mainTopic.topics?.some((subTopic) => slug === subTopic.slug)
                  ? " current"
                  : "")
              }
            >
              {name}
            </a>
          ))}
        </div>
        <div className={"tags" + (sticking ? " sticking" : "")}>
          {tags.map((tag, i) => {
            return tag?.tagName && tag.href ? (
              <div
                key={i}
                onClick={() => {
                  logEvent("FeaturedTag", "Open", tag.tagName, tag.href);
                }}
              >
                <Tag key={i} text={tag.tagName} href={tag.href} hasBackground />
              </div>
            ) : null;
          })}
        </div>
      </div>
      <DarkModeSwitcher
        className={
          sticking ? "sticky-bar-switcher sticking" : "sticky-bar-switcher"
        }
      />
      <div
        className={
          "fade-if-search-is-focused" +
          (inputIsFocused ? " search-is-focused" : "")
        }
      />
      <SearchBox
        className="search-desktop desktop"
        inputIsFocused={inputIsFocused}
        setFocused={setFocused}
        sticking={sticking}
      />
    </nav>
  );
};

export default MenuDesktop;
