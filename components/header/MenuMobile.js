import { useState, useEffect } from "react";
import Link from "next/link";
import { Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import MenuHamburger from "public/images/icons/menu-hamburger.svg";
import LogoFMC from "components/header/LogoFMC";
import SocialIcons from "components/elements/SocialIcons";
import Tag from "components/elements/Tag";
import Newsletter from "components/elements/Newsletter";
import SearchBox from "components/header/SearchBox";
import { logEvent } from "utils/analytics";

const MenuMobile = ({
  className,
  inputIsFocused,
  setFocused,
  tags = [],
  menu,
}) => {
  const [modal, setModal] = useState(false);

  useEffect(() => {
    if (modal) {
      logEvent("HamburgerMenu", "Open");
    }
  }, [modal]);

  const toggle = () => setModal(!modal);

  const toggleWindowScrollbar = (overflowState) => {
    const html = document?.querySelector("html");
    if (html) {
      html.style.overflowY = overflowState;
    }
  };

  return (
    <nav className="menu-mobile-modal">
      <MenuHamburger onClick={toggle} className="hamburger" />
      <Modal
        isOpen={modal}
        className={className}
        onOpened={() => toggleWindowScrollbar("hidden")}
        onClosed={() => toggleWindowScrollbar("auto")}
      >
        <ModalHeader toggle={toggle}>
          <LogoFMC /* sizeLogo={44} */ />
        </ModalHeader>
        <ModalBody>
          <SearchBox
            className="search-mobile"
            type="search"
            text="Keresés"
            inputIsFocused={inputIsFocused}
            setFocused={setFocused}
          />

          <div className="tags">
            {tags.map((tag, i) => {
              return tag?.tagName && tag.href ? (
                <Tag key={i} text={tag.tagName} href={tag.href} hasBackground />
              ) : null;
            })}
          </div>

          {menu.map(({ name, slug }) => (
            <Link prefetch={false} href={"/" + slug} key={slug}>
              <a className="a-global">{name}</a>
            </Link>
          ))}

          <a href="#newsletter" className="scroll-to-newsletter-button">
            Hírlevél
          </a>
        </ModalBody>
        <ModalFooter>
          <SocialIcons position="mobile-footer" />
          <div id="newsletter">
            <Newsletter className="newsletter-footer" />
          </div>
        </ModalFooter>
      </Modal>
    </nav>
  );
};

export default MenuMobile;
