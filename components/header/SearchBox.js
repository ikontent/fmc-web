import { useState, useEffect, useRef } from "react";
import { useRouter } from "next/router";
import SVGsearch from "public/images/icons/search-icon.svg";

import { logEvent } from "utils/analytics";

const SearchBox = ({
  inputIsFocused,
  setFocused,
  className,
  type = "text",
  text = "",
  sticking,
}) => {
  const router = useRouter();
  const searchInput = useRef();
  const [query, setQuery] = useState("");

  useEffect(() => {
    // Event listener is utilised to handle mutual
    // state dependency between <button> and <input>
    if (searchInput.current) {
      const focusListener = () => {
        setFocused(document.activeElement === searchInput);
      };
      searchInput.current.addEventListener("focus", focusListener);
      searchInput.current.addEventListener("blur", focusListener);
    }
  }, []);

  function passQuery(e) {
    e.preventDefault();
    // retain native form validation (while prevent submit)
    searchInput.current.reportValidity();
    if (
      searchInput.current.checkValidity() &&
      query.length > 2 &&
      inputIsFocused
    ) {
      router.push({
        pathname: "/kereses",
        query: { s: query },
      });
      searchInput.current.blur();
      setFocused(false);
      setQuery("");
      logEvent("Search", "Enter", "", query);
    }
  }

  // #############################################

  const [menuIsLeavingSticky, setMenuIsLeavingSticky] = useState();

  useEffect(() => {
    setMenuIsLeavingSticky(!sticking);
    setTimeout(() => {
      setMenuIsLeavingSticky(sticking);
    }, 1000);
  }, [sticking]);

  // #############################################

  return (
    <form
      className={className ? " " + className : ""}
      onSubmit={passQuery}
      onClick={() => {
        searchInput.current.focus();
        setFocused(true);
      }}
    >
      <input
        ref={searchInput}
        type={type}
        pattern=".{3,}"
        title="Legkevesebb három karakter szükséges"
        placeholder="Keress rá bármire"
        value={query}
        onChange={(q) => setQuery(q.target.value)}
        onClick={passQuery}
      />
      <button
        type="button"
        onClick={() => {
          searchInput.current.focus();
          setFocused(true);
        }}
        className={
          (inputIsFocused ? "button-if-input-is-focused" : "") +
          (menuIsLeavingSticky ? "button-if-menu-is-leaving-sticky" : "")
        }
      >
        <SVGsearch />
        {text && !inputIsFocused && <span>{text}</span>}
      </button>
    </form>
  );
};

export default SearchBox;
