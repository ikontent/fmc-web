import { useState, useEffect } from "react";
import axios from "axios";
import { STRAPI_URL } from "utils/store";

const WeatherApi = () => {
  const [weather, setWeather] = useState("");
  const [errorMessage, setError] = useState("");

  useEffect(() => {
    getWeather();
  }, []);

  const getWeather = async () => {
    try {
      const response = await axios(`${STRAPI_URL}/weather-api`);
      setError("");
      if (
        "condition" in response?.data?.current &&
        "temp_c" in response?.data?.current
      ) {
        setWeather(response.data);
      }
    } catch (e) {
      let error = e.response ? e.response.data.error : e;
      setWeather("");
      setError(
        error
          ? (error.code || error.name) + ": " + error.message
          : "An error occured."
      );
    }
  };

  return (
    <div id="weather-api">
      <div className="weather">
        {weather ? (
          <a href={"/idojaras"}>
            <img
              src={"https:" + weather.current.condition.icon}
              alt="Időjárás"
              loading="lazy"
            />
            {Math.round(weather.current.temp_c)} °C
          </a>
        ) : (
          errorMessage
        )}
      </div>
    </div>
  );
};

export default WeatherApi;
