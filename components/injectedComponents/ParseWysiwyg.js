import { useEffect, useState } from "react";
import {
  injectComponents,
  prepareWysiwyg,
  slotsForInjection,
} from "utils/wysiwyg";

import { unstable_batchedUpdates } from "react-dom";

/**
 * .igallery        [data-gtype]  // type
 * .igallery        [data-data]   // image data
 * .poll            [data-poll]   // poll #id
 * .relatedarticle  [data-data]   // {link, title, lead, image, hasVideo}
 * .typeform-widget [data-url]    // url
 */

const ParseWysiwyg = ({
  wysiwygContent = "",
  className = "",
  banner,
  recommendedArticles,
  screenWidth,
  showRecommended = true,
  showBanner = true,
}) => {
  const [htmlContent, setHtmlContent] = useState(wysiwygContent);
  const [wysiwygReady, setWysiwygReady] = useState();
  const [dataToInject, setDataToInject] = useState({
    galleriesData: [],
    pollIds: [],
    relatedArticles: [],
    domHierarchy: [],
    banner,
    recommendedArticles,
  });
  const [wasInjected, setWasInjected] = useState({
    mostPopular: false,
    newsletter: false,
    banner: false,
  });

  useEffect(() => {
    unstable_batchedUpdates(() => {
      setHtmlContent(
        slotsForInjection({
          htmlContent: prepareWysiwyg(htmlContent),
          screenWidth,
          dataToInject,
          setDataToInject,
          wasInjected,
          setWasInjected,
        })
      );
      setWysiwygReady(true);
    });
    // Inject language into blockquote
    const blockquote = document.querySelectorAll("blockquote");
    if (blockquote.length) {
      blockquote.forEach((d) => {
        d.lang = "hu";
      });
    }
  }, []);

  useEffect(() => {
    if (wysiwygReady) {
      injectComponents({
        dataToInject,
        wasInjected,
        showRecommended,
        showBanner,
      });
      (function () {
        var qs,
          js,
          q,
          s,
          d = document,
          gi = d.getElementById,
          ce = d.createElement,
          gt = d.getElementsByTagName,
          id = "typef_orm",
          b = "https://embed.typeform.com/";
        if (!gi.call(d, id)) {
          js = ce.call(d, "script");
          js.id = id;
          js.src = b + "embed.js";
          q = gt.call(d, "script")[0];
          q.parentNode.insertBefore(js, q);
        }
      })();
    }
  }, [htmlContent, wysiwygReady]);

  return (
    htmlContent && (
      <div
        className={`wysiwyg-content ${className}`}
        dangerouslySetInnerHTML={{
          __html: htmlContent,
        }}
      ></div>
    )
  );
};

export default ParseWysiwyg;
