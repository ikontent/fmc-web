import { useRef, useEffect } from "react";

const defaultOptions = {
  // ...
};

export default function Masonry({ children, className, style, options }) {
  const container = useRef(null);

  useEffect(async () => {
    let masonry;
    if (container.current) {
      const MasonryLayout = (await import("masonry-layout")).default;
      masonry = new MasonryLayout(container.current, {
        ...defaultOptions,
        ...options
      });
      masonry.once("layoutComplete", () => {
        container.current.style.opacity =
          style && (style.opacity || style.opacity === 0) ? style.opacity : "";
      });
      masonry.layout();
    }
    return () => {
      if (masonry) {
        masonry.destroy();
      }
    };
  }, [container]);

  return (
    <div
      ref={container}
      className={className}
      style={{
        ...style,
        opacity: 0
      }}
    >
      {children}
    </div>
  );
}
