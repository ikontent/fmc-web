import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { Modal, ModalHeader, ModalBody, UncontrolledAlert } from "reactstrap";
import api from "utils/api";
import HtmlHead from "components/elements/HtmlHead";
import SVGLogoSprite from "components/elements/SVGLogoSprite";
import Header from "components/header/Header";
import Footer from "components/footer/Footer";
import BreakingNews from "components/elements/BreakingNews";
import SVGInlineLogo from "components/elements/SVGInlineLogo";
import Banner from "components/customPositioned/Banner";
import BannerWrapperDesktop from "components/customPositioned/BannerWrapperDesktop";

import { getWeirdFMCFormat } from "utils/dateStuffs";

const Master = ({
  children,
  mainTopic,
  meta,
  headerFeaturedTags,
  banners,
  isPreview = false,
  isFrontPagePreview = false,
  setScreenWidth = () => {},
}) => {
  const [isDarkMode, setIsDarkMode] = useState(false);
  const router = useRouter();
  const [slug, setSlug] = useState();
  const [viewMode, setViewMode] = useState();

  useEffect(() => {
    window.addEventListener("resize", onResize, { passive: true });
    onResize();

    setSlug(router.isReady && router.asPath.replace(/\/([\w\d-]+)\/?.*/, "$1"));

    return () => {
      window.removeEventListener("resize", onResize);
    };
  }, []);

  function onResize() {
    setViewMode(window.innerWidth < 768 ? "mobile" : "desktop");
    setScreenWidth(() => window.innerWidth);
  }

  const [externalPost, setExternalPost] = useState(null);
  useEffect(async () => {
    try {
      if (router?.query?.externalId) {
        const externalNews = await api.getExternalNews(router.query.externalId);
        if (externalNews?.sourceUrl) {
          let videoId = externalNews.sourceUrl.substring(
            externalNews.sourceUrl.indexOf("&vid=") + 5
          );
          videoId = videoId.substring(0, videoId.indexOf("&"));
          externalNews.videoId = videoId;
        }
        setExternalPost(externalNews);
      }
    } catch (e) {
      console.log(e);
    }
  }, [router?.query?.externalId]);
  return (
    <div id="master-layout" className={isDarkMode ? "dark-mode" : ""}>
      <HtmlHead {...meta} />
      <SVGLogoSprite />
      {isFrontPagePreview && (
        <UncontrolledAlert className="preview-alert">
          Ez egy előnézeti oldal. A főoldal még nincs publikálva. Kérlek ne oszd
          meg az url-jét.
        </UncontrolledAlert>
      )}
      {isPreview && (
        <UncontrolledAlert className="preview-alert">
          Ez egy előnézeti oldal. A cikk még nincs publikálva. Kérlek ne oszd
          meg ennek a cikknek az url-jét.
        </UncontrolledAlert>
      )}

      <div
        className={
          banners?.common?.wrapper?.desktop?.length > 0
            ? "master-banner-wrapper"
            : ""
        }
      >
        <BannerWrapperDesktop banners={banners} />

        <Header
          mainTopic={mainTopic}
          viewMode={viewMode}
          slug={slug}
          headerFeaturedTags={headerFeaturedTags}
          banner={
            (banners?.common?.header?.content && banners.common.header) ||
            banners?.frontPage
          }
        />

        <div>
          <BreakingNews viewMode={viewMode} slug={slug} />

          {banners?.common?.wrapper?.mobile?.length > 0 ? (
            <Banner
              className="wrapper mobile"
              banner={{
                content: banners?.common.wrapper.mobile,
                url: banners?.common.wrapper.url,
                caption: null,
              }}
              position="Beöltöztetös hirdetés"
            />
          ) : (
            <></>
          )}

          <div className="global-limiter global-limiter-difference-px master-children">
            {children}
          </div>
        </div>

        <Footer />
      </div>

      {externalPost && (
        <Modal
          isOpen={!!externalPost}
          toggle={() => setExternalPost(null)}
          size={
            externalPost.videoId || externalPost.external_uuid ? "lg" : "md"
          }
        >
          <ModalHeader toggle={() => setExternalPost(null)}>
            {externalPost.title}
          </ModalHeader>
          <ModalBody>
            <div className="mb-3">
              <span className="float-left">
                <SVGInlineLogo
                  className="live-feed-item__logo"
                  medium={
                    externalPost?.source == "tv"
                      ? "fehervar-tv"
                      : externalPost?.source == "vorosmarty_yt" ||
                        externalPost?.source == "radio"
                      ? "vorosmarty-radio"
                      : "fmc"
                  }
                />
                <span className="ml-2">
                  {externalPost?.source == undefined
                    ? "FMC"
                    : externalPost?.source == "tv"
                    ? "Fehérvár TV"
                    : externalPost?.source == "vorosmarty_yt" ||
                      externalPost?.source == "radio"
                    ? "Vörösmarty Rádió"
                    : "FMC"}
                </span>
              </span>
              <span className="float-right">
                {getWeirdFMCFormat(new Date(externalPost.published_at))}
              </span>
              <span className="clearfix"></span>
            </div>
            <div
              className="external-article-content"
              dangerouslySetInnerHTML={{
                __html: externalPost.text,
              }}
            ></div>
            {externalPost?.source == "tv" && externalPost.videoId && (
              <video width="100%" controls autoPlay>
                <source
                  src={`https://static.fehervartv.neosoft.hu/videolibrary/video_mp4/${externalPost.videoId}.mp4`}
                  type="video/mp4"
                />
              </video>
            )}
            {(externalPost?.source == "vorosmarty_yt" ||
              externalPost?.source == "fmc_yt") && (
              <iframe
                className="mt-3"
                width="100%"
                height="431"
                src={`https://www.youtube.com/embed/${externalPost.external_uuid}`}
                frameborder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen
              ></iframe>
            )}
          </ModalBody>
        </Modal>
      )}
    </div>
  );
};

export default Master;
