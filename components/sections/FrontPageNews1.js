import { Col } from "reactstrap";
import Masonry from "components/layouts/Masonry";
import ArticleVerticalBig from "components/elements/articles/ArticleVerticalBig";
import ArticleVerticalNormal from "components/elements/articles/ArticleVerticalNormal";
import TextAdvertisement from "components/elements/TextAdvertisement";

export default function FrontPageNews1({ articleData, className }) {
  return (
    <Col
      tag="section"
      className={
        "front-page-news-1 global-box-px global-separated-xl-x" +
        (className ? " " + className : "")
      }
      xs={{ order: 1 }}
      xl={{ size: 6, order: 2 }}
    >
      {(articleData || []).slice(0, 1).map((article, index) => (
        <div
          key={`front-page-news-1-article-${index}`}
          className="front-page-news-1__leading-article"
        >
          <ArticleVerticalBig article={article} isNewsSectionOne={true} />
        </div>
      ))}

      <Masonry
        className="front-page-news-1__masonry"
        options={{
          columnWidth: ".front-page-news-1__masonry-column",
          gutter: ".front-page-news-1__masonry-gutter",
          // stamp: ".front-page-news-1__masonry-stamp",
          itemSelector: ".front-page-news-1__masonry-item",
        }}
      >
        <div className="front-page-news-1__masonry-column"></div>
        <div className="front-page-news-1__masonry-gutter"></div>
        {/* <div className="front-page-news-1__masonry-stamp"></div> */}
        {(articleData || []).slice(1).map((article, index) => (
          <div
            key={`front-page-news-1-article-${index + 1}`}
            className="front-page-news-1__masonry-item"
          >
            {article.isSponsoredContent ? (
              <div className="front-page-news-1__text-advertisement-wrapper">
                <TextAdvertisement advertisement={article} />
              </div>
            ) : (
              <ArticleVerticalNormal
                article={article}
                isNewsSectionOne={true}
              />
            )}
          </div>
        ))}
      </Masonry>
    </Col>
  );
}
