import { Col } from "reactstrap";
import Masonry from "components/layouts/Masonry";
import TextAdvertisement from "components/elements/TextAdvertisement";
import ArticleCardHorizontal from "components/elements/articles/ArticleCardHorizontal";

export default function FrontPageNews2({ articleData, advertisement }) {
  const contents = [...(articleData || [])];
  return (
    <Col
      tag="section"
      className="front-page-news-2 global-box-px global-separated-xl-x"
      xs={{ order: 1 }}
      xl={{ size: 6, order: 2 }}
    >
      <Masonry
        className="front-page-news-2__masonry"
        options={{
          columnWidth: ".front-page-news-2__masonry-column",
          gutter: ".front-page-news-2__masonry-gutter",
          // stamp: ".front-page-news-2__masonry-stamp",
          itemSelector: ".front-page-news-2__masonry-item",
        }}
      >
        <div className="front-page-news-2__masonry-column"></div>
        <div className="front-page-news-2__masonry-gutter"></div>
        {/* <div className="front-page-news-2__masonry-stamp"></div> */}
        {(contents || []).map((content, index) => (
          <div
            key={`content-${index + 1}`}
            className="front-page-news-2__masonry-item"
          >
            {content.isSponsoredContent ? (
              <div className="front-page-news-2__text-advertisement-wrapper">
                <TextAdvertisement advertisement={content} />
              </div>
            ) : (
              <ArticleCardHorizontal
                article={content}
                showTopic
                showTag
                showTagBoxOnTop
                hasConstantImageSize
              />
            )}
          </div>
        ))}
      </Masonry>
    </Col>
  );
}
