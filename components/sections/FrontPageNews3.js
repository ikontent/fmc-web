import { Col } from "reactstrap";
import Masonry from "components/layouts/Masonry";
import FacebookPage from "components/elements/FacebookPage";
import ArticleVerticalSection4 from "components/elements/articles/ArticleVerticalSection4";
import TextAdvertisement from "components/elements/TextAdvertisement";

export default function FrontPageNews3({ articleData }) {
  return (
    <Col className="front-page-news-3">
      <Masonry
        className="front-page-news-3__masonry"
        options={{
          columnWidth: ".front-page-news-3__masonry-column",
          gutter: ".front-page-news-3__masonry-gutter",
          stamp: ".front-page-news-3__masonry-stamp",
          itemSelector: ".front-page-news-3__masonry-item",
        }}
      >
        <div className="front-page-news-3__masonry-column"></div>
        <div className="front-page-news-3__masonry-gutter"></div>
        <div className="front-page-news-3__masonry-stamp">
          <FacebookPage position="frontpage" />
        </div>
        {(articleData || []).map((article, index) => (
          <div
            key={`front-page-news-3-article-${index}`}
            className="front-page-news-3__masonry-item"
          >
            {article.isSponsoredContent ? (
              <div className="front-page-news-3__text-advertisement-wrapper">
                <TextAdvertisement advertisement={article} />
              </div>
            ) : (
              <ArticleVerticalSection4 article={article} />
            )}
          </div>
        ))}
      </Masonry>
    </Col>
  );
}
