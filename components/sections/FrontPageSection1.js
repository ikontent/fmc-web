import { Row, Col } from "reactstrap";
import ArticleListBox from "components/elements/articles/ArticleListBox";
import MostPopularBox from "components/elements/articles/MostPopularBox";
import CustomDivider from "components/elements/CustomDivider";
import Banner from "components/customPositioned/Banner";
import { ArticleButton } from "components/elements/articles/ArticleComponents";
import FrontPageNews1 from "components/sections/FrontPageNews1";
import { isBannerVisible } from "utils/banner";

export default function FrontPageSection1({
  fehervarArticles,
  banners,
  freshArticles,
  feedArticles,
  mostPopularArticles,
  recommendedArticles,
}) {
  return (
    <Row
      tag="section"
      noGutters
      className="section1 global-section-py global-section-difference-px"
    >
      <FrontPageNews1
        articleData={feedArticles.slice(0, 5)}
        className="mobile"
        xs={{ order: 1 }}
      />

      <Col
        tag="aside"
        className="left-aside global-box-xs-pt global-box-reset-xl-pt global-box-px d-flex flex-column flex-md-column-reverse"
        xs={{ order: 2 }}
        md={{ size: 6, order: 2 }}
        xl={{ size: 3, order: 1 }}
      >
        {fehervarArticles?.length > 0 && (
          <>
            <CustomDivider className="mobile global-box-pb" />
            <div className="global-box-pb sticky-box">
              <div className="sticky">
                <ArticleListBox
                  title="Fehérvárról"
                  articles={fehervarArticles}
                  showPicture
                  className="global-box-pb"
                />
                <ArticleButton
                  text="Még több cikk"
                  url={"/tag/szekesfehervar"}
                />
              </div>
              {isBannerVisible(banners?.sidebarMiddle) && (
                <div className="global-box-pt">
                  <Banner
                    className="mobile"
                    banner={banners.sidebarMiddle}
                    isSide
                    position="Fehérvárról cikkek hirdetés"
                  />
                </div>
              )}
            </div>
          </>
        )}

        <div className="global-box-pb">
          <ArticleListBox
            className="mb-0"
            title="Percről percre"
            articles={freshArticles}
            isLiveFeed
          />
          <CustomDivider className="global-box-pt mobile" />
        </div>
      </Col>

      <FrontPageNews1 articleData={feedArticles} className="desktop" />

      <Col
        tag="aside"
        className="global-box-xs-pt global-box-reset-xl-pt global-box-px"
        xs={{ order: 3 }}
        md={{ size: 6, order: 3 }}
        xl={{ size: 3, order: 3 }}
      >
        <FrontPageNews1
          articleData={feedArticles.slice(5, 9)}
          className="mobile"
        />
        <CustomDivider className="global-box-py mobile" />

        {isBannerVisible(banners?.sidebarTop) && (
          <div className="global-box-pb">
            <Banner
              className="mb-0"
              isSide
              banner={banners.sidebarTop}
              position="Főoldali legnépszerűbb cikkek feletti hirdetés"
            />
          </div>
        )}

        <MostPopularBox articles={mostPopularArticles} isNumberedContent />

        {recommendedArticles?.length > 0 && (
          <div className="sticky-box global-box-pt desktop">
            <ArticleListBox
              className="sticky mb-0"
              articles={recommendedArticles}
              title="Kedvencek"
            />
          </div>
        )}
      </Col>
    </Row>
  );
}
