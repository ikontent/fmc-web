import { Row, Col } from "reactstrap";
import CustomDivider from "components/elements/CustomDivider";
import ArticleListBox from "components/elements/articles/ArticleListBox";
import Newsletter from "components/elements/Newsletter";
import Banner from "components/customPositioned/Banner";
import FrontPageNews2 from "components/sections/FrontPageNews2";
import { isBannerVisible } from "utils/banner";

export default function FrontPageSection3({
  recommendedArticles,
  banners,
  feedArticles,
  advertisement,
  usefulInformations,
}) {
  return (
    <Row
      tag="section"
      noGutters
      className="global-section-py global-section-difference-px"
    >
      <Col
        tag="aside"
        className="global-box-xs-pt global-box-reset-xl-pt global-box-px"
        xs={{ order: 2 }}
        md={{ size: 6, order: 2 }}
        xl={{ size: 3, order: 1 }}
      >
        {recommendedArticles?.length && (
          <div className="mobile">
            <ArticleListBox articles={recommendedArticles} title="Kedvencek" />

            <CustomDivider className="global-box-pb mt-4" />
          </div>
        )}

        <div className="sticky-box">
          <Newsletter className="sticky mb-0" />
        </div>

        {isBannerVisible(banners?.sidebarMiddle) && (
          <div className="desktop global-box-pt">
            <Banner
              className="mb-0"
              banner={banners.sidebarMiddle}
              isSide
              position="Főoldal, hírlevél feliratkozás alatti hirdetés"
            />
          </div>
        )}
      </Col>

      <FrontPageNews2
        articleData={feedArticles}
        advertisement={advertisement}
      />

      <Col
        tag="aside"
        className="global-box-xs-pt global-box-reset-xl-pt global-box-px"
        xs={{ order: 3 }}
        md={{ size: 6, order: 3 }}
        xl={{ size: 3, order: 3 }}
      >
        <div className="sticky-box">
          <ArticleListBox
            className="sticky mb-0"
            title="Fehérvári hasznos infók"
            articles={usefulInformations}
            isListedContent
          />
        </div>

        {/* {isBannerVisible(banners?.sidebarTop) && (
          <div className="mobile global-box-pt">
            <Banner
              className="my-0"
              banner={banners.sidebarTop}
              position="Főoldal, hasznos infók alatti hirdetés"
            />
          </div>
        )} */}
      </Col>
    </Row>
  );
}
