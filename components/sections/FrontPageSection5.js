import { Row } from "reactstrap";
import FrontPageNews3 from "components/sections/FrontPageNews3";

export default function FrontPageSection5({ feedArticles }) {
  return (
    <Row tag="section" noGutters className="global-section-p">
      <FrontPageNews3 articleData={feedArticles} />
    </Row>
  );
}
