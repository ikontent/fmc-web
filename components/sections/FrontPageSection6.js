import { useState, useEffect } from "react";
import api from "utils/api";
import { Row, Col } from "reactstrap";
import { ArticleTitle } from "components/elements/articles/ArticleComponents";
import ArticleVideo from "components/elements/articles/ArticleVideo";
import { imagePathHandler, getImageUrl } from "utils/helpers";
import { pathFactory } from "utils/path-factory";

export default function FrontPageSection6({ title, action }) {
  const [videos, setVideos] = useState([]);

  // Get the latest tv news and fake to an article
  useEffect(async () => {
    try {
      const result = await api.getVideos();
      if (!result?.length) {
        return;
      }

      let videos = [];

      const getMedium = (source) => {
        switch (source) {
          case "tv":
            return "fehervar-tv";
          case "radio":
            return "vorosmarty-radio";
          case "fmc_yt":
            return "fmc";
          case "vorosmarty_yt":
            return "vorosmarty-radio";
          default:
            return "fmc";
        }
      };

      const getFeaturedImage = (video) => {
        if (video?.source == "fmc_yt" || video?.source == "vorosmarty_yt") {
          return video.featured_image;
        }
        if (video?.sourceUrl && video.source == "tv") {
          let imageId = video.sourceUrl.substring(
            video.sourceUrl.indexOf("&vid=") + 5
          );
          imageId = imageId.substring(0, imageId.indexOf("&"));

          return `https://fehervartv.ne.hu//_user/videolibrary/video_img/${imageId}.jpg`;
        }
        return imagePathHandler(getImageUrl(video.featuredImage, "small"));
      };

      const getFeaturedTag = (video) => {
        return video.featuredTag
          ? video.featuredTag
          : video.source == "tv"
          ? {
              tagName: "Fehérvár TV",
            }
          : video.source == "radio" || video.source == "vorosmarty_yt"
          ? {
              tagName: "Vörösmarty Rádió",
            }
          : null;
      };

      for (const video of result) {
        const featuredTag = getFeaturedTag(video);

        videos.push({
          ...video,

          medium: getMedium(video.source),

          showFeaturedImage: true,

          _URL: video.source
            ? `/?externalId=${video.id}`
            : pathFactory.article(video),

          featuredImage: getFeaturedImage(video),

          featuredTag,
          topic: video.topic
            ? video.topic
            : !featuredTag
            ? { slug: "multimedia", name: "Multimédia" }
            : null,
        });
      }

      setVideos(videos);
    } catch (e) {
      console.log(e);
    }
  }, []);

  return videos.length > 0 ? (
    <section className="global-section-pt global-section-difference-pl global-section-pr global-item-pb multimedia">
      <Row noGutters>
        <Col className="global-box-pl">
          <ArticleTitle title={title} action={action} withoutSpace />
        </Col>
      </Row>

      <Row noGutters>
        {videos.map((video) => (
          <Col
            key={video.id}
            className="global-item-pt global-box-pl global-section-difference-pb"
            sm={6}
            lg={3}
          >
            <div className="global-box-difference-pb">
              <ArticleVideo article={video} />
            </div>
          </Col>
        ))}
      </Row>
    </section>
  ) : null;
}
