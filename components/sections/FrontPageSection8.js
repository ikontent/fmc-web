import { Row, Col } from "reactstrap";
import {
  ArticleTitle,
  ArticleButton,
} from "components/elements/articles/ArticleComponents";
import ArticleVerticalWithHeading from "components/elements/articles/ArticleVerticalWithHeading";

export default function FrontPageSection8({
  title,
  medium,
  action,
  defaultButtonLabel,
  columns,
}) {
  return columns?.length ? (
    <section className="fehervar-printed global-section-pt global-section-difference-pl global-section-pr global-item-pb">
      <Row noGutters>
        <Col className="global-box-pl">
          <ArticleTitle
            title={title}
            medium={medium}
            action={action}
            withoutSpace
          />
        </Col>
      </Row>

      <Row noGutters className="justify-content-center">
        {(columns || []).slice(0, 6).map((column, index) => (
          <Col
            className={`global-item-pt global-item-pl global-section-difference-pb${
              index > 3 ? " d-none d-md-block" : ""
            }`}
            key={`item-${index}`}
            xs={12}
            sm={6}
            md={4}
            lg={3}
            xl={2}
          >
            <ArticleVerticalWithHeading
              article={{ ...column.article, title: "" }}
              index={index}
              isItFehervariHetilap
            />

            <div className="pt-3 global-box-difference-pb">
              <ArticleButton
                text={column.buttonLabel || defaultButtonLabel}
                url={column.buttonURL}
                target="_blank"
              />
            </div>
          </Col>
        ))}
      </Row>
    </section>
  ) : null;
}
