import { Col } from "reactstrap";
import CustomDivider from "components/elements/CustomDivider";
import {
  ArticleTitle,
  ArticleButton,
} from "components/elements/articles/ArticleComponents";
import ArticleVerticalWithHeading from "components/elements/articles/ArticleVerticalWithHeading";

export default function TopicSuggest({
  className = "d-flex",
  separatorClassName = "d-none",
  title,
  articles,
  buttonLabel,
  buttonURL,
  xs,
  sm,
  md,
  lg,
  xl,
}) {
  return (
    <Col
      tag="section"
      className={`flex-column align-items-stretch global-section-pb${
        className ? ` ${className}` : ""
      }`}
      xs={xs}
      sm={sm}
      md={md}
      lg={lg}
      xl={xl}
    >
      <div className="flex-fill global-box-px global-separated-l">
        <ArticleTitle title={title} withoutSpace />

        <div className="global-box-difference-pb">
          {(articles || []).map((article, index) => (
            <div
              className={`global-item-py${index ? " global-separated-t" : ""}`}
              key={`article-${index}`}
            >
              <ArticleVerticalWithHeading
                article={article}
                index={index}
                onlyFirstImage
                showPublishedAt={true}
              />
            </div>
          ))}
        </div>

        <ArticleButton text={buttonLabel} url={buttonURL} />
      </div>
    </Col>
  );
}
