import { Row } from "reactstrap";
import TopicSuggest from "components/sections/TopicSuggest";

export default function TopicSuggestSection({
  defaultButtonLabel = "",
  columns,
}) {
  return (
    <Row
      tag="section"
      noGutters
      className="global-section-difference-px with-vertical-separator topic-suggestion"
    >
      {(columns || []).map((column, index) => (
        <TopicSuggest
          key={`column-${index}`}
          className={index > 1 ? "d-none d-sm-flex" : undefined}
          separatorClassName={
            index
              ? `d-block${index === 1 ? " d-sm-none" : ""}${
                  index > 1 ? " d-lg-none" : ""
                }`
              : undefined
          }
          title={column.title}
          articles={column.articles}
          buttonLabel={column.buttonLabel || defaultButtonLabel}
          buttonURL={column.buttonURL}
          sm={6}
          lg={3}
        />
      ))}
    </Row>
  );
}
