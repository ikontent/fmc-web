import Link from "next/link";
import { useRouter } from "next/router";

export default function TopicMenu({ mainTopic = {} }) {
  const router = useRouter();
  const subTopics = mainTopic.topics;
  const slug = router.asPath.replace(/\/([\w\d-]+)\/?.*/, "$1");

  subTopics.sort((a, b) => a.position - b.position);

  return subTopics ? (
    <nav className="topic-menu">
      <Link prefetch={false} href={"/" + mainTopic.slug}>
        <a className={"a-global" + (slug === mainTopic.slug ? " current" : "")}>
          Összes
        </a>
      </Link>

      {subTopics.map((topic) => (
        <Link prefetch={false} href={"/" + topic.slug} key={topic.slug}>
          <a className={"a-global" + (slug === topic.slug ? " current" : "")}>
            {topic.name}
          </a>
        </Link>
      ))}
    </nav>
  ) : null;
}
