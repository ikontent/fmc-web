import { Row, Col } from "reactstrap";
import TopicMenu from "components/topics/TopicMenu";
import ArticleListBox from "components/elements/articles/ArticleListBox";
import MostPopularBox from "components/elements/articles/MostPopularBox";
import CustomDivider from "components/elements/CustomDivider";
import Banner from "components/customPositioned/Banner";
import Newsletter from "components/elements/Newsletter";
import ArticleCardHorizontal from "components/elements/articles/ArticleCardHorizontal";
import ArticleButtonRandom from "components/elements/articles/ArticleButtonRandom";
import Pagination from "components/elements/Pagination";
import { isBannerVisible } from "utils/banner";

export default function TopicSection({
  title,
  topic,
  mainTopic,
  displayArticlesFrom,
  displayArticlesTo,
  articles,
  mostPopularArticles,
  banners,
  section,
  _pagination,
  usefulInformations,
  // screenWidth,
}) {
  const filteredArticles =
    articles.slice(displayArticlesFrom, displayArticlesTo) || [];

  return (
    <Row tag="section" noGutters className="global-section-md-pt">
      {/* ############### MAIN ################ */}

      <Col className="global-box-px" md={{ offset: 1, size: 7 }}>
        {section === 1 && <TopicMenu mainTopic={mainTopic} />}
        {title && title}
        {filteredArticles.map((article, index) => (
          <div key={index}>
            <ArticleCardHorizontal
              article={article}
              showTopic
              showTag
              showTagBoxWithin
              showPublishedAt
            />
            {filteredArticles.length - 1 > index && (
              <CustomDivider className="global-box-py" />
            )}
          </div>
        ))}

        {topic?.parent && displayArticlesTo < articles.length && (
          <CustomDivider className="global-box-py" />
        )}

        {displayArticlesTo > articles.length - 1 && (
          <>
            <Row className="global-box-py article-buttons">
              <Col tag="nav" xl={6}>
                <Pagination
                  _pagination={_pagination}
                  size="md"
                  // size={screenWidth && screenWidth < 576 ? "sm" : "md"}
                />
              </Col>
              <Col tag="nav" xl={6}>
                {topic?.slug != "hirek" && <ArticleButtonRandom />}
              </Col>
            </Row>
          </>
        )}
      </Col>
      <Col size={1} className="d-none d-lg-block" />

      {/* ############### SIDE ################ */}

      <Col
        tag="aside"
        className="
          global-box-px
          global-separated-l
          global-box-xs-pt
          global-box-reset-md-pt
          desktop"
        md={4}
        lg={3}
      >
        {section === 1 && (
          <>
            {isBannerVisible(banners.common?.sidebarTop) && (
              <div>
                <Banner
                  banner={banners.common.sidebarTop}
                  isSide
                  position={`${topic.name} rovat oldal, oldalsáv teteje hirdetés`}
                />
              </div>
            )}
            <div className="global-box-pb sticky-box">
              <MostPopularBox
                className="sticky"
                articles={mostPopularArticles}
                isNumberedContent
              />
            </div>
          </>
        )}

        {section === 2 && (
          <>
            <div className="sticky">
              <div className="global-box-pb">
                <Newsletter />
              </div>
              {isBannerVisible(banners.common?.sidebarMiddle) && (
                <div className="global-box-pb">
                  <Banner
                    banner={banners.common.sidebarMiddle}
                    isSide
                    position={`${topic.name} rovat oldal, oldalsáv köztes hirdetés`}
                  />
                </div>
              )}
            </div>
          </>
        )}
        {section === 3 && (
          <div className="sticky-box">
            <ArticleListBox
              className="sticky"
              title="Fehérvári hasznos infók"
              articles={usefulInformations}
              isListedContent
            />
          </div>
        )}
      </Col>
    </Row>
  );
}
