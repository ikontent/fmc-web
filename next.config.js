const withPlugins = require("next-compose-plugins"); //https://stackoverflow.com/a/60190226
const api = require("./utils/api-server");
const withSvgr = require("next-svgr");
const logger = require("./utils/logger");
const repeatFetch = require("./utils/repeat-fetch");
const { pathFactory, slugCreator } = require("./utils/path-factory");
const { generatePaginatedPages, pick } = require("./utils/helpers-server");
const getFavoriteArticles = require("./utils/favoriteArticles");
const { sitemap } = require("./utils/sitemap");

module.exports = withPlugins([withSvgr, {}], {
  exportPathMap: async () => {
    const {
      ITERATION_COUNT_FOR_DEV,
      POSTS_PER_PAGE = 1000,
      NEXT_PUBLIC_STRAPI_URL,
    } = process.env;

    // new logger
    const log = logger();
    const { now } = log;

    // ################ DATA FETCH - START ################

    log(`NEXT CONFIG START - ${new Date()}`);
    log("DATA FETCH START");

    // ==== NON-REPETITIVE DATA FETCH - START ====
    const [
      frontpageSettings,
      pages,
      topics,
      authors,
      magazines,
      articlesCount,
      siteSettings,
      // nameDay,
      // weatherAPI,
    ] = await Promise.all([
      api.getFrontpageSettings(),
      api.getPages(),
      api.getTopics(),
      api.getAuthors(),
      api.getMagazines(),
      api.getArticlesCount(),
      api.getSiteSettings(),
      // api.getNameDay(),
      // api.getWeatherAPI(),
    ]);

    log("NON-REPETITIVE DATA FETCH DONE");

    // ==== NON-REPETITIVE DATA FETCH - END ====

    // ==== ARTICLE DATA FETCH - START ====
    const articles = await repeatFetch(
      articlesCount,
      POSTS_PER_PAGE,
      ITERATION_COUNT_FOR_DEV,
      api.getArticles,
      log,
      "ARTICLE DATA"
    );
    log("ARTICLE DATA FETCH DONE");
    // ==== ARTICLE DATA FETCH - END ====

    // ==== FAVORITES DATA FETCH - START ====
    const recommendedArticles = await getFavoriteArticles();
    log("FAVORITES DATA FETCH DONE");
    // ==== FAVORITES DATA FETCH - END ====

    log("DATA FETCH DONE");

    // ################ DATA FETCH - END ################

    // ################ STATIC DATA (mock too!) - START ################

    // TODO:
    // news block lengths (on the front page)
    newsBlockLengths = [9, 14, 19];

    // "Fehérvári hasznos infók"
    const usefulInformations = [
      {
        title: "Buszmenetrend",
        _URL: "/page/buszmenetrend",
      },
      {
        title: "Szolgáltatók",
        _URL: "/page/szolgaltatok",
      },
      {
        title: "Orvosi ügyeletek",
        _URL: "/page/orvosi-ugyeletek",
      },
      {
        title: "Ügyintézés",
        _URL: "/page/ugyintezes",
      },
    ];

    const banners = {
      /*
      https://docs.google.com/spreadsheets/d/1R4ejO3BBm1tf0Ws8AmliGKsdpNfYvbuI/edit#gid=443914569

      {
        content: can be
          • <html> as string | examples:
            `<video autoplay muted controls playsinline preload="auto"><source src="/images/banners/vakcinainfo/640x360-Harmadik_oltas_november.mp4">Your browser does not support the video tag.</video>`
            `<img src="/images/banners/vakcinainfo/300x600_MK_harmadik_oltas.jpg" alt="vakcina info" />`
          • array of <html> as string, to rotate them randomly
        url: (target when banner is clicked) can also be either string or array of strings
        campaign: string | need for Google Analytics,
        start: string | "yyyy-mm-dd"
        end: string | "yyyy-mm-dd"
        caption: string | default: "hirdetés",  inject a space to display none: " ",
      }
      wrapper {} is an exception, so left in place in full 
        */
      common: {
        wrapper: {
          mobile: "",
          desktop: "",
          left: "",
          right: "",
          url: "",
        },
        header: {},

        // home, article, topic: #1
        sidebarTop: {
          content:
            '<img src="/images/banners/allampapir/AKK_image_banner__300x250px_230406.jpg" />',
          url: "https://www.allampapir.hu",
          start: "2023-04-06",
          end: "2023-04-26",
        },

        // home, article: #3, topic: #2
        sidebarMiddle: {
          content:
            '<img src="/images/banners/aldi/ALDI_Szekesfehervar_Nyitas_300x600.jpg" />',
          url: "https://www.youtube.com/watch?v=ok78tHlI3h0",
          start: "2023-01-31",
          end: "2023-02-20",
        },

        // home: #4, article: #5, topic: #3
        footer: {
          content:
            '<video autoplay muted controls playsinline preload="auto"><source src="/images/banners/marcius15/640x360-MK-2022_marcius_15_videobanner.mp4">Your browser does not support the video tag.</video>',
          url: "https://www.youtube.com/watch?v=ok78tHlI3h0",
          start: "2023-01-31",
          end: "2023-02-27",
        },
      },

      // home: #2
      // IMPORTANT!: the mobile counterpart of "home #2" is "common > header" above
      // header's banner slot is sync'd with this config (Master.js: banner={banners?.common?.header || banners?.frontPage})
      frontPage: {
        content:
          '<video autoplay muted controls playsinline preload="auto"><source src="/images/banners/tobb-mint-egy-eve-tart-a-haboru/640x360_tobb_mint_egy_eve.mp4">Your browser does not support the video tag.</video>',
        url: "https://youtu.be/sS8e3F8Ui5U",
        start: "2023-02-28",
        end: "2023-03-20",
      },

      articlePage: {
        // article: #2
        lead: {},

        // Injected slot does not always displays!
        // Only if there are x number of article lines.
        // article: #not-referenced
        injected: {},

        // article: #4
        afterArticle: {},
      },
    };

    const advertisements = {
      news2: null,
    };

    // ################ STATIC DATA (mock too!) - END ################

    // ################ DATA PROCESSING - START ################

    let paths = {};

    const headerFeaturedTags = (
      frontpageSettings.menuFeaturedTags || []
    ).reduce((acc, item) => {
      if (item.label && item.tag) {
        return [
          ...acc,
          {
            tagName: `#${item.label}`,
            slug: item.tag.slug,
            href: pathFactory.tag(item.tag),
          },
        ];
      } else {
        return acc;
      }
    }, []);

    const postMap = {};
    const topicMap = {};
    const tagsPosts = {};
    const tags = {};
    const topicsPosts = {};
    // A main topics azok a topicok amiknek nincs szülője.
    // A mainTopicsPosts ba azok a postok kerülnek amik a main topoicba
    // tartoznak vagy a main topic gyermek topikjaiba. Időrendbe.
    const mainTopicsPosts = {};

    const commonProps = {
      siteSettings,
      // nameDay,
      // weatherAPI,
    };

    // ==== PREPARE FEHERVAR ARTICLES - START ====
    // need to double feedArticles' iteration because both postsMap & feharvarArticles
    // requires each other (they cannot be ordered in any working way)
    const feedArticlesIds = frontpageSettings.liveFrontpagePosts
      .slice(0, newsBlockLengths[0])
      .reduce((acc, curr) => {
        acc[curr.postId] = true;
        return acc;
      }, {});
    const fehervarArticlesUnsliced = [];
    // ==== PREPARE FEHERVAR ARTICLES - END ====

    //TODOFÜLÖP - go to node hajnal3 cron 10db 257 ticket

    // ==== ARTICLE DATA PROCESSING - START ====

    // TODO: MIGRATION
    function addDataToArticle(article) {
      article._URL = pathFactory.article(article);
    }
    topics &&
      topics.forEach((topic) => {
        topicMap[topic.id] = topic;
      });

    // data addition to articles
    articles.forEach((article) => {
      addDataToArticle(article);

      postMap[article.id] = article;

      if (article.featuredTag) {
        const tagName = article.featuredTag.tagName;
        if (!tagsPosts[tagName]) {
          tagsPosts[tagName] = [];
        }
        if (!tags[tagName]) {
          tags[tagName] = article.featuredTag;
        }
        if (tagName === "Székesfehérvár" && !feedArticlesIds[article.id]) {
          fehervarArticlesUnsliced.push(article);
        }

        tagsPosts[tagName].push(article);
      }

      if (article.tags) {
        article.tags.forEach((tag) => {
          const tagName = tag.tagName;
          if (!tagsPosts[tagName]) {
            tagsPosts[tagName] = [];
          }
          if (!tags[tagName]) {
            tags[tagName] = tag;
          }
          if (tagName === "Székesfehérvár" && !feedArticlesIds[article.id]) {
            fehervarArticlesUnsliced.push(article);
          }

          tagsPosts[tagName].push(article);
        });
      }

      if (article.topic) {
        const topicSlug = article.topic.slug;
        if (!topicsPosts[topicSlug]) {
          topicsPosts[topicSlug] = [];
        }

        topicsPosts[topicSlug].push(article);

        if (article.topic.parent) {
          const parentTopic = topicMap[article.topic.parent];

          if (!mainTopicsPosts[parentTopic.slug]) {
            mainTopicsPosts[parentTopic.slug] = [];
          }
          mainTopicsPosts[parentTopic.slug].push(article);
        } else {
          if (!mainTopicsPosts[topicSlug]) {
            mainTopicsPosts[topicSlug] = [];
          }
          mainTopicsPosts[topicSlug].push(article);
        }
      }
    });

    let mostPopularArticles = [];
    let isMostPopularInProgress = true;
    const nowMostPopular = Date.now();
    const sevenDays = 7 * 24 * 60 * 60 * 1000;

    articles.forEach((article) => {
      if (isMostPopularInProgress) {
        if (
          nowMostPopular - new Date(article.published_at).valueOf() <
          sevenDays
        ) {
          if (
            article.enableInMostPopular === null || // this is ugly but null should be handled as true
            article.enableInMostPopular === true
          ) {
            mostPopularArticles.push(article);
          }
        } else {
          isMostPopularInProgress = false;
        }
      }

      article.similarArticles = [];
      (article.similarPosts || []).forEach((id) => {
        if (postMap[id]) {
          article.similarArticles.push(
            pick(
              postMap[id],
              "id",
              "_URL",
              "featuredImage",
              "showFeaturedImage",
              "title",
              "lead",
              "published_at",
              "featuredTag",
              "topic"
            )
          );
        }
      });

      (article.generatedSimilarPosts || []).forEach((id) => {
        if (postMap[id]) {
          article.similarArticles.push(
            pick(
              postMap[id],
              "id",
              "_URL",
              "featuredImage",
              "showFeaturedImage",
              "title",
              "lead",
              "published_at",
              "featuredTag",
              "topic"
            )
          );
        }
      });

      delete article.similarPosts;
      delete article.generatedSimilarPosts;

      delete article.status;
      if (article.author) {
        delete article.author.created_at;
        delete article.author.updated_at;
        delete article.author.confirmed;
        delete article.author.blocked;
        delete article.author.allowPageGeneration;
        delete article.author.email;
        delete article.author.about;

        if (article.author.avatar) {
          if (article.author.avatar.formats) {
            article.author.avatar = {
              url: article.author.avatar.formats.thumbnail.url,
            };
          } else {
            article.author.avatar = {
              url: article.author.avatar.url,
            };
          }
        }
      }

      if (article.featuredImage) {
        article.featuredImage = pick(
          article.featuredImage,
          "url",
          "formats",
          "author",
          "width",
          "height",
          "mime",
          "alt",
          "title"
        );

        if (article.featuredImage.formats) {
          for (const key in article.featuredImage.formats) {
            article.featuredImage.formats[key] = {
              url: article.featuredImage.formats[key].url,
              width: article.featuredImage.formats[key].width,
              height: article.featuredImage.formats[key].height,
              mime: article.featuredImage.formats[key].mime,
            };
          }
        }
      }

      if (article.featuredTag) {
        delete article.featuredTag.created_at;
        delete article.featuredTag.updated_at;
      }

      if (article.topic) {
        article.topic = pick(article.topic, "id", "name", "slug");
      }

      if (article.tags) {
        article.tags.forEach((x) => {
          delete x.created_at;
          delete x.updated_at;
        });
      }

      if (article.secondaryAuthors && article.secondaryAuthors.length) {
        article.secondaryAuthors.forEach((x) => {
          delete x.created_at;
          delete x.updated_at;
          delete x.confirmed;
          delete x.blocked;
          delete x.allowPageGeneration;
        });
      }
    });

    mostPopularArticles = mostPopularArticles
      .sort((articleA, articleB) => articleB.viewNum - articleA.viewNum)
      .slice(0, 6) // 5 + buffers for articlePage's own id
      .map((mpa) => pick(mpa, "id", "_URL", "title"));

    frontpageSettings.SidebarSponsoredPost &&
      mostPopularArticles.splice(2, 0, {
        title: frontpageSettings.SidebarSponsoredPost.title,
        _URL: pathFactory.article(frontpageSettings.SidebarSponsoredPost),
        sponsoredId: frontpageSettings.SidebarSponsoredPost.id,
      });

    log("DATA ADDITION TO ARTICLES IS DONE");

    // feed articles (hírfolyam 1-3)
    const feedArticles = frontpageSettings.liveFrontpagePosts
      .slice(0, newsBlockLengths[0] + newsBlockLengths[1] + newsBlockLengths[2])
      .reduce((acc, curr) => {
        if (curr && postMap[curr.postId]) {
          acc.push({
            ...pick(
              postMap[curr.postId],
              "id",
              "_URL",
              "featuredImage",
              "showFeaturedImage",
              "title",
              "lead",
              "published_at",
              "featuredTag",
              "topic",
              "isHighlightedContent",
              "isSponsoredContent",
              "sensitive"
            ),
            frontPageDisplayStyle: curr.frontPageDisplayStyle,
          });
        }
        return acc;
      }, []);

    // feed articles (címlap hírfolyam szekciók 1-3)
    // news1
    const news1 = feedArticles.slice(0, newsBlockLengths[0]);

    // news2
    const news2 = feedArticles.slice(
      newsBlockLengths[0],
      newsBlockLengths[0] + newsBlockLengths[1]
    );

    // news3
    const news3 = feedArticles.slice(
      newsBlockLengths[0] + newsBlockLengths[1],
      newsBlockLengths[0] + newsBlockLengths[1] + newsBlockLengths[2]
    );

    log("ARTICLES ARE SORTED BY POPULARITY");

    // topic columns (Kiemelt rovatok a címlapon)
    //TODOFÜLÖP rovathierarchia upgrade
    const mainColumns = [];
    for (let i = 1; i <= 4; i++) {
      const topic = frontpageSettings.featuredTopics[`topic${i}`];
      if (topic) {
        if (topicsPosts[topic.slug] && topicsPosts[topic.slug].length) {
          let counter = 0,
            guard = topicsPosts[topic.slug].length,
            withFeatured,
            first,
            second,
            curr;
          while (guard && !(withFeatured && first && second)) {
            curr = topicsPosts[topic.slug][counter];

            if (!feedArticlesIds[curr.id]) {
              if (withFeatured) {
                if (first) {
                  second = pick(
                    curr,
                    "id",
                    "_URL",
                    "featuredImage",
                    "showFeaturedImage",
                    "title",
                    "lead",
                    "published_at",
                    "featuredTag",
                    "topic"
                  );
                } else {
                  first = pick(
                    curr,
                    "id",
                    "_URL",
                    "featuredImage",
                    "showFeaturedImage",
                    "title",
                    "lead",
                    "published_at",
                    "featuredTag",
                    "topic"
                  );
                }
              } else {
                if (curr.featuredImage && curr.showFeaturedImage) {
                  withFeatured = pick(
                    curr,
                    "id",
                    "_URL",
                    "featuredImage",
                    "showFeaturedImage",
                    "title",
                    "lead",
                    "published_at",
                    "featuredTag",
                    "topic"
                  );
                }
              }
            }
            counter++;
            guard--;
          }
          const vmi = { withFeatured, first, second };
          mainColumns.push({
            title: topic.name,
            articles: ["withFeatured", "first", "second"].reduce(
              (acc, curr) => {
                if (vmi[curr]) {
                  if (
                    vmi[curr].featuredImage &&
                    vmi[curr].featuredImage.formats
                  ) {
                    for (const key in vmi[curr].featuredImage.formats) {
                      vmi[curr].featuredImage.formats[key] = {
                        url: vmi[curr].featuredImage.formats[key].url,
                      };
                    }
                  }

                  acc.push({ ...vmi[curr], highlighted: false });
                }
                return acc;
              },
              []
            ),
            buttonURL: pathFactory.topic(topic),
          });
        }
      }
    }

    log("FRONTPAGE FEATURED TOPICS DATA PROCESSING DONE");

    // "FehérVár hetilap" weekly/magazine
    const fehervarWeeklySection = {
      title: "FehérVár hetilap",
      action: { text: "Összes újság", url: "/hetilap" },
      medium: "fehervar-hetilap",
      defaultButtonLabel: "Letöltés",

      columns: (magazines || []).map((magazine) => ({
        article: {
          id: magazine.id,
          lead: magazine.text,
          published_at: magazine.date,
          featuredImage: magazine.image,
        },
        buttonURL:
          magazine.magazine && magazine.magazine.url
            ? `${NEXT_PUBLIC_STRAPI_URL}${magazine.magazine.url}`
            : "#",
      })),
    };

    // frontpage featured tag blocks (kiemelt tag-ek)

    const getArticlesforDivider = (tags) => {
      if (tags.length === 0) return [];
      return tags
        .reduce((acc, curr) => {
          if (!curr.tagName || !tagsPosts[curr.tagName]) return [...acc];
          return [...acc, ...tagsPosts[curr.tagName].slice(0, 3)];
        }, [])
        .sort((a, b) => {
          return new Date(b.published_at) - new Date(a.published_at);
        })
        .slice(0, 3);
    };

    const newsFlowDivider1 =
      frontpageSettings.FeaturedTagBlocks.length >= 1
        ? {
            title: frontpageSettings.FeaturedTagBlocks[0].blockName,
            articles: getArticlesforDivider(
              frontpageSettings.FeaturedTagBlocks[0].tags
            ),
          }
        : null;

    const newsFlowDivider2 =
      frontpageSettings.FeaturedTagBlocks.length >= 2
        ? {
            title: frontpageSettings.FeaturedTagBlocks[1].blockName,
            articles: getArticlesforDivider(
              frontpageSettings.FeaturedTagBlocks[1].tags
            ),
          }
        : null;

    const newsFlowDivider3 =
      frontpageSettings.FeaturedTagBlocks.length >= 3
        ? {
            title: frontpageSettings.FeaturedTagBlocks[2].blockName,
            articles: getArticlesforDivider(
              frontpageSettings.FeaturedTagBlocks[2].tags
            ),
          }
        : null;

    // "Fehérvárról"
    const fehervarArticles = (fehervarArticlesUnsliced || [])
      .slice(0, 5)
      .map((x) =>
        pick(
          x,
          "id",
          "_URL",
          "featuredImage",
          "title",
          "lead",
          "published_at",
          "featuredTag",
          "topic"
        )
      );

    // Multimédia (elvárt működés: első helyre a "Fehérvár TV" tag-el ellátott cikk kerüljön időrendben a legfrisebb, a maradék három helyre a multimédia topicból időrendbe)
    /*
    let videoArticles = (topicsPosts["multimedia"] || []).slice(0, 3).map((x) =>
      pick(
        x,
        "id",
        "_URL",
        "featuredImage",
        "title",
        "lead",
        "published_at",
        "featuredTag",
        "topic",
        "tags" //TDOFULOP: Itt igazából csak a tag-ek tagName paraméterére van szükségem
      )
    );

    for (i = 0; i < videoArticles.length; i++) {
      if (
        videoArticles[i].featuredImage &&
        videoArticles[i].featuredImage.formats
      ) {
        for (const key in videoArticles[i].featuredImage.formats) {
          videoArticles[i].featuredImage.formats[key] = {
            url: videoArticles[i].featuredImage.formats[key].url,
          };
        }
      }
    }*/

    log("ARTICLE DATA PROCESSING DONE");

    // ==== ARTICLE DATA PROCESSING - END ====

    log("DATA PROCESSING DONE");

    // ################ DATA PROCESSING - END ################

    // ################ PAGES SETUP - START ################

    // ==== INDEX PAGE - START ====

    paths["/"] = {
      page: "/indexPage",
      query: {
        ...commonProps,
        news1,
        news2,
        news3,
        fehervarArticles,
        mostPopularArticles,
        recommendedArticles,
        newsFlowDivider1,
        usefulInformations,
        newsFlowDivider2,
        newsFlowDivider3,
        //videoArticles,
        mainColumns,
        fehervarWeeklySection,
        headerFeaturedTags,
        banners,
        advertisements,
      },
    };

    log("INDEX PAGE EXPORT DONE");

    // ==== INDEX PAGE - END ====

    // ==== MAGAZINES PAGE - START ====

    generatePaginatedPages(
      "/hetilap", // url
      "/magazinePage", // page
      "magazines", // prop name
      magazines, // prop content
      6, // max paginated item per page
      2, // half range
      {
        banners,
        mostPopularArticles,
        mainColumns,
        //videoArticles,
        headerFeaturedTags,
      },
      paths
    );

    log("MAGAZINES PAGE EXPORT DONE");

    // ==== MAGAZINES PAGE - END ====

    // ==== PAGE PAGE - START ====

    pages &&
      pages.forEach((page) => {
        const path = pathFactory.page(page);
        paths[path] = {
          page: "/pagePage",
          query: {
            ...commonProps,
            page,
            path,
            mainColumns,
            //videoArticles,
            banners,
            fehervarWeeklySection,
            mostPopularArticles,
            usefulInformations,
            headerFeaturedTags,
            recommendedArticles,
          },
        };
      });

    log("PAGE PAGES EXPORT DONE");

    // ==== PAGE PAGE - END ====

    // ==== ARTICLE PAGE - START ====

    articles.forEach((article) => {
      paths[article._URL] = {
        page: "/articlePage",
        query: {
          exportedArticle: article,
          isPreview: false,
          mostPopularArticles,
          usefulInformations,
          mainColumns,
          //videoArticles,
          recommendedArticles,
          banners,
          // banner: advertisements.articlePage,
          headerFeaturedTags,
        },
      };
    });

    log("ARTICLE PAGES EXPORT DONE");

    // ==== ARTICLE PAGE - END ====

    // ==== PREVIEW PAGE - START ====
    // Do not move this key to .env. This is a secret page. The robots are disallowed.
    const PREVIEW_CLIENT_URL_KEY = "e8d6cf19cf9047279ea4a77ce3a04e0a";
    paths[`/preview/${PREVIEW_CLIENT_URL_KEY}`] = {
      page: "/articlePage",
      query: {
        exportedArticle: null,
        isPreview: true,
        topics,
        mostPopularArticles,
        usefulInformations,
        mainColumns,
        //videoArticles,
        recommendedArticles,
        banners,
      },
    };

    log("PREVIEW PAGE EXPORT DONE");
    // ==== PREVIEW PAGE - END ====

    // ==== PREVIEW FRONTPAGE - START ====
    // Do not move this key to .env. This is a secret page. The robots are disallowed.
    paths[`/preview-frontpage/${PREVIEW_CLIENT_URL_KEY}`] = {
      page: "/indexPagePreview",
      query: {
        ...commonProps,
        exportedArticle: null,
        isFrontPagePreview: true,
        news1,
        news2,
        news3,
        fehervarArticles,
        mostPopularArticles,
        recommendedArticles,
        newsFlowDivider1,
        usefulInformations,
        newsFlowDivider2,
        newsFlowDivider3,
        mainColumns,
        fehervarWeeklySection,
        headerFeaturedTags,
        banners,
        advertisements,
      },
    };

    log("PREVIEW PAGE EXPORT DONE");
    // ==== PREVIEW FRONTPAGE - END ====

    // ==== TAG PAGE - START ====

    if (ITERATION_COUNT_FOR_DEV) {
      console.log("DEV DEV DEV DEV");
      //BECAFUSE OF UGLY TAGNAMES (?%/ etc)
      Object.entries(tagsPosts).forEach(([tagName, posts]) => {
        if (
          !["*", "(", ")", "+", "?", "/"].some((c) => tagName.indexOf(c) != -1)
        ) {
          const tag = tags[tagName];

          generatePaginatedPages(
            pathFactory.tag(tag), // url
            "/tagPage", // page
            "tagArticles", // prop name
            posts,
            24, // max paginated item per page
            2, // half range
            {
              // ...commonProps,
              tagName,
              mostPopularArticles,
              usefulInformations,
              mainColumns,
              //videoArticles,
              banners,
              fehervarWeeklySection,
              headerFeaturedTags,
            },
            paths
          );
        }
      });
    } else {
      Object.entries(tagsPosts).forEach(([tagName, posts]) => {
        const tag = tags[tagName];

        generatePaginatedPages(
          pathFactory.tag(tag), // url
          "/tagPage", // page
          "tagArticles", // prop name
          posts,
          24, // max paginated item per page
          2, // half range
          {
            // ...commonProps,
            tagName,
            mostPopularArticles,
            usefulInformations,
            mainColumns,
            //videoArticles,
            banners,
            fehervarWeeklySection,
            headerFeaturedTags,
          },
          paths
        );
      });
    }

    log("TAG PAGES EXPORT DONE");

    // ==== TAG PAGE - END ====

    // ==== TOPIC PAGE - START ====

    //TODOFÜLÖP rovathierarchia upgrade
    topics &&
      topics.forEach((topic) => {
        // A főrovatok (mainTopics) tartalmazzák az alrovatok postjait is
        const posts = topic.parent
          ? topicsPosts[topic.slug]
          : mainTopicsPosts[topic.slug];
        let subTopicRecommendation = [];

        if (!topic.parent) {
          const childTopics = topic.topics;
          const subTopicRecommendationCount = Math.min(2, childTopics.length);

          // generate two or one subtopic index
          if (subTopicRecommendationCount > 0) {
            let uniqueIndexes = [];
            do {
              const newIndex = Math.floor(Math.random() * childTopics.length);
              if (!uniqueIndexes.includes(newIndex)) {
                uniqueIndexes.push(newIndex);
              }
            } while (uniqueIndexes.length != subTopicRecommendationCount);

            // generate two or one subtopic recommendations
            for (let i = 0; i < uniqueIndexes.length; i++) {
              subTopicRecommendation.push({
                title: childTopics[uniqueIndexes[i]].name,
                articles: topicsPosts[childTopics[uniqueIndexes[i]].slug]
                  ? topicsPosts[childTopics[uniqueIndexes[i]].slug].slice(0, 3)
                  : [],
                url: `/${childTopics[uniqueIndexes[i]].slug}`,
              });
            }
          }
        }

        generatePaginatedPages(
          pathFactory.topic(topic),
          "/topicPage",
          "topicArticles",
          posts || [],
          21,
          2, // half range
          {
            topic,
            topics,
            banners,
            mostPopularArticles,
            mainColumns,
            //videoArticles,
            fehervarWeeklySection,
            subTopicRecommendation,
            usefulInformations,
            headerFeaturedTags,
          },
          paths
        );
      });

    log("TOPIC PAGES EXPORT DONE");

    // ==== TOPIC PAGE - END ====

    // ==== SEARCH RESULTS PAGE - START ====
    paths["/kereses"] = {
      page: "/searchPage",
      query: {
        banners,
        mostPopularArticles,
        //videoArticles,
        mainColumns,
        fehervarWeeklySection,
        headerFeaturedTags,
        // bannerMain2:
        //   (advertisements &&
        //     advertisements.banner &&
        //     advertisements.banner.frontPageMain2) ||
        //   undefined,
      },
    };

    log("SEARCH RESULTS PAGE EXPORT DONE");

    // ==== SEARCH RESULTS PAGE - END ====

    // ==== AUTHOR PAGE - START ====

    authors &&
      authors.forEach((author) => {
        if (author.allowPageGeneration) {
          //TODOFÜLÖP átvinni a post iterációba és mapet építeni
          const articlesOfAuthor =
            articles.filter(
              (article) =>
                (article.author && article.author.id === author.id) ||
                (article.secondaryAuthors.length &&
                  article.secondaryAuthors.some(
                    (secondaryAuthor) => secondaryAuthor.id === author.id
                  ))
            ) || [];

          generatePaginatedPages(
            pathFactory.author(author), // url
            "/authorPage", // page
            "articlesOfAuthor", // prop name
            articlesOfAuthor, // prop content
            24, // max paginated item per page
            2, // half range
            {
              _URL: pathFactory.author(author),
              author,
              numberOfAllArticles: articlesOfAuthor.length,
              mostPopularArticles,
              usefulInformations,
              mainColumns,
              //videoArticles,
              banners,
              fehervarWeeklySection,
              headerFeaturedTags,
            },
            paths
          );
        }
      });

    log("AUTHOR PAGE EXPORT DONE");

    // ==== AUTHOR PAGE - END ====

    // ==== 404 PAGE - START ====
    paths["/custom404"] = {
      page: "/404Custom",
      query: {
        mostPopularArticles,
        usefulInformations,
        mainColumns,
        //videoArticles,
        banners,
        fehervarWeeklySection,
        headerFeaturedTags,
      },
    };

    log("404 PAGE EXPORT DONE");

    // ==== 404 PAGE - END ====

    log("EXPORT DONE");

    // ################ GENERATE SITEMAP ################

    log("SITEMAP GENERATION START");

    sitemap(paths);

    log("SITEMAP GENERATION FINISH");

    // ################ PAGES SETUP - END ################
    return paths;
  },
});

//TODOFÜLÖP pathfactory encodeuri kiszedés
