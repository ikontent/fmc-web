import { useRouter } from "next/router";
import { useEffect } from "react";
import HtmlHead from "components/elements/HtmlHead";
import { Row, Col } from "reactstrap";
import MasterLayout from "components/layouts/Master";
import CustomDivider from "components/elements/CustomDivider";
import Banner from "components/customPositioned/Banner";
import FrontPageSection6 from "components/sections/FrontPageSection6";
import TopicSuggestSection from "components/sections/TopicSuggestSection";
import FrontPageSection8 from "components/sections/FrontPageSection8";
import ArticleButtonRandom from "components/elements/articles/ArticleButtonRandom";
import { isBannerVisible } from "utils/banner";

const NotFound = ({
  random404Articles = [],
  mainColumns,
  videoArticles,
  banners,
  fehervarWeeklySection,
  headerFeaturedTags,
}) => {
  const router = useRouter();

  useEffect(() => {
    setTimeout(() => {
      router.push("/");
    }, 5000);
  }, []);

  return (
    <MasterLayout banners={banners}>
      <HtmlHead
        title="404"
        description="A keresett oldal nem található"
        keywords="404"
        headerFeaturedTags={headerFeaturedTags}
      />
      <main id="custom-404">
        <Row tag="section" noGutters>
          <Col
            sm={{ offset: 2, size: 8 }}
            lg={{ offset: 3, size: 6 }}
            className="custom-top global-section-px"
          >
            <h3>Ajaj... Az oldal nem található!</h3>
            <p className="mb-2">
              Ne aggódj, használd a keresőt vagy 5 másodperc múlva a főoldalra
              irányítunk! Addig is íme néhány fmc-s tipp:
            </p>
          </Col>
        </Row>

        <Row noGutters>
          <Col
            xs={{ offset: 1, size: 10 }}
            sm={{ offset: 2, size: 8 }}
            md={{ offset: 3, size: 6 }}
            lg={{ offset: 4, size: 4 }}
            className="global-box-pb"
          >
            <ArticleButtonRandom />
          </Col>
        </Row>

        <CustomDivider className="global-section-px pb-12 pt-30" />

        <TopicSuggestSection
          defaultButtonLabel="Még több cikk"
          columns={mainColumns}
        />

        <div className="">
          <FrontPageSection6
            title="Multimédia"
            action={{ text: "Még több cikk", url: "/multimedia" }}
            videoArticles={videoArticles}
          />
        </div>

        {isBannerVisible(banners.common?.footer) && (
          <>
            <div className="global-section-px mobile">
              <Banner
                banner={banners.common.footer}
                className="mt-0 mb-2"
                position="Lábléc mobil hirdetés"
              />
            </div>

            <div className="global-section-px desktop">
              <div className="global-box-py global-separated-y">
                <Banner
                  banner={banners.common.footer}
                  className="my-0"
                  position="Lábléc desktop hirdetés"
                />
              </div>
            </div>
          </>
        )}

        <FrontPageSection8 {...fehervarWeeklySection} />
      </main>
    </MasterLayout>
  );
};

NotFound.getInitialProps = async function ({ query: props }) {
  return {};
};

export default NotFound;
