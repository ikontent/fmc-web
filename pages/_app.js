import "assets/app.scss";

function MyApp({ Component, pageProps, router, ...rest }) {
  return (
    <Component {...(process.browser ? __NEXT_DATA__.query : router.query)} />
  );
}

export default MyApp;
