import HtmlHead from "components/elements/HtmlHead";
import { useState, useEffect } from "react";
import { Row, Col } from "reactstrap";
import Gallery from "react-grid-gallery";

import MasterLayout from "components/layouts/Master";

import ArticleMeta from "components/elements/ArticleMeta";
import CustomDivider from "components/elements/CustomDivider";
import FBscrollTagsShare from "components/elements/FBscrollTagsShare";
import PhotoMadeBy from "components/elements/PhotoMadeBy";
import FrontPageSection6 from "components/sections/FrontPageSection6";
import TopicSuggestSection from "components/sections/TopicSuggestSection";
import Newsletter from "components/elements/Newsletter";
import ShareIcons from "components/elements/ShareIcons";
import SimilarArticles from "components/elements/SimilarArticles";
import TimeStampAlert from "components/elements/TimeStampAlert";
import AdultOverlay from "components/elements/AdultOverlay";

import Banner from "components/customPositioned/Banner";
import ArticleListBox from "components/elements/articles/ArticleListBox";
import MostPopularBox from "components/elements/articles/MostPopularBox";
import ParseWysiwyg from "components/injectedComponents/ParseWysiwyg";

import { stripLead } from "utils/wysiwyg";
import api from "utils/api";
import { STRAPI_URL } from "utils/store";
import { pathFactory } from "utils/path-factory";
import { imagePathHandler, getImageUrl } from "utils/helpers";

const ArticlePage = ({
  exportedArticle = null,
  isPreview,
  mostPopularArticles,
  usefulInformations,
  mainColumns,
  videoArticles,
  recommendedArticles,
  banners,
  headerFeaturedTags,
}) => {
  //////////////////// PREVIEW ARTICLE
  const [article, setArticle] = useState(exportedArticle);
  const [screenWidth, setScreenWidth] = useState();

  // If there is no exported article, we are probably in preview mode
  if (isPreview) {
    useEffect(async () => {
      try {
        const urlParams = new URLSearchParams(window.location.search);
        const id = urlParams.get("id");
        const previewApiKey = urlParams.get("previewApiKey");

        const article = await api.getArticlePreview(id, previewApiKey);
        if (article) {
          setArticle(article);
        } else {
          location.href = "/custom404";
        }
      } catch (e) {
        console.log(e);
      }
    }, []);
  }
  //////////////////// END PREVIEW ARTICLE

  // Increment article viewNum
  if (!isPreview) {
    useEffect(() => {
      setTimeout(async () => {
        api.incrementPostView(article.id);
      }, 5000);
    }, []);
  }

  // Correct cover image title
  useEffect(() => {
    let attempts = 10;

    (function updateImgTitle() {
      const ReactGridGallery = document.querySelector(
        ".ReactGridGallery_tile-viewport > img"
      );
      if (ReactGridGallery) {
        ReactGridGallery.title = article.featuredImage.title;
      } else if (attempts--) {
        setTimeout(() => {
          updateImgTitle();
        }, 1000);
      }
    })();
  }, [screenWidth]);

  const [isAdultOverlayOpened, setIsAdultOverlayOpened] = useState(
    article?.isAdultContent
  );

  if (!article) {
    return <></>;
  }

  const coverImageData =
    article.featuredImage &&
    (article.featuredImage.formats?.large?.url || article.featuredImage.url)
      ? [
          {
            src: `${STRAPI_URL}${
              article.featuredImage.formats?.large?.url ||
              article.featuredImage.url
            }`,
            thumbnail: `${STRAPI_URL}${
              article.featuredImage.formats?.large?.url ||
              article.featuredImage.url
            }`,
            thumbnailCaption: "",
            thumbnailWidth: 1,
            thumbnailHeight: 1,
            alt: article.featuredImage.alt || "",
            caption: (
              <>
                {article?.featuredImage?.title && (
                  <span>{article.featuredImage.title}</span>
                )}
                {article?.featuredImage?.author && (
                  <span>{article.featuredImage.author}</span>
                )}
              </>
            ),
          },
        ]
      : null;

  const ogImage = getImageUrl(article.featuredImage, "medium", false);

  const mobileSizeDataFI =
    article.featuredImage?.formats?.xsmall ||
    article.featuredImage?.formats?.small ||
    article.featuredImage?.formats?.medium ||
    article.featuredImage;

  return (
    <MasterLayout
      headerFeaturedTags={headerFeaturedTags}
      setScreenWidth={setScreenWidth}
      isPreview={exportedArticle === null}
      banners={article.sensitive ? {} : banners}
    >
      <HtmlHead
        title={article.title}
        description={article.lead}
        keywords={article.keywords}
        url={article._URL}
        image={ogImage ? imagePathHandler(ogImage?.url) : undefined}
        imageWidth={ogImage ? ogImage.width : 1200}
        imageHeight={ogImage ? ogImage.height : 680}
        imageMime={ogImage?.mime}
        noIndex={isPreview}
      />
      <main id="article">
        <Row noGutters className="global-section-difference-px">
          <Col tag="aside" md={1} className="global-box-px left-aside desktop">
            <ShareIcons
              title={article.title}
              slug={article.slug}
              path={pathFactory.article(article)}
            />
          </Col>

          <Col
            md={7}
            lg={8}
            className={isAdultOverlayOpened ? " adult-container" : ""}
          >
            {isAdultOverlayOpened && (
              <AdultOverlay
                onPassed={() => {
                  setIsAdultOverlayOpened(false);
                }}
              />
            )}
            <div
              className={`global-box-px main${
                isAdultOverlayOpened ? " adult" : ""
              }`}
            >
              <TimeStampAlert
                time={article.firstPublishedAt || article.published_at}
              />

              <h1 className="titled-cover-image">
                <span className={"mobile"}>{article.title}</span>
                <ArticleMeta article={article} className="mobile" />

                {article.showFeaturedImage && coverImageData ? (
                  <div className="position-relative">
                    <div className="gallery-box desktop">
                      <Gallery
                        enableImageSelection={false}
                        backdropClosesModal={true}
                        images={coverImageData}
                      />
                    </div>

                    <img
                      className="img-fluid mobile"
                      src={coverImageData[0].thumbnail}
                      alt="Fehérvár Médiacentrum fotója"
                      width={mobileSizeDataFI.width}
                      height={mobileSizeDataFI.height}
                    />
                    {article?.featuredImage?.author && (
                      <div
                        className={
                          article.titlePosition &&
                          article.titlePosition == "top"
                            ? "author align-top"
                            : "author align-bottom"
                        }
                      >
                        <div className="author-inner">
                          {article.featuredImage.author}
                        </div>
                      </div>
                    )}
                    <span
                      className={`title desktop ${
                        article.titlePosition && article.titlePosition == "top"
                          ? "align-top"
                          : ""
                      }
                    `}
                    >
                      {article.title}
                    </span>
                  </div>
                ) : (
                  <span className={"desktop"}>{article.title}</span>
                )}

                <ArticleMeta article={article} className="desktop" />
              </h1>

              {article.lead && (
                <div className="wysiwyg-content wysiwyg-lead">
                  {stripLead(article.lead)}
                </div>
              )}

              {article.showBanners !== false &&
                !article.sensitive &&
                banners.articlePage?.lead?.content && (
                  <Banner
                    banner={banners.articlePage.lead}
                    position="Cikk oldal, lead alatti hirdetés"
                  />
                )}

              <ParseWysiwyg
                wysiwygContent={article.content}
                screenWidth={screenWidth}
                banner={
                  article.showBanners === false ||
                  article.sensitive ||
                  !banners.articlePage?.injected?.content
                    ? null
                    : banners.articlePage.injected
                }
                recommendedArticles={recommendedArticles}
                // Strapi booleans are null by default > previous articles have null value
                showRecommended={
                  article.showFavourites === false ? false : true
                }
                showBanner={article.showBanners === false ? false : true}
              />

              {article.showFeaturedImage === false && (
                <PhotoMadeBy name={article.featuredImage?.author} />
              )}

              {article.showBanners !== false &&
                !article.sensitive &&
                banners.articlePage?.afterArticle?.content && (
                  <Banner
                    banner={banners.articlePage.afterArticle}
                    position="Cikk utáni hirdetés"
                  />
                )}

              <FBscrollTagsShare
                tags={article.tags}
                sensitivity={article.sensitive}
                url={article._URL}
              />

              <div className="mobile">
                <MostPopularBox
                  className="my-30"
                  articles={mostPopularArticles}
                  isNumberedContent
                  withLastSeparator
                  exportedArticleId={article.id}
                />

                {article.showBanners !== false &&
                  !article.sensitive &&
                  banners.common?.sidebarTop?.content && (
                    <Banner
                      banner={banners.common.sidebarTop}
                      position="Cikk oldal, oldalsáv teteje hirdetés"
                    />
                  )}

                <ArticleListBox
                  className="sticky"
                  title="Fehérvári hasznos infók"
                  articles={usefulInformations}
                  isListedContent
                />
              </div>

              {/* Sensitive tartalom esetén nem jelenhet meg hasonló cikkek és cikkajánló gomb */}
              {!article.sensitive && <SimilarArticles article={article} />}
            </div>
          </Col>
          <Col
            tag="aside"
            md={4}
            lg={3}
            className="global-box-px global-separated-l right-aside desktop"
          >
            <div className="sticky-box">
              {article.showBanners !== false &&
                !article.sensitive &&
                banners.common?.sidebarTop?.content && (
                  <div className="global-box-pb">
                    <Banner
                      banner={banners.common.sidebarTop}
                      isSide
                      position="Cikk oldal, oldalsáv teteje hirdetés"
                    />
                  </div>
                )}
              <div className="sticky">
                <MostPopularBox
                  articles={mostPopularArticles}
                  isNumberedContent
                  exportedArticleId={article.id}
                />
              </div>
            </div>
            <div className="sticky-box">
              <div className="my-20 sticky">
                <Newsletter />
                {article.showBanners !== false &&
                  !article.sensitive &&
                  banners.common?.sidebarMiddle?.content && (
                    <div className="global-box-pt">
                      <Banner
                        banner={banners.common.sidebarMiddle}
                        isSide
                        position="Cikk oldal, oldalsáv köztes hirdetés"
                      />
                    </div>
                  )}
              </div>
            </div>

            <div className="sticky-box">
              <ArticleListBox
                className="sticky"
                title="Fehérvári hasznos infók"
                articles={usefulInformations}
                isListedContent
              />
            </div>

            {/* Strapi booleans are null by default > old articles have null value  */}
            {article.showFavourites !== false && (
              <div className="sticky-box">
                <ArticleListBox
                  className="sticky"
                  articles={recommendedArticles}
                  title="Kedvencek"
                />
              </div>
            )}
          </Col>
        </Row>
        <FrontPageSection6
          title="Multimédia"
          action={{ text: "Még több cikk", url: "/multimedia" }}
          videoArticles={videoArticles}
        />
        <CustomDivider className="global-section-p" />
        <TopicSuggestSection
          defaultButtonLabel="Még több cikk"
          columns={mainColumns}
        />

        {article.showBanners !== false &&
          !article.sensitive &&
          banners.common?.footer?.content && (
            <div className="global-section-p">
              <Banner
                banner={banners.common.footer}
                position="Cikk oldali lábléc hirdetés"
              />
            </div>
          )}
      </main>
    </MasterLayout>
  );
};

ArticlePage.getInitialProps = async function ({ query: props }) {
  return {};
};

export default ArticlePage;
