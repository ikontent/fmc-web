import HtmlHead from "components/elements/HtmlHead";
import { Row, Col } from "reactstrap";
import MasterLayout from "components/layouts/Master";
import AuthorCard from "components/elements/AuthorCard";
import ArticleCardHorizontal from "components/elements/articles/ArticleCardHorizontal";
import Pagination from "components/elements/Pagination";
import CustomDivider from "components/elements/CustomDivider";
import ArticleListBox from "components/elements/articles/ArticleListBox";
import MostPopularBox from "components/elements/articles/MostPopularBox";
import Banner from "components/customPositioned/Banner";
import Newsletter from "components/elements/Newsletter";
import FrontPageSection6 from "components/sections/FrontPageSection6";
import TopicSuggestSection from "components/sections/TopicSuggestSection";
import FrontPageSection8 from "components/sections/FrontPageSection8";
import { isBannerVisible } from "utils/banner";

const authorPage = ({
  author = {},
  articlesOfAuthor = [],
  _pagination,
  numberOfAllArticles,
  mostPopularArticles,
  usefulInformations,
  mainColumns,
  videoArticles,
  banners,
  fehervarWeeklySection,
  headerFeaturedTags,
}) => {
  return (
    <MasterLayout headerFeaturedTags={headerFeaturedTags} banners={banners}>
      <HtmlHead
        title={author.name}
        description={author.name}
        keywords={author.name}
        url={_pagination.pageURL}
      />
      <Row
        tag="section"
        noGutters
        id="author-page"
        className="global-section-pt"
      >
        {/* ############### MAIN ################ */}
        <Col
          className="global-box-px"
          md={{ size: 8 }}
          xl={{ offset: 1, size: 7 }}
        >
          <AuthorCard
            author={author}
            numberOfAllArticles={numberOfAllArticles}
          />

          {articlesOfAuthor.map((article, index) => (
            <div key={index}>
              <ArticleCardHorizontal
                article={article}
                showPublishedAt
                showTopic
                showTag
                showTagBoxWithin
              />
              {articlesOfAuthor.length - 1 > index && (
                <CustomDivider className="global-box-py" />
              )}
            </div>
          ))}
          {_pagination.pageCount > 1 && (
            <>
              <Row className="global-box-py article-buttons">
                <Col>
                  <Pagination _pagination={_pagination} size="md" />
                </Col>
              </Row>
            </>
          )}
        </Col>
        <Col size={1} className="d-none d-xl-block" />

        {/* ############### SIDE ################ */}

        <Col
          tag="aside"
          className="
          global-box-px
          global-separated-l
          desktop"
          md={4}
          xl={3}
        >
          <div className="sticky-box">
            {isBannerVisible(banners.common?.sidebarTop) && (
              <Banner
                banner={banners.common.sidebarTop}
                isSide
                position="Szerző oldal, oldalsáv teteje hirdetés"
              />
            )}
            <div className="global-box-pb sticky">
              <MostPopularBox
                articles={mostPopularArticles}
                isNumberedContent
              />
            </div>
          </div>

          <div className="sticky-box">
            <div className="sticky">
              <div className="global-box-pb">
                <Newsletter />
              </div>
              {isBannerVisible(banners.common?.sidebarMiddle) && (
                <Banner
                  banner={banners.common.sidebarMiddle}
                  isSide
                  position="Szerző oldal, oldalsáv köztes hirdetés"
                />
              )}
            </div>
          </div>

          <div className="sticky-box">
            <ArticleListBox
              className="sticky"
              title="Fehérvári hasznos infók"
              articles={usefulInformations}
              isListedContent
            />
          </div>
        </Col>
      </Row>

      {/* ############### MAIN CONTINOUS MOBILE ################ */}

      <div className="global-section-px mobile">
        <div className="global-box-py">
          {isBannerVisible(banners.common?.sidebarTop) && (
            <Banner
              banner={banners.common.sidebarTop}
              className="mt-0 mb-2"
              position="Szerző oldali mobil hirdetés"
            />
          )}
        </div>
        <MostPopularBox articles={mostPopularArticles} isNumberedContent />
        <div className="global-box-py">
          <Newsletter />
        </div>
      </div>

      <div className="mobile">
        <FrontPageSection6
          title="Multimédia"
          action={{ text: "Még több cikk", url: "/multimedia" }}
          videoArticles={videoArticles}
        />
      </div>

      {/* ############### MAIN CONTINOUS MIX ################ */}

      <div className="desktop">
        <FrontPageSection6
          title="Multimédia"
          action={{ text: "Még több cikk", url: "/multimedia" }}
          videoArticles={videoArticles}
        />
      </div>

      <CustomDivider className="global-section-px py-12" />

      <TopicSuggestSection
        defaultButtonLabel="Még több cikk"
        columns={mainColumns}
      />

      {isBannerVisible(banners.common?.footer) && (
        <>
          <div className="global-section-px mobile">
            <Banner
              banner={banners.common.footer}
              className="mt-0 mb-2"
              position="Szerző oldal, mobil lábléc hirdetés"
            />
          </div>

          <div className="global-section-px desktop">
            <div className="global-box-py global-separated-y">
              <Banner
                banner={banners.common.footer}
                className="my-0"
                position="Szerző oldal, desktop lábléc hirdetés"
              />
            </div>
          </div>
        </>
      )}

      <FrontPageSection8 {...fehervarWeeklySection} />
    </MasterLayout>
  );
};

authorPage.getInitialProps = async function ({ query: props }) {
  return {};
};

export default authorPage;
