import { useState, useEffect } from "react";
import api from "utils/api";
import MasterLayout from "components/layouts/Master";
import FrontPageSection1 from "components/sections/FrontPageSection1";
import DividerTrio from "components/elements/DividerTrio";
import FrontPageSection3 from "components/sections/FrontPageSection3";
import FrontPageSection5 from "components/sections/FrontPageSection5";
import FrontPageSection6 from "components/sections/FrontPageSection6";
import TopicSuggestSection from "components/sections/TopicSuggestSection";
import FrontPageSection8 from "components/sections/FrontPageSection8";
import CustomDivider from "components/elements/CustomDivider";
import Banner from "components/customPositioned/Banner";
import { pathFactory } from "utils/path-factory";
import { isBannerVisible } from "utils/banner";

const IndexPage = ({
  news1,
  news2,
  news3,
  fehervarArticles,
  mostPopularArticles,
  recommendedArticles,
  newsFlowDivider1,
  usefulInformations,
  newsFlowDivider2,
  newsFlowDivider3,
  //videoArticles,
  mainColumns,
  fehervarWeeklySection,
  banners,
  advertisements,
  headerFeaturedTags,
  isFrontPagePreview = false,
}) => {
  const [freshArticles, setFreshArticles] = useState([]);

  useEffect(async () => {
    try {
      const externalNews = await api.getExternalNews();
      setFreshArticles(
        externalNews.map((e) => {
          return {
            ...e,
            _URL: e?.sourceUrl ? undefined : pathFactory.article(e),
          };
        })
      );
    } catch (e) {
      console.log(e);
    }
  }, []);

  return (
    <MasterLayout
      headerFeaturedTags={headerFeaturedTags}
      banners={banners}
      banner={banners.common?.header}
      meta={{
        additionalMeta: [
          {
            name: "facebook-domain-verification",
            content: "9q0a6rhnwplx003i106z0ljgnni43l",
          },
        ],
      }}
      isFrontPagePreview={isFrontPagePreview}
    >
      <main id="index-page">
        <FrontPageSection1
          fehervarArticles={fehervarArticles}
          banners={banners.common}
          freshArticles={freshArticles}
          feedArticles={news1}
          mostPopularArticles={mostPopularArticles}
          recommendedArticles={recommendedArticles}
        />

        {newsFlowDivider1?.articles?.length > 0 && (
          <DividerTrio {...newsFlowDivider1} />
        )}

        {isBannerVisible(banners.frontPage) && (
          <div className="desktop">
            <CustomDivider className="global-section-px" />
            <aside className="global-section-px global-box-py">
              <Banner
                className="front-page-desktop"
                banner={banners.frontPage}
                position="Főoldal, desktop fehérvári és ajánlott cikkek közti hirdetés"
              />
            </aside>
          </div>
        )}

        <CustomDivider className="global-section-px" zIndex="16" />

        <FrontPageSection3
          recommendedArticles={recommendedArticles}
          banners={banners.common}
          feedArticles={news2}
          advertisement={advertisements.news2}
          usefulInformations={usefulInformations}
        />

        {newsFlowDivider2?.articles?.length > 0 && (
          <DividerTrio {...newsFlowDivider2} />
        )}

        <CustomDivider className="global-section-px" />

        <FrontPageSection5 feedArticles={news3} />

        <FrontPageSection6
          title="Multimédia"
          action={{ text: "Még több cikk", url: "/multimedia" }}
        />

        <TopicSuggestSection
          defaultButtonLabel="Még több cikk"
          columns={mainColumns}
        />

        {isBannerVisible(banners.common?.footer) && (
          <>
            <div className="mobile">
              <aside className="global-section-px">
                <div className="global-box-sm-py global-separated-sm-y">
                  <Banner
                    banner={banners.common.footer}
                    position="Főoldal, mobil lábléc hirdetés"
                  />
                </div>
              </aside>
            </div>
            <div className="desktop">
              <CustomDivider className="my-17" />
              <Banner
                banner={banners.common.footer}
                position="Főoldal, desktop lábléc hirdetés"
              />
            </div>
          </>
        )}

        <FrontPageSection8 {...fehervarWeeklySection} />
      </main>
    </MasterLayout>
  );
};

IndexPage.getInitialProps = async function ({ query: props }) {
  return {};
};

export default IndexPage;
