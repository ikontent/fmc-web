import { useState, useEffect } from "react";
import api from "utils/api";
import IndexPage from "./indexPage";

const IndexPagePreview = ({
  news1,
  news2,
  news3,
  fehervarArticles,
  mostPopularArticles,
  recommendedArticles,
  newsFlowDivider1,
  usefulInformations,
  newsFlowDivider2,
  newsFlowDivider3,
  //videoArticles,
  mainColumns,
  fehervarWeeklySection,
  banners,
  advertisements,
  headerFeaturedTags,
  isFrontPagePreview = false,
}) => {
  const [newsCollection, setNewsCollection] = useState({
    news1: [],
    news2: [],
    news3: [],
  });

  useEffect(async () => {
    try {
      const urlParams = new URLSearchParams(window.location.search);
      const previewApiKey = urlParams.get("previewApiKey");
      const { result: previewNewsCollection } = await api.getFrontpagePreview(
        previewApiKey
      );
      if (previewNewsCollection) {
        setNewsCollection({
          news1: previewNewsCollection.news1,
          news2: previewNewsCollection.news2,
          news3: previewNewsCollection.news3,
        });
      } else {
        location.href = "/custom404";
      }
    } catch (e) {
      console.log(e);
    }
  }, []);

  return newsCollection.news1.length > 0 ? (
    <IndexPage
      news1={newsCollection.news1}
      news2={newsCollection.news2}
      news3={newsCollection.news3}
      fehervarArticles={fehervarArticles}
      mostPopularArticles={mostPopularArticles}
      recommendedArticles={recommendedArticles}
      newsFlowDivider1={newsFlowDivider1}
      usefulInformations={usefulInformations}
      newsFlowDivider2={newsFlowDivider2}
      newsFlowDivider3={newsFlowDivider3}
      //videoArticles,
      mainColumns={mainColumns}
      fehervarWeeklySection={fehervarWeeklySection}
      banners={banners}
      advertisements={advertisements}
      headerFeaturedTags={headerFeaturedTags}
      isFrontPagePreview={isFrontPagePreview}
    />
  ) : null;
};

IndexPagePreview.getInitialProps = async function ({ query: props }) {
  return {};
};

export default IndexPagePreview;
