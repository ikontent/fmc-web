import HtmlHead from "components/elements/HtmlHead";
import { Row, Col } from "reactstrap";
import MasterLayout from "components/layouts/Master";
import FrontPageSection6 from "components/sections/FrontPageSection6";
import TopicSuggestSection from "components/sections/TopicSuggestSection";
import CustomDivider from "components/elements/CustomDivider";
import Banner from "components/customPositioned/Banner";
import ArticleListBox from "components/elements/articles/ArticleListBox";
import MostPopularBox from "components/elements/articles/MostPopularBox";
import Newsletter from "components/elements/Newsletter";
import Pagination from "components/elements/Pagination";
import ArticleVerticalWithHeading from "components/elements/articles/ArticleVerticalWithHeading";
import {
  ArticleTitle,
  ArticleButton,
} from "components/elements/articles/ArticleComponents";
import { STRAPI_URL } from "utils/store";
import { isBannerVisible } from "utils/banner";

const MagazinePage = ({
  _pagination,
  banners,
  mostPopularArticles,
  mainColumns,
  videoArticles,
  magazines,
  headerFeaturedTags,
}) => {
  return (
    <MasterLayout headerFeaturedTags={headerFeaturedTags} banners={banners}>
      <HtmlHead
        title="Fehérvár hetilapok"
        description="Fehérvár hetilapok"
        keywords="Fehérvár, hetilapok, magazinok"
        url={_pagination.pageURL}
      />
      <main id="magazines">
        <Row noGutters className="global-section-px mobile">
          <Col className="mt-50">
            <ArticleTitle title={"FehérVár hetilap"} />
          </Col>
        </Row>
        <Row>
          <Col
            className="global-box-px"
            md={{ offset: 1, size: 6 }}
            lg={{ offset: 1, size: 7 }}
          >
            <h2 className="mt-3">Fehérvár hetilapok</h2>
            <Row noGutters className="justify-content-center">
              {(magazines || []).slice(0, 6).map((magazine, index) => (
                <Col
                  className="global-item-pt global-item-pl global-section-difference-pb"
                  key={`item-${index}`}
                  xs={12}
                  sm={6}
                  md={4}
                  lg={3}
                >
                  <ArticleVerticalWithHeading
                    article={{
                      id: magazine.id,
                      lead: magazine.text,
                      published_at: magazine.date,
                      featuredImage: magazine.image,
                      title: "",
                    }}
                    index={index}
                    isItFehervariHetilap
                  />

                  <div className="pt-3 global-box-difference-pb">
                    <ArticleButton
                      text="Letöltés"
                      url={
                        magazine?.magazine?.url
                          ? `${STRAPI_URL}${magazine.magazine.url}`
                          : "#"
                      }
                      target="_blank"
                    />
                  </div>
                </Col>
              ))}
            </Row>
            <div className="text-center mt-4">
              <Pagination _pagination={_pagination} size="md" />
            </div>
          </Col>
          <Col size={1} />

          {/* ############### SIDE ################ */}

          <Col
            tag="aside"
            className="
            mt-3
          global-box-px
          global-separated-l
          global-box-xs-pt
          global-box-reset-md-pt
          desktop"
            md={4}
            lg={3}
          >
            <div className="sticky">
              <div className="global-box-pb">
                <Newsletter />
              </div>
              {isBannerVisible(banners.common?.sidebarTop) && (
                <div className="global-box-pb">
                  <Banner
                    banner={banners.common.sidebarTop}
                    isSide
                    position="Magazin oldal, oldalsáv teteje hirdetés"
                  />
                </div>
              )}
            </div>
          </Col>
        </Row>

        <div className="global-section-px mobile">
          {isBannerVisible(banners.common?.sidebarTop) && (
            <div className="global-box-py">
              <Banner
                banner={banners.common.sidebarTop}
                position="Magazin oldal, mobil oldalsáv teteje hirdetés"
              />
            </div>
          )}

          <div className="global-box-pb">
            <MostPopularBox articles={mostPopularArticles} isNumberedContent />
          </div>
          <div className="global-box-pb">
            <Newsletter />
          </div>
          <CustomDivider className="pt-10" />
        </div>
        <div className="mobile">
          <FrontPageSection6
            title="Multimédia"
            action={{ text: "Még több cikk", url: "/multimedia" }}
            videoArticles={videoArticles}
          />
        </div>
        <div className="desktop">
          <FrontPageSection6
            title="Multimédia"
            action={{ text: "Még több cikk", url: "/multimedia" }}
            videoArticles={videoArticles}
          />
        </div>
        <CustomDivider className="global-section-px my-12" />
        <TopicSuggestSection
          defaultButtonLabel="Még több cikk"
          columns={mainColumns}
        />

        {isBannerVisible(banners.common?.footer) && (
          <>
            <div className="global-section-px mobile">
              <Banner
                banner={banners.common.footer}
                position="Magazin oldal, mobil lábléc hirdetés"
              />
            </div>

            <div className="global-section-px desktop">
              <div className="global-box-py global-separated-y">
                <Banner
                  banner={banners.common.footer}
                  position="Magazin oldal, desktop lábléc hirdetés"
                />
              </div>
            </div>
          </>
        )}
      </main>
    </MasterLayout>
  );
};

MagazinePage.getInitialProps = async function ({ query: props }) {
  return {};
};

export default MagazinePage;
