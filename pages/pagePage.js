import HtmlHead from "components/elements/HtmlHead";
import { Row, Col } from "reactstrap";
// import Pagination from "components/elements/Pagination";
import MasterLayout from "components/layouts/Master";
import CustomDivider from "components/elements/CustomDivider";
import ArticleListBox from "components/elements/articles/ArticleListBox";
import MostPopularBox from "components/elements/articles/MostPopularBox";
import Banner from "components/customPositioned/Banner";
import Newsletter from "components/elements/Newsletter";
import FrontPageSection6 from "components/sections/FrontPageSection6";
import TopicSuggestSection from "components/sections/TopicSuggestSection";
import FrontPageSection8 from "components/sections/FrontPageSection8";
import WysiwygContent from "components/customPositioned/WysiwygContent";
import { isBannerVisible } from "utils/banner";

const OWN_URL = process.env.NEXT_PUBLIC_OWN_URL;

const PagePage = ({
  page,
  path,
  mostPopularArticles,
  mainColumns,
  videoArticles,
  banners,
  fehervarWeeklySection,
  headerFeaturedTags,
  usefulInformations,
  recommendedArticles,
}) => {
  return (
    <MasterLayout headerFeaturedTags={headerFeaturedTags} banners={banners}>
      <HtmlHead
        title={page.title}
        description={page.meta_description}
        keywords={page.meta_keywords}
        url={OWN_URL + path}
      />
      <main id="page-page">
        <Row noGutters>
          <Col
            className="global-box-px"
            md={{ offset: 1, size: 6 }}
            lg={{ offset: 1, size: 7 }}
          >
            <h2 className="title">{page.title}</h2>
            <WysiwygContent post={page} />
          </Col>
          <Col size={1} />
          {/* ############### SIDE ################ */}

          <Col
            tag="aside"
            className="
            mt-3
          global-box-px
          global-separated-l
          global-box-xs-pt
          global-box-reset-md-pt
          desktop"
            md={4}
            lg={3}
          >
            <div className="sticky-box">
              {isBannerVisible(banners.common?.sidebarTop) && (
                <div className="global-box-pb">
                  <Banner
                    banner={banners.common.sidebarTop}
                    isSide
                    position="Általános oldal, oldalsáv teteje hirdetés"
                  />
                </div>
              )}
              <div className="sticky">
                <MostPopularBox
                  articles={mostPopularArticles}
                  isNumberedContent
                />
              </div>
            </div>
            <div className="sticky-box">
              <div className="my-12 sticky">
                <Newsletter />
                {isBannerVisible(banners.common?.sidebarMiddle) && (
                  <div className="global-box-pt">
                    <Banner
                      banner={banners.common.sidebarMiddle}
                      isSide
                      position="Általános oldal, oldalsáv köztes hirdetés"
                    />
                  </div>
                )}
              </div>
            </div>

            <div className="sticky-box">
              <ArticleListBox
                className="sticky"
                title="Fehérvári hasznos infók"
                articles={usefulInformations}
                isListedContent
              />
            </div>
            <div className="sticky-box">
              <ArticleListBox
                className="sticky"
                articles={recommendedArticles}
                title="Kedvencek"
              />
            </div>
          </Col>
        </Row>

        <CustomDivider className="global-section-px" />

        <TopicSuggestSection
          defaultButtonLabel="Még több cikk"
          columns={mainColumns}
        />

        <div className="">
          <FrontPageSection6
            title="Multimédia"
            action={{ text: "Még több cikk", url: "/multimedia" }}
            videoArticles={videoArticles}
          />
        </div>

        {isBannerVisible(banners.common?.footer) && (
          <>
            <div className="global-section-px mobile">
              <Banner
                banner={banners.common.footer}
                className="mt-0 mb-2"
                position="Általános oldal, mobil lábléc hirdetés"
              />
            </div>

            <div className="global-section-px desktop">
              <div className="global-box-py global-separated-y">
                <Banner
                  banner={banners.common.footer}
                  className="my-0"
                  position="Általános oldal, desktop lábléc hirdetés"
                />
              </div>
            </div>
          </>
        )}

        <FrontPageSection8 {...fehervarWeeklySection} />
      </main>
    </MasterLayout>
  );
};

PagePage.getInitialProps = async function ({ query: props }) {
  return {};
};

export default PagePage;
