import HtmlHead from "components/elements/HtmlHead";
import { useState, useEffect, useRef, useMemo } from "react";
import { Row, Col, Spinner } from "reactstrap";
import api from "utils/api";
import MasterLayout from "components/layouts/Master";

import ArticleCardHorizontal from "components/elements/articles/ArticleCardHorizontal";
import CustomDivider from "components/elements/CustomDivider";
import MostPopularBox from "components/elements/articles/MostPopularBox";
import Banner from "components/customPositioned/Banner";
import Newsletter from "components/elements/Newsletter";
import FrontPageSection6 from "components/sections/FrontPageSection6";
import TopicSuggestSection from "components/sections/TopicSuggestSection";
import FrontPageSection8 from "components/sections/FrontPageSection8";
import { pathFactory } from "utils/path-factory";
import Tag from "components/elements/Tag";
import { isBannerVisible } from "utils/banner";

const OWN_URL = process.env.NEXT_PUBLIC_OWN_URL;

const SearchResults = ({
  mostPopularArticles,
  banners,
  videoArticles,
  mainColumns,
  fehervarWeeklySection,
  headerFeaturedTags,
}) => {
  const RESULTS_PER_PAGE = 10;

  const searchQuery = useRef();

  const [allResults, setAllResults] = useState();
  const [displayedResults, setDisplayedResults] = useState();
  const [pageNum, setPageNum] = useState(1);

  const countTopicsAndTags = useMemo(() => {
    return allResults
      ? (allResults.topics.length || 0) + (allResults.tags.length || 0)
      : 0;
  }, [allResults]);

  const countAuthorsAndPosts = useMemo(() => {
    return allResults
      ? (allResults.authors.length || 0) + (allResults.posts.length || 0)
      : 0;
  }, [allResults]);

  const handleScrollDown = () => {
    if (
      countAuthorsAndPosts / RESULTS_PER_PAGE > pageNum &&
      window.innerHeight + window.scrollY >=
        document.body.offsetHeight -
          3000 /* TODO: repleace this to Intersection API */
    ) {
      setPageNum((p) => p + 1);
    }
  };

  useEffect(async () => {
    searchQuery.current = new URLSearchParams(window.location.search).get("s");
    if (!allResults) {
      const fetchedResults = await api.getSearchResults(searchQuery.current);
      setAllResults(fetchedResults);
    }
  }, [allResults]);

  useEffect(() => {
    if (allResults)
      setDisplayedResults(
        [...allResults.authors, ...allResults.posts].slice(
          0,
          pageNum * RESULTS_PER_PAGE
        )
      );
  }, [allResults, pageNum]);

  useEffect(() => {
    window.addEventListener("scroll", handleScrollDown);
    return () => window.removeEventListener("scroll", handleScrollDown);
  }, [handleScrollDown]);

  return (
    <MasterLayout headerFeaturedTags={headerFeaturedTags} banners={banners}>
      <HtmlHead
        title={`Találatok a következőre: “${searchQuery.current}”`}
        description="fmc.hu keresés"
        keywords={`fmc.hu, keresés, ${searchQuery.current}`}
        url={`${OWN_URL}/kereses?s=${searchQuery.current}`}
      />
      <Row
        tag="section"
        noGutters
        id="search-page"
        className="global-section-md-pt"
      >
        {/* ############### MAIN ################ */}
        <Col
          className="global-box-px"
          md={{ size: 8 }}
          xl={{ offset: 1, size: 7 }}
        >
          <h2 className="mt-md-10">
            Találatok a következőre: “{searchQuery.current}”
          </h2>

          {displayedResults ? (
            displayedResults.length === 0 ? (
              <>
                <div className="mt-5"></div>
                <CustomDivider />
                <div className="no-search-result">Sajnos nincs találat :(</div>
              </>
            ) : (
              <>
                <div className="mb-5">
                  {countTopicsAndTags + countAuthorsAndPosts} találat
                </div>
                <CustomDivider className="global-box-pb" />

                {allResults.topics.length || allResults.tags.length ? (
                  <>
                    <div className="topics-and-tags">
                      {allResults.topics.length
                        ? allResults.topics.map((item) => (
                            <Tag
                              text={item.name}
                              hasBackground
                              hasBlackText
                              href={pathFactory.topic(item)}
                              key={item.id + item.name}
                            />
                          ))
                        : null}

                      {allResults.tags.length
                        ? allResults.tags.map((item) => (
                            <Tag
                              text={"#" + item.tagName}
                              hasBackground
                              href={pathFactory.tag(item)}
                              key={item.id + item.tagName}
                            />
                          ))
                        : null}
                    </div>
                    <CustomDivider className="global-box-py" />
                  </>
                ) : null}

                {displayedResults.map((item, index) => (
                  <div key={index}>
                    {"email" in item ? (
                      <ArticleCardHorizontal
                        article={{
                          _URL: pathFactory.author(item),
                          title: item.name,
                          featuredImage: item.avatar,
                          showFeaturedImage: true,
                          lead: item.about,
                        }}
                      />
                    ) : (
                      <ArticleCardHorizontal
                        article={item}
                        showPublishedAt
                        showTopic
                        showTag
                        showTagBoxWithin
                      />
                    )}
                    {displayedResults.length - 1 > index && (
                      <CustomDivider className="global-box-py" />
                    )}
                  </div>
                ))}
              </>
            )
          ) : (
            <div className="text-center mt-5">
              <Spinner type="grow" color="dark" />
            </div>
          )}

          <Row>
            <Col className="text-center">
              {pageNum ===
                Math.ceil(
                  (countTopicsAndTags + countAuthorsAndPosts) / RESULTS_PER_PAGE
                ) && (
                <>
                  <CustomDivider className="global-box-pt" />
                  <div className="end-of-search-results">Találatok vége</div>
                </>
              )}
            </Col>
          </Row>
        </Col>
        <Col size={1} className="d-none d-xl-block" />

        {/* ############### SIDE ################ */}

        <Col
          tag="aside"
          className="
        global-box-px
        mb-17
        global-separated-l
        desktop"
          md={4}
          xl={3}
        >
          <div className="sticky-box">
            {isBannerVisible(banners.common?.sidebarTop) && (
              <Banner
                banner={banners.common.sidebarTop}
                isSide
                position="Kereső oldal, oldalsáv teteje hirdetés"
              />
            )}
            {displayedResults?.length && (
              <MostPopularBox
                className="sticky"
                articles={mostPopularArticles}
                isNumberedContent
              />
            )}
          </div>
        </Col>
      </Row>

      {/* ############### MAIN CONTINOUS MOBILE ################ */}

      <div className="global-section-px mobile">
        {displayedResults?.length ? <CustomDivider className="mt-17" /> : <></>}
        {isBannerVisible(banners.common?.sidebarTop) && (
          <Banner
            banner={banners.common.sidebarTop}
            className="mt-17 mb-25"
            position="Kereső oldal, mobil oldalsáv teteje hirdetés"
          />
        )}
        {displayedResults?.length && (
          <>
            <MostPopularBox
              articles={mostPopularArticles}
              isNumberedContent
              withLastSeparator
            />
            <Newsletter className="my-25" />
          </>
        )}
      </div>

      {/* ############### MAIN CONTINOUS COMMON ################ */}

      <CustomDivider className="global-section-px my-17" />

      <TopicSuggestSection
        defaultButtonLabel="Még több cikk"
        columns={mainColumns}
      />

      {/* <CustomDivider className="global-section-px" /> */}

      <div className="desktop">
        <FrontPageSection6
          title="Multimédia"
          action={{ text: "Még több cikk", url: "/multimedia" }}
          videoArticles={videoArticles}
        />
      </div>
      <div className="mobile">
        <FrontPageSection6
          title="Multimédia"
          action={{ text: "Még több cikk", url: "/multimedia" }}
          videoArticles={videoArticles}
        />
      </div>

      {isBannerVisible(banners.common?.footer) ? (
        <>
          <div className="global-section-px mobile">
            <Banner
              banner={banners.common.footer}
              className="mt-0 mb-2"
              position="Kereső oldal, mobil lábléc hirdetés"
            />
          </div>

          <div className="global-section-px desktop">
            <div className="global-box-py global-separated-y">
              <Banner
                banner={banners.common.footer}
                className="my-0"
                position="Kereső oldal, desktop lábléc hirdetés"
              />
            </div>
          </div>
        </>
      ) : (
        <>{/* <CustomDivider className="global-section-px" /> */}</>
      )}

      <FrontPageSection8 {...fehervarWeeklySection} />
    </MasterLayout>
  );
};

SearchResults.getInitialProps = async function ({ query: props }) {
  return {};
};

export default SearchResults;
