// import { useState } from "react";
import HtmlHead from "components/elements/HtmlHead";
import { Row, Col } from "reactstrap";
import MasterLayout from "components/layouts/Master";
import DividerTrio from "components/elements/DividerTrio";
import TopicSection from "components/topics/TopicSection";
import FrontPageSection6 from "components/sections/FrontPageSection6";
import TopicSuggestSection from "components/sections/TopicSuggestSection";
import FrontPageSection8 from "components/sections/FrontPageSection8";
import CustomDivider from "components/elements/CustomDivider";
import { ArticleTitle } from "components/elements/articles/ArticleComponents";
import Banner from "components/customPositioned/Banner";
import MostPopularBox from "components/elements/articles/MostPopularBox";
import Newsletter from "components/elements/Newsletter";
import TopicMenu from "components/topics/TopicMenu";
import { isBannerVisible } from "utils/banner";

const OWN_URL = process.env.NEXT_PUBLIC_OWN_URL;

const TopicPage = ({
  topic,
  topics,
  topicArticles,
  _pagination,
  banners,
  mostPopularArticles,
  mainColumns,
  videoArticles,
  fehervarWeeklySection,
  subTopicRecommendation,
  usefulInformations,
  headerFeaturedTags,
}) => {
  const mainTopic = topic.parent
    ? topics.find((t) => t.id === topic.parent.id)
    : topic;

  // const [screenWidth, setScreenWidth] = useState();

  return (
    <MasterLayout
      mainTopic={mainTopic}
      headerFeaturedTags={headerFeaturedTags}
      banners={banners}
      // setScreenWidth={setScreenWidth}
    >
      <HtmlHead
        title={topic.name + " - fmc.hu"}
        description={topic.name}
        keywords={topic.name}
        url={`${OWN_URL}${_pagination.pageURL}`}
      />
      <main id="topic-page">
        <Row noGutters className="global-section-px mobile">
          <Col className="mt-10">
            <ArticleTitle title={mainTopic.name} />
          </Col>
        </Row>

        {topicArticles.length == 0 && (
          <>
            <Row tag="section" noGutters className="global-section-md-pt">
              <Col className="global-box-px" md={{ offset: 1, size: 7 }}>
                <TopicMenu mainTopic={mainTopic} />
              </Col>
            </Row>
            <Row tag="section" noGutters>
              <Col
                sm={{ offset: 2, size: 8 }}
                lg={{ offset: 3, size: 6 }}
                className="custom-top global-section-px py-5 text-center"
              >
                <h3>Sajnos ebben a rovatban még nincsenek cikkek</h3>
                <p className="mb-2">Látogass vissza később</p>
              </Col>
            </Row>
          </>
        )}

        {[
          { indexFrom: 0, numberOfArticles: 8 },
          { indexFrom: 8, numberOfArticles: 9 },
          { indexFrom: 17, numberOfArticles: 8 },
        ].map(
          (section, index) =>
            topicArticles.length >= section.indexFrom + 1 && (
              <div key={"section-" + index}>
                <TopicSection
                  // As TopicSection contains both masonry and sidebar, therefore providing
                  // the entire feedArticles mimics production operation better, allowing
                  // sidebar components to fish their data among the full, un-sliced range.
                  topic={topic}
                  topics={topics}
                  mainTopic={mainTopic}
                  articles={topicArticles}
                  _pagination={_pagination}
                  section={index + 1}
                  displayArticlesFrom={section.indexFrom}
                  displayArticlesTo={
                    section.indexFrom + section.numberOfArticles
                  }
                  mostPopularArticles={mostPopularArticles}
                  usefulInformations={usefulInformations}
                  banners={banners}
                  // screenWidth={screenWidth}
                />

                {!topic.parent &&
                index < 2 &&
                subTopicRecommendation.length > index &&
                topicArticles.length >=
                  section.indexFrom + section.numberOfArticles &&
                subTopicRecommendation[index].articles?.length > 0 ? (
                  <>
                    <CustomDivider className="global-section-px global-section-pt" />
                    <DividerTrio
                      title={subTopicRecommendation[index].title}
                      action={{
                        text: "Még több cikk",
                        url: subTopicRecommendation[index].url,
                      }}
                      articles={subTopicRecommendation[index].articles}
                    />
                    <CustomDivider className="global-section-px global-section-pb" />
                  </>
                ) : null}
              </div>
            )
        )}

        <div className="global-section-px mobile">
          {isBannerVisible(banners.common?.sidebarTop) && (
            <div className="global-box-py">
              <Banner
                banner={banners.common.sidebarTop}
                position={`${topic.name} rovat oldal, mobil oldalsáv teteje hirdetés`}
              />
            </div>
          )}

          <div className="global-box-pb">
            <MostPopularBox articles={mostPopularArticles} isNumberedContent />
          </div>
          <div className="global-box-pb">
            <Newsletter />
          </div>

          {isBannerVisible(banners.common?.sidebarMiddle) && (
            <Banner
              banner={banners.common.sidebarMiddle}
              position={`${topic.name} rovat oldal, mobil oldalsáv köztes hirdetés`}
            />
          )}
        </div>

        <div className="mobile">
          <FrontPageSection6
            title="Multimédia"
            action={{ text: "Még több cikk", url: "/multimedia" }}
            videoArticles={videoArticles}
          />
        </div>

        <div className="desktop">
          <FrontPageSection6
            title="Multimédia"
            action={{ text: "Még több cikk", url: "/multimedia" }}
            videoArticles={videoArticles}
          />
        </div>

        <TopicSuggestSection
          defaultButtonLabel="Még több cikk"
          columns={mainColumns}
        />

        {isBannerVisible(banners.common?.footer) && (
          <>
            <div className="global-section-px mobile">
              <Banner
                banner={banners.common.footer}
                position={`${topic.name} rovat oldal, mobil lábléc hirdetés`}
              />
            </div>

            <div className="global-section-px desktop">
              <div className="global-box-py global-separated-y">
                <Banner
                  banner={banners.common.footer}
                  position={`${topic.name} rovat oldal, desktop lábléc hirdetés`}
                />
              </div>
            </div>
          </>
        )}

        <FrontPageSection8 {...fehervarWeeklySection} />
      </main>
    </MasterLayout>
  );
};

TopicPage.getInitialProps = async function ({ query: props }) {
  return {};
};

export default TopicPage;
