#!/bin/bash
set -e;

npm run onlyexport;

echo "PUBLISH SCRIPT START"
echo $(date);
A=$(date +%s);
echo "there is no dist env or folder is not exist!!!!";
touch dist;
rm -rf dist_moved;
mv dist dist_moved;
mv out dist;
echo "SWAPPING END TIME IN SEC: " $(($(date +%s)-$A));
rm -rf dist_moved;
echo "DELETE OLD END TIME IN SEC: " $(($(date +%s)-$A));
echo "PUBLISH SCRIPT ENDED";

