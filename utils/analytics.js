export const logEvent = (category, action, label, value) => {
  if (category && action) {
    window.gtag("event", "send", {
      hitType: action,
      eventCategory: category,
      eventAction: value,
      eventLabel: label,
    });
    window.ga("send", {
      hitType: action,
      eventCategory: category,
      eventAction: value,
      eventLabel: label,
    });
  }
};

export const gaEvent = ({ category, action, label, value, nonInteraction }) => {
  if (category && action) {
    window.gtag("event", "send", {
      hitType: action,
      eventCategory: category,
      eventAction: value,
      eventLabel: label,
      nonInteraction,
    });
    window.ga("send", {
      hitType: action,
      eventCategory: category,
      eventAction: value,
      eventLabel: label,
      nonInteraction,
    });
  }
};
