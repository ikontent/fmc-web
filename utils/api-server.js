const axios = require("axios");
const { NEXT_PUBLIC_STRAPI_URL } = process.env;

const getArticlesCount = async () => {
  const url = `${NEXT_PUBLIC_STRAPI_URL}/posts/count`;

  try {
    const { data } = await axios(url);

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getArticlesByIds = async (ids = []) => {
  if (ids.length === 0) {
    return null;
  }

  const url = `${NEXT_PUBLIC_STRAPI_URL}/posts?id_in=${ids.join("&id_in=")}`;

  try {
    const { data } = await axios(url);

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getArticlesBySlugs = async (slugs = []) => {
  if (slugs.length === 0) {
    return null;
  }

  const url = `${NEXT_PUBLIC_STRAPI_URL}/posts?slug_in=${slugs.join(
    "&slug_in="
  )}`;

  try {
    const { data } = await axios(url);

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getArticles = async (start = 0, limit = 1000) => {
  const url = `${NEXT_PUBLIC_STRAPI_URL}/posts?_start=${start}&_limit=${limit}&_sort=published_at:DESC`;

  try {
    const { data } = await axios(url);

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getMagazines = async (start = 0, limit = 1000) => {
  const url = `${NEXT_PUBLIC_STRAPI_URL}/magazines?_start=${start}&_limit=${limit}&_sort=date:DESC`;

  try {
    const { data } = await axios(url);

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getExternalNewsCount = async () => {
  const url = `${NEXT_PUBLIC_STRAPI_URL}/external-news/count`;

  try {
    const { data } = await axios(url);

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getExternalNews = async (start = 0, limit = 1000) => {
  const url = `${NEXT_PUBLIC_STRAPI_URL}/external-news?_start=${start}&_limit=${limit}&_sort=published_at:DESC`;

  try {
    const { data } = await axios(url);

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getSiteSettings = async () => {
  let url = `${NEXT_PUBLIC_STRAPI_URL}/site-settings`;

  try {
    const { data } = await axios(url);

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getFrontpageSettings = async () => {
  let url = `${NEXT_PUBLIC_STRAPI_URL}/front-page-editor`;

  try {
    const { data } = await axios(url);

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getTagsCount = async () => {
  const url = `${NEXT_PUBLIC_STRAPI_URL}/tags/count`;

  try {
    const { data } = await axios(url);

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getTags = async (start = 0, limit = 1000) => {
  const url = `${NEXT_PUBLIC_STRAPI_URL}/tags?_start=${start}&_limit=${limit}`;

  try {
    const { data } = await axios(url);

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getTagByTagName = async (tagName, start = 0, limit = 1) => {
  const url = `${NEXT_PUBLIC_STRAPI_URL}/tags?tagName=${tagName}&_start=${start}&_limit=${limit}`;

  try {
    const { data } = await axios(url);

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getTopicsCount = async () => {
  const url = `${NEXT_PUBLIC_STRAPI_URL}/topics/count`;

  try {
    const { data } = await axios(url);

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getTopics = async (start = 0, limit = 1000) => {
  const url = `${NEXT_PUBLIC_STRAPI_URL}/topics?_start=${start}&_limit=${limit}`;

  try {
    const { data } = await axios(url);

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getNameDay = async () => {
  let url = `https://api.nevnapok.eu/ma`;

  try {
    const { data } = await axios(url);

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getWeatherAPI = async () => {
  let url = `${NEXT_PUBLIC_STRAPI_URL}/weather-api`;

  try {
    const { data } = await axios(url);

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getAuthorsCount = async () => {
  const url = `${NEXT_PUBLIC_STRAPI_URL}/users/count`;

  try {
    const { data } = await axios(url);

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getAuthors = async (start = 0, limit = 1000) => {
  const url = `${NEXT_PUBLIC_STRAPI_URL}/users?_start=${start}&_limit=${limit}`;

  try {
    const { data } = await axios(url);

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getPages = async (id = null) => {
  let url = `${NEXT_PUBLIC_STRAPI_URL}/pages`;
  if (Number.isInteger(id)) {
    url += `/${id}`;
  }

  try {
    const { data: count } = await axios(url + "/count");
    const { data } = await axios(url + `?_limit=${count}`);

    return data;
  } catch (error) {
    handleError(error);
  }
};

const handleError = (error) => {
  console.log(error.response ? error.response.data : error);
  // Sentry here
  return error.response
    ? error.response.data
    : "Hiba történt, kérjük próbálja meg később!";
};

module.exports = {
  getAuthorsCount,
  getAuthors,
  getArticles,
  getArticlesByIds,
  getArticlesBySlugs,
  getMagazines,
  getArticlesCount,
  getExternalNewsCount,
  getExternalNews,
  getFrontpageSettings,
  getPages,
  getNameDay,
  getSiteSettings,
  getTagsCount,
  getTags,
  getTagByTagName,
  getTopicsCount,
  getTopics,
  getWeatherAPI,
};
