const axios = require("axios");
const { NEXT_PUBLIC_STRAPI_URL } = process.env;

const getArticles = async (start = 0, limit = 100) => {
  const url = `${NEXT_PUBLIC_STRAPI_URL}/posts`;

  try {
    const { data: count } = await axios(url + "/count");

    const { data } = await axios(
      (url += `?_start_from=${start}&_limit=${limit}&_sort=published_at:DESC`)
    );

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getArticle = async (id) => {
  try {
    const { data } = await axios(`${NEXT_PUBLIC_STRAPI_URL}/posts/${id}`);

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getArticlePreview = async (id, PREVIEW_KEY) => {
  try {
    const { data } = await axios(
      `${NEXT_PUBLIC_STRAPI_URL}/posts/preview/${PREVIEW_KEY}/${id}`
    );

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getFrontpagePreview = async (PREVIEW_KEY) => {
  try {
    const { data } = await axios(
      `${NEXT_PUBLIC_STRAPI_URL}/frontpage-posts/preview/${PREVIEW_KEY}`
    );

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getExternalNews = async (id = null) => {
  const url = id
    ? `${NEXT_PUBLIC_STRAPI_URL}/external-news/${id}`
    : `${NEXT_PUBLIC_STRAPI_URL}/external-news/get-latest`;

  try {
    const { data } = await axios(url);

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getVideos = async () => {
  try {
    const { data } = await axios(
      `${NEXT_PUBLIC_STRAPI_URL}/external-news/get-videos`
    );

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getSiteSettings = async (id = null) => {
  let url = `${NEXT_PUBLIC_STRAPI_URL}/site-settings`;
  if (Number.isInteger(id)) {
    url += `/${id}`;
  }

  try {
    const { data } = await axios(url);
    return data;
  } catch (error) {
    handleError(error);
  }
};

const getFrontpageSettings = async () => {
  let url = `${NEXT_PUBLIC_STRAPI_URL}/front-page-editor`;

  try {
    const { data } = await axios(url);

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getTags = async (id = null) => {
  let url = `${NEXT_PUBLIC_STRAPI_URL}/tags`;
  if (Number.isInteger(id)) {
    url += `/${id}`;
  }

  try {
    const { data: count } = await axios(url + "/count");
    const { data } = await axios(url + `?_limit=${count}`);

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getTopics = async (id = null) => {
  let url = `${NEXT_PUBLIC_STRAPI_URL}/topics`;
  if (Number.isInteger(id)) {
    url += `/${id}`;
  }

  try {
    const { data: count } = await axios(url + "/count");
    const { data } = await axios(url + `?_limit=${count}`);

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getNameDay = async () => {
  let url = `https://api.nevnapok.eu/ma`;

  try {
    const { data } = await axios(url);

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getWeatherAPI = async () => {
  let url = `${NEXT_PUBLIC_STRAPI_URL}/weather-api`;

  try {
    const { data } = await axios(url);

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getAuthors = async (id = null) => {
  let url = `${NEXT_PUBLIC_STRAPI_URL}/users`;
  if (Number.isInteger(id)) {
    url += `/${id}`;
  }

  try {
    const { data: count } = await axios(url + "/count");
    const { data } = await axios(url + `?_limit=${count}`);

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getPages = async (id = null) => {
  let url = `${NEXT_PUBLIC_STRAPI_URL}/pages`;
  if (Number.isInteger(id)) {
    url += `/${id}`;
  }

  try {
    const { data: count } = await axios(url + "/count");
    const { data } = await axios(url + `?_limit=${count}`);

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getSearchResults = async (search) => {
  try {
    const [
      { data: topics },
      { data: tags },
      { data: authors },
      { data: posts },
    ] = await Promise.all([
      axios(
        `${NEXT_PUBLIC_STRAPI_URL}/topics?_where[_or][0][name_contains]=${search}&_where[_or][1][slug_contains]=${search}`
      ),

      axios(
        `${NEXT_PUBLIC_STRAPI_URL}/tags?_where[_or][0][tagName_contains]=${search}&_where[_or][1][slug_contains]=${search}`
      ),

      axios(
        `${NEXT_PUBLIC_STRAPI_URL}/users?_where[_or][0][email_contains]=${search}&_where[_or][1][name_contains]=${search}&_where[_or][2][about_contains]=${search}&_where[_or][3][slug_contains]=${search}`
      ),

      axios(
        `${NEXT_PUBLIC_STRAPI_URL}/posts?_where[_or][0][title_contains]=${search}&_where[_or][1][lead_contains]=${search}&_where[_or][2][content_contains]=${search}&_sort=published_at:DESC`
      ),
    ]);

    return { topics, tags, authors, posts };
  } catch (error) {
    handleError(error);
  }
};

const incrementPostView = async (id) => {
  try {
    await axios({
      method: "POST",
      url: `${NEXT_PUBLIC_STRAPI_URL}/posts/increment-view/${id}`,
    });
  } catch (error) {
    handleError(error);
  }
};

const getRandomArticle = async () => {
  try {
    const { data } = await axios(`${NEXT_PUBLIC_STRAPI_URL}/posts/get-random`);

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getRandomArticleByTopic = async (topicId) => {
  try {
    const { data } = await axios(
      `${NEXT_PUBLIC_STRAPI_URL}/posts/get-random-by-topic/${topicId}`
    );

    return data;
  } catch (error) {
    handleError(error);
  }
};

const getRandomArticleByTag = async (tagId) => {
  try {
    const { data } = await axios(
      `${NEXT_PUBLIC_STRAPI_URL}/posts/get-random-by-tag/${tagId}`
    );

    return data;
  } catch (error) {
    handleError(error);
  }
};

const handleError = (error) => {
  console.log(error.response ? error.response.data : error);
  // Sentry here
  return error.response
    ? error.response.data
    : "Hiba történt, kérjük próbálja meg később!";
};

export default {
  getAuthors,
  getArticle,
  getArticlePreview,
  getArticles,
  getExternalNews,
  getFrontpageSettings,
  getPages,
  getNameDay,
  getSiteSettings,
  getVideos,
  getTags,
  getTopics,
  getWeatherAPI,
  getSearchResults,
  incrementPostView,
  getRandomArticle,
  getRandomArticleByTopic,
  getRandomArticleByTag,
  getFrontpagePreview,
};
