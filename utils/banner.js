export const isBannerVisible = (banner, content = banner.content) => {
  const aDayInMillisec = Number(864e5);

  return (
    content &&
    (banner.start ? Date.parse(banner.start) < Date.now() : true) &&
    (banner.end ? Date.now() < Date.parse(banner.end) + aDayInMillisec : true)
  );
};
