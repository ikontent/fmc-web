const MONTHNAMEMAP = {
  0: "Január",
  1: "Február",
  2: "Március",
  3: "Április",
  4: "Május",
  5: "Június",
  6: "Július",
  7: "Augusztus",
  8: "Szeptember",
  9: "Október",
  10: "November",
  11: "December",
};

export function getIsTodayOrYesterday(dateStr, todayObj, dateObj) {
  const today = todayObj || new Date();
  const day = dateObj || new Date(dateStr);

  if (
    day.getDate() == today.getDate() &&
    day.getMonth() == today.getMonth() &&
    day.getFullYear() == today.getFullYear()
  ) {
    return { isToday: true };
  } else {
    const yesterday = new Date();
    yesterday.setDate(today.getDate() - 1); //this steps back today by one day and it handles year or month changes either
    if (
      day.getDate() == yesterday.getDate() &&
      day.getMonth() == yesterday.getMonth() &&
      day.getFullYear() == yesterday.getFullYear()
    ) {
      return { isYesterday: true };
    }
  }

  return null;
}

export function daysPassedSince(dateStr) {
  if (typeof window === "undefined") {
    return;
  }

  const today = new Date();
  const day = new Date(dateStr);

  if (day.toString() === "Invalid Date") {
    return "Invalid Date";
  }

  const hoursString = getTwoDigitStringFromNumber(day.getHours());
  const minutesString = getTwoDigitStringFromNumber(day.getMinutes());

  const isTodayOrYesterday = getIsTodayOrYesterday(null, today, day);

  if (isTodayOrYesterday) {
    return `${
      isTodayOrYesterday.isToday ? " ma," : " tegnap,"
    } ${hoursString}:${minutesString}`;
  } else {
    return getWeirdFMCFormat(day);
  }
}

export function getWeirdFMCFormat(dateObj) {
  return `${dateObj.getFullYear()}. ${getTwoDigitStringFromNumber(
    dateObj.getMonth() + 1
  )}. ${getTwoDigitStringFromNumber(
    dateObj.getDate()
  )}., ${getTwoDigitStringFromNumber(
    dateObj.getHours()
  )}:${getTwoDigitStringFromNumber(dateObj.getMinutes())}`;
}

export function getYYYYMMMMDDFormat(dateObj) {
  return `${dateObj.getFullYear()}. ${
    MONTHNAMEMAP[dateObj.getMonth()]
  } ${dateObj.getDate()}.`;
}

export function getNevnapAPIFormat(dateObj) {
  return `${dateObj.getFullYear()}. ${MONTHNAMEMAP[
    dateObj.getMonth()
  ].toLowerCase()} ${dateObj.getDate()}., ${new Intl.DateTimeFormat("hu-HU", {
    weekday: "long",
  }).format(dateObj)}`;
}

function getTwoDigitStringFromNumber(number) {
  if (number < 10) {
    return `0${number}`;
  } else {
    return `${number}`;
  }
}
