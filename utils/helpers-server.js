/**
 * Generate page paths with pagination data.
 *
 * @param {string} url            The base url. Example: news -> first page: /news, second page: /news2, third page: /news/3, ...
 * @param {*} pageName            The page file name from the page directory
 * @param {*} itemPropName        The prop name to pass the page the sliced items/page
 * @param {*} items               All of the items. Which wil be paginated
 * @param {*} perPage             How many items showed in one page
 * @param {*} props               Any additional props to pass the page
 * @returns
 */
const generatePaginatedPages = (
  url,
  pageName,
  itemPropName, // prop name
  items = [], // prop content
  perPage = 10,
  halfRange = 2,
  props = {},
  paths
) => {
  if (items.length) {
    const pageCount = Math.ceil(items.length / perPage);

    halfRange =
      pageCount >= 2 * halfRange + 1
        ? halfRange
        : (pageCount - 1) / 2 > 1
        ? Math.floor((pageCount - 1) / 2)
        : 0;

    for (let page = 1; page <= pageCount; page++) {
      const prevPage =
        page - 1 > 1 ? `${url}/${page - 1}` : page - 1 === 1 ? `${url}` : null;
      const nextPage = page + 1 <= pageCount ? `${url}/${page + 1}` : null;

      const pages = [];
      const loopStart = Math.max(
        page - halfRange - Math.max(halfRange - (pageCount - page), 0),
        1
      );
      const loopEnd = Math.min(
        page + halfRange + Math.max(halfRange - page + 1, 0),
        pageCount
      );
      for (let i = loopStart; i <= loopEnd; i++) {
        pages.push({
          number: i,
          url: `${url}${i === 1 ? "" : `/${i}`}`,
          isActive: i === page,
        });
      }

      const pageURL = `${url}${page === 1 ? "" : `/${page}`}`;
      paths[pageURL] = {
        page: pageName,
        query: {
          ...props,
          [itemPropName]: items.slice(
            (page - 1) * perPage,
            (page - 1) * perPage + perPage
          ),
          _pagination: {
            first: `${url}`,
            prev: prevPage,
            pages: pages,
            next: nextPage,
            last: `${url}/${pageCount}`,
            pageCount,
            pageURL,
          },
        },
      };
    }
  } else {
    paths[url] = {
      page: pageName,
      query: {
        ...props,
        [itemPropName]: [],
        _pagination: {
          pageCount: 0,
          pageURL: url,
        },
      },
    };
  }
};

const pick = (obj, ...props) => {
  const result = {};
  for (const prop of props) {
    if (obj && obj.hasOwnProperty(prop)) {
      result[prop] = obj[prop];
    }
  }
  return result;
};

const getRandomUniqueElements = (array, numberOfElements) => {
  if (numberOfElements >= array.length) {
    return array;
  }
  let result = [];
  let arr = [];
  while (arr.length < numberOfElements) {
    var r = Math.floor(Math.random() * array.length);
    if (arr.indexOf(r) === -1) {
      arr.push(r);
      result.push(array[r]);
    }
  }
  return result;
};

module.exports = { generatePaginatedPages, pick, getRandomUniqueElements };
