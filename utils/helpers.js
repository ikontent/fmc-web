import { STRAPI_URL } from "utils/store";

/**
 * imagePathHandler function.
 * Returns the image path or a placeholder path.
 * @component
 * @param {string} imageURL - The url of the image.
 * @param {string} [prefix=STRAPI_URL] - A custom prefix in front of image's url
 * @param {string} [customPlaceholder="/images/placeholders/placeholder.png"] - A custom placeholder you want to replace the default placeholder with.
 */

const imagePathHandler = (imageURL, prefix, customPlaceholder) => {
  let finalURL = "",
    finalPrefix = "",
    finalPlaceholder = "",
    defaultPlaceholder = "/images/placeholders/placeholder.png";

  if (imageURL) {
    if (prefix) finalPrefix = prefix;
    else finalPrefix = STRAPI_URL;
    finalURL = finalPrefix + imageURL;
  } else {
    if (customPlaceholder) finalPlaceholder = customPlaceholder;
    else finalPlaceholder = defaultPlaceholder;

    finalURL = finalPlaceholder;
  }
  return imageURL ? finalURL : finalPlaceholder;
};

/**
 * Fet desired image size url. If the image size does not exists, we try to get a smaller image.
 * Note: you need to set the sizes array based on strapi image-manipulation.js breakpoints array.
 *
 * @param {Object} img            Strapi img object response
 * @param {String} desiredSize    Strapi format size string
 * @param {Boolean} onlyUrl       If set to false the whole image object returned
 * @returns
 */
const getImageUrl = (img, desiredSize = "small", onlyUrl=true) => {
  if (img?.formats?.[desiredSize]) {
    return onlyUrl ? img.formats[desiredSize].url : img.formats[desiredSize];
  }
  if (desiredSize == "full") {
    return onlyUrl ? img?.url || null : img;
  }
  if (img?.formats) {
    const sizes = ["large", "medium", "small", "xsmall"];
    const index = sizes.findIndex((size) => size == desiredSize);
    if (index == -1) {
      return onlyUrl ? img.url : img;
    }

    let i = index + 1;
    while (i < sizes.length && img.formats?.[sizes[i]] == undefined) {
      i++;
    }
    if (i > sizes.length - 1) {
      return onlyUrl ? img.url : img;
    } else {
      return onlyUrl ? img.formats?.[sizes[i]].url : img.formats?.[sizes[i]];
    }
  } else {
    return onlyUrl ? img?.url || null : img;
  }
};

export { imagePathHandler, getImageUrl };
