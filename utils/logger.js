function logger() {
  const now = Date.now();
  function log(message) {
    console.log(`${message}, time elapsed in sec: `, (Date.now() - now) / 1000);
  }
  log.now = now;
  return log;
}

module.exports = logger;
