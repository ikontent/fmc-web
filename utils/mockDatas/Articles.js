// 2 days
const mockPreviousPeriod = 2 * 24 * 60 * 60 * 1000;

function getRandomDate() {
  return new Date(new Date().getTime() - Math.random() * mockPreviousPeriod);
}

const articles = [
  {
    created_at: getRandomDate(),
    headings: ["Video", "Videó", "Pénzügyek", "Hírek"],
    headline:
      "Fehérváron csak kiemelt parkolási üövezetben lehetséges további engedély hiányában a parkolás",
    lead:
      "leasdd long longer leasdd long lsdnger lead losdng longer lead long longer lead losdng losdngerlead long longer lead sdlong longer lead long longer lead lsdong longer lead long lonsdgesdrlead long longer lead long longer lead sdlong losdnger lead long longer lead losdng longerlead long longer lead long lodsnger lead long longer lead longsd longer lesdad lonsdg longer",
    imgURL: "/images/placeholders/placeholder.png",
    tags: {
      tagNames: ["Szórakozás", "Hírek", "Celebek"],
      color: "",
    },
    updated_at: "2021-03-05T08:20:03.000Z",
  },
  {
    created_at: getRandomDate(),
    headings: ["Blog", "Videó", "Pénzügyek", "Hírek"],
    headline: "Mikor szálhattak el a karácsonyfa árak, melyiket érdemes venni?",
    highlighted: true,
    lead:
      "lead long longer lead long longer lead long longer lead long longer lead long longer",
    imgURL: "/images/placeholders/placeholder.png",
    tags: {
      tagNames: ["Szórakozás", "Hírek", "Celebek"],
      color: "badge-gray",
    },
    updated_at: "2021-03-05T08:20:03.000Z",
  },
  {
    created_at: getRandomDate(),
    headings: ["Sport", "Videó", "Pénzügyek", "Hírek"],
    headline: "Fontos, hogy úvjuk a bőrünket a hidegben!",
    highlighted: true,
    lead:
      "lead long longer lead long longer lead long longer lead long longer ",
    imgURL: "/images/placeholders/placeholder.png",
    tags: {
      tagNames: ["Szórakozás", "Hírek", "Celebek"],
      color: "badge-dark",
    },
    updated_at: "2021-03-05T08:20:03.000Z",
  },
  {
    created_at: getRandomDate(),
    headings: ["Kvíz", "Videó", "Pénzügyek", "Hírek"],
    headline: "Igaz, vagy hamis? 10 érdekesség fehérvárról",
    imgURL: "/images/placeholders/placeholder.png",
    tags: {
      tagNames: ["Szórakozás", "Hírek", "Celebek"],
      color: "badge-gray",
    },
    updated_at: "2021-03-05T08:20:03.000Z",
  },
  {
    created_at: getRandomDate(),
    headings: ["Kvíz", "Videó", "Pénzügyek", "Hírek"],
    highlighted: true,
    headline: "Kitálalt a Bárdi Autó: minden BMW használhatatlan!",
    lead: "lead long longer  long longer lead long longer",
    imgURL: "/images/placeholders/placeholder.png",
    tags: {
      tagNames: ["Szórakozás", "Hírek", "Celebek"],
      color: "badge-dark",
    },
    updated_at: "2021-03-05T08:20:03.000Z",
  },
  {
    created_at: getRandomDate(),
    headings: ["Kvíz", "Videó", "Pénzügyek", "Hírek"],
    headline: "Kitálalt a Bárdi Autó: minden Audi használhatatlan!",
    imgURL: "/images/placeholders/placeholder.png",
    tags: {
      tagNames: ["Szórakozás", "Hírek", "Celebek"],
      color: "",
    },
    updated_at: "2021-03-05T08:20:03.000Z",
  },
  {
    created_at: getRandomDate(),
    headings: ["Kvíz", "Videó", "Pénzügyek", "Hírek"],
    highlighted: true,
    headline: "Kitálalt a Bárdi Autó: minden Tesla használhatatlan!",
    lead: "lead long longer  long longer lead long longer",
    tags: {
      tagNames: ["Szórakozás", "Hírek", "Celebek"],
      color: "",
    },
    updated_at: "2021-03-05T08:20:03.000Z",
  },
  {
    created_at: getRandomDate(),
    headings: ["Kvíz", "Videó", "Pénzügyek", "Hírek"],
    highlighted: true,
    headline: "Kitálalt a Bárdi Autó: minden Tesla használhatatlan!",
    tags: {
      tagNames: ["Szórakozás", "Hírek", "Celebek"],
      color: "",
    },
    updated_at: "2021-03-05T08:20:03.000Z",
  },
  {
    created_at: getRandomDate(),
    headings: ["Kvíz", "Videó", "Pénzügyek", "Hírek"],
    headline: "Kitálalt a Bárdi Autó: minden Bentley használhatatlan!",
    tags: {
      tagNames: ["Szórakozás", "Hírek", "Celebek"],
      color: "badge-dark",
    },
    updated_at: "2021-03-05T08:20:03.000Z",
  },
  {
    created_at: getRandomDate(),
    headings: ["Kvíz", "Videó", "Pénzügyek", "Hírek"],
    highlighted: true,
    headline: "Kitálalt a Bárdi Autó: minden BMW használhatatlan!",
    lead: "lead long longer  long longer lead long longer",
    tags: {
      tagNames: ["Szórakozás", "Hírek", "Celebek"],
      color: "",
    },
    updated_at: "2021-03-05T08:20:03.000Z",
  },
  {
    created_at: getRandomDate(),
    headings: ["Kvíz", "Videó", "Pénzügyek", "Hírek"],
    headline: "Kitálalt a Bárdi Autó: minden Audi használhatatlan!",
    tags: {
      tagNames: ["Szórakozás", "Hírek", "Celebek"],
      color: "",
    },
    updated_at: "2021-03-05T08:20:03.000Z",
  },
  {
    created_at: getRandomDate(),
    headings: ["Kvíz", "Videó", "Pénzügyek", "Hírek"],
    highlighted: true,
    headline: "Kitálalt a Bárdi Autó: minden Tesla használhatatlan!",
    imgURL: "/images/placeholders/placeholder.png",
    lead: "lead long longer  long longer lead long longer",
    tags: {
      tagNames: ["Szórakozás", "Hírek", "Celebek"],
      color: "",
    },
    updated_at: "2021-03-05T08:20:03.000Z",
  },
  {
    created_at: getRandomDate(),
    headings: ["Kvíz", "Videó", "Pénzügyek", "Hírek"],
    headline: "Kitálalt a Bárdi Autó: minden Bentley használhatatlan!",
    tags: {
      tagNames: ["Szórakozás", "Hírek", "Celebek"],
      color: "",
    },
    updated_at: "2021-03-05T08:20:03.000Z",
  },
];

export default articles;
