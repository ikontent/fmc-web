//this is duplicated here because this module is used by server and by client
function getTwoDigitStringFromNumber(number) {
  if (number < 10) {
    return `0${number}`;
  } else {
    return `${number}`;
  }
}

function getSlugFormat(dateString) {
  const dateObj = new Date(dateString);

  return `${dateObj.getFullYear()}/${getTwoDigitStringFromNumber(
    dateObj.getMonth() + 1
  )}/${getTwoDigitStringFromNumber(dateObj.getDate())}`;
}

const pathFactory = {
  article: (article) =>
    `/${getSlugFormat(
      article.firstPublishedAt || article.published_at || article.created_at
    )}/${article.slug}`,
  page: (page) => `/page/${page.slug}`,
  tag: (tagOrName) =>
    typeof tagOrName === "string"
      ? `/tag/${tagOrName}`
      : `/tag/${tagOrName.slug || tagOrName.tagName}`,
  topic: (topic) => `/${topic.slug}`,
  author: (author) => `/author/${author.slug}`,
};

module.exports = { pathFactory };
