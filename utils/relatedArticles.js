import { STRAPI_URL } from "utils/store";
import cheerio from "cheerio";

export default function renderRelatedArticles(html) {
  const $ = cheerio.load(html);
  $(".relatedarticle").each((i, gallery) => {
    try {
      const data = JSON.parse(decodeURI($(gallery).attr("data-data")));

      $(gallery).replaceWith(`
        <div class="relatedarticle article-list-box border p-12 my-25">
          <a href="${data.link}">
          <div>
            <div class="custom-article-components-article-title d-flex justify-content-between align-items-center article-title-negative-margin-left mb-1">
                <div class="d-flex align-items-center">
                  <h5 class="mb-0 default-section-title">Kapcsolódó</h5>
                </div>
            </div>
          </div>

          <div class="article-list-box-item">
            <div class="d-flex">
              <div class="d-flex align-items-center" style="width:100%">
                <div class="d-flex justify-content-between" style="width:100%">
                  <div>
                    <h6 class="mb-0 cursor-pointer default-article-headline-normal">${
                      data.title || ""
                    }</h6>
                    <p class="default-article-lead-normal mt-3 desktop">${
                      data.lead || ""
                    }</p>
                  </div>

                  <div class="d-block text-decoration-none img-box mr-20">
                    <img class="img rounded Ximg-optimizer" src="${STRAPI_URL}${
        data.image
      }" alt="${data.title || "Fehérvár Médiacentrum fotója"}">
                      ${
                        data.hasVideo
                          ? `<div class="play-box d-flex align-items-center justify-content-center"><img src="/images/icons/play-icon.svg" alt="Play" class="play-icon"></div>`
                          : ``
                      }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </a>
      </div>`);
    } catch (e) {
      $(gallery).replaceWith(``);
    }
  });

  return $.html();
}
