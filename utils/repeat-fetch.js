function getNumberOfRepeat(count, slice, max) {
  return max
    ? Math.min(max, Math.ceil(count / slice))
    : Math.ceil(count / slice);
}

function fastConcat(array, part) {
  Array.prototype.push.apply(array, part);
}

function slowConcat(array, part) {
  const arrayLength = array.length;
  const partLength = part.length;
  for (let i = 0; i < partLength; ++i) {
    array[arrayLength + i] = part[i]; // ez nem "part[i-1]" kéne h legyen?
  }
}

async function repeatFetch(count, slice, max, getter, logger, message) {
  const array = [];
  const concat = slice < 32768 ? fastConcat : slowConcat;
  const repeat = getNumberOfRepeat(count, slice, max);
  for (let i = 0, part; i < repeat; ++i) {
    logger(`${message} FETCH ITERATION #${i}`);
    part = await getter(i * slice, slice);
    concat(array, part);
  }
  return array;
}

module.exports = repeatFetch;
