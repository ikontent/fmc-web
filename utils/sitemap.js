const { writeFileSync } = require("fs");
// const { pipeline } = require("stream");
// const { createReadStream, createWriteStream } = require("fs");
// const { createGzip } = require("zlib");

const sitemap = (paths) => {
  if (process.env.NEXT_PUBLIC_OWN_URL) {
    const domain = process.env.NEXT_PUBLIC_OWN_URL;

    let content =
      '<?xml version="1.0" encoding="UTF-8"?>' +
      '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';

    Object.keys(paths).map(
      (path) =>
        (content +=
          path === "/"
            ? `
          <url><loc>${
            domain + path
          }</loc><changefreq>always</changefreq><priority>0.9</priority></url>`
            : path.search(/(\d\d\/){3}/) > -1
            ? `
        <url><loc>${
          domain + path
        }</loc><changefreq>daily</changefreq><priority>0.7</priority></url>`
            : path === "/hetilap" ||
              path.includes("/page/") ||
              path.includes("/preview/") ||
              path.includes("/kereses") ||
              path.includes("/author/") ||
              path.includes("/custom404")
            ? `
        <url><loc>${domain + path}</loc></url>`
            : // tags & topics
              `
          <url><loc>${
            domain + path
          }</loc><changefreq>weekly</changefreq><priority>0.2</priority></url>`)
    );

    content += "</urlset>";

    writeFileSync("public/sitemap.xml", content);

    // const gzip = createGzip();
    // const source = createReadStream(content);
    // const destination = createWriteStream("sitemap.xml.gz");

    // pipeline(source, gzip, destination, (err) => {
    //   if (err) {
    //     console.error("An error occurred:", err);
    //     process.exitCode = 1;
    //   }
    // });
  }
};

module.exports = { sitemap };
