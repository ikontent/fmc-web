let _appJsThis;

export const STRAPI_URL = process.env.NEXT_PUBLIC_STRAPI_URL;
export const OWN_URL = process.env.NEXT_PUBLIC_OWN_URL;
export const BITBUCKET_PIPELINE_BRANCH =
  process.env.NEXT_PUBLIC_BITBUCKET_PIPELINE_BRANCH;
export const SENTRY_DSN = process.env.NEXT_PUBLIC_SENTRY_DSN;
export const PREVIEW_KEY = process.env.NEXT_PUBLIC_PREVIEW_KEY;
export const FB_APP_ID = process.env.NEXT_PUBLIC_FB_APP_ID;
export const GANALYTICS_ID = process.env.NEXT_PUBLIC_GANALYTICS_ID;
export const FB_PIXEL_ID = process.env.NEXT_PUBLIC_FB_PIXEL_ID;

export const DEFAULT_LANGUAGE = "en";

class Store {
  constructor() {
    if (!Store.instance) {
      this._data = {
        immutableData: {},
      };
      this.functions = {};
      Store.instance = this;
    }

    return Store.instance;
  }

  get(key) {
    return this._data.immutableData[key];
  }

  getAll() {
    return this._data.immutableData;
  }

  set(obj, doNotRenderFlag) {
    const newData = { ...this._data.immutableData, ...obj };
    this._data.immutableData = newData;
    _appJsThis && !doNotRenderFlag && _appJsThis.setState({});
  }

  setAppJsThis(appJsThis) {
    if (appJsThis && !_appJsThis) {
      _appJsThis = appJsThis;
    }
  }
}

const instance = new Store();
Object.freeze(instance);

export default instance;
