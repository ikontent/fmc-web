const minute = 60 * 1000;
const hour = 60 * 60 * 1000;
const day = 24 * 60 * 60 * 1000;

function getDateOfDate(date) {
  return new Date(date.getFullYear(), date.getMonth(), date.getDate());
}

export function getElapsedTimeText(date) {
  date = new Date(date);
  const currentDate = new Date();

  const time = date.getTime();
  const currentTime = currentDate.getTime();
  const timeDifference = currentTime - time;

  if (timeDifference < 60 * 1000) {
    return "1p";
  }
  if (timeDifference < 60 * 60 * 1000) {
    return `${Math.floor(timeDifference / minute)}p`;
  }
  if (timeDifference < day) {
    return `${Math.floor(timeDifference / hour)}ó`;
  }
  return `${Math.floor(timeDifference / day)}n`;
}
