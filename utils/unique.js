function unique(array) {
  const values = {};
  const newArray = [];
  const length = array.length;
  for (let i = 0, j = 0, value; i < length; ++i) {
    value = array[i];
    if (values[value] !== 1) {
      values[value] = 1;
      newArray[j] = value;
      ++j;
    }
  }
  return newArray;
}

module.exports = unique;
