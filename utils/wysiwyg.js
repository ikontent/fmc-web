import ReactDOM from "react-dom";
// https://github.com/vercel/next.js/issues/16864#issuecomment-700593327
import Router from "next/router";
import { RouterContext } from "next/dist/next-server/lib/router-context";

import { STRAPI_URL } from "utils/store";
import cheerio from "cheerio";
import ImageGallery from "components/elements/react-image-gallery/src/ImageGallery";
import Gallery from "react-grid-gallery";
import Poll from "components/customPositioned/Poll";
import ArticleCardHorizontal from "components/elements/articles/ArticleCardHorizontal";
import { ArticleTitle } from "components/elements/articles/ArticleComponents";
import Banner from "components/customPositioned/Banner";
import ArticleListBox from "components/elements/articles/ArticleListBox";
import Newsletter from "components/elements/Newsletter";

export const stripLead = (lead) => (lead ? lead.replace(/<[^>\s]+>/g, "") : "");

export const cropLead = (lead) =>
  lead
    ? lead.length < 300
      ? stripLead(lead)
      : stripLead(lead).slice(0, 300) + "..."
    : "";

// #########################################################

export const prepareWysiwyg = (htmlContent) => {
  return (
    (htmlContent || "")
      // replace typeform links to typeform element
      .replace(
        /(https:\/\/\w*\.typeform\.com\/(to|form)\/)(\w*)((\?|\/)([a-zA-Z0-9-_=]*))?/g,
        (all, g1, g2, g3) =>
          `<div class="injected-component typeform-widget" data-url="https://form.typeform.com/to/${g3}?typeform-medium=embed-snippet" style="width: 100%; height: 500px;"></div>`
      )
  );
};

// #########################################################

export const slotsForInjection = ({
  htmlContent,
  screenWidth,
  dataToInject,
  setDataToInject,
  wasInjected,
  setWasInjected,
}) => {
  const $ = cheerio.load(htmlContent);

  const _dataToInject = { ...dataToInject };
  const _wasInjected = { ...wasInjected };

  // ###################### PREPARE ALL IFRAME MEDIA  #########################
  $("iframe").each((_, iframe) => {
    const src = $(iframe).attr("src");

    if (
      src.includes("https://www.youtube.com") ||
      src.includes("https://fehervartv.hu/embed.php")
    ) {
      // https://www.tiny.cloud/docs/tinymce/6/pageembed/#default-css
      // fmc-cms > node_modules/bootstrap/dist/css/bootstrap.css
      $(iframe).parent().addClass("embed-responsive");
      $(iframe).parent().addClass("embed-responsive-16by9");
      const newSrc = $(iframe)
        .attr("src")
        .replace(/width=[^&\n]*/, "width=500");
      $(iframe).attr("src", newSrc);
      $(iframe).attr("width", "500px");
      $(iframe).attr("loading", "lazy");
    }

    if (src.includes("https://www.facebook.com")) {
      const maxWidth = $(iframe).attr("width") + "px" || "500px";

      const aspectRatio =
        $(iframe).attr("height") && $(iframe).attr("width")
          ? ($(iframe).attr("height") / $(iframe).attr("width")) * 100 + "%"
          : "56.25%";

      const fbParagraph = $(
        `<p class="fb-paragraph" style="max-width: ${maxWidth}"></p>`
      );

      const fbSpan = $(
        `<span class="fb-span" style="padding-top: ${aspectRatio}"></span>`
      );

      $(iframe).wrap(fbParagraph).wrap(fbSpan);
      $(iframe).attr("loading", "lazy");
    }
  });

  // ###################### SLOTS FOR AUTHOR-INJECTED COMPONENTS #########################
  $(".relatedarticle").each((i, related) => {
    _dataToInject.relatedArticles.push(
      JSON.parse(decodeURI($(related).attr("data-data")))
    );

    $(related)
      .addClass("injected-component")
      .removeAttr("contenteditable")
      .removeAttr("data-data")
      .attr("id", `relatedarticle-${i}`);
  });

  $(".poll").each((_, poll) => {
    _dataToInject.pollIds.push(Number($(poll).attr("data-poll")));

    $(poll).removeAttr("contenteditable").empty();
  });

  $(".igallery").each((i, gallery) => {
    const type = $(gallery).attr("data-gtype");
    const images = JSON.parse(decodeURI($(gallery).attr("data-data"))).map(
      (img) => {
        // grid
        if (type == "4") {
          return {
            src: STRAPI_URL + img.url,
            thumbnail: STRAPI_URL + img.thumbnail,
            thumbnailCaption: "",
            caption: (
              <>
                {img.caption ? <span>{img.caption}</span> : <span></span>}
                {img.name && <span>{img.name}</span>}
              </>
            ),
            thumbnailWidth: img?.thumbwidth || 156,
            thumbnailHeight: img?.thumbheight || 156,
          };
          // gallery & slideshow
        } else {
          return {
            original: STRAPI_URL + img.url,
            thumbnail: STRAPI_URL + img.thumbnail,
            originalAlt: img.alternativeText,
            description: img.caption ? img.caption : "",
            author: img.name,
          };
        }
      }
    );

    //  if (type == 2 && images.length == 1) {
    //      $(gallery).replaceWith(`
    //       <figure class="igallery-image igallery-type-1 figure"><img class="figure-img img-fluid" alt="Fehérvár Médiacentrum fotója" src="${images[0].original}">
    //         <figcaption>
    //           <p>${images[0].description}</p>
    //           <p>${images[0].author}</p>
    //         </figcaption>
    //       </figure>`);
    //     return;
    //   } else {
    _dataToInject.galleriesData[i] = { type, images };

    $(gallery).replaceWith(
      `<div class="row no-gutters">
          <div
            class="injected-component igallery igallery-type-${type}"
            id="igallery-${i}"
          ></div>
        </div>`
    );
    // }
  });

  // ###################### SLOTS FOR AUTO-INJECTED COMPONENTS #####################
  const numberOfRows = (txt) => {
    const wysiwygWidth =
      screenWidth < 768
        ? screenWidth
        : screenWidth < 992
        ? screenWidth * 0.5
        : screenWidth < 1920
        ? screenWidth * 0.6
        : 1920 * 0.6;

    // character's width: 7.3px @ 100% zoom
    return Math.ceil((7.3 * txt.length) / wysiwygWidth);
  };

  let totalRows = 0;

  $("body")
    .children()
    .each((i, elem) => {
      const text = $(elem).text();
      const rows = numberOfRows(text);
      totalRows += rows;

      if ($(elem).not(":header")) {
        if (totalRows > 5 && !_wasInjected.banner) {
          $(
            '<div class="injected-component injected-banner"></div>'
          ).insertBefore($(elem));
          _wasInjected.banner = true;
        }

        if (_dataToInject.relatedArticles.length === 0) {
          if (totalRows > 25 && !_wasInjected.mostPopular) {
            $(
              '<div class="injected-component injected-mostpopular"></div>'
            ).insertBefore($(elem));
            _wasInjected.mostPopular = true;
          }

          if (totalRows > 40 && !_wasInjected.newsletter) {
            $(
              '<div class="injected-component injected-newsletter"></div>'
            ).insertBefore($(elem));
            _wasInjected.newsletter = true;
          }
        }
      }
    });

  if (!_wasInjected.banner) {
    $("body").append('<div class="injected-component injected-banner"></div>');
    _wasInjected.banner = true;
  }

  setDataToInject(_dataToInject);
  setWasInjected(_wasInjected);

  return $.html();
};

// #########################################################

export const injectComponents = ({
  dataToInject,
  wasInjected,
  showRecommended,
  showBanner,
}) => {
  const galleryConfig = {
    thumbnailPosition: "bottom",
    showBullets: true,
  };

  const slideshowConfig = {
    showBullets: true,
    showPlayButton: false,
    showFullscreenButton: false,
    showThumbnails: false,
  };

  for (const [i, galleryItems] of dataToInject.galleriesData.entries()) {
    switch (galleryItems?.type) {
      case "2":
        ReactDOM.render(
          <ImageGallery items={galleryItems.images} {...galleryConfig} />,
          document.getElementById(`igallery-${i}`)
        );
        break;
      case "3":
        ReactDOM.render(
          <ImageGallery items={galleryItems.images} {...slideshowConfig} />,
          document.getElementById(`igallery-${i}`)
        );
        break;
      case "4":
        ReactDOM.render(
          <Gallery
            images={galleryItems.images}
            enableImageSelection={false}
            backdropClosesModal={true}
          />,
          document.getElementById(`igallery-${i}`)
        );
        break;
    }
  }

  dataToInject.pollIds.forEach((pollId) =>
    ReactDOM.render(
      <Poll key={"poll-" + pollId} pollId={Number(pollId)} />,
      document.querySelector(`[data-poll="${pollId}"]`)
    )
  );

  dataToInject.relatedArticles.forEach((article, indexArticle) => {
    const id = "#relatedarticle-" + indexArticle;
    ReactDOM.render(
      // https://github.com/vercel/next.js/issues/16864#issuecomment-700593327
      <RouterContext.Provider value={Router}>
        <div className="related-article-frame">
          <ArticleTitle title={"Kapcsolódó"} />
          <ArticleCardHorizontal
            article={{
              _URL: article.link,
              featuredImage: { url: article.image },
              lead: cropLead(article.lead),
              title: article.title,
              id: article.id,
              videoURL: article.hasVideo,
              showFeaturedImage: article.showFeaturedImage || true,
            }}
            tagsAboveImage
            showTopic
            showTag
            hasConstantImageSize
            key={"related-article-" + indexArticle}
          />
        </div>
      </RouterContext.Provider>,
      document.querySelector(id)
    );
  });

  {
    wasInjected.mostPopular &&
      showRecommended &&
      ReactDOM.render(
        // https://github.com/vercel/next.js/issues/16864#issuecomment-700593327
        <RouterContext.Provider value={Router}>
          <div className="border pt-12 px-12 my-15">
            <ArticleListBox
              title="Kedvencek"
              articles={dataToInject.recommendedArticles}
            />
          </div>
        </RouterContext.Provider>,
        document.querySelector(".injected-mostpopular")
      );
  }

  {
    wasInjected.banner &&
      showBanner &&
      dataToInject.banner &&
      ReactDOM.render(
        <Banner banner={dataToInject.banner} position="Cikkbe épült banner" />,
        document.querySelector(".injected-banner")
      );
  }

  // {
  //   wasInjected.newsletter &&
  //     ReactDOM.render(
  //       <Newsletter />,
  //       document.querySelector(".injected-newsletter")
  //     );
  // }
};
